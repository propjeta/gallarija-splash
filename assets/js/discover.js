        // Create Base64 Object
        var Base64 = {
        	_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        	encode: function (e) {
        		var t = "";
        		var n, r, i, s, o, u, a;
        		var f = 0;
        		e = Base64._utf8_encode(e);
        		while (f < e.length) {
        			n = e.charCodeAt(f++);
        			r = e.charCodeAt(f++);
        			i = e.charCodeAt(f++);
        			s = n >> 2;
        			o = (n & 3) << 4 | r >> 4;
        			u = (r & 15) << 2 | i >> 6;
        			a = i & 63;
        			if (isNaN(r)) {
        				u = a = 64
        			} else if (isNaN(i)) {
        				a = 64
        			}
        			t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        		}
        		return t
        	},
        	decode: function (e) {
        		var t = "";
        		var n, r, i;
        		var s, o, u, a;
        		var f = 0;
        		e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        		while (f < e.length) {
        			s = this._keyStr.indexOf(e.charAt(f++));
        			o = this._keyStr.indexOf(e.charAt(f++));
        			u = this._keyStr.indexOf(e.charAt(f++));
        			a = this._keyStr.indexOf(e.charAt(f++));
        			n = s << 2 | o >> 4;
        			r = (o & 15) << 4 | u >> 2;
        			i = (u & 3) << 6 | a;
        			t = t + String.fromCharCode(n);
        			if (u != 64) {
        				t = t + String.fromCharCode(r)
        			}
        			if (a != 64) {
        				t = t + String.fromCharCode(i)
        			}
        		}
        		t = Base64._utf8_decode(t);
        		return t
        	},
        	_utf8_encode: function (e) {
        		e = e.replace(/\r\n/g, "\n");
        		var t = "";
        		for (var n = 0; n < e.length; n++) {
        			var r = e.charCodeAt(n);
        			if (r < 128) {
        				t += String.fromCharCode(r)
        			} else if (r > 127 && r < 2048) {
        				t += String.fromCharCode(r >> 6 | 192);
        				t += String.fromCharCode(r & 63 | 128)
        			} else {
        				t += String.fromCharCode(r >> 12 | 224);
        				t += String.fromCharCode(r >> 6 & 63 | 128);
        				t += String.fromCharCode(r & 63 | 128)
        			}
        		}
        		return t
        	},
        	_utf8_decode: function (e) {
        		var t = "";
        		var n = 0;
        		var r = c1 = c2 = 0;
        		while (n < e.length) {
        			r = e.charCodeAt(n);
        			if (r < 128) {
        				t += String.fromCharCode(r);
        				n++
        			} else if (r > 191 && r < 224) {
        				c2 = e.charCodeAt(n + 1);
        				t += String.fromCharCode((r & 31) << 6 | c2 & 63);
        				n += 2
        			} else {
        				c2 = e.charCodeAt(n + 1);
        				c3 = e.charCodeAt(n + 2);
        				t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
        				n += 3
        			}
        		}
        		return t
        	}
        }

        var $j = jQuery.noConflict();
        $j(document).ready(function () {

        	$j('select').select2();

        	function ran() {
        		return Math.floor(Math.random() * 1000);
        	}

        	function unique(list) {
        		var result = [];
        		$j.each(list, function (i, e) {
        			if ($j.inArray(e, result) == -1) result.push(e);
        		});
        		return result;
        	}

        	// Define elements
        	var area_zoom_element = document.getElementById('line_zoom');
        	// Initialize chart
        	var area_zoom = echarts.init(area_zoom_element);

        	var options = {

        		// Define colors
        		color: ['#b6a2de', '#26A69A'], //color: ['#b6a2de','#26A69A','#2ec7c9','#b6a2de'], f17a52 03A9F4  //Fav: '#2196F3','#ff5252' / '#b6a2de','#26A69A' / "#0092ff", "#d74e67"

        		// Global text styles
        		textStyle: {
        			fontFamily: 'Roboto, Arial, Verdana, sans-serif',
        			fontSize: 13
        		},

        		// Chart animation duration
        		animationDuration: 750,

        		// Setup grid
        		grid: {
        			left: 0,
        			right: 40,
        			top: 35,
        			bottom: 60,
        			containLabel: true
        		},

        		// Add legend  
        		legend: {
        			data: [],
        			itemHeight: 8,
        			itemGap: 20,
        			textStyle: {
						color: '#fff',
						fontFamily: 'Roboto, Arial, Verdana, sans-serif',
        				fontSize: 16
        			}
        		},

        		// Add tooltip
        		tooltip: {
        			trigger: 'axis',
        			backgroundColor: 'rgba(255,255,255,1)',
        			padding: [10, 15],
        			textStyle: {
        				fontSize: 13,
        				fontFamily: 'Roboto, sans-serif',
        				color: '#212121'
        			}
        		},

        		// Horizontal axis
        		xAxis: [{
        			type: 'category',
        			boundaryGap: false,
        			axisLabel: {
        				color: '#fff'
        			},
        			axisLine: {
        				lineStyle: {
        					color: '#757575'
        				}
        			},
        			data: []
        		}],

        		// Vertical axis
        		yAxis: [{
        			type: 'value',
        			axisLabel: {
        				formatter: '€{value} ',
        				color: '#fff'
        			},
        			axisLine: {
        				lineStyle: {
        					color: '#757575'
        				}
        			},
        			splitLine: {
        				lineStyle: {
        					color: 'rgba(255,255,255,0.03)'
        				}
        			},
        			splitArea: {
        				show: true,
        				areaStyle: {
        					color: ['#212121', 'rgba(255,255,255,0.001)']
        				}
        			}
        		}],

        		// Zoom control
        		dataZoom: [{
        				type: 'inside',
        				//start: 30,
        				//end: 70
        			},
        			{
        				show: true,
        				type: 'slider',
        				//start: 30,
        				//end: 70,
        				height: 40,
        				bottom: 0,
        				textStyle: {
        					color: 'rgba(255,255,255,0.8)',
        					fontWeight: 'normal'
        				},
        				bacgroundColor: 'rgba(255,255,255,1)',
        				dataBackground: {
        					lineStyle: {
        						color: 'rgba(255,255,255,0.6)'
        					},
        					areaStyle: {
        						color: 'rgba(255,255,255,0.05)'
        					}
        				},
        				borderColor: 'rgba(255,255,255,0.05)',
        				fillerColor: 'rgba(0,0,0,0.1)',
        				handleStyle: {
        					color: 'rgba(255,255,255,0.8)',
        				}
        			}
        		],

        		// Add series
        		series: [{
        				name: 'Software',
        				type: 'line',
        				smooth: true,
        				symbolSize: 6,
        				areaStyle: {
        					normal: {
        						opacity: 0.1
        					}
        				},
        				itemStyle: {
        					normal: {
        						borderWidth: 2
        					}
        				},
        				data: []
        			},
        			{
        				name: 'Hardware',
        				type: 'line',
        				smooth: true,
        				symbolSize: 6,
        				areaStyle: {
        					normal: {
        						opacity: 0.1
        					}
        				},
        				itemStyle: {
        					normal: {
        						borderWidth: 2
        					}
        				},
        				data: []
        			}
        		]
        	};
        	area_zoom.setOption(options);

        	// Resize function
        	var triggerChartResize = function () {
        		area_zoom_element && area_zoom.resize();
        	};

        	// On window resize
        	var resizeCharts;
        	window.onresize = function () {
        		clearTimeout(resizeCharts);
        		resizeCharts = setTimeout(function () {
        			triggerChartResize();
        		}, 200);
			};
			
			
            //Select random values
            var randomplaces = ["81","89","142","100","119","74","1","11","124","83","88","98","71","106","12","140","97","14","42"];
            var randomtypes = ["1","2","4","25","32","9","10","27","14","7","17","11","24","29","23","26","5"];
        	$j('#category').val($j('#category option')[Math.floor(Math.random() * $j('#category option').length)].value).trigger('change');
        	$j('#type').val(randomtypes[Math.floor(Math.random() * randomtypes.length)]).trigger('change');
        	$j('#place1').val(randomplaces[Math.floor(Math.random() * randomplaces.length)]).trigger('change');
        	$j('#place2').val(randomplaces[Math.floor(Math.random() * randomplaces.length)]).trigger('change');

        	$j('select').on('change', function () {

        		var cat = parseInt($j('#category').val());
        		var type = parseInt($j('#type').val());
        		var place1 = parseInt($j('#place1').val());
        		var place2 = parseInt($j('#place2').val());
        		var place1_label = $j('#select2-place1-container').text();
        		var place2_label = $j('#select2-place2-container').text();

        		//Proper way
        		if (cat > 0 && type > 0 && place1 > 0 && place2 > 0) {
        			var id = Base64.encode((cat - 1) + ';' + type + ';' + place1 + ';' + place2 + ';');
        			$j.ajax({
        				url: "https://search.gallarija.mt/stats/" + id,
        				type: "GET",
        				success: function (data, status) {

        					results = data;
        					console.log(results);

        					if (results.total > 0) {

        						let place1array = {};
        						let place2array = {};

        						//Repair Data
        						results.items.forEach(function (item) {
        							if (item._source.place == place1) {
        								place1array[item._source.periodStart] = item._source.averagePrice;
        							}
        							if (item._source.place == place2) {
        								place2array[item._source.periodStart] = item._source.averagePrice;
        							}
        						});
        						//console.log(JSON.stringify(place1array)); console.log(JSON.stringify(place2array));
        						//Get unique dates between 2 places
        						let dates = unique($j.merge($j.merge([], Object.keys(place1array)), Object.keys(place2array)));
        						var final = {};
        						//console.log(JSON.stringify(dates));
        						//Merge and add 0 values if missing
        						dates.forEach(function (date) {
        							final[date] = {
        								place1: 0,
        								place2: 0
        							};
        							if (place1array.hasOwnProperty(date)) {
        								final[date].place1 = place1array[date];
        							}
        							if (place2array.hasOwnProperty(date)) {
        								final[date].place2 = place2array[date];
        							}
        						});

        						//Order dates
        						//console.log(JSON.stringify(final));
        						var orderedDates = [];
        						Object.keys(final).sort(function (a, b) {
        							return moment(a, 'DD/MM/YYYY').toDate() - moment(b, 'DD/MM/YYYY').toDate();
        						}).forEach(function (key) {
        							orderedDates.push(key);
        						})
        						//console.log(orderedDates);
        						var place1axis = [];
        						var place2axis = [];
        						orderedDates.forEach(function (date) {
        							place1axis.push(final[date].place1);
        							place2axis.push(final[date].place2);
        						});

        						//console.log(orderedDates);
        						//console.log(place1axis);
        						//console.log(place2axis);

        						//fix 0 values
        						for (var i = 0; i < place1axis.length; i++) {
        							if (i == 0 && place1axis[i] == 0) {
        								if (place1axis[i + 1] > 0) {
        									place1axis[i] = place1axis[i + 1];
        								}
        							} else {
        								//Trick to create average of previous and next if missing
        								if (place1axis[i] == 0 && place1axis[i + 1] > 0 && place1axis[i - 1] > 0) {
        									place1axis[i] = Math.round((place1axis[i + 1] + place1axis[i - 1]) / 2);
        								}
        								//if it's still 0
        								if (place1axis[i] == 0 && place1axis[i - 1] > 0) {
        									place1axis[i] = place1axis[i - 1];
        								}
        							}
        						}
        						for (var i = 0; i < place2axis.length; i++) {
        							if (i == 0 && place2axis[i] == 0) {
        								if (place2axis[i + 1] > 0) {
        									place2axis[i] = place2axis[i + 1];
        								}
        							} else {
        								//Trick to create average of previous and next if missing
        								if (place2axis[i] == 0 && place2axis[i + 1] > 0 && place2axis[i - 1] > 0) {
        									place2axis[i] = Math.round((place2axis[i + 1] + place2axis[i - 1]) / 2);
        								}
        								//if it's still 0
        								if (place2axis[i] == 0 && place2axis[i - 1] > 0) {
        									place2axis[i] = place2axis[i - 1];
        								}
        							}
        						}

        						//console.log(orderedDates);
        						//console.log(place1axis);
        						//console.log(place2axis);

        					} else {
        						orderedDates = ['Nothing Found'];
        						place1axis = [0];
								place2axis = [0];
								iziToast.error({
									title: '',
									theme: 'light',
									overlay: false,
									iconColor: '#ffffff',
									backgroundColor: '#bd5b5b',
									transitionIn: 'fadeInUp',
									transitionOut: 'fadeOutDown',
									   icon: ' fa-exclamation',
									balloon: true,
									position: 'bottomRight',
									zindex: 99999999,
									overlayClose: true,
									overlayColor: 'rgba(0, 0, 0, 0.8)',
									message: 'No trends found for this combination. Try something else!',
								  });
        					}

        					//Update values chart
        					area_zoom.clear();
        					options.series[0].data = place1axis;
        					options.series[1].data = place2axis;
        					options.series[0].name = place1_label;
        					options.series[1].name = place2_label;
        					options.legend.data = [place1_label, place2_label];
        					options.xAxis[0].data = orderedDates;
        					if (cat == 1) {
        						//options.yAxis[0].name = '(Monthly)';
        					}

        					area_zoom.setOption(options);

        					//console.log(JSON.stringify(options));

        				},
        				error: function (err) {
        					if (err.status == 429) {
        						window.location.href = "/429";
        					} else {
        						window.location.href = "/400";
        					}
        				}
        			});
        		}

			});
			
			//Trigger Random values
			$j('#category').trigger('change');

        });