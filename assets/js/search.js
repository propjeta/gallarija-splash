var $j = jQuery.noConflict();
var listings = [];
var ptitle = "";

var p_places = ["0","Attard","Baħar iċ-Ċagħaq, Naxxar","Baħrija, Rabat","Balzan","Bengħajsa, Birżebbuġa","Bidnija, Mosta","Binġemma, Rabat","Birgu, Vittoriosa","Birguma, Naxxar","Bir id-Deheb, Żejtun","Birkirkara","Birżebbuġa","Blata l-Bajda, Ħamrun","Żurrieq","Bubaqra, Żurrieq","Bugibba, St. Paul's Bay","Bulebel, Żejtun","Burmarrad, St. Paul's Bay","Buskett, Siġġiewi","Cirkewwa, Mellieħa","Cospicua, Bormla","Delimara, Marsaxlokk","Dingli","Dwejra (Gozo)","Fgura","Fleur-de-Lys, Birkirkara","Floriana","Fontana (Gozo)","Fort Cambridge, Sliema","Fort Chambray, Għajnsielem","Għadira, Mellieħa","Għajn Tuffieħa, Manikata","Għajnsielem (Gozo)","Ghammar, Għasri","Għarb (Gozo)","Għargħur","Għasri (Gozo)","Għaxaq","Ġnejna, Mġarr","Gudja","Gwardamanġa, Pietà","Gżira","Ħal Far, Birżebbuġa","Ħal Farruġ, Luqa","Saghtrija, Żebbuġ","Ħamrun","Tal-Ħandaq, Qormi","High Ridge, Swieqi","Ibraġ, Swieqi","Iklin","Mqabba","Ta' Xbiex","Kalafrana, Birżebbuġa","Kalkara","Kappara, San Ġwann","Kempinski, San Lawrenz","Kerċem (Gozo)","Kirkop","Kortin, Mellieħa","Kuncizzjoni, Ħamrun","L-Andrijiet, Rabat","Lija","Luqa","Madliena, Swieqi","Magħtab","Manikata","Manoel Island, Gżira","Marfa, Mellieħa","Marsalforn (Gozo)","Marsa","Marsaskala","Marsaxlokk","Mdina","Mellieħa","Mensija, San Ġwann","Mercury Tower, St Julian's","Mġarr","Mġarr (Gozo)","Mistra, St. Paul's Bay","Mizieb, Attard","Mosta","Mrieħel, Birkirkara","Msida","Mtahleb, Rabat","Mtarfa","Munxar (Gozo)","Nadur (Gozo)","Naxxar","St Julian's","Paola","Pembroke","Pender Gardens, St Julian's","Pietà","Portomaso, St Julian's","Qajjenza, Birżebbuġa","Qala (Gozo)","Qawra","Qormi","Qrendi","Rabat","Rikasoli, Kalkara","Safi","Salina, Naxxar","San Blas (Gozo)","Valletta","San Ġwann","San Lawrenz (Gozo)","Tarxien","San Pawl tat-Tarġa, Naxxar","Sannat (Gozo)","Santa Lucija","Santa Lucija (Gozo)","Santa Marija Estate, Mellieħa","Santa Venera","Savoy Gardens, Gżira","Selmun, Mellieħa","Senglea, Isla","Siġġiewi","Sliema","Smart City, Kalkara","Southridge, Mellieħa","St. Andrew's, Għargħur","Paceville, St Julian's","St. Paul's Bay","Swatar, Birkirkara","Swieqi","Ta' Monita, Marsaskala","Ta' Qali, Attard","Ta' Ġiorni, St Julian's","Tal-Virtu, Rabat","Tas-Sellum, Mellieħa","Tigne, Sliema","Victoria (Gozo)","Wardija, St Paul's Bay","Xagħra (Gozo)","Xemxija, St. Paul's Bay","Xewkija (Gozo)","Xgħajra","Xlendi (Gozo)","Zabbar","Żebbiegħ, Mġarr","Żebbuġ","Żebbuġ (Gozo)","Żejtun"];

var p_types = ["0","Apartment","Maisonette","Studio Flat","Penthouse","Bungalow","Farmhouse","House Of Character","Palazzo","Terraced House","Town House","Villa","Block Of Apartment","Airspace","Garage/Parking Facilities","Plot","Site","Duplex","Land","Boathouses","Farm","Luxury Estate","Development","Bar","Restaurant","Shop","Showroom","Office","Factory","Storage","Club","Hotels & Guesthouses","Commercial Premises"];

// Create Base64 Object
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

function gec(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
	  var c = ca[i];
	  while (c.charAt(0) == ' ') {
		c = c.substring(1);
	  }
	  if (c.indexOf(name) == 0) {
		return c.substring(name.length, c.length);
	  }
	}
	return "0";
  }

function today(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; 
	var yyyy = today.getFullYear();
	var h = today.getHours();
	var m = today.getMinutes();
	var s = today.getSeconds();

	if(dd<10) { dd = '0'+dd } 
	if(mm<10) { mm = '0'+mm } 
	if(h<10) { h = '0'+h } 
	if(m<10) { m = '0'+m } 
	if(s<10) { s = '0'+s } 
	today = dd + '/' + mm + '/' + yyyy + ' ' + h + ':' + m + ':' + s;
	return today;
}

var imgcounter = 0;
var totalimages = 0;
function checkheight(img){
	if (img.naturalWidth < 300) { $j(img).attr('style', 'width:auto !important;'); }
	if (img.height > 499 && img.naturalHeight > 499) { $j(img).attr('style', 'object-fit: none; object-position: center; width:100%; height:499px !important;'); }
	if (img.height < 191) { $j(img).attr('style', 'height:200px !important; width:auto !important;'); }
    imgcounter++;
}

function imgError(img, url){
	if (img.src != url) { img.src = url; } else { img.src = '/assets/images/placeholder.jpg';}
}

function getTitle(filter){

	try{
		
		var urlid = Base64.decode(filter).split(';');
		var filters = {};
			
		urlid.forEach(function(element) {
			try{
			let key = element.split(':')[0];
			let values = element.split(':')[1].split('/');
			let filter = {[key] : values};
			$j.extend(filters, filter);
			} catch (e){}
		});

		let typesarray = [];
		let placearray = [];
		let types = 0;
		let places = 0;
		let cat = "";
		let rent = "";
		let p1 = "";
		let p2 = "";
		
		if (filters.hasOwnProperty('c') && filters.hasOwnProperty('pb') && filters['c'] == 'b'){ 
			cat = "for sale"; p1 = (parseInt(filters['pb'][0].replace(/,/g, ''))*1000).toLocaleString(); p2 = (parseInt(filters['pb'][1].replace(/,/g, ''))*1000).toLocaleString(); 
		}  
		else if (filters.hasOwnProperty('c') && filters.hasOwnProperty('pr') && filters['c'] == 'r'){
			cat = "to let"; rent = " / Month"; p1 = parseInt(filters['pr'][0].replace(/,/g, '')).toLocaleString(); p2 = parseInt(filters['pr'][1].replace(/,/g, '')).toLocaleString();  
		}
		if (filters.hasOwnProperty('0')){ 
			filters['0'].forEach(function(element ,i) {
				types++;
  				if (i < 5 && !isNaN(element)) { typesarray.push(p_types[parseInt(element)]); }
			});
		}
		if (filters.hasOwnProperty('1')){ 
			filters['1'].forEach(function(element ,i) {
				places++;
  				if (i < 5 && !isNaN(element)) { placearray.push(p_places[parseInt(element)]); }
			});
		}

		if(types < 5) { types = typesarray.join(', '); } else { types = typesarray.join(', ') + " and " + types + " more types ";}
		if(places < 5) { places = placearray.join(', '); } else { places = placearray.join(', ') + " and " + places + " more locations ";}
		
		return `You’ve searched for ${types == 0 ? "any type of property" : types} ${cat} in ${places == 0 ? "any location" : places} ranging from €${p1}${rent} to €${p2}${rent}.`;
	
	} catch (e){
		console.log(e);
	}

}

if (window.location.pathname == "/search/") {
	
	if (window.location.href.indexOf('?') > -1) {
	
  	var search = window.location.href.split('?')[1];
	if (search.indexOf('@') > -1) { search = search.split('@')[0];}
	var url = window.location.href.split('?')[1].split('!')[0];

  	$j.ajax({
    	url: "https://search.gallarija.mt/"+url+'&'+Base64.encode(gec('_ga'))+'&',
    	type: "GET",
    	success: function(data, status){
    	
    		ptitle = getTitle(search);
 			listings = data;
 			
			 //Save history
			localStorage.removeItem("his-"+search);
 			localStorage.setItem("his-"+search, Base64.encode(today()+","+listings.total));
 			
    	},
    	error: function(err){
    	
    		if(err.status == 429){window.location.href = "/429";}
    		else {window.location.href = "/400";}
    		
    	} 	
  	});
  	
	} else {
  	
  	//Redirect to homepage
  	window.location.href = "/";
  	
	}

}
