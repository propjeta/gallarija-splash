var $j = jQuery.noConflict();

function allStorage() {

    var values = [],
        keys = Object.keys(localStorage),
        i = keys.length;

    while ( i-- ) {
        values.push( localStorage.getItem(keys[i]) );
    }

    return values;
}

$j(document).ready(function() {

	var localitems = allStorage();
	if(localitems.length > 0){
		$j('#totalfav').html(localitems.length+' Propert'+ (localitems.length > 1 ? 'ies' : 'y') +' Found');
	}else{
		$j('.extras').addClass('tohide');
		$j('#totalfav').html('No Properties Found');
		$j('#pagetitle').html('You have no<br/>favourite listings yet');
	}
	
	
	
});
