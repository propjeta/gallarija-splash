/*-------------------------------------------------------------------------

	1.	Plugin Init
	2.	Helper Functions
	3.	Shortcode Stuff
	4.	Header + Search
	5.	Page Specific
	6.  Scroll to top 
	7.	Cross Browser Fixes


-------------------------------------------------------------------------*/






/*-------------------------------------------------------------------------*/
/*	1.	Plugin Init
/*-------------------------------------------------------------------------*/


/*!
Waypoints - 4.0.1
*/
!function(){"use strict";function t(o){if(!o)throw new Error("No options passed to Waypoint constructor");if(!o.element)throw new Error("No element option passed to Waypoint constructor");if(!o.handler)throw new Error("No handler option passed to Waypoint constructor");this.key="waypoint-"+e,this.options=t.Adapter.extend({},t.defaults,o),this.element=this.options.element,this.adapter=new t.Adapter(this.element),this.callback=o.handler,this.axis=this.options.horizontal?"horizontal":"vertical",this.enabled=this.options.enabled,this.triggerPoint=null,this.group=t.Group.findOrCreate({name:this.options.group,axis:this.axis}),this.context=t.Context.findOrCreateByElement(this.options.context),t.offsetAliases[this.options.offset]&&(this.options.offset=t.offsetAliases[this.options.offset]),this.group.add(this),this.context.add(this),i[this.key]=this,e+=1}var e=0,i={};t.prototype.queueTrigger=function(t){this.group.queueTrigger(this,t)},t.prototype.trigger=function(t){this.enabled&&this.callback&&this.callback.apply(this,t)},t.prototype.destroy=function(){this.context.remove(this),this.group.remove(this),delete i[this.key]},t.prototype.disable=function(){return this.enabled=!1,this},t.prototype.enable=function(){return this.context.refresh(),this.enabled=!0,this},t.prototype.next=function(){return this.group.next(this)},t.prototype.previous=function(){return this.group.previous(this)},t.invokeAll=function(t){var e=[];for(var o in i)e.push(i[o]);for(var n=0,r=e.length;r>n;n++)e[n][t]()},t.destroyAll=function(){t.invokeAll("destroy")},t.disableAll=function(){t.invokeAll("disable")},t.enableAll=function(){t.Context.refreshAll();for(var e in i)i[e].enabled=!0;return this},t.refreshAll=function(){t.Context.refreshAll()},t.viewportHeight=function(){return window.innerHeight||document.documentElement.clientHeight},t.viewportWidth=function(){return document.documentElement.clientWidth},t.adapters=[],t.defaults={context:window,continuous:!0,enabled:!0,group:"default",horizontal:!1,offset:0},t.offsetAliases={"bottom-in-view":function(){return this.context.innerHeight()-this.adapter.outerHeight()},"right-in-view":function(){return this.context.innerWidth()-this.adapter.outerWidth()}},window.Waypoint=t}(),function(){"use strict";function t(t){window.setTimeout(t,1e3/60)}function e(t){this.element=t,this.Adapter=n.Adapter,this.adapter=new this.Adapter(t),this.key="waypoint-context-"+i,this.didScroll=!1,this.didResize=!1,this.oldScroll={x:this.adapter.scrollLeft(),y:this.adapter.scrollTop()},this.waypoints={vertical:{},horizontal:{}},t.waypointContextKey=this.key,o[t.waypointContextKey]=this,i+=1,n.windowContext||(n.windowContext=!0,n.windowContext=new e(window)),this.createThrottledScrollHandler(),this.createThrottledResizeHandler()}var i=0,o={},n=window.Waypoint,r=window.onload;e.prototype.add=function(t){var e=t.options.horizontal?"horizontal":"vertical";this.waypoints[e][t.key]=t,this.refresh()},e.prototype.checkEmpty=function(){var t=this.Adapter.isEmptyObject(this.waypoints.horizontal),e=this.Adapter.isEmptyObject(this.waypoints.vertical),i=this.element==this.element.window;t&&e&&!i&&(this.adapter.off(".waypoints"),delete o[this.key])},e.prototype.createThrottledResizeHandler=function(){function t(){e.handleResize(),e.didResize=!1}var e=this;this.adapter.on("resize.waypoints",function(){e.didResize||(e.didResize=!0,n.requestAnimationFrame(t))})},e.prototype.createThrottledScrollHandler=function(){function t(){e.handleScroll(),e.didScroll=!1}var e=this;this.adapter.on("scroll.waypoints",function(){(!e.didScroll||n.isTouch)&&(e.didScroll=!0,n.requestAnimationFrame(t))})},e.prototype.handleResize=function(){n.Context.refreshAll()},e.prototype.handleScroll=function(){var t={},e={horizontal:{newScroll:this.adapter.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.adapter.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};for(var i in e){var o=e[i],n=o.newScroll>o.oldScroll,r=n?o.forward:o.backward;for(var s in this.waypoints[i]){var a=this.waypoints[i][s];if(null!==a.triggerPoint){var l=o.oldScroll<a.triggerPoint,h=o.newScroll>=a.triggerPoint,p=l&&h,u=!l&&!h;(p||u)&&(a.queueTrigger(r),t[a.group.id]=a.group)}}}for(var c in t)t[c].flushTriggers();this.oldScroll={x:e.horizontal.newScroll,y:e.vertical.newScroll}},e.prototype.innerHeight=function(){return this.element==this.element.window?n.viewportHeight():this.adapter.innerHeight()},e.prototype.remove=function(t){delete this.waypoints[t.axis][t.key],this.checkEmpty()},e.prototype.innerWidth=function(){return this.element==this.element.window?n.viewportWidth():this.adapter.innerWidth()},e.prototype.destroy=function(){var t=[];for(var e in this.waypoints)for(var i in this.waypoints[e])t.push(this.waypoints[e][i]);for(var o=0,n=t.length;n>o;o++)t[o].destroy()},e.prototype.refresh=function(){var t,e=this.element==this.element.window,i=e?void 0:this.adapter.offset(),o={};this.handleScroll(),t={horizontal:{contextOffset:e?0:i.left,contextScroll:e?0:this.oldScroll.x,contextDimension:this.innerWidth(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:e?0:i.top,contextScroll:e?0:this.oldScroll.y,contextDimension:this.innerHeight(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};for(var r in t){var s=t[r];for(var a in this.waypoints[r]){var l,h,p,u,c,d=this.waypoints[r][a],f=d.options.offset,w=d.triggerPoint,y=0,g=null==w;d.element!==d.element.window&&(y=d.adapter.offset()[s.offsetProp]),"function"==typeof f?f=f.apply(d):"string"==typeof f&&(f=parseFloat(f),d.options.offset.indexOf("%")>-1&&(f=Math.ceil(s.contextDimension*f/100))),l=s.contextScroll-s.contextOffset,d.triggerPoint=Math.floor(y+l-f),h=w<s.oldScroll,p=d.triggerPoint>=s.oldScroll,u=h&&p,c=!h&&!p,!g&&u?(d.queueTrigger(s.backward),o[d.group.id]=d.group):!g&&c?(d.queueTrigger(s.forward),o[d.group.id]=d.group):g&&s.oldScroll>=d.triggerPoint&&(d.queueTrigger(s.forward),o[d.group.id]=d.group)}}return n.requestAnimationFrame(function(){for(var t in o)o[t].flushTriggers()}),this},e.findOrCreateByElement=function(t){return e.findByElement(t)||new e(t)},e.refreshAll=function(){for(var t in o)o[t].refresh()},e.findByElement=function(t){return o[t.waypointContextKey]},window.onload=function(){r&&r(),e.refreshAll()},n.requestAnimationFrame=function(e){var i=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||t;i.call(window,e)},n.Context=e}(),function(){"use strict";function t(t,e){return t.triggerPoint-e.triggerPoint}function e(t,e){return e.triggerPoint-t.triggerPoint}function i(t){this.name=t.name,this.axis=t.axis,this.id=this.name+"-"+this.axis,this.waypoints=[],this.clearTriggerQueues(),o[this.axis][this.name]=this}var o={vertical:{},horizontal:{}},n=window.Waypoint;i.prototype.add=function(t){this.waypoints.push(t)},i.prototype.clearTriggerQueues=function(){this.triggerQueues={up:[],down:[],left:[],right:[]}},i.prototype.flushTriggers=function(){for(var i in this.triggerQueues){var o=this.triggerQueues[i],n="up"===i||"left"===i;o.sort(n?e:t);for(var r=0,s=o.length;s>r;r+=1){var a=o[r];(a.options.continuous||r===o.length-1)&&a.trigger([i])}}this.clearTriggerQueues()},i.prototype.next=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints),o=i===this.waypoints.length-1;return o?null:this.waypoints[i+1]},i.prototype.previous=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints);return i?this.waypoints[i-1]:null},i.prototype.queueTrigger=function(t,e){this.triggerQueues[e].push(t)},i.prototype.remove=function(t){var e=n.Adapter.inArray(t,this.waypoints);e>-1&&this.waypoints.splice(e,1)},i.prototype.first=function(){return this.waypoints[0]},i.prototype.last=function(){return this.waypoints[this.waypoints.length-1]},i.findOrCreate=function(t){return o[t.axis][t.name]||new i(t)},n.Group=i}(),function(){"use strict";function t(t){this.$element=e(t)}var e=window.jQuery,i=window.Waypoint;e.each(["innerHeight","innerWidth","off","offset","on","outerHeight","outerWidth","scrollLeft","scrollTop"],function(e,i){t.prototype[i]=function(){var t=Array.prototype.slice.call(arguments);return this.$element[i].apply(this.$element,t)}}),e.each(["extend","inArray","isEmptyObject"],function(i,o){t[o]=e[o]}),i.adapters.push({name:"jquery",Adapter:t}),i.Adapter=t}(),function(){"use strict";function t(t){return function(){var i=[],o=arguments[0];return t.isFunction(arguments[0])&&(o=t.extend({},arguments[1]),o.handler=arguments[0]),this.each(function(){var n=t.extend({},o,{element:this});"string"==typeof n.context&&(n.context=t(this).closest(n.context)[0]),i.push(new e(n))}),i}}var e=window.Waypoint;window.jQuery&&(window.jQuery.fn.waypoint=t(window.jQuery)),window.Zepto&&(window.Zepto.fn.waypoint=t(window.Zepto))}();

//visible
!function(t){var i=t(window);t.fn.visible=function(t,e,o){if(!(this.length<1)){var r=this.length>1?this.eq(0):this,n=r.get(0),f=i.width(),h=i.height(),o=o?o:"both",l=e===!0?n.offsetWidth*n.offsetHeight:!0;if("function"==typeof n.getBoundingClientRect){var g=n.getBoundingClientRect(),u=g.top>=0&&g.top<h,s=g.bottom>0&&g.bottom<=h,c=g.left>=0&&g.left<f,a=g.right>0&&g.right<=f,v=t?u||s:u&&s,b=t?c||a:c&&a;if("both"===o)return l&&v&&b;if("vertical"===o)return l&&v;if("horizontal"===o)return l&&b}else{var d=i.scrollTop(),p=d+h,w=i.scrollLeft(),m=w+f,y=r.offset(),z=y.top,B=z+r.height(),C=y.left,R=C+r.width(),j=t===!0?B:z,q=t===!0?z:B,H=t===!0?R:C,L=t===!0?C:R;if("both"===o)return!!l&&p>=q&&j>=d&&m>=L&&H>=w;if("vertical"===o)return!!l&&p>=q&&j>=d;if("horizontal"===o)return!!l&&m>=L&&H>=w}}}}(jQuery);


/*
* jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
*/
jQuery.easing["jswing"]=jQuery.easing["swing"];jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(a,b,c,d,e){return jQuery.easing[jQuery.easing.def](a,b,c,d,e)},easeInQuad:function(a,b,c,d,e){return d*(b/=e)*b+c},easeOutQuad:function(a,b,c,d,e){return-d*(b/=e)*(b-2)+c},easeInOutQuad:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b+c;return-d/2*(--b*(b-2)-1)+c},easeInCubic:function(a,b,c,d,e){return d*(b/=e)*b*b+c},easeOutCubic:function(a,b,c,d,e){return d*((b=b/e-1)*b*b+1)+c},easeInOutCubic:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b*b+c;return d/2*((b-=2)*b*b+2)+c},easeInQuart:function(a,b,c,d,e){return d*(b/=e)*b*b*b+c},easeOutQuart:function(a,b,c,d,e){return-d*((b=b/e-1)*b*b*b-1)+c},easeInOutQuart:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b*b*b+c;return-d/2*((b-=2)*b*b*b-2)+c},easeInQuint:function(a,b,c,d,e){return d*(b/=e)*b*b*b*b+c},easeOutQuint:function(a,b,c,d,e){return d*((b=b/e-1)*b*b*b*b+1)+c},easeInOutQuint:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b*b*b*b+c;return d/2*((b-=2)*b*b*b*b+2)+c},easeInSine:function(a,b,c,d,e){return-d*Math.cos(b/e*(Math.PI/2))+d+c},easeOutSine:function(a,b,c,d,e){return d*Math.sin(b/e*(Math.PI/2))+c},easeInOutSine:function(a,b,c,d,e){return-d/2*(Math.cos(Math.PI*b/e)-1)+c},easeInExpo:function(a,b,c,d,e){return b==0?c:d*Math.pow(2,10*(b/e-1))+c},easeOutExpo:function(a,b,c,d,e){return b==e?c+d:d*(-Math.pow(2,-10*b/e)+1)+c},easeInOutExpo:function(a,b,c,d,e){if(b==0)return c;if(b==e)return c+d;if((b/=e/2)<1)return d/2*Math.pow(2,10*(b-1))+c;return d/2*(-Math.pow(2,-10*--b)+2)+c},easeInCirc:function(a,b,c,d,e){return-d*(Math.sqrt(1-(b/=e)*b)-1)+c},easeOutCirc:function(a,b,c,d,e){return d*Math.sqrt(1-(b=b/e-1)*b)+c},easeInOutCirc:function(a,b,c,d,e){if((b/=e/2)<1)return-d/2*(Math.sqrt(1-b*b)-1)+c;return d/2*(Math.sqrt(1-(b-=2)*b)+1)+c},easeInElastic:function(a,b,c,d,e){var f=1.70158;var g=0;var h=d;if(b==0)return c;if((b/=e)==1)return c+d;if(!g)g=e*.3;if(h<Math.abs(d)){h=d;var f=g/4}else var f=g/(2*Math.PI)*Math.asin(d/h);return-(h*Math.pow(2,10*(b-=1))*Math.sin((b*e-f)*2*Math.PI/g))+c},easeOutElastic:function(a,b,c,d,e){var f=1.70158;var g=0;var h=d;if(b==0)return c;if((b/=e)==1)return c+d;if(!g)g=e*.3;if(h<Math.abs(d)){h=d;var f=g/4}else var f=g/(2*Math.PI)*Math.asin(d/h);return h*Math.pow(2,-10*b)*Math.sin((b*e-f)*2*Math.PI/g)+d+c},easeInOutElastic:function(a,b,c,d,e){var f=1.70158;var g=0;var h=d;if(b==0)return c;if((b/=e/2)==2)return c+d;if(!g)g=e*.3*1.5;if(h<Math.abs(d)){h=d;var f=g/4}else var f=g/(2*Math.PI)*Math.asin(d/h);if(b<1)return-.5*h*Math.pow(2,10*(b-=1))*Math.sin((b*e-f)*2*Math.PI/g)+c;return h*Math.pow(2,-10*(b-=1))*Math.sin((b*e-f)*2*Math.PI/g)*.5+d+c},easeInBack:function(a,b,c,d,e,f){if(f==undefined)f=1.70158;return d*(b/=e)*b*((f+1)*b-f)+c},easeOutBack:function(a,b,c,d,e,f){if(f==undefined)f=1.70158;return d*((b=b/e-1)*b*((f+1)*b+f)+1)+c},easeInOutBack:function(a,b,c,d,e,f){if(f==undefined)f=1.70158;if((b/=e/2)<1)return d/2*b*b*(((f*=1.525)+1)*b-f)+c;return d/2*((b-=2)*b*(((f*=1.525)+1)*b+f)+2)+c},easeInBounce:function(a,b,c,d,e){return d-jQuery.easing.easeOutBounce(a,e-b,0,d,e)+c},easeOutBounce:function(a,b,c,d,e){if((b/=e)<1/2.75){return d*7.5625*b*b+c}else if(b<2/2.75){return d*(7.5625*(b-=1.5/2.75)*b+.75)+c}else if(b<2.5/2.75){return d*(7.5625*(b-=2.25/2.75)*b+.9375)+c}else{return d*(7.5625*(b-=2.625/2.75)*b+.984375)+c}},easeInOutBounce:function(a,b,c,d,e){if(b<e/2)return jQuery.easing.easeInBounce(a,b*2,0,d,e)*.5+c;return jQuery.easing.easeOutBounce(a,b*2-e,0,d,e)*.5+d*.5+c}})


/*! Mousewheel by Brandon Aaron (http://brandon.aaron.sh) */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?module.exports=a:a(jQuery)}(function(a){function b(b){var g=b||window.event,h=i.call(arguments,1),j=0,l=0,m=0,n=0,o=0,p=0;if(b=a.event.fix(g),b.type="mousewheel","detail"in g&&(m=-1*g.detail),"wheelDelta"in g&&(m=g.wheelDelta),"wheelDeltaY"in g&&(m=g.wheelDeltaY),"wheelDeltaX"in g&&(l=-1*g.wheelDeltaX),"axis"in g&&g.axis===g.HORIZONTAL_AXIS&&(l=-1*m,m=0),j=0===m?l:m,"deltaY"in g&&(m=-1*g.deltaY,j=m),"deltaX"in g&&(l=g.deltaX,0===m&&(j=-1*l)),0!==m||0!==l){if(1===g.deltaMode){var q=a.data(this,"mousewheel-line-height");j*=q,m*=q,l*=q}else if(2===g.deltaMode){var r=a.data(this,"mousewheel-page-height");j*=r,m*=r,l*=r}if(n=Math.max(Math.abs(m),Math.abs(l)),(!f||f>n)&&(f=n,d(g,n)&&(f/=40)),d(g,n)&&(j/=40,l/=40,m/=40),j=Math[j>=1?"floor":"ceil"](j/f),l=Math[l>=1?"floor":"ceil"](l/f),m=Math[m>=1?"floor":"ceil"](m/f),k.settings.normalizeOffset&&this.getBoundingClientRect){var s=this.getBoundingClientRect();o=b.clientX-s.left,p=b.clientY-s.top}return b.deltaX=l,b.deltaY=m,b.deltaFactor=f,b.offsetX=o,b.offsetY=p,b.deltaMode=0,h.unshift(b,j,l,m),e&&clearTimeout(e),e=setTimeout(c,200),(a.event.dispatch||a.event.handle).apply(this,h)}}function c(){f=null}function d(a,b){return k.settings.adjustOldDeltas&&"mousewheel"===a.type&&b%120===0}var e,f,g=["wheel","mousewheel","DOMMouseScroll","MozMousePixelScroll"],h="onwheel"in document||document.documentMode>=9?["wheel"]:["mousewheel","DomMouseScroll","MozMousePixelScroll"],i=Array.prototype.slice;if(a.event.fixHooks)for(var j=g.length;j;)a.event.fixHooks[g[--j]]=a.event.mouseHooks;var k=a.event.special.mousewheel={version:"3.1.12",setup:function(){if(this.addEventListener)for(var c=h.length;c;)this.addEventListener(h[--c],b,!1);else this.onmousewheel=b;a.data(this,"mousewheel-line-height",k.getLineHeight(this)),a.data(this,"mousewheel-page-height",k.getPageHeight(this))},teardown:function(){if(this.removeEventListener)for(var c=h.length;c;)this.removeEventListener(h[--c],b,!1);else this.onmousewheel=null;a.removeData(this,"mousewheel-line-height"),a.removeData(this,"mousewheel-page-height")},getLineHeight:function(b){var c=a(b),d=c["offsetParent"in a.fn?"offsetParent":"parent"]();return d.length||(d=a("body")),parseInt(d.css("fontSize"),10)||parseInt(c.css("fontSize"),10)||16},getPageHeight:function(b){return a(b).height()},settings:{adjustOldDeltas:!0,normalizeOffset:!0}};a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})});

(function($, window, document) {


jQuery(document).ready(function($){

/****************** Nectar ******************/	

// Create cross browser requestAnimationFrame method:
window.requestAnimationFrame = window.requestAnimationFrame
|| window.mozRequestAnimationFrame
|| window.webkitRequestAnimationFrame
|| window.msRequestAnimationFrame
|| function(f){setTimeout(f, 1000/60)}
	 
var nectarDOMInfo = {
	
	usingMobileBrowser: (navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)) ? true : false,

	getWindowSize: function() {
		nectarDOMInfo.windowHeight = window.innerHeight;
		nectarDOMInfo.windowWidth = window.innerWidth;
		
		nectarDOMInfo.adminBarHeight = ($('#wpadminbar').length > 0) ? $('#wpadminbar').height() : 0;
		nectarDOMInfo.secondaryHeaderHeight = ($('#header-secondary-outer').length > 0) ? $('#header-secondary-outer').height() : 0;
	},

	scrollPosMouse: function() {
		return $(window).scrollTop();
	},
	
	scrollPosRAF: function() {
		nectarDOMInfo.scrollTop = $(window).scrollTop();
		requestAnimationFrame(nectarDOMInfo.scrollPosRAF);
	},
	

	bindEvents: function() {
		
		if(!nectarDOMInfo.usingMobileBrowser) {
			$(window).on('scroll',function(){ nectarDOMInfo.scrollTop = nectarDOMInfo.scrollPosMouse(); });
		}
		
		$(window).on('resize',nectarDOMInfo.getWindowSize);
		
	}

}

nectarDOMInfo.getWindowSize();
nectarDOMInfo.scrollTop = nectarDOMInfo.scrollPosMouse();

if(nectarDOMInfo.usingMobileBrowser) { requestAnimationFrame(nectarDOMInfo.scrollPosRAF); } 

nectarDOMInfo.bindEvents();


//set row sizes
//fullWidthSections();
fwsClasses();

	
/*!
 * jQuery Transit - CSS3 transitions and transformations
 * (c) 2011-2012 Rico Sta. Cruz <rico@ricostacruz.com>
 * MIT Licensed.
 *
 * http://ricostacruz.com/jquery.transit
 * http://github.com/rstacruz/jquery.transit
 */
(function(k){k.transit={version:"0.9.9",propertyMap:{marginLeft:"margin",marginRight:"margin",marginBottom:"margin",marginTop:"margin",paddingLeft:"padding",paddingRight:"padding",paddingBottom:"padding",paddingTop:"padding"},enabled:true,useTransitionEnd:false};var d=document.createElement("div");var q={};function b(v){if(v in d.style){return v}var u=["Moz","Webkit","O","ms"];var r=v.charAt(0).toUpperCase()+v.substr(1);if(v in d.style){return v}for(var t=0;t<u.length;++t){var s=u[t]+r;if(s in d.style){return s}}}function e(){d.style[q.transform]="";d.style[q.transform]="rotateY(90deg)";return d.style[q.transform]!==""}var a=navigator.userAgent.toLowerCase().indexOf("chrome")>-1;q.transition=b("transition");q.transitionDelay=b("transitionDelay");q.transform=b("transform");q.transformOrigin=b("transformOrigin");q.transform3d=e();var i={transition:"transitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",WebkitTransition:"webkitTransitionEnd",msTransition:"MSTransitionEnd"};var f=q.transitionEnd=i[q.transition]||null;for(var p in q){if(q.hasOwnProperty(p)&&typeof k.support[p]==="undefined"){k.support[p]=q[p]}}d=null;k.cssEase={_default:"ease","in":"ease-in",out:"ease-out","in-out":"ease-in-out",snap:"cubic-bezier(0,1,.5,1)",easeOutCubic:"cubic-bezier(.215,.61,.355,1)",easeInOutCubic:"cubic-bezier(.645,.045,.355,1)",easeInCirc:"cubic-bezier(.6,.04,.98,.335)",easeOutCirc:"cubic-bezier(.075,.82,.165,1)",easeInOutCirc:"cubic-bezier(.785,.135,.15,.86)",easeInExpo:"cubic-bezier(.95,.05,.795,.035)",easeOutExpo:"cubic-bezier(.19,1,.22,1)",easeInOutExpo:"cubic-bezier(1,0,0,1)",easeInQuad:"cubic-bezier(.55,.085,.68,.53)",easeOutQuad:"cubic-bezier(.25,.46,.45,.94)",easeInOutQuad:"cubic-bezier(.455,.03,.515,.955)",easeInQuart:"cubic-bezier(.895,.03,.685,.22)",easeOutQuart:"cubic-bezier(.165,.84,.44,1)",easeInOutQuart:"cubic-bezier(.77,0,.175,1)",easeInQuint:"cubic-bezier(.755,.05,.855,.06)",easeOutQuint:"cubic-bezier(.23,1,.32,1)",easeInOutQuint:"cubic-bezier(.86,0,.07,1)",easeInSine:"cubic-bezier(.47,0,.745,.715)",easeOutSine:"cubic-bezier(.39,.575,.565,1)",easeInOutSine:"cubic-bezier(.445,.05,.55,.95)",easeInBack:"cubic-bezier(.6,-.28,.735,.045)",easeOutBack:"cubic-bezier(.175, .885,.32,1.275)",easeInOutBack:"cubic-bezier(.68,-.55,.265,1.55)"};k.cssHooks["transit:transform"]={get:function(r){return k(r).data("transform")||new j()},set:function(s,r){var t=r;if(!(t instanceof j)){t=new j(t)}if(q.transform==="WebkitTransform"&&!a){s.style[q.transform]=t.toString(true)}else{s.style[q.transform]=t.toString()}k(s).data("transform",t)}};k.cssHooks.transform={set:k.cssHooks["transit:transform"].set};if(k.fn.jquery<"1.8"){k.cssHooks.transformOrigin={get:function(r){return r.style[q.transformOrigin]},set:function(r,s){r.style[q.transformOrigin]=s}};k.cssHooks.transition={get:function(r){return r.style[q.transition]},set:function(r,s){r.style[q.transition]=s}}}n("scale");n("translate");n("rotate");n("rotateX");n("rotateY");n("rotate3d");n("perspective");n("skewX");n("skewY");n("x",true);n("y",true);function j(r){if(typeof r==="string"){this.parse(r)}return this}j.prototype={setFromString:function(t,s){var r=(typeof s==="string")?s.split(","):(s.constructor===Array)?s:[s];r.unshift(t);j.prototype.set.apply(this,r)},set:function(s){var r=Array.prototype.slice.apply(arguments,[1]);if(this.setter[s]){this.setter[s].apply(this,r)}else{this[s]=r.join(",")}},get:function(r){if(this.getter[r]){return this.getter[r].apply(this)}else{return this[r]||0}},setter:{rotate:function(r){this.rotate=o(r,"deg")},rotateX:function(r){this.rotateX=o(r,"deg")},rotateY:function(r){this.rotateY=o(r,"deg")},scale:function(r,s){if(s===undefined){s=r}this.scale=r+","+s},skewX:function(r){this.skewX=o(r,"deg")},skewY:function(r){this.skewY=o(r,"deg")},perspective:function(r){this.perspective=o(r,"px")},x:function(r){this.set("translate",r,null)},y:function(r){this.set("translate",null,r)},translate:function(r,s){if(this._translateX===undefined){this._translateX=0}if(this._translateY===undefined){this._translateY=0}if(r!==null&&r!==undefined){this._translateX=o(r,"px")}if(s!==null&&s!==undefined){this._translateY=o(s,"px")}this.translate=this._translateX+","+this._translateY}},getter:{x:function(){return this._translateX||0},y:function(){return this._translateY||0},scale:function(){var r=(this.scale||"1,1").split(",");if(r[0]){r[0]=parseFloat(r[0])}if(r[1]){r[1]=parseFloat(r[1])}return(r[0]===r[1])?r[0]:r},rotate3d:function(){var t=(this.rotate3d||"0,0,0,0deg").split(",");for(var r=0;r<=3;++r){if(t[r]){t[r]=parseFloat(t[r])}}if(t[3]){t[3]=o(t[3],"deg")}return t}},parse:function(s){var r=this;s.replace(/([a-zA-Z0-9]+)\((.*?)\)/g,function(t,v,u){r.setFromString(v,u)})},toString:function(t){var s=[];for(var r in this){if(this.hasOwnProperty(r)){if((!q.transform3d)&&((r==="rotateX")||(r==="rotateY")||(r==="perspective")||(r==="transformOrigin"))){continue}if(r[0]!=="_"){if(t&&(r==="scale")){s.push(r+"3d("+this[r]+",1)")}else{if(t&&(r==="translate")){s.push(r+"3d("+this[r]+",0)")}else{s.push(r+"("+this[r]+")")}}}}}return s.join(" ")}};function m(s,r,t){if(r===true){s.queue(t)}else{if(r){s.queue(r,t)}else{t()}}}function h(s){var r=[];k.each(s,function(t){t=k.camelCase(t);t=k.transit.propertyMap[t]||k.cssProps[t]||t;t=c(t);if(k.inArray(t,r)===-1){r.push(t)}});return r}function g(s,v,x,r){var t=h(s);if(k.cssEase[x]){x=k.cssEase[x]}var w=""+l(v)+" "+x;if(parseInt(r,10)>0){w+=" "+l(r)}var u=[];k.each(t,function(z,y){u.push(y+" "+w)});return u.join(", ")}k.fn.transition=k.fn.transit=function(z,s,y,C){var D=this;var u=0;var w=true;if(typeof s==="function"){C=s;s=undefined}if(typeof y==="function"){C=y;y=undefined}if(typeof z.easing!=="undefined"){y=z.easing;delete z.easing}if(typeof z.duration!=="undefined"){s=z.duration;delete z.duration}if(typeof z.complete!=="undefined"){C=z.complete;delete z.complete}if(typeof z.queue!=="undefined"){w=z.queue;delete z.queue}if(typeof z.delay!=="undefined"){u=z.delay;delete z.delay}if(typeof s==="undefined"){s=k.fx.speeds._default}if(typeof y==="undefined"){y=k.cssEase._default}s=l(s);var E=g(z,s,y,u);var B=k.transit.enabled&&q.transition;var t=B?(parseInt(s,10)+parseInt(u,10)):0;if(t===0){var A=function(F){D.css(z);if(C){C.apply(D)}if(F){F()}};m(D,w,A);return D}var x={};var r=function(H){var G=false;var F=function(){if(G){D.unbind(f,F)}if(t>0){D.each(function(){this.style[q.transition]=(x[this]||null)})}if(typeof C==="function"){C.apply(D)}if(typeof H==="function"){H()}};if((t>0)&&(f)&&(k.transit.useTransitionEnd)){G=true;D.bind(f,F)}else{window.setTimeout(F,t)}D.each(function(){if(t>0){this.style[q.transition]=E}k(this).css(z)})};var v=function(F){this.offsetWidth;r(F)};m(D,w,v);return this};function n(s,r){if(!r){k.cssNumber[s]=true}k.transit.propertyMap[s]=q.transform;k.cssHooks[s]={get:function(v){var u=k(v).css("transit:transform");return u.get(s)},set:function(v,w){var u=k(v).css("transit:transform");u.setFromString(s,w);k(v).css({"transit:transform":u})}}}function c(r){return r.replace(/([A-Z])/g,function(s){return"-"+s.toLowerCase()})}function o(s,r){if((typeof s==="string")&&(!s.match(/^[\-0-9\.]+$/))){return s}else{return""+s+r}}function l(s){var r=s;if(k.fx.speeds[r]){r=k.fx.speeds[r]}return o(r,"ms")}k.transit.getTransitionValue=g})(jQuery);




  // ========================= smartresize ===============================

  /*
   * smartresize: debounced resize event for jQuery
   *
   * latest version and complete README available on Github:
   * https://github.com/louisremi/jquery.smartresize.js
   *
   * Copyright 2011 @louis_remi
   * Licensed under the MIT license.
   */

  var $event = $.event,
      dispatchMethod = $.event.handle ? 'handle' : 'dispatch',
      resizeTimeout;

  $event.special.smartresize = {
    setup: function() {
      $(this).bind( "resize", $event.special.smartresize.handler );
    },
    teardown: function() {
      $(this).unbind( "resize", $event.special.smartresize.handler );
    },
    handler: function( event, execAsap ) {
      // Save the context
      var context = this,
          args = arguments;

      // set correct event type
      event.type = "smartresize";

      if ( resizeTimeout ) { clearTimeout( resizeTimeout ); }
      resizeTimeout = setTimeout(function() {
        $event[ dispatchMethod ].apply( context, args );
      }, execAsap === "execAsap"? 0 : 100 );
    }
  };

  $.fn.smartresize = function( fn ) {
    return fn ? this.bind( "smartresize", fn ) : this.trigger( "smartresize", ["execAsap"] );
  };


/***************** Global vars ******************/
var $standAnimatedColTimeout = [];
var $animatedSVGIconTimeout = [];
var $svg_icons = [];
var $nectarCustomSliderRotate;

/***************** Smooth Scrolling ******************/

	function niceScrollInit(){
		if(!$().niceScroll) { return; }
		
		$("html").niceScroll({
			scrollspeed: 60,
			mousescrollstep: 40,
			cursorwidth: 15,
			cursorborder: 0,
			cursorcolor: '#303030',
			cursorborderradius: 6,
			autohidemode: false,
			horizrailenabled: false
		});
		
		
		if($('#boxed').length == 0){
			$('body, body #header-outer, body #header-secondary-outer, body #search-outer').css('padding-right','16px');
		} else if($('body[data-ext-responsive="true"]').length == 0 ) {
			$('body').css('padding-right','16px');
		}
		
		$('html').addClass('no-overflow-y');
	}

	var $smoothActive = $('body').attr('data-smooth-scrolling'); 
	var $smoothCache = ( $smoothActive == 1 ) ? true : false;
	
	if( $smoothActive == 1 && $(window).width() > 690 && $('body').outerHeight(true) > $(window).height() && Modernizr.csstransforms3d && !navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini)/)){ niceScrollInit(); } else {
		$('body').attr('data-smooth-scrolling','0');
	}
	
	//chrome ss
	if($smoothCache == false && $('body.material').length == 0 && navigator.platform.toUpperCase().indexOf('MAC') === -1 && !navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini)/) && $(window).width() > 690 && $('#nectar_fullscreen_rows').length == 0) {
		!function(){function e(){var e=!1;e&&c("keydown",r),v.keyboardSupport&&!e&&u("keydown",r)}function t(){if(document.body){var t=document.body,n=document.documentElement,o=window.innerHeight,r=t.scrollHeight;if(S=document.compatMode.indexOf("CSS")>=0?n:t,w=t,e(),x=!0,top!=self)y=!0;else if(r>o&&(t.offsetHeight<=o||n.offsetHeight<=o)){var a=!1,i=function(){a||n.scrollHeight==document.height||(a=!0,setTimeout(function(){n.style.height=document.height+"px",a=!1},500))};if(n.style.height="auto",setTimeout(i,10),S.offsetHeight<=o){var l=document.createElement("div");l.style.clear="both",t.appendChild(l)}}v.fixedBackground||b||(t.style.backgroundAttachment="scroll",n.style.backgroundAttachment="scroll")}}function n(e,t,n,o){if(o||(o=1e3),d(t,n),1!=v.accelerationMax){var r=+new Date,a=r-C;if(a<v.accelerationDelta){var i=(1+30/a)/2;i>1&&(i=Math.min(i,v.accelerationMax),t*=i,n*=i)}C=+new Date}if(M.push({x:t,y:n,lastX:0>t?.99:-.99,lastY:0>n?.99:-.99,start:+new Date}),!T){var l=e===document.body,u=function(){for(var r=+new Date,a=0,i=0,c=0;c<M.length;c++){var s=M[c],d=r-s.start,f=d>=v.animationTime,h=f?1:d/v.animationTime;v.pulseAlgorithm&&(h=p(h));var m=s.x*h-s.lastX>>0,w=s.y*h-s.lastY>>0;a+=m,i+=w,s.lastX+=m,s.lastY+=w,f&&(M.splice(c,1),c--)}l?window.scrollBy(a,i):(a&&(e.scrollLeft+=a),i&&(e.scrollTop+=i)),t||n||(M=[]),M.length?N(u,e,o/v.frameRate+1):T=!1};N(u,e,0),T=!0}}function o(e){x||t();var o=e.target,r=l(o);if(!r||e.defaultPrevented||s(w,"embed")||s(o,"embed")&&/\.pdf/i.test(o.src))return!0;var a=e.wheelDeltaX||0,i=e.wheelDeltaY||0;return a||i||(i=e.wheelDelta||0),!v.touchpadSupport&&f(i)?!0:(Math.abs(a)>1.2&&(a*=v.stepSize/120),Math.abs(i)>1.2&&(i*=v.stepSize/120),n(r,-a,-i),void e.preventDefault())}function r(e){var t=e.target,o=e.ctrlKey||e.altKey||e.metaKey||e.shiftKey&&e.keyCode!==H.spacebar;if(/input|textarea|select|embed/i.test(t.nodeName)||t.isContentEditable||e.defaultPrevented||o)return!0;if(s(t,"button")&&e.keyCode===H.spacebar)return!0;var r,a=0,i=0,u=l(w),c=u.clientHeight;switch(u==document.body&&(c=window.innerHeight),e.keyCode){case H.up:i=-v.arrowScroll;break;case H.down:i=v.arrowScroll;break;case H.spacebar:r=e.shiftKey?1:-1,i=-r*c*.9;break;case H.pageup:i=.9*-c;break;case H.pagedown:i=.9*c;break;case H.home:i=-u.scrollTop;break;case H.end:var d=u.scrollHeight-u.scrollTop-c;i=d>0?d+10:0;break;case H.left:a=-v.arrowScroll;break;case H.right:a=v.arrowScroll;break;default:return!0}n(u,a,i),e.preventDefault()}function a(e){w=e.target}function i(e,t){for(var n=e.length;n--;)E[A(e[n])]=t;return t}function l(e){var t=[],n=S.scrollHeight;do{var o=E[A(e)];if(o)return i(t,o);if(t.push(e),n===e.scrollHeight){if(!y||S.clientHeight+10<n)return i(t,document.body)}else if(e.clientHeight+10<e.scrollHeight&&(overflow=getComputedStyle(e,"").getPropertyValue("overflow-y"),"scroll"===overflow||"auto"===overflow))return i(t,e)}while(e=e.parentNode)}function u(e,t,n){window.addEventListener(e,t,n||!1)}function c(e,t,n){window.removeEventListener(e,t,n||!1)}function s(e,t){return(e.nodeName||"").toLowerCase()===t.toLowerCase()}function d(e,t){e=e>0?1:-1,t=t>0?1:-1,(k.x!==e||k.y!==t)&&(k.x=e,k.y=t,M=[],C=0)}function f(e){if(e){e=Math.abs(e),D.push(e),D.shift(),clearTimeout(z);var t=h(D[0],120)&&h(D[1],120)&&h(D[2],120);return!t}}function h(e,t){return Math.floor(e/t)==e/t}function m(e){var t,n,o;return e*=v.pulseScale,1>e?t=e-(1-Math.exp(-e)):(n=Math.exp(-1),e-=1,o=1-Math.exp(-e),t=n+o*(1-n)),t*v.pulseNormalize}function p(e){return e>=1?1:0>=e?0:(1==v.pulseNormalize&&(v.pulseNormalize/=m(1)),m(e))}var w,g={frameRate:150,animationTime:500,stepSize:120,pulseAlgorithm:!0,pulseScale:8,pulseNormalize:1,accelerationDelta:20,accelerationMax:1,keyboardSupport:!0,arrowScroll:50,touchpadSupport:!0,fixedBackground:!0,excluded:""},v=g,b=!1,y=!1,k={x:0,y:0},x=!1,S=document.documentElement,D=[120,120,120],H={left:37,up:38,right:39,down:40,spacebar:32,pageup:33,pagedown:34,end:35,home:36},v=g,M=[],T=!1,C=+new Date,E={};setInterval(function(){E={}},1e4);var z,A=function(){var e=0;return function(t){return t.uniqueID||(t.uniqueID=e++)}}(),N=function(){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||function(e,t,n){window.setTimeout(e,n||1e3/60)}}(),K=/chrome/i.test(window.navigator.userAgent),L=null;"onwheel"in document.createElement("div")?L="wheel":"onmousewheel"in document.createElement("div")&&(L="mousewheel"),L&&K&&(u(L,o),u("mousedown",a),u("load",t))}();
	}



/****************split line text******************/
if($('.nectar-recent-posts-single_featured.multiple_featured').length > 0) {
  splitLineText();
}
	
/****************full page******************/
var $usingFullScreenRows = false;
var $fullscreenSelector = '';
var $disableFPonMobile = ($('#nectar_fullscreen_rows[data-mobile-disable]').length > 0) ? $('#nectar_fullscreen_rows').attr('data-mobile-disable') : 'off';
var $onMobileBrowser = navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/);

if(!$onMobileBrowser) {
	$disableFPonMobile = 'off';
}


	
/*-------------------------------------------------------------------------*/
/*	2.	Helper Functions
/*-------------------------------------------------------------------------*/

	jQuery.fn.setCursorPosition = function(position){
	    if(this.lengh == 0) return this;
	    return $(this).setSelection(position, position);
	}
	
	jQuery.fn.setSelection = function(selectionStart, selectionEnd) {
	    if(this.lengh == 0) return this;
	    input = this[0];
	
	    if (input.createTextRange) {
	        var range = input.createTextRange();
	        range.collapse(true);
	        range.moveEnd('character', selectionEnd);
	        range.moveStart('character', selectionStart);
	        range.select();
	    } else if (input.setSelectionRange) {
	        input.focus();
	        input.setSelectionRange(selectionStart, selectionEnd);
	    }
	
	    return this;
	}
	
	

	$.extend($.expr[':'], {
	    transparent: function(elem, i, attr){
	      return( $(elem).css("opacity") === "0" );
	    }
	});
	

	function getQueryParams(qs) {
	    qs = qs.split("+").join(" ");
	    var params = {},
	        tokens,
	        re = /[?&]?([^=]+)=([^&]*)/g;

	    while (tokens = re.exec(qs)) {
	        params[decodeURIComponent(tokens[1])]
	            = decodeURIComponent(tokens[2]);
	    }

	    return params;
	}

	var $_GET = getQueryParams(document.location.search);

	
	//count
	var CountUp = function(target, startVal, endVal, decimals, duration, options) {

    // make sure requestAnimationFrame and cancelAnimationFrame are defined
    // polyfill for browsers without native support
    // by Opera engineer Erik Möller
    var lastTime = 0;
    var vendors = ['webkit', 'moz', 'ms', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
          window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }
    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
    }

    var self = this;

     // default options
    self.options = {
        useEasing : true, // toggle easing
        useGrouping : true, // 1,000,000 vs 1000000
        separator : ',', // character to use as a separator
        decimal : '.', // character to use as a decimal
        easingFn: null, // optional custom easing closure function, default is Robert Penner's easeOutExpo
        formattingFn: null // optional custom formatting function, default is self.formatNumber below
    };
    // extend default options with passed options object
    for (var key in options) {
        if (options.hasOwnProperty(key)) {
            self.options[key] = options[key];
        }
    }
    if (self.options.separator === '') { self.options.useGrouping = false; }
    if (!self.options.prefix) self.options.prefix = '';
    if (!self.options.suffix) self.options.suffix = '';

    self.d = (typeof target === 'string') ? document.getElementById(target) : target;
    self.startVal = Number(startVal);
    self.endVal = Number(endVal);
    self.countDown = (self.startVal > self.endVal);
    self.frameVal = self.startVal;
    self.decimals = Math.max(0, decimals || 0);
    self.dec = Math.pow(10, self.decimals);
    self.duration = Number(duration) * 1000 || 2000;

    self.formatNumber = function(nStr) {
        nStr = nStr.toFixed(self.decimals);
        nStr += '';
        var x, x1, x2, rgx;
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? self.options.decimal + x[1] : '';
        rgx = /(\d+)(\d{3})/;
        if (self.options.useGrouping) {
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + self.options.separator + '$2');
            }
        }
        return self.options.prefix + x1 + x2 + self.options.suffix;
    };
    // Robert Penner's easeOutExpo
    self.easeOutExpo = function(t, b, c, d) {
        return c * (-Math.pow(2, -10 * t / d) + 1) * 1024 / 1023 + b;
    };

    self.easingFn = self.options.easingFn ? self.options.easingFn : self.easeOutExpo;
    self.formattingFn = self.options.formattingFn ? self.options.formattingFn : self.formatNumber;

    self.version = function () { return '1.7.1'; };

    // Print value to target
    self.printValue = function(value) {
        var result = self.formattingFn(value);

        if (self.d.tagName === 'INPUT') {
            this.d.value = result;
        }
        else if (self.d.tagName === 'text' || self.d.tagName === 'tspan') {
            this.d.textContent = result;
        }
        else {
            this.d.innerHTML = result;
        }
    };

    self.count = function(timestamp) {

        if (!self.startTime) { self.startTime = timestamp; }

        self.timestamp = timestamp;
        var progress = timestamp - self.startTime;
        self.remaining = self.duration - progress;

        // to ease or not to ease
        if (self.options.useEasing) {
            if (self.countDown) {
                self.frameVal = self.startVal - self.easingFn(progress, 0, self.startVal - self.endVal, self.duration);
            } else {
                self.frameVal = self.easingFn(progress, self.startVal, self.endVal - self.startVal, self.duration);
            }
        } else {
            if (self.countDown) {
                self.frameVal = self.startVal - ((self.startVal - self.endVal) * (progress / self.duration));
            } else {
                self.frameVal = self.startVal + (self.endVal - self.startVal) * (progress / self.duration);
            }
        }

        // don't go past endVal since progress can exceed duration in the last frame
        if (self.countDown) {
            self.frameVal = (self.frameVal < self.endVal) ? self.endVal : self.frameVal;
        } else {
            self.frameVal = (self.frameVal > self.endVal) ? self.endVal : self.frameVal;
        }

        // decimal
        self.frameVal = Math.round(self.frameVal*self.dec)/self.dec;

        // format and print value
        self.printValue(self.frameVal);

        // whether to continue
        if (progress < self.duration) {
            self.rAF = requestAnimationFrame(self.count);
        } else {
            if (self.callback) { self.callback(); }
        }
    };
    // start your animation
    self.start = function(callback) {
        self.callback = callback;
        self.rAF = requestAnimationFrame(self.count);
        return false;
    };
    // toggles pause/resume animation
    self.pauseResume = function() {
        if (!self.paused) {
            self.paused = true;
            cancelAnimationFrame(self.rAF);
        } else {
            self.paused = false;
            delete self.startTime;
            self.duration = self.remaining;
            self.startVal = self.frameVal;
            requestAnimationFrame(self.count);
        }
    };
    // reset to startVal so animation can be run again
    self.reset = function() {
        self.paused = false;
        delete self.startTime;
        self.startVal = startVal;
        cancelAnimationFrame(self.rAF);
        self.printValue(self.startVal);
    };
    // pass a new endVal and start animation
    self.update = function (newEndVal) {
        cancelAnimationFrame(self.rAF);
        self.paused = false;
        delete self.startTime;
        self.startVal = self.frameVal;
        self.endVal = Number(newEndVal);
        self.countDown = (self.startVal > self.endVal);
        self.rAF = requestAnimationFrame(self.count);
    };

    // format startVal on initialization
    self.printValue(self.startVal);
};
	
var easeOutCubic = function(t, b, c, d) {
    return c*((t=t/d-1)*t*t + 1) + b;
};	
	
	
/*-------------------------------------------------------------------------*/
/*	3.	Shortcode Stuff
/*-------------------------------------------------------------------------*/


function vcWaypoints() {
	$($fullscreenSelector+' .wpb_animate_when_almost_visible').each(function() {
		  
			var $vcOffsetPos = ($('#nectar_fullscreen_rows').length > 0) ? '200%' : '90%';

			var $that = $(this);
			var waypoint = new Waypoint({
	 			element: $that,
	 			 handler: function(direction) {

	 			 	if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('animated')) { 
					     waypoint.destroy();
					     return;
					}
	 				$that.addClass("animated");
	 				$that.addClass("wpb_start_animation");
	 				waypoint.destroy();
					
					if($that.is('.nectar-button') && $('body[data-button-style*="rounded_shadow"]').length > 0) {
						setTimeout(function(){
							$that.removeClass('wpb_start_animation');
						},1100);
					}
				
				},
				offset: $vcOffsetPos

			}); 

	});
}
			 
/***************** Button ******************/
	
	$.cssHooks.color = {
	    get: function(elem) {
	        if (elem.currentStyle)
	            var color = elem.currentStyle["color"];
	        else if (window.getComputedStyle)
	            var color = document.defaultView.getComputedStyle(elem,
	                null).getPropertyValue("color");
	        if (color.search("rgb") == -1)
	            return color;
	        else {
	            color = color.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	            function hex(x) {
	                return ("0" + parseInt(x).toString(16)).slice(-2);
	            }
	            if(color) {
	            	return "#" + hex(color[1]) + hex(color[2]) + hex(color[3]);
	            }
	        }
	    }
	}

	$.cssHooks.backgroundColor = {
	    get: function(elem) {
	        if (elem.currentStyle)
	            var bg = elem.currentStyle["backgroundColor"];
	        else if (window.getComputedStyle)
	            var bg = document.defaultView.getComputedStyle(elem,
	                null).getPropertyValue("background-color");
	        if (bg.search("rgb") == -1)
	            return bg;
	        else {
	            bg = bg.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	            function hex(x) {
	                return ("0" + parseInt(x).toString(16)).slice(-2);
	            }
	            if(bg) {
	            	return "#" + hex(bg[1]) + hex(bg[2]) + hex(bg[3]);
	            }
	        }
	    }
	}
	
	function shadeColor(hex, lum) {

	  // validate hex string
		hex = String(hex).replace(/[^0-9a-f]/gi, '');
		if (hex.length < 6) {
			hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
		}
		lum = lum || 0;

		// convert to decimal and change luminosity
		var rgb = "#", c, i;
		for (i = 0; i < 3; i++) {
			c = parseInt(hex.substr(i*2,2), 16);
			c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
			rgb += ("00"+c).substr(c.length);
		}

		return rgb;
	}

	//color
	function coloredButtons() {
		$('.nectar-button.see-through[data-color-override], .nectar-button.see-through-2[data-color-override], .nectar-button.see-through-3[data-color-override]').each(function(){
			
			var $usingMaterialSkin = ($('body.material[data-button-style^="rounded"]').length > 0) ? true : false;
			$(this).css('visibility','visible');
			
			if($(this).hasClass('see-through-3') && $(this).attr('data-color-override') == 'false') {
				return true;
			}
			//if($(this).attr('data-color-override') != 'false'){

				if($(this).attr('data-color-override') != 'false') {
					var $color = $(this).attr('data-color-override') ;
				} else {
					if($(this).parents('.dark').length > 0) 
						var $color = '#000000';
					else 
						var $color = '#ffffff';
				}

				if(!$(this).hasClass('see-through-3')) $(this).css('color',$color);
				$(this).find('i').css('color',$color);
				
				var colorInt = parseInt($color.substring(1),16);
				var $hoverColor = ($(this).has('[data-hover-color-override]')) ? $(this).attr('data-hover-color-override') : 'no-override';
				var $hoverTextColor = ($(this).has('[data-hover-text-color-override]')) ? $(this).attr('data-hover-text-color-override') : '#fff';
				
		   		var R = (colorInt & 0xFF0000) >> 16;
		    	var G = (colorInt & 0x00FF00) >> 8;
		   		var B = (colorInt & 0x0000FF) >> 0;
		   		
		   		$opacityStr = ($(this).hasClass('see-through-3')) ? '1': '0.75';

				$(this).css('border-color','rgba('+R+','+G+','+B+','+$opacityStr+')');
				
				//material buttons w/ icons starting
				if($usingMaterialSkin) {
					$(this).find('i').css({'background-color': 'rgba('+R+','+G+','+B+',1)', 'box-shadow': '0px 8px 15px rgba('+R+','+G+','+B+',0.24)'});
				}
				
				if($(this).hasClass('see-through')) {
					
					var $that = $(this);
					
					$(this).on('mouseenter touchstart', function(){
						$that.css('border-color','rgba('+R+','+G+','+B+',1)');
					});
					
					$(this).on('mouseleave touchtouchend', function(){
						$that.css('border-color','rgba('+R+','+G+','+B+',1)');
						$opacityStr = ($(this).hasClass('see-through-3')) ? '1': '0.75';
						$that.css('border-color','rgba('+R+','+G+','+B+','+$opacityStr+')');
					});
					
					
				} else {
					
					$(this).find('i').css('color', $hoverTextColor);
					
					if($hoverColor != 'no-override'){
						
						var $that = $(this);
						
						$(this).on('mouseenter touchstart', function(){
							
							$that.css({
								'border-color': $hoverColor,
								'background-color': $hoverColor,
								'color': $hoverTextColor
							});
							
							//material buttons w/ icons over
							if($usingMaterialSkin) {
								$that.find('i').css({'background-color': '', 'box-shadow': ''});
							}
							
						});
						
						$(this).on('mouseleave touchtouchend', function(){
								
								$opacityStr = ($(this).hasClass('see-through-3')) ? '1': '0.75';
								
								//material buttons w/ icons leave
								if($usingMaterialSkin) {
									$that.find('i').css({'background-color': 'rgba('+R+','+G+','+B+',1)', 'box-shadow': '0px 8px 15px rgba('+R+','+G+','+B+',0.24)'});
								}
								
								if(!$that.hasClass('see-through-3')) {
									$that.css({
										'border-color':'rgba('+R+','+G+','+B+','+$opacityStr+')',
										'background-color': 'transparent',
										'color': $color
									});
								} else {
									$that.css({
										'border-color':'rgba('+R+','+G+','+B+','+$opacityStr+')',
										'background-color': 'transparent'
									});
								}
							
						});
							

					
					} else {
						
						var $that = $(this);
						
						$(this).on('mouseenter touchstart', function(){
							
								$that.css({
									'border-color': $hoverColor,
									'color': $hoverTextColor
								});
								
						});
						
						$(this).on('mouseleave touchtouchend', function(){
							
								$opacityStr = ($that.hasClass('see-through-3')) ? '1': '0.75';
								$that.css({
									'border-color':'rgba('+R+','+G+','+B+','+$opacityStr+')',
									'color':  $hoverTextColor
								});
								
						});
							

					
					}
			//	}
			
			}
		});
		
		$('.nectar-button:not(.see-through):not(.see-through-2):not(.see-through-3)[data-color-override]').each(function(){
			
			$(this).css('visibility','visible');
			
			if($(this).attr('data-color-override') != 'false'){
				
				var $color = $(this).attr('data-color-override');
				$(this).removeClass('accent-color').removeClass('extra-color-1').removeClass('extra-color-2').removeClass('extra-color-3');
				$(this).css('background-color',$color);
				
			}
			
		});


		//solid color tilt 
		if($('.swiper-slide .solid_color_2').length > 0 || $('.tilt-button-inner').length > 0) {

			var $tiltButtonCssString = '';

			$('.swiper-slide .solid_color_2 a').each(function(i){
				
				$(this).addClass('instance-'+i);

				if($(this).attr('data-color-override') != 'false') {
					var $color = $(this).attr('data-color-override');
				} else {
					if($(this).parents('.dark').length > 0) 
						var $color = '#000000';
					else 
						var $color = '#ffffff';
				}

				$(this).css('color',$color);
				$(this).find('i').css('color',$color);
				
				var $currentColor = $(this).css('background-color');
				var $topColor = shadeColor($currentColor, 0.13);
				var $bottomColor = shadeColor($currentColor, -0.15);
	
				$tiltButtonCssString += '.swiper-slide .solid_color_2 a.instance-'+i + ':after { background-color: '+$topColor+';  }' + ' .swiper-slide .solid_color_2 a.instance-'+i + ':before { background-color: '+$bottomColor+'; } ';

			});


			$('.tilt-button-wrap a').each(function(i){
				
				$(this).addClass('instance-'+i);

				var $currentColor = $(this).css('background-color');

				if($(this).attr('data-color-override') != 'false') {
					var $color = $(this).attr('data-color-override');
					$(this).css('background-color',$color);
					$currentColor = $color;
				} 
			
				var $topColor = shadeColor($currentColor, 0.13);
				var $bottomColor = shadeColor($currentColor, -0.15);
	
				$tiltButtonCssString += '.tilt-button-wrap a.instance-'+i + ':after { background-color: '+$topColor+';  }' + ' .tilt-button-wrap a.instance-'+i + ':before { background-color: '+$bottomColor+'; } ';

			});

			var head = document.head || document.getElementsByTagName('head')[0];
   			var style = document.createElement('style');

   			style.type = 'text/css';
			if (style.styleSheet){
			  style.styleSheet.cssText = $tiltButtonCssString;
			} else {
			  style.appendChild(document.createTextNode($tiltButtonCssString));
			}

			head.appendChild(style);
		}


		//transparent 3d
		if($('.nectar-3d-transparent-button').length > 0) {

			var $3dTransButtonCssString = '';
			$('.nectar-3d-transparent-button').each(function(i){

				var $that = $(this);
				var $size = $that.attr('data-size');
				var $padding = 0;

		

				//size

				if($size == 'large') {
					$padding = 46;
					$font_size = 16;
				} else if($size == 'medium') {
					$padding = 30;
					$font_size = 16;
				} else if($size == 'small') {
					$padding = 20;
					$font_size = 12;
				} else if($size == 'jumbo') {
					$padding = 54;
					$font_size = 24;
				} else if($size == 'extra_jumbo') {
					$padding = 100;
					$font_size = 64;
				}

				$that.find('svg text').attr('font-size',$font_size);
				$boundingRect = $(this).find('.back-3d .button-text')[0].getBoundingClientRect();

				$text_width = $boundingRect.width;
				$text_height = $font_size*1.5;

				$extraMult = (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) ? 0 : 1;

				$that.css({'width': ($text_width+$padding*1.5)+'px','height': ($text_height+$padding)+'px'});
				$that.find('> a').css({'height': ($text_height+$padding)+'px'});

				$that.find('.back-3d svg, .front-3d svg').css({'width': ($text_width+$padding*1.5)+'px','height': ($text_height+$padding)+'px'}).attr('viewBox','0 0 '+ ($text_width+$padding) + ' ' + ($text_height+$padding));
				if($size == 'jumbo')
					$that.find('svg text').attr('transform','matrix(1 0 0 1 '+($text_width+$padding*1.5)/2 +' ' + (($text_height+$padding) / 1.68) +')');
				else if($size == 'extra_jumbo')
					$that.find('svg text').attr('transform','matrix(1 0 0 1 '+($text_width+$padding*1.6)/2 +' ' + (($text_height+$padding) / 1.6) +')');
				else if($size == 'large') {
					$that.find('svg text').attr('transform','matrix(1 0 0 1 '+($text_width+$padding*1.5)/2 +' ' + (($text_height+$padding) / 1.7) +')');
				}
				else {
					$that.find('svg text').attr('transform','matrix(1 0 0 1 '+($text_width+$padding*1.5)/2 +' ' + (($text_height+$padding) / 1.65) +')');
				}
				$that.find('.front-3d ').css('transform-origin','50% 50% -'+($text_height+$padding)/2+'px');
				$that.find('.back-3d').css('transform-origin','50% 50% -'+($text_height+$padding)/2+'px');

				//mask
				$(this).find('.front-3d svg > rect').attr('id','masked-rect-id-'+i);
				$(this).find('.front-3d defs mask').attr('id','button-text-mask-'+i);

				$that.css('visibility','visible');
				$3dTransButtonCssString+= '#masked-rect-id-'+i+' { mask: url(#button-text-mask-'+i+'); -webkit-mask: url(#button-text-mask-'+i+')} ';

			});

			//extra jumbo resize
			function createExtraJumboSize() {
				$('.nectar-3d-transparent-button').each(function(i){
					
					if($(this).css('visibility') != 'visible') return;

					var $that = $(this);
					var $size = $that.attr('data-size');
					if($size == 'extra_jumbo') {

						$extraMult = (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) ? 0 : 1;

						if(window.innerWidth < 1000 && window.innerWidth > 690) {
							$padding = 64;
							$font_size = 34;
							$that.find('.back-3d rect').attr('stroke-width','12');
							$vert_height_divider = 1.7;
						} else if(window.innerWidth <= 690 ) {
							$padding = 46;
							$font_size = 16;
							$that.find('.back-3d rect').attr('stroke-width','10');
							$vert_height_divider = 1.7;
						}  else {
							$padding = 100;
							$font_size = 64;
							$that.find('.back-3d rect').attr('stroke-width','20');
							$vert_height_divider = 1.6;
						}
			

						$that.find('svg text').attr('font-size',$font_size);

						$boundingRect = $(this).find('.back-3d .button-text')[0].getBoundingClientRect();
						$text_width = $boundingRect.width;
						$text_height = $font_size*1.5;

						$that.css({'width': ($text_width+$padding*1.5)+'px','height': ($text_height+$padding)+'px'});
						$that.find('> a').css({'height': ($text_height+$padding)+'px'});

						$that.find('.back-3d svg, .front-3d svg').css({'width': ($text_width+$padding*1.5)+'px','height': ($text_height+$padding)+'px'}).attr('viewBox','0 0 '+ ($text_width+$padding) + ' ' + ($text_height+$padding));

						$that.find('svg text').attr('transform','matrix(1 0 0 1 '+($text_width+$padding*1.6)/2 +' ' + (($text_height+$padding) / $vert_height_divider) +')');

						$that.find('.front-3d ').css('transform-origin','50% 50% -'+($text_height+$padding)/2+'px');
						$that.find('.back-3d').css('transform-origin','50% 50% -'+($text_height+$padding)/2+'px');

					}
				});
			}
			createExtraJumboSize();
			$(window).on('smartresize',createExtraJumboSize);

			var head = document.head || document.getElementsByTagName('head')[0];
				var style = document.createElement('style');

				style.type = 'text/css';
			if (style.styleSheet){
			  style.styleSheet.cssText = $3dTransButtonCssString;
			} else {
			  style.appendChild(document.createTextNode($3dTransButtonCssString));
			}

			head.appendChild(style);
		}

		//gradient btn init
		setTimeout(function(){
			$('.nectar-button.extra-color-gradient-1 .start, .nectar-button.extra-color-gradient-2 .start, .nectar-button.see-through-extra-color-gradient-1 .start, .nectar-button.see-through-extra-color-gradient-2 .start').removeClass('loading');
		},150);
		//no grad for ff
		if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1 || navigator.userAgent.indexOf("MSIE ") > -1 || navigator.userAgent.match(/Trident\/7\./)) {
			$('.nectar-button.extra-color-gradient-1, .nectar-button.extra-color-gradient-2, .nectar-button.see-through-extra-color-gradient-1, .nectar-button.see-through-extra-color-gradient-2').addClass('no-text-grad');
		}
	}	

	coloredButtons();


	//large icon hover
	function largeIconHover(){
		$('.icon-3x').each(function(){
			$(this).closest('.col').hover(function(){
				$(this).find('.icon-3x').addClass('hovered')
			},function(){
				$('.icon-3x').removeClass('hovered')
			});
		});

		//remove gradient from FF
		if( navigator.userAgent.indexOf("MSIE ") > -1 || navigator.userAgent.match(/Trident\/7\./))
			$('[class^="icon-"].extra-color-gradient-1, [class^="icon-"].extra-color-gradient-2, [class^="icon-"][data-color="extra-color-gradient-1"], [class^="icon-"][data-color="extra-color-gradient-2"], .nectar_icon_wrap[data-color*="extra-color-gradient"] .nectar_icon, .nectar-gradient-text').addClass('no-grad');
	}
	largeIconHover();


/***************** Column Hover BG ******************/

function columnBGColors() {	
	
	var $columnColorCSS = '';

	$('.wpb_column').each(function(i){

		$(this).addClass('instance-'+i);

		//bg color
		if($(this).attr('data-has-bg-color') == 'true') {
			if($(this).is('[data-animation*="reveal"]') && $(this).hasClass('has-animation')) {
				$columnColorCSS += '.wpb_column.instance-'+i+ ' > .column-bg-overlay { background-color:' + $(this).attr('data-bg-color') + ';  opacity: '+$(this).attr('data-bg-opacity')+'; }';
			}
			else {
				$columnColorCSS += '.wpb_column.instance-'+i+ ' > .column-bg-overlay{ background-color:' + $(this).attr('data-bg-color') + ';  opacity: '+$(this).attr('data-bg-opacity')+'; }';
			}
		}

		//hover bg color
		if($(this).is('[data-hover-bg^="#"]')) {
			if($(this).is('[data-animation*="reveal"]') && $(this).hasClass('has-animation')) {
				 $columnColorCSS += '.wpb_column.instance-'+i+ ':hover > .column-bg-overlay { background-color: '+$(this).attr('data-hover-bg') + '!important; opacity: '+$(this).attr('data-hover-bg-opacity')+'!important; }';
			}
			else {
	   		 $columnColorCSS += '.wpb_column.instance-'+i+ ':hover > .column-bg-overlay { background-color: '+$(this).attr('data-hover-bg') + '!important; opacity: '+$(this).attr('data-hover-bg-opacity')+'!important; }';
			}

		}
	});

	if($columnColorCSS.length > 1) {
		var head = document.head || document.getElementsByTagName('head')[0];
		var style = document.createElement('style');

		style.type = 'text/css';
		if (style.styleSheet){
		  style.styleSheet.cssText = $columnColorCSS;
		} else {
		  style.appendChild(document.createTextNode($columnColorCSS));
		}

		head.appendChild(style);
	}

}
columnBGColors();



/***************** svg icons *******************/

var $svg_icons = [];
function svgAnimations() {
	
	$svgOffsetPos = ($('#nectar_fullscreen_rows').length > 0) ? '200%' : 'bottom-in-view';

	if($svg_icons.length == 0) {

		$('.svg-icon-holder:not(.animated-in)').has('svg').each(function(i){
			var $that = $(this);

			if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|BlackBerry|Opera Mini)/)) $that.attr('data-animation','false');

			//size
			$that.find('svg').css({'height': parseInt($that.attr('data-size')) +'px', 'width': parseInt($that.attr('data-size')) +'px'});

			//animation
			$(this).find('svg').attr('id','nectar-svg-animation-instance-'+i);
			var $animationSpeed = ($that.is('[data-animation-speed]') && $that.attr('data-animation-speed').length > 0) ? $that.attr('data-animation-speed') : 200;
			if($that.attr('data-animation') == 'false') { 
				$animationSpeed = 1;
				$that.css('opacity','1');
			}

			if(!$that.hasClass('bound')) {
				$svg_icons[i] = new Vivus($that.find('svg').attr('id'), {type: 'delayed', pathTimingFunction: Vivus.EASE_OUT, animTimingFunction: Vivus.LINEAR, duration: $animationSpeed, onReady: svgInit });
			}

			if($animationSpeed !== 1) {

				var $that = $(this);
				var waypoint = new Waypoint({
		 			element: $that,
		 			 handler: function(direction) {
		 			 	if( $that.hasClass('animated-in')) { 
						     waypoint.destroy();
						     return;
						}

		 			 	checkIfReady();
						$that.addClass('animated-in');
						waypoint.destroy();
					},
					offset: $svgOffsetPos

				}); 

			} else {
				checkIfReady();
			}

			function checkIfReady() {
				var $animationDelay = ($that.is('[data-animation-delay]') && $that.attr('data-animation-delay').length > 0 && $that.attr('data-animation') != 'false') ? $that.attr('data-animation-delay') : 0;
				
				var $iconID = $that.find('svg').attr('id').replace(/[^0-9]/g,'');

				if($svg_icons[$iconID].isReady == true) {
					 setTimeout(function(){ $that.css('opacity','1'); $svg_icons[$iconID].reset().play(); },$animationDelay);
				} else {
					setTimeout(checkIfReady,50);
				}
			}

			function svgInit() {

				//set size
				$that.css({'height': parseInt($that.attr('data-size')) +'px', 'width': parseInt($that.attr('data-size')) +'px'});

				//var $iconID = $that.find('svg').attr('id');
				//$iconID = $iconID.replace(/[^0-9]/g,'');
				
				//stop animation until user scrolls to it
				//svg_icons[$iconID].reset().stop();

				//set color
				//var svgDoc = $that.find('object')[0].contentDocument;

				//var styleElement = svgDoc.createElementNS("http://www.w3.org/2000/svg", "style");
				//styleElement.textContent = "svg, svg path { stroke: "+$that.css('color')+"; fill: none; }"; 
				//svgDoc.getElementById("Layer_1").appendChild(styleElement);

			}

			$that.addClass('bound');

		});	
	} else {
		$('.svg-icon-holder').addClass('animated-in').css('opacity','1');
	}
	
	//full vc row support
	$('#nectar_fullscreen_rows .svg-icon-holder.animated-in').has('svg').each(function(i){
		
		var $animationDelay = ($(this).is('[data-animation-delay]') && $(this).attr('data-animation-delay').length > 0 && $(this).attr('data-animation') != 'false') ? $(this).attr('data-animation-delay') : 0;
		var $that = $(this);

		var $iconID = $that.find('svg').attr('id').replace(/[^0-9]/g,'');

		clearTimeout($animatedSVGIconTimeout[i]);

		if($that.attr('data-animation') == 'false') { 
			$animationSpeed = 1;
			$that.css('opacity','1');
			$svg_icons[$iconID].finish();
		} else {
			if($(this).parents('.active').length > 0 || $(this).parents('#footer-outer').length > 0 || $('body.mobile').length > 0) {
				$svg_icons[$iconID].reset();
				$animatedSVGIconTimeout[i] = setTimeout(function(){ $svg_icons[$iconID].play(); },$animationDelay);
			}

			else {
				$svg_icons[$iconID].reset().stop();
			}
		}
	});
}
//svg in equal height column resize
if($('.vc_row-o-equal-height .svg-icon-holder[data-animation="true"]').length > 0 && $('#nectar_fullscreen_rows').length == 0) {
	 $(window).on('smartresize', function(){
	    clearTimeout($svgResizeTimeout);
	    $svgResizeTimeout = setTimeout(function(){ 

	    	if($svg_icons.length > 0) {
	        	$('.svg-icon-holder.animated-in').each(function(i){
					$(this).css('opacity','1');
					var $iconID = $(this).attr('id').replace(/[^0-9]/g,'');
					$svg_icons[$iconID].finish();
				});
	        }

	     },300);
	});
}


/***************** PARALLAX SECTIONS ******************/
	
	


	var $window = $(window);
	var windowHeight = $window.height();

		
	$window.unbind('scroll.parallaxSections').unbind('resize.parallaxSections');
	$window.unbind('resize.parallaxSectionsUpdateHeight');
	$window.unbind('load.parallaxSectionsOffsetL');
	$window.unbind('resize.parallaxSectionsOffsetR');

	$window.on('resize.parallaxSectionsUpdateHeight',psUpdateWindowHeight);

	function psUpdateWindowHeight() {
		windowHeight = $window.height();
	}

	function psUpdateOffset($this) {
		$this.each(function(){
	  	    firstTop = $this.offset().top;
		});
	}
	
	$.fn.parallaxScroll = function(xpos, speedFactor, outerHeight) {
		var $this = $(this);
		var getHeight;
		var firstTop;
		var paddingTop = 0;
		
		var $windowDOMWidth = window.innerWidth, $windowDOMHeight = window.innerHeight;
		var $orientationChange = 0;
		
		//get the starting position of each element to have parallax applied to it		
		$this.each(function(){
		    firstTop = $this.offset().top;
		});
		
		
		$window.on('resize.parallaxSectionsOffsetR',psUpdateOffset($this));
		$window.on('load.parallaxSectionsOffsetL',psUpdateOffset($this));
	
		getHeight = function(jqo) {
			return jqo.outerHeight(true);
		};
		 
			
		// setup defaults if arguments aren't specified
		if (arguments.length < 1 || xpos === null) xpos = "50%";
		if (arguments.length < 2 || speedFactor === null) speedFactor = 0.25;
		if (arguments.length < 3 || outerHeight === null) outerHeight = true;
		
		// function to be called whenever the window is scrolled or resized

		var $element, top, height;
		
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");
		var $onMobileBrowser = navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|BlackBerry|Opera Mini)/);		
		
		var $toTransformOrNot = ($smoothCache == true) ? true : false;
		
		if(!$toTransformOrNot) {
			$this.find('.row-bg.using-image, .page-header-bg-image, .image-bg, .video-wrap').addClass('translate');
		}
		
		var $ifFast = 0;
		$element = $this;
		
		height = getHeight($element);
		
		var classic_mobile_menu_open = false;
		
		setInterval(function(){
			height = getHeight($element);
			
			classic_mobile_menu_open = ( $('body.classic_mobile_menu_open.mobile').length > 0 ) ? true : false;
			
		},600);
		
		var firstSection = ( ($element.parents('.top-level').length > 0 && $element.parents('.parallax_slider_outer').length > 0) || ($element.parents('.top-level').length > 0 && $element.is('.nectar-recent-posts-single_featured') ) || $element.is('.wpb_row.top-level') || $('.wpb_row').length == 0) ? true : false;
		
		//for first nectar slider shortcode without page builder or portfolio first
		if( $('.wpb_row').length == 0 && $element.parents('.parallax_slider_outer').length > 0 && $element.is('[data-full-width="true"]') ||
	      ($('#portfolio-extra').length > 0 && $element.parents('.parallax_slider_outer').length > 0 && $element.parents('.wpb_row').length > 0 && $element.parents('.wpb_row').index() == '0') ) {
			firstSection = true;
		}
		
		var nectarSliderElBool = $this.is('.nectar-slider-wrap');
		var pageHeaderBool = ($this.find('.page-header-bg-image').length > 0) ? true : false;
		
		var $elToParallax = false;
		
		if(nectarSliderElBool) {
			
				if($this.find('.video-wrap').length > 0 || $this.find('.image-bg').length > 0) {
					$elToParallax = $this.find('.video-wrap, .image-bg');
				} 
		}
		else {
			
				if($this.find('.row-bg.using-image').length > 0) {
					
					$elToParallax = $this.find('.row-bg.using-image');
					
				} else if($this.find('.page-header-bg-image').length > 0) {
					
					$elToParallax = $this.find('.page-header-bg-image');
				}
				
		}
		

		function update(){
			
				firstTop = $element.offset().top;
				
				// Check if totally above or totally below viewport
				if ($elToParallax == false || firstTop + height < nectarDOMInfo.scrollTop || firstTop > nectarDOMInfo.scrollTop + windowHeight || $('body.material-ocm-open').length > 0) {
					
				} else {

		        //for IE, Safari or any setup using the styled scrollbar default to animating the BG pos
		        if ($toTransformOrNot) {
		        	$this.find('.row-bg.using-image').css('backgroundPosition', xpos + " " + Math.round((firstTop - nectarDOMInfo.scrollTop) * speedFactor) + "px");
		        }
		       	//for Firefox/Chrome use a higher performing method
		        else  {
							
							//nectar slider
							if(nectarSliderElBool) {
									
								 if(firstSection) {
									 //top level row
 										if(!classic_mobile_menu_open) {
									 		$this.find('.video-wrap, .image-bg').css({ 'transform':  'translate3d(0, ' + parseFloat(nectarDOMInfo.scrollTop * speedFactor)  + 'px, 0)' }); 
									  }
								 } else {
									 	$this.find('.video-wrap, .image-bg').css({ 'transform':  'translate3d(0, ' + parseFloat((($windowDOMHeight + nectarDOMInfo.scrollTop -  firstTop) * speedFactor) ) + 'px, 0)' }); 
								 }
								
							} else {
								
								//rows
								if(firstSection) {
									//top level row
									if(!classic_mobile_menu_open) {
		        				$elToParallax.css({ 'transform':  'translate3d(0, ' + parseFloat(nectarDOMInfo.scrollTop * speedFactor)  + 'px, 0)' });
									}
								} else {
									//regular rows
									$elToParallax.css({ 'transform':  'translate3d(0, ' + parseFloat((($windowDOMHeight + nectarDOMInfo.scrollTop -  firstTop) * speedFactor) ) + 'px, 0), scale(1.005)' });
								}
								
								//page header
								if(pageHeaderBool && !classic_mobile_menu_open) {
									$elToParallax.css({ 'transform':  'translate3d(0, ' + parseFloat(nectarDOMInfo.scrollTop * speedFactor) + 'px, 0)' });
								}
							
							}
							
						} 
				
				}
				

			//if on mobile, auto RAF
			if($onMobileBrowser){
				requestAnimationFrame(update); 
			}
			
		}		

		if (window.addEventListener) {
			
			if(!navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|BlackBerry|Opera Mini)/)){
					 window.addEventListener('scroll', function(){ 
			          requestAnimationFrame(update); 
			     }, false);
					 
					 $(window).resize(function(){
						 $windowDOMWidth = window.innerWidth;
						 $windowDOMHeight = window.innerHeight;
					 });
					 
			 } else {
				 	//if on mobile, auto RAF
					requestAnimationFrame(update); 
					

					window.addEventListener("orientationchange", function() {
						 $orientationChange = 1;
					});
					
					$(window).resize(function(){
							if( ($(window).width() != $windowDOMWidth && $(window).height != $windowDOMHeight) || $orientationChange == 1){

									//store the current window dimensions
									$windowDOMWidth = window.innerWidth;
									$windowDOMHeight = window.innerHeight;
									
									$orientationChange = 0;
							}
					});
					 
			 }
			 
		}

		$window.on('resize.parallaxSections',update);

		update();
	};

 
 


	
/***************** Full Width Section ******************/

	//nectar slider in container but fullwidth fix
	$('.wpb_row .vc_col-sm-12 .nectar-slider-wrap[data-full-width="true"]').each(function(){
		if($(this).parents('.wpb_row.full-width-section').length == 0 && $(this).parents('.wpb_row.full-width-content').length == 0) {
			$(this).parents('.wpb_row').addClass('full-width-section');
		}
	});

	function fullWidthSections(){

		var $windowInnerWidth = window.innerWidth;
		var $scrollBar = ($('#ascrail2000').length > 0 && $windowInnerWidth > 1000) ? -13 : 0;
		var $bodyBorderWidth = ($('.body-border-right').length > 0 && $windowInnerWidth > 1000) ? parseInt($('.body-border-right').width())*2 : 0;
		
		if($('#boxed').length == 1){
			$ustOutOfSight = ((parseInt($('.container-wrap').width()) - parseInt($('.main-content').width())) / 2) + 4;
		} else {
			
			//if the ext responsive mode is on - add the extra padding into the calcs
			var $extResponsivePadding = ($('body[data-ext-responsive="true"]').length > 0 && $windowInnerWidth >= 1000) ? 180 : 0;
			var $leftHeaderSize = ($('#header-outer[data-format="left-header"]').length > 0 && $windowInnerWidth >= 1000) ? parseInt($('#header-outer[data-format="left-header"]').width()) : 0;
			if($(window).width() - $leftHeaderSize - $bodyBorderWidth  <= parseInt($('.main-content').css('max-width'))) { 
				var $windowWidth = parseInt($('.main-content').css('max-width'));

				//no need for the scrollbar calcs with ext responsive on desktop views
				if($extResponsivePadding == 180) $windowWidth = $windowWidth - $scrollBar;

			} else { 
				var $windowWidth = $(window).width() - $leftHeaderSize - $bodyBorderWidth;
			}

			
			$contentWidth = parseInt($('.main-content').css('max-width'));

			//single post fullwidth
			if($('body.single-post[data-ext-responsive="true"]').length > 0 && $('.container-wrap.no-sidebar').length > 0 ) {
				$contentWidth = $('.post-area').width();
				$extResponsivePadding = 0;
			}
			
			$ustOutOfSight = Math.ceil( (($windowWidth + $extResponsivePadding + $scrollBar - $contentWidth) / 2) )
		}
		
	    //full width content sections
	    $('.carousel-outer').has('.carousel-wrap[data-full-width="true"]').css('overflow','visible');
	    
	    $('.carousel-wrap[data-full-width="true"], .portfolio-items[data-col-num="elastic"]:not(.fullwidth-constrained), .full-width-content').each(function(){
			
			var $leftHeaderSize = ($('#header-outer[data-format="left-header"]').length > 0 && $windowInnerWidth >= 1000) ? parseInt($('#header-outer[data-format="left-header"]').width()) : 0;
			var $bodyBorderWidth = ($('.body-border-right').length > 0 && $windowInnerWidth > 1000) ? (parseInt($('.body-border-right').width())*2) - 2 : 0;

			//single post fullwidth
			if($('#boxed').length == 1){

				var $mainContentWidth = ($('#nectar_fullscreen_rows').length == 0) ? parseInt($('.main-content').width()) : parseInt($(this).parents('.container').width());

				if($('body.single-post[data-ext-responsive="true"]').length > 0 && $('.container-wrap.no-sidebar').length > 0 && $(this).parents('.post-area').length > 0) {
					$contentWidth = $('.post-area').width();
					$extResponsivePadding = 0;
					$windowWidth = $(window).width() - $bodyBorderWidth;
					$ustOutOfSight = Math.ceil( (($windowWidth + $extResponsivePadding + $scrollBar - $contentWidth) / 2) )
				} else {
					if($(this).parents('.page-submenu').length > 0)
						$ustOutOfSight = ((parseInt($('.container-wrap').width()) - $mainContentWidth) / 2);
					else 
						$ustOutOfSight = ((parseInt($('.container-wrap').width()) - $mainContentWidth) / 2) + 4;
				}
			} else {
				if($('body.single-post[data-ext-responsive="true"]').length > 0 && $('.container-wrap.no-sidebar').length > 0 && $(this).parents('.post-area').length > 0) {
					$contentWidth = $('.post-area').width();
					$extResponsivePadding = 0;
					$windowWidth = $(window).width() - $leftHeaderSize - $bodyBorderWidth;
				} else {

					var $mainContentMaxWidth = ($('#nectar_fullscreen_rows').length == 0) ? parseInt($('.main-content').css('max-width')) : parseInt($(this).parents('.container').css('max-width'));

					//when using gutter on portfolio don't add extra space for scroll bar
			    	if($('#boxed').length == 0 && $(this).hasClass('portfolio-items') && $(this).is('[data-gutter*="px"]') && $(this).attr('data-gutter').length > 0 && $(this).attr('data-gutter') != 'none') {
			    		$scrollBar = ($('#ascrail2000').length > 0 && $windowInnerWidth > 1000) ? -13 : 0;
			    	}

					if($(window).width() - $leftHeaderSize - $bodyBorderWidth <= $mainContentMaxWidth) { 
						$windowWidth = $mainContentMaxWidth;
						//no need for the scrollbar calcs with ext responsive on desktop views
						if($extResponsivePadding == 180) $windowWidth = $windowWidth - $scrollBar;
					}
					$contentWidth = $mainContentMaxWidth;
					$extResponsivePadding = ($('body[data-ext-responsive="true"]').length > 0 && window.innerWidth >= 1000) ? 180 : 0;
					if($leftHeaderSize > 0) $extResponsivePadding = ($('body[data-ext-responsive="true"]').length > 0 && window.innerWidth >= 1000) ? 120 : 0;
				}

				$ustOutOfSight = Math.ceil( (($windowWidth + $extResponsivePadding + $scrollBar - $contentWidth) / 2) )
			}

			$extraSpace = 0;
			if( $(this).hasClass('carousel-wrap')) $extraSpace = 1;
			if( $(this).hasClass('portfolio-items')) $extraSpace = 5;
			
	    	$carouselWidth = ($('#boxed').length == 1) ? $mainContentWidth + parseInt($ustOutOfSight*2) : $(window).width() - $leftHeaderSize - $bodyBorderWidth +$extraSpace  + $scrollBar ;

	    	//when using gutter on portfolio don't add extra space
	    	if($('#boxed').length == 0 && $(this).hasClass('portfolio-items') && $(this).is('[data-gutter*="px"]') && $(this).attr('data-gutter').length > 0 && $(this).attr('data-gutter') != 'none') {
	    		if($(window).width() > 1000)
	    			$carouselWidth = $(window).width() - $leftHeaderSize - $bodyBorderWidth + $scrollBar + 3
	    		else 
	    			$carouselWidth = $(window).width() - $leftHeaderSize - $bodyBorderWidth + $scrollBar 
	    	}

	    	if($(this).parent().hasClass('default-style')) { 

	    		var $mainContentWidth = ($('#nectar_fullscreen_rows').length == 0) ? parseInt($('.main-content').width()) : parseInt($(this).parents('.container').width());
	    		
	    		if($('#boxed').length != 0) {
	    			$carouselWidth = ($('#boxed').length == 1) ? $mainContentWidth + parseInt($ustOutOfSight*2) : $(window).width() - $leftHeaderSize + $extraSpace + $scrollBar ;
				}
				else {
					$carouselWidth = ($('#boxed').length == 1) ? $mainContentWidth + parseInt($ustOutOfSight*2) : ($(window).width() - $leftHeaderSize - $bodyBorderWidth) - (($(window).width()- $leftHeaderSize - $bodyBorderWidth)*.025) + $extraSpace + $scrollBar ;
					$windowWidth = ($(window).width() - $leftHeaderSize - $bodyBorderWidth <= $mainContentWidth) ? $mainContentWidth : ($(window).width() - $leftHeaderSize - $bodyBorderWidth) - (($(window).width()- $leftHeaderSize - $bodyBorderWidth)*.025);
					$ustOutOfSight = Math.ceil( (($windowWidth + $scrollBar - $mainContentWidth) / 2) )
				}
			}

			else if($(this).parent().hasClass('spaced')) { 

				var $mainContentWidth = ($('#nectar_fullscreen_rows').length == 0) ? parseInt($('.main-content').width()) : parseInt($(this).parents('.container').width());

				if($('#boxed').length != 0) {
	    			$carouselWidth = ($('#boxed').length == 1) ? $mainContentWidth + parseInt($ustOutOfSight*2) - ($(window).width()*.02) : $(window).width() + $extraSpace + $scrollBar ;
				} else {
					$carouselWidth = ($('#boxed').length == 1) ? $mainContentWidth + parseInt($ustOutOfSight*2) : ($(window).width()- $leftHeaderSize - $bodyBorderWidth)  - Math.ceil(($(window).width()- $leftHeaderSize - $bodyBorderWidth)*.02) + $extraSpace + $scrollBar ;
					var $windowWidth2 = ($(window).width() - $leftHeaderSize - $bodyBorderWidth <= $mainContentWidth) ? $mainContentWidth : ($(window).width() - $leftHeaderSize - $bodyBorderWidth) - (($(window).width()- $leftHeaderSize - $bodyBorderWidth)*.02);
					$ustOutOfSight = Math.ceil( (($windowWidth2 + $scrollBar - $mainContentWidth) / 2) +2)
				}
			}
	    	
	    	if(!$(this).parents('.span_9').length > 0 && !$(this).parent().hasClass('span_3') && $(this).parent().attr('id') != 'sidebar-inner' && $(this).parent().attr('id') != 'portfolio-extra' 
	      && !$(this).find('.carousel-wrap[data-full-width="true"]').length > 0
				&& !$(this).find('.nectar-carousel-flickity-fixed-content').length > 0
	    	&& !$(this).find('.portfolio-items:not(".carousel")[data-col-num="elastic"]').length > 0){

	    		//escape if inside woocoommerce page and not using applicable layout
	    		if($('.single-product').length > 0 && $(this).parents('#tab-description').length > 0 && $(this).parents('.full-width-tabs').length == 0) {
	    			$(this).css({
						'visibility': 'visible'
					});	
	    		} else {
	    			if($(this).hasClass('portfolio-items')) {
		    				$(this).css({
								'transform': 'translateX(-'+ $ustOutOfSight + 'px)',
								'margin-left': 0,
								'left': 0,
								'width': $carouselWidth,
								'visibility': 'visible'
							});	
	    			} else {
							
							//fullscreen page rows left fix
							if($('#nectar_fullscreen_rows').length > 0 && $(this).hasClass('wpb_row')) {
								$(this).css({
									'margin-left': - $ustOutOfSight,
									'width': $carouselWidth,
									'visibility': 'visible'
								});	
							} else {
								$(this).css({
									'left': 0,
									'margin-left': - $ustOutOfSight,
									'width': $carouselWidth,
									'visibility': 'visible'
								});	
							}
							
	    			}
					
				}
			}  else if($(this).parent().attr('id') == 'portfolio-extra' && $('#full_width_portfolio').length != 0) {
				$(this).css({
					'left': 0,
					'margin-left': - $ustOutOfSight,
					'width': $carouselWidth,
					'visibility': 'visible'
				});	
			}
			
			else {

				$(this).css({
					'margin-left': 0,
					'width': 'auto',
					'left': '0',
					'visibility': 'visible'
				});	
			}
	    	
	    });
			
	}
	
	var $contentElementsNum = ($('#portfolio-extra').length == 0) ? $('.main-content > .row > *').length : $('.main-content > .row #portfolio-extra > *').length ;

	function parallaxSrollSpeed(speedString) {

		var ua = window.navigator.userAgent;	
		var msie = ua.indexOf("MSIE ");
		var speed;

		 //not as modern browsers
		 if ($smoothCache == true) {
			 switch(speedString) {
			   	  case 'slow':
			   	      speed = 0.2;
			   	      break;
			   	  case 'medium': 
			   	  	  speed = 0.4;
			   	      break;
			   	  case 'fast': 
			    	  speed = 0.6;
			   	       break;
			   }
		}
		 //chrome/ff
		 else {
		 	 switch(speedString) {
			   	  case 'slow':
			   	      speed = 0.6;
			   	      break;
			   	  case 'medium': 
			   	  	  speed = 0.4;
			   	      break;
			   	  case 'fast': 
			    	  speed = 0.25;
			   	       break;
			   }
		}

		   return speed;
	}

	function parallaxScrollInit(){
		
		if(nectarDOMInfo.usingMobileBrowser && $('body[data-remove-m-parallax="1"]').length > 0) {
			return;
		}
		
		parallaxRowsBGCals();
		
		$('.nectar-recent-posts-single_featured, .wpb_row.parallax_section, #page-header-bg[data-parallax="1"] .page-header-bg-image-wrap, .parallax_slider_outer .nectar-slider-wrap').each(function(){
		   var $id = $(this).attr('id');	

		    var ua = window.navigator.userAgent;	
		    var msie = ua.indexOf("MSIE ");

			if ($smoothCache == true)  {
				
		   		if($(this).find('[data-parallax-speed="fixed"]').length == 0) { 
						
							if($(this).find('.row-bg').length == 0) {
								$('#'+$id).parallaxScroll("50%", 0.25);
							} else {
								$('#'+$id + ".parallax_section").parallaxScroll("50%", parallaxSrollSpeed($(this).find('.row-bg').attr('data-parallax-speed')) );
							}
					}
					
		  } else if($(this).find('[data-parallax-speed="fixed"]').length == 0) {
				
					if($(this).find('.row-bg').length == 0) {
							//set default scroll speed if not defined
							$('#'+$id).parallaxScroll("50%", 0.25);
					} else {
							$('#'+$id + ".parallax_section").parallaxScroll("50%", parallaxSrollSpeed($(this).find('.row-bg').attr('data-parallax-speed')) );
					}
		  }
			
			$(this).addClass('nectar-parallax-enabled');
			
		});
		
	}
	
	
	
	//add first class for rows for page header trans effect (zoom only as of now)
	$('.full-width-section.wpb_row, .full-width-content.wpb_row').each(function(){
		
		if(!$(this).parent().hasClass('span_9') && !$(this).parent().hasClass('span_3') && $(this).parent().attr('id') != 'sidebar-inner'){
			
			
			if($(this).parents('#portfolio-extra').length > 0 && $('#full_width_portfolio').length == 0) return false;
				
			if($(this).index() == '0' && $('#page-header-bg').length == 0 && $('.page-header-no-bg').length == 0 
							 && $('.project-title').length == 0 && $('body.single').length == 0 
							 && $('.project-title').length == 0 ) {

				$(this).addClass('first-section');
				var $that = $(this);
				setTimeout( function() { $that.addClass('loaded'); },50);
				
			} 
				

		}
	});	
	
	
	

	parallaxScrollInit();
	parallaxRowsBGCals();


	function parallaxRowsBGCals(){
		
		if(nectarDOMInfo.usingMobileBrowser && $('body[data-remove-m-parallax="1"]').length > 0) {
			return;
		}

		$('.nectar-recent-posts-single_featured, .wpb_row.parallax_section, #page-header-bg[data-parallax="1"] .page-header-bg-image-wrap, .parallax_slider_outer .nectar-slider-wrap .slide-bg-wrap').each(function(){

			 var ua = window.navigator.userAgent;
		   var msie = ua.indexOf("MSIE ");

			 if ($smoothCache == true) {
			 	 $(this).find('.row-bg').css({'height': $(this).outerHeight(true)*2.8, 'margin-top': '-' + ($(this).outerHeight(true)*2.8)/2 + 'px' });
			 } else {
				 
				 if($(this).find('.row-bg').length == 0 && $(this).find('.page-header-bg-image').length > 0 ) {
					 	//$(this).find('.page-header-bg-image').css({'height': $(this).outerHeight(true)   });
				 } 
				 else if($(this).find('.row-bg').length == 0 && $(this).find('.image-bg').length > 0 ) {
					 	
						var $non_page_builder_slider = false;
						//for first nectar slider shortcode without page builder
				 		if( $('.wpb_row').length == 0 && $(this).parents('.nectar-slider-wrap[data-full-width="true"]').length > 0 && $(this).parents('.parallax_slider_outer').length > 0 && $(this).parents('.parallax_slider_outer').index() == '0' ) {
				 			$non_page_builder_slider = true;
				 		}
						//portfolio first
						if($('#portfolio-extra').length > 0 && $(this).parents('.wpb_row').length > 0 && $(this).parents('.parallax_slider_outer').length > 0 && $(this).parents('.wpb_row').index() == '0' ) {
								$non_page_builder_slider = true;
						}
					 
					 if($(this).parents('.top-level').length > 0 || $non_page_builder_slider) {
						 $(this).find('.image-bg').css({'height':  Math.ceil( $(this).parent().offset().top * 0.25 ) + $(this).outerHeight(true) });
					 } else {
						 	$(this).find('.image-bg').css({'height': Math.ceil( $(window).height() * 0.25 ) + $(this).outerHeight(true)   });
					 }
					 
				 }
				 else if($(this).find('.row-bg').length == 0 && $(this).find('.video-wrap').length > 0 ) {
					 
					 
					 var $non_page_builder_slider = false;
					 //for first nectar slider shortcode without page builder
					 if( $('.wpb_row').length == 0 && $(this).parents('.nectar-slider-wrap[data-full-width="true"]').length > 0 && $(this).parents('.parallax_slider_outer').length > 0 && $(this).parents('.parallax_slider_outer').index() == '0' ) {
						 $non_page_builder_slider = true;
					 } 
					 //portfolio first
					 if($('#portfolio-extra').length > 0 && $(this).parents('.wpb_row').length > 0 && $(this).parents('.parallax_slider_outer').length > 0 && $(this).parents('.wpb_row').index() == '0' ) {
							 $non_page_builder_slider = true;
					 }
					 
					 if($(this).parents('.top-level').length > 0 || $non_page_builder_slider) {
						 $(this).find('.video-wrap').css({'height': Math.ceil( $(this).parent().offset().top * 0.25 ) + $(this).outerHeight(true) });
					 } else {
						 	$(this).find('.video-wrap').css({'height': Math.ceil( $(window).height() * 0.25 ) + $(this).outerHeight(true)   });
					 }
					 
					 var vid = $(this).find('.video-wrap video');
					 var vid_w_orig = 1280;
					 var vid_h_orig = 720;

					 // get the parent element size
					 var container_w = vid.parent().width();
					 var container_h = vid.parent().height();	 
					 var scale_w = container_w / vid_w_orig;
					 var scale_h = container_h / vid_h_orig;
					 var scale = scale_w > scale_h ? scale_w : scale_h;
					 
					 // scale the video
					 vid.width(scale * vid_w_orig);
					 vid.height(scale * vid_h_orig);

				 }  
				 else {
					 	if($(this).is('.nectar-recent-posts-single_featured') && $(this).parents('.top-level').length > 0 ) {
							
						}
					 	else if( !$(this).hasClass('top-level') ) {
							var $ifFast = ($(this).find('.row-bg[data-parallax-speed="fast"]').length > 0) ? 60 : 0;
						 	$(this).find('.row-bg').css({'height': Math.ceil( $(window).height() * parallaxSrollSpeed($(this).find('.row-bg').attr('data-parallax-speed')) ) + $(this).outerHeight(true) + $ifFast  });
						} 
						
				 }
			 
			 }
			 
		});
	}
	
	//if fullwidth section is first or last, remove the margins so it fits flush against header/footer
	function fwsClasses() {
		
		$('.wpb_wrapper > .nectar-slider-wrap[data-full-width="true"]').each(function(){
			if(!$(this).parent().hasClass('span_9') && !$(this).parent().hasClass('span_3') && $(this).parent().attr('id') != 'sidebar-inner'){
				if($(this).parents('.wpb_row').index() == '0'){
					$(this).addClass('first-nectar-slider');
				} 
			}
		});

		if($('#portfolio-extra').length == 0) {
			$contentElementsNum = ($('.main-content > .row > .wpb_row').length > 0) ? $('.main-content > .row > .wpb_row').length : $('.main-content > .row > *').length;
		} else {
			$contentElementsNum = $('.main-content > .row #portfolio-extra > *').length;
		}

		$('.full-width-section, .full-width-content:not(.page-submenu .full-width-content):not(.blog-fullwidth-wrap), .row > .nectar-slider-wrap[data-full-width="true"], .wpb_wrapper > .nectar-slider-wrap[data-full-width="true"], .portfolio-items[data-col-num="elastic"]').each(function(){
			
			if(!$(this).parent().hasClass('span_9') && !$(this).parent().hasClass('span_3') && $(this).parent().attr('id') != 'sidebar-inner'){
				
				if($(this).parents('.wpb_row').length > 0){ 
				
					if($(this).parents('#portfolio-extra').length > 0 && $('#full_width_portfolio').length == 0) return false;
					
					////first
					if($(this).parents('.wpb_row').index() == '0' && $('#page-header-bg').length != 0 ){
						//$(this).css('margin-top','-2.1em').addClass('first-section nder-page-header');
					} 
					else if($(this).parents('.wpb_row').index() == '0' && $('#page-header-bg').length == 0 && $('.page-header-no-bg').length == 0 
					         && $('.project-title').length == 0 && $(this).parents('.wpb_row').index() == '0' 
					         && $('.project-title').length == 0 
					         && $('body[data-bg-header="true"]').length == 0) {

					     if($('.single').length == 0) {
					     	$('.container-wrap').css('padding-top','0px');
					     } else {
					     	$(this).addClass('first-section');
					     } 	
						
					} 
					
					//check if it's also last (i.e. the only fws)
					if($(this).parents('.wpb_row').index() == $contentElementsNum-1 && $('#respond').length == 0 ) { 
						if($(this).attr('id') != 'portfolio-filters-inline') {
							$('.container-wrap').css('padding-bottom','0px');
							$('#call-to-action .triangle').remove();
						}
					} 
				
				} else {

					if($(this).parents('#portfolio-extra').length > 0 && $('#full_width_portfolio').length == 0) return false;
					
					if( $(this).find('.portfolio-filters-inline').length == 0 && $(this).attr('id') != 'post-area' ) {
						
						////first
						if($(this).index() == '0' && $('#page-header-bg').length != 0 ){
							//$(this).css('margin-top','-2.1em').addClass('first-section nder-page-header');
			
						} 
						else if($(this).index() == '0' && $('#page-header-bg').length == 0 && $(this).index() == '0' && $('.page-header-no-bg').length == 0 && 
						        $(this).index() == '0' && !$(this).hasClass('blog_next_prev_buttons') && !$(this).hasClass('nectar-shop-outer') && $(this).parents('.pum-container').length == 0 ) {
						     
						      if($('body[data-header-resize="0"]').length == 1 && $('.single').length == 0 || $('body.material').length > 0 && $('.single').length == 0) { 
											
											if(!$('body.blog .blog-fullwidth-wrap > .masonry:not(.meta-overlaid)').length > 0) {
						          	$('.container-wrap').css('padding-top','0px');
											}
											
						      } else {
						      	  $(this).addClass('first-section');
						      }   	
							
						} 
						
						//check if it's also last (i.e. the only fws)
						if($(this).index() == $contentElementsNum-1 && $('#respond').length == 0 && $('body.woocommerce-checkout').length == 0) { 
							$('.container-wrap').css('padding-bottom','0px');
							$('.bottom_controls').css('margin-top','0px');
							$('#call-to-action .triangle').remove();
						} 
					}
					
				}
			}
		});




		
		$('#portfolio-extra > .nectar-slider-wrap[data-full-width="true"], .portfolio-wrap').each(function(){
			//check if it's last 
			if($(this).index() == $contentElementsNum-1 && $('#commentform').length == 0 && $('#pagination').length == 0) { 
				if(parseInt($('.container-wrap').css('padding-bottom')) > 0) $(this).css('margin-bottom','-40px');
				$('#call-to-action .triangle').remove();
			}
		});
		
		
		
		$('.portfolio-filters').each(function(){
			////first
			if($(this).index() == '0' && $('#page-header-bg').length != 0 || $(this).index() == '0' ){
				$(this).addClass('first-section nder-page-header');
			}  else if($(this).index() == '0' && $('#page-header-bg').length == 0 || $(this).index() == '0'){
				$(this).css({'margin-top':'0px'}).addClass('first-section');
			}
		});
		
		$('.portfolio-filters-inline').each(function(){
			////first
			if($(this).parents('.wpb_row').length > 0){ 
				
				if($(this).parents('.wpb_row').index() == '0' && $('#page-header-bg').length != 0 || $(this).parents('.wpb_row').index() == '0' ){
					if($('body[data-header-resize="0"]').length == 0) { 
						//$(this).css({'margin-top':'-2.1em', 'padding-top' : '19px'}).addClass('first-section nder-page-header');
					}
				}  
				
			} else {
				if($(this).index() == '0' && $('#page-header-bg').length != 0 || $(this).index() == '0' ){
					$(this).css({'margin-top':'-2.1em', 'padding-top' : '19px'}).addClass('first-section nder-page-header');
				}  else if($(this).index() == '0' && $('#page-header-bg').length == 0 || $(this).index() == '0'){
					
					//if($('body[data-header-resize="0"]').length == 1 || $('body.material').length > 0) { 
				          $(this).css({'margin-top':'-30px', 'padding-top' : '50px'}).addClass('first-section');
				      //} else {
				      //	  $(this).css({'margin-top':'-70px', 'padding-top' : '50px'}).addClass('first-section');
				      //} 

				
				}
			}
			
		});
		

		
	}
	
	
	
	//sizing for fullwidth sections that are image only

	function fullwidthImgOnlySizingInit(){
		////set inital sizes
		$('.full-width-section:not(.custom-skip)').each(function(){
			
			var $fwsHeight = $(this).outerHeight(true);

			//make sure it's empty and also not being used as a small dvider
			if($(this).find('.span_12 *').length == 0 && $.trim( $(this).find('.span_12').text() ).length == 0  && $fwsHeight > 40){
				$(this).addClass('bg-only');
				$(this).css({'height': $fwsHeight, 'padding-top': '0px', 'padding-bottom': '0px'});
				$(this).attr('data-image-height',$fwsHeight);
			}

		});
	}

	function fullwidthImgOnlySizing(){

		$('.full-width-section.bg-only').each(function(){
			var $initialHeight = $(this).attr('data-image-height');
			
			if( window.innerWidth < 1000 && window.innerWidth > 690 ) {
				$(this).css('height', $initialHeight - $initialHeight*.60);
			} 
			
			else if( window.innerWidth <= 690 ) {
				$(this).css('height', $initialHeight - $initialHeight*.78);
			} 
			
			else if( window.innerWidth < 1300 && window.innerWidth >= 1000  ) {
				$(this).css('height', $initialHeight - $initialHeight*.33);
			} 
			
			else {
				$(this).css('height', $initialHeight);
			}
			
		});
		
	}

	fullwidthImgOnlySizingInit();
	fullwidthImgOnlySizing();
	
	
	
	//change % padding on rows to be relative to screen
	function fullWidthRowPaddingAdjustInit(){
		if($('#boxed').length == 0){
			$('.full-width-section, .full-width-content').each(function(){
				var $topPadding = $(this)[0].style.paddingTop;
				var $bottomPadding = $(this)[0].style.paddingBottom;

				if($topPadding.indexOf("%") >= 0) $(this).attr('data-top-percent',$topPadding);
				if($bottomPadding.indexOf("%") >= 0) $(this).attr('data-bottom-percent',$bottomPadding);
				

			});
		}
	}

	function fullWidthRowPaddingAdjustCalc(){
		if($('#boxed').length == 0){
			$('.full-width-section[data-top-percent], .full-width-section[data-bottom-percent], .full-width-content[data-top-percent],  .full-width-content[data-bottom-percent]').each(function(){

				var $windowHeight = $(window).width();
				var $topPadding = ($(this).attr('data-top-percent')) ? $(this).attr('data-top-percent') : 'skip';
				var $bottomPadding = ($(this).attr('data-bottom-percent')) ? $(this).attr('data-bottom-percent') : 'skip';

				//top
				if($topPadding != 'skip') {
					$(this).css('padding-top',$windowHeight*(parseInt($topPadding)/100));
				}

				//bottom
				if($bottomPadding != 'skip'){
					$(this).css('padding-bottom',$windowHeight*(parseInt($bottomPadding)/100));
				}
				

			});
		}
	}
	if(nectarDOMInfo.usingMobileBrowser) {
		fullWidthRowPaddingAdjustCalc();
	}

	
	//full width content column sizing
	function fullWidthContentColumns(){

		//standard carousel
   	  	$('.main-content > .row > .full-width-content, #portfolio-extra > .full-width-content, .woocommerce-tabs #tab-description > .full-width-content, .post-area.span_12 article .content-inner > .full-width-content').each(function(){
			

			//only set the height if more than one column
			if($(this).find('> .span_12 > .col').length > 1){
				
				var tallestColumn = 0;
				var $columnInnerHeight = 0;
				
				$(this).find('> .span_12 > .col').each(function(){

					$column_inner_selector = ($(this).find('> .vc_column-inner > .wpb_wrapper').length > 0) ? '.vc_column-inner' : '.column-inner-wrap > .column-inner';
					
					var $padding = parseInt($(this).css('padding-top'));
					//var $padding = (!$(this).is('[data-animation*="reveal"]')) ? parseInt($(this).css('padding-top')) : parseInt($(this).find('> .column-inner-wrap > .column-inner').css('padding-top')); start to reveal fix
					($(this).find('> '+$column_inner_selector+' > .wpb_wrapper').height() + ($padding*2) > tallestColumn) ? tallestColumn = $(this).find('> '+$column_inner_selector+' > .wpb_wrapper').height() + ($padding*2)  : tallestColumn = tallestColumn;
				});	
	    	 	
	    	 	$(this).find('> .span_12 > .col').each(function(){

	    	 		$column_inner_selector = ($(this).find('> .vc_column-inner > .wpb_wrapper').length > 0) ? '.vc_column-inner' : '.column-inner-wrap > .column-inner';
					
	    	 		//columns with content
		    	 	if($(this).find('> '+$column_inner_selector+' > .wpb_wrapper > *').length > 0){
		    	 		//added in 7.6 to fix equal height columns 
		    	 		if(!$(this).parent().parent().hasClass('vc_row-o-equal-height')) $(this).css('height',tallestColumn);
		    	 	} 
		    	 	//empty columns
		    	 	else {
		    	 		$(this).css('min-height',tallestColumn);
		    	 		if($(this).is('[data-animation*="reveal"]')) $(this).find('.column-inner').css('min-height',tallestColumn);
		    	 	}
	    	 	});
	         	
	         	//nested column height
	         	var $childRows = $(this).find('> .span_12 > .col .wpb_row').length;
	         	if(window.innerWidth > 1000) { 
	         		
	         		var $padding = parseInt($(this).find('> .span_12 > .col').css('padding-top'));
	         		
	         		//$(this).find('> .span_12 > .col .wpb_row .col').css('min-height',(tallestColumn-($padding*2))/$childRows + 'px'); 
	         	} else {
	         		$(this).find('> .span_12 > .col .wpb_row .col').css('min-height','0px'); 
	         	}
	         	
	         	
	         	//vertically center
	         	if($(this).hasClass('vertically-align-columns') && window.innerWidth > 1000 && !$(this).hasClass('vc_row-o-equal-height')){
	         		
	         		//parent columns
		         	$(this).find('> .span_12 > .col').each(function(){

		         		$column_inner_selector = ($(this).find('> .vc_column-inner > .wpb_wrapper').length > 0) ? '.vc_column-inner' : '.column-inner-wrap > .column-inner';
						
						$columnInnerHeight = $(this).find('> '+$column_inner_selector+' > .wpb_wrapper').height();
						var $marginCalc = ($(this).height()/2)-($columnInnerHeight/2);
						if($marginCalc <= 0) $marginCalc = 0;
						
						$(this).find('> '+$column_inner_selector+' > .wpb_wrapper').css('margin-top',$marginCalc);
						$(this).find('> '+$column_inner_selector+' > .wpb_wrapper').css('margin-bottom',$marginCalc);
						
					});	
	
					
				}
			
			}
			
   	  	});

		//requal height columns in container type with reveal columns
		$('.main-content > .row > .wpb_row:not(.full-width-content).vc_row-o-equal-height').each(function(){
			if($(this).find('>.span_12>.wpb_column[data-animation*="reveal"]').length >0) {
				var tallestColumn = 0;
				var $columnInnerHeight = 0;
				
				$(this).find('> .span_12 > .col').each(function(){
					
					var $padding = parseInt($(this).find('> .column-inner-wrap > .column-inner').css('padding-top'));
					($(this).find('> .column-inner-wrap > .column-inner').height() + ($padding*2) > tallestColumn) ? tallestColumn = $(this).find('> .column-inner-wrap > .column-inner').height() + ($padding*2)  : tallestColumn = tallestColumn;
				});	
	    	 	
	    	 	$(this).find('> .span_12 > .col').each(function(){
					
	    	 		//columns with content
		    	 	if($(this).find('> .column-inner-wrap > .column-inner .wpb_wrapper > *').length > 0){
		    	 		$(this).find('> .column-inner-wrap').css('height',tallestColumn);
		    	 	} 
		    	 	//empty columns
		    	 	else {
		    	 		$(this).css('min-height',tallestColumn);
		    	 		if($(this).is('[data-animation*="reveal"]')) $(this).find('.column-inner').css('min-height',tallestColumn);
		    	 	}
	    	 	});

			}	
		});

		//using equal height option, top/bottom padding % needs to be convered into px for cross browser (flex bug)
		$('.wpb_row.vc_row-o-equal-height>.span_12>.wpb_column[class*="padding-"][data-padding-pos="all"]').each(function(){
			$(this).css({ 'padding-top': $(this).css('padding-left'), 'padding-bottom': $(this).css('padding-left')});
		});
		
	}
	
	fullWidthContentColumns();
	if($('.owl-carousel').length > 0) owlCarouselInit();


var $mouseParallaxScenes = [];
function mouseParallaxInit(){
	$('.wpb_row:has(.nectar-parallax-scene)').each(function(i){

		var $headerNavSpace = ($('body[data-header-format="left-header"]').length > 0 && $(window).width() > 1000) ? 0 : $('#header-space').height();
		
		/*
		if($(this).hasClass('first-section')) { 
			$('body #header-outer[data-transparent-header="true"] .ns-loading-cover').show();
			
			if($('body #header-outer[data-transparent-header="true"]').length > 0) { 
				$(this).css('overflow','hidden');
				$(this).find('.nectar-slider-loading').css({
					'top': $headerNavSpace,
					'margin-top' : '-1px'
				});
				$(this).find('.nectar-slider-loading .loading-icon').css({
					'height' :  $('.first-section .nectar-parallax-scene').height() - $headerNavSpace + 'px',
					'opacity' : '1'
				});
			}
		} */

		var $strength = parseInt($(this).find('.nectar-parallax-scene').attr('data-scene-strength'));

		$mouseParallaxScenes[i] = $(this).find('.nectar-parallax-scene').parallax({
			scalarX: $strength,
	  		scalarY: $strength
		});

		//wait until the images in the scene have loaded
		var images = $(this).find('.nectar-parallax-scene li');
		
		$.each(images, function(){
			if($(this).find('div').length > 0) {
			    var el = $(this).find('div'),
			    image = el.css('background-image').replace(/"/g, '').replace(/url\(|\)$/ig, '');
			    if(image && image !== '' && image !== 'none')
			        images = images.add($('<img>').attr('src', image));
			}
		});

		var $that = $(this);
		
		/* images.imagesLoaded(function(){

			$that.find('> .nectar-slider-loading, .full-page-inner > .nectar-slider-loading').fadeOut(800,'easeInOutExpo');
			if($that.hasClass('first-section')) { 
				$('body #header-outer[data-transparent-header="true"] .ns-loading-cover').fadeOut(800,'easeInOutExpo',function(){
    				$(this).remove();
    			});
			}
		}); */

	});
}
mouseParallaxInit();


	
/***************** Checkmarks ******************/

function ulChecks() {
	$('ul.checks li').prepend('<i class="icon-ok-sign"></i>');
}
ulChecks();

/***************** Image with Animation / Col Animation *******************/


	
function colAndImgAnimations(){

	$colAndImgOffsetPos = ($('#nectar_fullscreen_rows').length > 0) ? '200%' : '88%';
	$colAndImgOffsetPos2 = ($('#nectar_fullscreen_rows').length > 0) ? '200%' : '70%';

	$($fullscreenSelector+'img.img-with-animation').each(function() {
		
		var $that = $(this);
		var $animationEasing = ($('body[data-cae]').length > 0) ? $('body').attr('data-cae') : 'easeOutSine';
		var $animationDuration = ($('body[data-cad]').length > 0) ? $('body').attr('data-cad') : '650';

		var waypoint = new Waypoint({
 			element: $that,
 			 handler: function(direction) {
			   
					if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('animated-in')) { 
						 waypoint.destroy();
						return;
					}

					if(!navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/) || $('body[data-responsive="0"]').length > 0) {
				
						if($that.attr('data-animation') == 'fade-in-from-left'){
							$that.delay($that.attr('data-delay')).transition({
								'opacity' : 1,
								'x' : '0px'
							},$animationDuration, $animationEasing);
						} else if($that.attr('data-animation') == 'fade-in-from-right'){
							$that.delay($that.attr('data-delay')).transition({
								'opacity' : 1,
								'x' : '0px'
							},$animationDuration, $animationEasing);
						} else if($that.attr('data-animation') == 'fade-in-from-bottom'){
							$that.delay($that.attr('data-delay')).transition({
								'opacity' : 1,
								'y' : '0px'
							},$animationDuration, $animationEasing);
						} else if($that.attr('data-animation') == 'fade-in') {
							$that.delay($that.attr('data-delay')).transition({
								'opacity' : 1
							},$animationDuration, $animationEasing);	
						} else if($that.attr('data-animation') == 'grow-in') {
							setTimeout(function(){ 
								$that.transition({ scale: 1, 'opacity':1 },$animationDuration,$animationEasing);
							},$that.attr('data-delay'));
						}
						else if($that.attr('data-animation') == 'flip-in') {
							setTimeout(function(){ 
								$that.transition({  rotateY: 0, 'opacity':1 },$animationDuration, $animationEasing);
							},$that.attr('data-delay'));
						}
						else if($that.attr('data-animation') == 'flip-in-vertical') {
							setTimeout(function(){ 
								$that.transition({  rotateX: 0, 'opacity':1 },$animationDuration, $animationEasing);
							},$that.attr('data-delay'));
						}

						$that.addClass('animated-in');
						
					}

					waypoint.destroy();

			  },
			  offset: $colAndImgOffsetPos
		});

		
	
	});


	$($fullscreenSelector+'.nectar_cascading_images').each(function() {
		
		var $that = $(this);
		var $animationEasing = ($('body[data-cae]').length > 0) ? $('body').attr('data-cae') : 'easeOutSine';
		var $animationDuration = ($('body[data-cad]').length > 0) ? $('body').attr('data-cad') : '650';
		var $animationDelay = ($(this).is('[data-animation-timing]')) ? $(this).attr('data-animation-timing') : 175;
		$animationDelay = parseInt($animationDelay);

		var waypoint = new Waypoint({
 			element: $that,
 			 handler: function(direction) {
			   
					if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('animated-in')) { 
						 waypoint.destroy();
						return;
					}

					if(!navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/) || $('body[data-responsive="0"]').length > 0) {
					
						$that.find('.cascading-image').each(function(i){

							var $that2 = $(this);

							if($that2.attr('data-animation') == 'flip-in' || $that2.attr('data-animation') == 'flip-in-vertical') {
								setTimeout(function(){
									$that2.find('.inner-wrap').css({
										'opacity' : 1,
										'transform' : 'rotate(0deg) translateZ(0px)'
									});
								}, i* $animationDelay);
							} else {
								setTimeout(function(){
									$that2.find('.inner-wrap').css({
										'opacity' : 1,
										'transform' : 'translateX(0px) translateY(0px) scale(1,1) translateZ(0px)'
									});
								}, i* $animationDelay);
							}
					

						});

						$that.addClass('animated-in');
						
					}

					waypoint.destroy();

			  },
			  offset: $colAndImgOffsetPos
		});

		
	
	});
	

	
	$($fullscreenSelector+'.col.has-animation:not([data-animation*="reveal"]), '+$fullscreenSelector+'.wpb_column.has-animation:not([data-animation*="reveal"]), '+$fullscreenSelector+'.nectar-fancy-box.has-animation').each(function(i) {
	    
		var $that = $(this);
		var $animationEasing = ($('body[data-cae]').length > 0) ? $('body').attr('data-cae') : 'easeOutSine';
		var $animationDuration = ($('body[data-cad]').length > 0) ? $('body').attr('data-cad') : '650';

		//set perspective for vertical flip
		if($that.is('[data-animation="flip-in-vertical"]')) {
			$that.parents('.col.span_12').addClass('flip-in-vertical-wrap');
		}

		var waypoint = new Waypoint({
 			element: $that,
 			 handler: function(direction) {
				
				if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('animated-in')) { 
					 waypoint.destroy();
					return;
				}

				if(!navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/) || $('body[data-responsive="0"]').length > 0) {
				 	
				 	
					if($that.attr('data-animation') == 'fade-in-from-left'){
						$standAnimatedColTimeout[i] = setTimeout(function(){ 
							$that.transition({
								'opacity' : 1,
								'x' : '0px'
							},$animationDuration,$animationEasing);
						},$that.attr('data-delay'));
					} else if($that.attr('data-animation') == 'fade-in-from-right'){
						$standAnimatedColTimeout[i] = setTimeout(function(){ 
							$that.transition({
								'opacity' : 1,
								'x' : '0px'
							},$animationDuration,$animationEasing);
						},$that.attr('data-delay'));
					} else if($that.attr('data-animation') == 'fade-in-from-bottom'){
						$standAnimatedColTimeout[i] = setTimeout(function(){ 
							$that.transition({
								'opacity' : 1,
								'y' : '0px'
							},$animationDuration,$animationEasing);
						},$that.attr('data-delay'));
					} else if($that.attr('data-animation') == 'fade-in') {
						$standAnimatedColTimeout[i] = setTimeout(function(){ 
							$that.transition({
								'opacity' : 1
							},$animationDuration,$animationEasing);	
						},$that.attr('data-delay'));
					} else if($that.attr('data-animation') == 'grow-in') {
						$standAnimatedColTimeout[i] = setTimeout(function(){ 
							$that.transition({ scale: 1, 'opacity':1 },$animationDuration,$animationEasing);
						},$that.attr('data-delay'));
					} else if($that.attr('data-animation') == 'flip-in') {
						$standAnimatedColTimeout[i] = setTimeout(function(){ 
							$that.transition({  rotateY: 0, 'opacity':1 },$animationDuration, $animationEasing);
						},$that.attr('data-delay'));
					} else if($that.attr('data-animation') == 'flip-in-vertical') {
						$standAnimatedColTimeout[i] = setTimeout(function(){ 
							$that.transition({  rotateX: 0, y: 0, 'opacity':1 },$animationDuration, $animationEasing);
						},$that.attr('data-delay'));
					}

					//boxed column hover fix
					if($that.hasClass('boxed')) {
						$that.addClass('no-pointer-events');
						setTimeout(function(){
							$that.removeClass('no-pointer-events');
						},parseInt($animationDuration) + parseInt($that.attr('data-delay')) + 30 );
					}

					$that.addClass('animated-in');
				
				}

				waypoint.destroy();
			},
			offset: $colAndImgOffsetPos
		});
	
	});

	
	$($fullscreenSelector+'.wpb_column.has-animation[data-animation*="reveal"]').each(function() {
	    
		var $that = $(this);
		var $animationEasing = ($('body[data-cae]').length > 0) ? $('body').attr('data-cae') : 'easeOutSine';
		var $animationDuration = ($('body[data-cad]').length > 0) ? $('body').attr('data-cad') : '650';

		var waypoint = new Waypoint({
 			element: $that,
 			 handler: function(direction) {
				
				if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('animated-in')) { 
					 waypoint.destroy();
					return;
				}

				if(!navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/) || $('body[data-responsive="0"]').length > 0) {
					
					if($that.attr('data-animation') == 'reveal-from-bottom' || $that.attr('data-animation') == 'reveal-from-top') {
						setTimeout(function(){ 
							if($that.hasClass('animated-in')) $that.find('.column-inner-wrap, .column-inner').transition({  'y': 0 },$animationDuration, $animationEasing,function(){ if($that.hasClass('animated-in')) $that.find('.column-inner-wrap, .column-inner').addClass('no-transform'); });
						},$that.attr('data-delay'));
					} else if($that.attr('data-animation') == 'reveal-from-right' || $that.attr('data-animation') == 'reveal-from-left') {
						setTimeout(function(){ 
							if($that.hasClass('animated-in'))  $that.find('.column-inner-wrap, .column-inner').transition({  'x': 0 },$animationDuration, $animationEasing,function(){ if($that.hasClass('animated-in')) $that.find('.column-inner-wrap, .column-inner').addClass('no-transform'); });
						},$that.attr('data-delay'));
					} 

					$that.addClass('animated-in');
				
				}

				waypoint.destroy();
			},
			offset: $colAndImgOffsetPos2
		});
	
	}); 	

	
}


function cascadingImageBGSizing() {
	$('.nectar_cascading_images').each(function(){

		//handle max width for cascading images in equal height columns
		if($(this).parents('.vc_row-o-equal-height').length > 0 && $(this).parents('.wpb_column').length > 0) 
			$(this).css('max-width',$(this).parents('.wpb_column').width());

		//set size for layers with no images
		$(this).find('.bg-color').each(function(){
			var $bgColorHeight = 0;
			var $bgColorWidth = 0;
			if($(this).parent().find('.img-wrap').length == 0) {
				$firstSibling = $(this).parents('.cascading-image').siblings('.cascading-image[data-has-img="true"]').first();

				$firstSibling.css({'position':'relative', 'visiblity':'hidden'});
				$bgColorHeight = $firstSibling.find('.img-wrap').height();
				$bgColorWidth = $firstSibling.find('.img-wrap').width();
				if($firstSibling.index() == 0) {
					$firstSibling.css({'visiblity':'visible'});
				} else {
					$firstSibling.css({'position':'absolute', 'visiblity':'visible'});
				}
			} else {
				$bgColorHeight = $(this).parent().find('.img-wrap').height();
				$bgColorWidth = $(this).parent().find('.img-wrap').width();
			}

			$(this).css({'height': $bgColorHeight,'width': $bgColorWidth});
		});
	});
}

if($('.nectar_cascading_images').length > 0) {
	imagesLoaded($('.nectar_cascading_images'),function(instance){
		cascadingImageBGSizing();
	});
}

function splitLineHeadings() {

	$splitLineOffsetPos = ($('#nectar_fullscreen_rows').length > 0) ? '200%' : 'bottom-in-view';
	$($fullscreenSelector+'.nectar-split-heading').each(function() {

		var $that = $(this);
		var $animationEasing = ($('body[data-cae]').length > 0) ? $('body').attr('data-cae') : 'easeOutSine';
		var $animationDuration = ($('body[data-cad]').length > 0) ? $('body').attr('data-cad') : '650';

		var waypoint = new Waypoint({
				element: $that,
				 handler: function(direction) {
				
				if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('animated-in')) { 
					 waypoint.destroy();
					return;
				}

				if(!navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/) || $('body[data-responsive="0"]').length > 0) {
				 	
					
					$that.find('.heading-line').each(function(i){
						//if($that.parents('.first-section').length > 0 && $('body[data-aie="zoom-out"]').length > 0) i = i+4;
						$(this).find('> div').delay(i*70).transition({
							'y' : '0px'
						},$animationDuration,$animationEasing);

					});
					

					$that.addClass('animated-in');
				
				}

				waypoint.destroy();
			},
			offset: $splitLineOffsetPos
		});

	});
}

	
/***************** Custom tablet default column widths  ******************/
	
	//add one-fourth class
	function oneFourthClasses() {
		$('.col.span_3, .vc_span3, .vc_col-sm-3').each(function(){
			if( !$(this).is('[data-t-w-inherits="small_desktop"]') ) {
				var $currentDiv = $(this);
				var $nextDiv = $(this).next('div');
				if( $nextDiv.hasClass('span_3') && !$currentDiv.hasClass('one-fourths') || $nextDiv.hasClass('vc_span3') && !$currentDiv.hasClass('one-fourths') || $nextDiv.hasClass('vc_col-sm-3') && !$currentDiv.hasClass('one-fourths') ) {
					$currentDiv.addClass('one-fourths clear-both');
					$nextDiv.addClass('one-fourths right-edge');
				}
			}
		});
		
		//make empty second 1/2 half columsn display right on iPad
		$('.span_12 .col.span_6').each(function(){
			if($(this).next('div').hasClass('span_6') && $.trim( $(this).next('div').html() ).length == 0 ) {
				$(this).addClass('empty-second')
			}
		}); 
		
	}
	oneFourthClasses();

/***************** Column Borders ******************/
function animatedColBorders(){
	$progressBarsOffsetPos = ($('#nectar_fullscreen_rows').length > 0) ? '200%' : '75%';
	$($fullscreenSelector+'.wpb_column[data-border-animation="true"]').each(function(i){

		var $that = $(this);
		var waypoint = new Waypoint({
 			element: $that,
 			 handler: function(direction) {
			   
					if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('completed')) { 
						 waypoint.destroy();
						return;
					}

					$borderDelay = ($that.attr('data-border-animation-delay').length > 0) ? parseInt($that.attr('data-border-animation-delay')) : 0;
					setTimeout(function(){
						$that.find('.border-wrap').addClass('animation');
						$that.find('.border-wrap').addClass('completed');
					},$borderDelay)
					

					waypoint.destroy();

			  },
			  offset: $progressBarsOffsetPos
		});

	});
}


/***************** Dividers ******************/
function dividers() {
	$dividerOffsetPos = ($('#nectar_fullscreen_rows').length > 0) ? '200%' : 'bottom-in-view';

	$($fullscreenSelector+'.divider-small-border[data-animate="yes"], '+$fullscreenSelector+'.divider-border[data-animate="yes"]').each(function(i){

		var $lineDur = ($(this).hasClass('divider-small-border')) ? 1300 : 1500;
		var $that = $(this);
		var waypoint = new Waypoint({
 			element: $that,
 			 handler: function(direction) {
			   
					if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('completed')) { 
						 waypoint.destroy();
						return;
					}
				
					$that.each(function(i){

						$(this).css({'transform':'scale(0,1)', 'visibility': 'visible'});
						var $that = $(this);
						
						$that.delay($that.attr('data-animation-delay')).transition({
							'transform' : 'scale(1, 1)'
						},$lineDur, 'cubic-bezier(.18,1,.22,1)');
						
					});

					$that.addClass('completed');

					waypoint.destroy();

			  },
			  offset: $dividerOffsetPos
		});

	});
}

//if($('.nectar-box-roll').length == 0) dividers();


/***************** Icon List ******************/
function iconList() {
	$iconListOffsetPos = ($('#nectar_fullscreen_rows').length > 0) ? '250%' : '75%';

	$($fullscreenSelector+'.nectar-icon-list[data-animate="true"]').each(function(i){

		var $that = $(this);
		var waypoint = new Waypoint({
 			element: $that,
 			 handler: function(direction) {
			   
					if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('completed')) { 
						 waypoint.destroy();
						return;
					}
				
					$that.each(function(i){
						
						var $listItemAnimationDelay = ( $that.is('[data-direction="horizontal"]') ) ? 100 : 300;
						
						$(this).find('.nectar-icon-list-item').each(function(i){
							var $thatt = $(this);
							setTimeout(function(){ $thatt.addClass('animated') },i*$listItemAnimationDelay);
						});
						
					});

					$that.addClass('completed');

					waypoint.destroy();

			  },
			  offset: $iconListOffsetPos
		});

	});
}

//bg color match 

function narrowParentBGC(element) {
	
	var narrowedBGC;
	
	if(element.parents('.wpb_column[data-bg-color*="#"]').length > 0 && element.parents('.wpb_column[data-bg-opacity="1"]').length > 0) {
		var narrowedBGC = element.parents('.wpb_column').attr('data-bg-color');
	}
	else if(element.parents('.wpb_row').length > 0 && element.parents('.wpb_row').find('.row-bg.using-bg-color').length > 0) {
		var narrowedBGC = element.parents('.wpb_row').find('.row-bg.using-bg-color').css('background-color');
	}
	else {
		if($('#nectar_fullscreen_rows').length > 0)
			var narrowedBGC = $('#nectar_fullscreen_rows > .wpb_row .full-page-inner-wrap').css('background-color');
		else 
			var narrowedBGC = $('.container-wrap').css('background-color');
	}
	
	return narrowedBGC;
	
}

var nectarMatchingBGCss = '';

//icon list
$('.nectar-icon-list[data-icon-style="border"], .nectar_icon_wrap[data-style="border-animation"][data-color*="extra-color-gradient-"]').each(function(i){
	
	var $bgColorToSet = narrowParentBGC($(this));
	
	if($(this).hasClass('nectar-icon-list')) 
		$(this).find('.list-icon-holder').css('background-color',$bgColorToSet);
	else {
		//must be set in css, can't manip pseudo
		$(this).addClass('instance-'+i);
		nectarMatchingBGCss += '.nectar_icon_wrap.instance-'+i+' .nectar_icon:before { background-color: '+$bgColorToSet+'!important; }';
	}
		
});

//material gradient btns
$('body.material .nectar-button.see-through[class*="m-extra-color-gradient"]').each(function(i){
	
	var $bgColorToSet = narrowParentBGC($(this));

	$(this).addClass('instance-'+i);
	nectarMatchingBGCss += '.nectar-button.see-through.instance-'+i+':after { background-color: '+$bgColorToSet+'!important; }';
	
});

if(nectarMatchingBGCss.length > 0) {
	var head = document.head || document.getElementsByTagName('head')[0];
	var style = document.createElement('style');

	style.type = 'text/css';
	if (style.styleSheet){
	  style.styleSheet.cssText = nectarMatchingBGCss;
	} else {
	  style.appendChild(document.createTextNode(nectarMatchingBGCss));
	}

	head.appendChild(style);
}

/***************** Animated Title ******************/
function animated_titles() {
	$animatedTitlesOffsetPos = ($('#nectar_fullscreen_rows').length > 0) ? '200%' : 'bottom-in-view';

	$($fullscreenSelector+'.nectar-animated-title').each(function(i){

		var $that = $(this);
		var waypoint = new Waypoint({
 			element: $that,
 			 handler: function(direction) {
			   
					if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('completed')) { 
						 waypoint.destroy();
						return;
					}

					$that.addClass('completed');

					waypoint.destroy();

			  },
			  offset: $animatedTitlesOffsetPos
		});

	});
}


/***************** Highlighted Text ******************/
function highlighted_text() {
	$highlightedTextOffsetPos = ($('#nectar_fullscreen_rows').length > 0) ? '200%' : 'bottom-in-view';

	$($fullscreenSelector+'.nectar-highlighted-text').each(function(i){
		
		/*custom color*/
		if($(this).is('[data-using-custom-color="true"]')) {
			
				var highlightedColorCss = '';
				var $custom_highlight_color = $(this).attr('data-color');
			
				$(this).addClass('instance-'+i);
			
				highlightedColorCss += '.nectar-highlighted-text.instance-' + i + ' em:before { background-color: ' + $custom_highlight_color + '; }';
			
				if(highlightedColorCss.length > 1) {
		
					var head = document.head || document.getElementsByTagName('head')[0];
					var style = document.createElement('style');
		
						style.type = 'text/css';
					if (style.styleSheet){
					  style.styleSheet.cssText = highlightedColorCss;
					} else {
					  style.appendChild(document.createTextNode(highlightedColorCss));
					}
		
					head.appendChild(style);
				}
			
		}
		
		if(nectarDOMInfo.usingMobileBrowser) {
			$(this).find('em').addClass('animated');
		}
		
		/*waypoint*/
		var $that = $(this);
		var waypoint = new Waypoint({
 			element: $that,
 			 handler: function(direction) {
			   
					if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('animated')) { 
						 waypoint.destroy();
						return;
					}
					
					$that.find('em').each(function(i){
						var $highlighted_em = $(this);
						setTimeout(function(){
							$highlighted_em.addClass('animated');
						},i*300);

					});

					waypoint.destroy();

			  },
			  offset: $highlightedTextOffsetPos
		});

	});
}


/***************** WP Media Embed / External Embed ******************/

//this isn't the for the video shortcode* This is to help any external iframe embed fit & resize correctly 
function responsiveVideoIframesInit(){
	$('iframe').each(function(){
		
		//make sure the iframe has a src (things like adsense don't)
		if(typeof $(this).attr('src') != 'undefined' && !$(this).hasClass('iframe-embed') && $(this).parents('.ult_modal').length == 0 && $(this).parents('.ls-slide').length == 0 && $(this).parents('.esg-entry-media').length == 0 && $(this).parents('.wpb_video_widget.wpb_content_element').length == 0){
			
			if( $(this).attr('src').toLowerCase().indexOf("youtube") >= 0 || $(this).attr('src').toLowerCase().indexOf("vimeo") >= 0  || $(this).attr('src').toLowerCase().indexOf("twitch.tv") >= 0 || $(this).attr('src').toLowerCase().indexOf("kickstarter") >= 0 || $(this).attr('src').toLowerCase().indexOf("embed-ssl.ted") >= 0  || $(this).attr('src').toLowerCase().indexOf("dailymotion") >= 0) {
				
				$(this).addClass('iframe-embed');	

				$(this).attr('data-aspectRatio', $(this).height() / $(this).width()).removeAttr('height').removeAttr('width');
				
				if($(this).parents('.post-area.masonry.classic').length > 0) {
						$(this).attr('data-aspectRatio', '0.56').removeAttr('height').removeAttr('width');
				}
				//add wmode=transparent to all youytube embeds to fix z-index issues in IE
				/*if($(this).attr('src').indexOf('wmode=transparent') == -1) {
					if($(this).attr('src').indexOf('?') == -1){
						$(this).attr('src',$(this).attr('src') + '?wmode=transparent');
					} else {
						$(this).attr('src',$(this).attr('src') + '&wmode=transparent');
					}
				} */
			}
			 
		} else {
			//if($(this).parents('ins').length == 0){ 
			//	$(this).wrap('<div class="iframe-embed-standard"/>');	
			//}
		}
		
	});


}

function responsiveVideoIframes(){
	 $('iframe[data-aspectRatio]').each(function() {
	 	var newWidth = $(this).parent().width();
	 	 
		var $el = $(this);
		
		//in nectar slider
		if($(this).parents('.swiper-slide').length > 0) {
			if($(this).is(':visible')) $el.width(newWidth).height(newWidth * $el.attr('data-aspectRatio'));
		} 
		//all others
		else {
			$el.width(newWidth).height(newWidth * $el.attr('data-aspectRatio'));
		}
		
		
	});
}


function videoshortcodeSize(){
	$('.wp-video').each(function(){


		//if($(this).find('.mejs-poster-img').length > 0){
		//	$(this).attr('data-aspectRatio', parseInt($(this).find('.mejs-poster-img').height()) / parseInt($(this).find('.wp-video-shortcode').css('width')));
		//} else {
		//	$(this).attr('data-aspectRatio', parseInt($(this).find('.mejs-overlay').height()) / parseInt($(this).find('.wp-video-shortcode').css('width')));

		//}

		//var newWidth = $(this).width();
	 	 
		//var $el = $(this).find('.wp-video-shortcode');
		//$(this).width(newWidth).height(newWidth * $(this).attr('data-aspectRatio'));
		//$(this).find('.mejs-container').css('height','100%');
	});
}

responsiveVideoIframesInit();
responsiveVideoIframes();


//unwrap post and protfolio videos
$('.video-wrap iframe').unwrap();
$('#sidebar iframe[src]').unwrap();

//$('video:not(.slider-video)').attr('width','100%');
//$('video:not(.slider-video)').attr('height','100%'); 

$('audio').attr('width','100%');
$('audio').attr('height','100%');

$('audio').css('visibility','visible');

if($('body').hasClass('mobile')){
	$('video').css('visibility','hidden');
} else {
	$('video').css('visibility','visible');
}


$('.wp-video').each(function(){
	 video = $(this).find('video').get(0);
	 video.addEventListener('loadeddata', function() {
	   videoshortcodeSize();
	   $(window).trigger('resize');
	 }, false);
});

//webkit video back button fix 
$('.main-content iframe[src]').each(function(){
	//$(this).attr('src',$(this).attr('src'));
	$(this).css({'opacity':'1', 'visibility':'visible'});
});


/***************** Nectar Video BG ******************/

	$('.wpb_row:has(".nectar-video-wrap"):not(.fp-section)').each(function(i){
		$(this).css('z-index',100 + i);
	});

	var min_w = 1200; // minimum video width allowed
	var vid_w_orig;  // original video dimensions
	var vid_h_orig;
	
    vid_w_orig = 1280;
    vid_h_orig = 720;
 
	function resizeVideoToCover() {
		$('.nectar-video-wrap').each(function(i){
			if($(this).find('video').length == 0 ) return;

			if($(this).parents('#page-header-bg').length > 0) {
				if($('.container-wrap.auto-height').length > 0) return false;
				var $containerHeight = $(this).parents('#page-header-bg').outerHeight();			
				var $containerWidth = $(this).parents('#page-header-bg').outerWidth();
			} else {
				var $containerHeight = $(this).parents('.wpb_row').outerHeight();			
				var $containerWidth = ($(this).parents('.full-width-section').length > 0) ? window.innerWidth : $(this).parents('.wpb_row').outerWidth();			
			}
			
		    // set the video viewport to the window size
		    $(this).width($containerWidth);
				if($(this).parents('#page-header-bg').length > 0) {
			    $(this).height($containerHeight);
				}
		
		    // use largest scale factor of horizontal/vertical
		    var scale_h = $containerWidth / vid_w_orig;
		    var scale_v = ($containerHeight - $containerHeight) / vid_h_orig; 
		    var scale = scale_h > scale_v ? scale_h : scale_v;
			
			//update minium width to never allow excess space
		    min_w = 1280/720 * ($containerHeight+40);
		    
		    // don't allow scaled width < minimum video width
		    if (scale * vid_w_orig < min_w) {scale = min_w / vid_w_orig;}
		        
		    // now scale the video
		    $(this).find('video, .mejs-overlay, .mejs-poster').width(Math.ceil(scale * vid_w_orig +0));
		    $(this).find('video, .mejs-overlay, .mejs-poster').height(Math.ceil(scale * vid_h_orig +0));
		    
		    // and center it by scrolling the video viewport
		    $(this).scrollLeft(($(this).find('video').width() - $containerWidth) / 2);
		    $(this).scrollTop(($(this).find('video').height() - ($containerHeight)) / 2);
		    $(this).find('.mejs-overlay, .mejs-poster').scrollTop(($(this).find('video').height() - ($containerHeight)) / 2);


		    //align bottom
		    if($(this).attr('data-bg-alignment') == 'center bottom' || $(this).attr('data-bg-alignment') == 'bottom'){
		    	$(this).scrollTop(($(this).find('video').height() - ($containerHeight+6)));
		    }
		    //align top
		    else if($(this).attr('data-bg-alignment') == 'center top' || $(this).attr('data-bg-alignment') == 'top') {
		    	$(this).scrollTop(0);
		    } 
				
				//add loaded class
				$(this).addClass('position-loaded');
		});
	}
    
    //init
    function videoBGInit(){
	    setTimeout(function(){
	    	resizeVideoToCover();
	    	$('.video-color-overlay').each(function(){
	    		$(this).css('background-color',$(this).attr('data-color'));
	    	});
	    	$('.nectar-video-wrap').each(function(i){

	    		if($(this).find('video').length == 0) return;

	    		var $headerVideo = ($(this).parents('#page-header-bg').length > 0) ? true : false;
	    		var $that = $(this);

	    		 var videoReady = setInterval(function(){

	        		if($that.find('video').get(0).readyState > 3) {
								
								if(!nectarDOMInfo.usingMobileBrowser) {
		        			$that.transition({'opacity':'1'},400);
				    			$that.find('video').transition({'opacity':'1'},400);
				    			$that.parent().find('.video-color-overlay').transition({'opacity':'0.7'},400);
									if($headerVideo == true) {
					    			pageHeaderTextEffect();
								  }
								}

							//remove page loading screen
							$('#ajax-loading-screen').addClass('loaded');
							setTimeout(function(){ $('#ajax-loading-screen').addClass('hidden'); },1000);
					

							clearInterval(videoReady);
						}
						
	    		},60);
					
					if(nectarDOMInfo.usingMobileBrowser) {
						if($that.parents('.full-width-section').length > 0) {
							$that.css('left','50%');
						} else {
							$that.css('left','0px');
						}
						
						if($headerVideo == true) {
							pageHeaderTextEffect();
						}
						$that.find('video')[0].onplay = function(){
							$that.transition({'opacity':'1'},400);
							$that.find('video').transition({'opacity':'1'},400);
							$that.parent().find('.video-color-overlay').transition({'opacity':'0.7'},400);
						};
					}
	    		
	    	});
	    },300);

		if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)){

				
				$('.nectar-video-wrap').each(function(){
					
						if(!$(this).find('video').is('[muted]')) {

								//autoplay not supported unless muted
								$(this).parent().find('.mobile-video-image').show();
								$(this).remove();
						}
						
				});
		
		
		}

		
		 if(navigator.userAgent.indexOf('Chrome') > 0 && !/Edge\/12./i.test(navigator.userAgent) && !/Edge\/\d./i.test(navigator.userAgent)) { 
		 	
		 	$('.nectar-video-wrap').each(function(i){
		 		if(jQuery(this).find('video source[type="video/webm"]').length > 0 ) {
				  	var webmSource = jQuery(this).find('video source[type="video/webm"]').attr('src') + "?id="+Math.ceil(Math.random()*10000);
		          	var firstVideo = jQuery(this).find('video').get(0);
		          	firstVideo.src = webmSource;
		          	firstVideo.load();
		         }
            });
	    }




	    jQuery(".vc_row").each(function() {
	        var youtubeUrl, youtubeId, $row = jQuery(this);
	        $row.find('.nectar-youtube-bg').length > 0 ? (youtubeUrl = $row.find('.nectar-youtube-bg span').text(), youtubeId = nectarExtractYoutubeId(youtubeUrl), youtubeId && ($row.find(".vc_video-bg").remove(), nectarInsertYoutubeVideoAsBackground($row.find('.nectar-youtube-bg'), youtubeId))) : $row.find(".nectar-youtube-bg").remove()

	        //remove yt url
	        $row.find('.nectar-youtube-bg span').remove();
					
					if(!nectarDOMInfo.usingMobileBrowser) {
		        $row.find('.nectar-video-wrap, .nectar-youtube-bg').css({'opacity':'1','width':'100%', 'height':'100%'});
					}
	         $row.find('.video-color-overlay').transition({'opacity':'0.7'},400);
	    });
		

		function nectarInsertYoutubeVideoAsBackground($element, youtubeId, counter) {
		    if ("undefined" == typeof YT || void 0 === YT.Player) return 100 < (counter = void 0 === counter ? 0 : counter) ? void console.warn("Too many attempts to load YouTube api") : void setTimeout(function() {
		        nectarInsertYoutubeVideoAsBackground($element, youtubeId, counter++)
		    }, 100);
		    var $container = $element.prepend('<div class="vc_video-bg"><div class="inner"></div></div>').find(".inner");
		    new YT.Player($container[0], {
		        width: "100%",
		        height: "100%",
		        videoId: youtubeId,
		        playerVars: {
		            playlist: youtubeId,
		            iv_load_policy: 3,
		            enablejsapi: 1,
		            disablekb: 1,
		            autoplay: 1,
		            controls: 0,
		            showinfo: 0,
		            rel: 0,
		            loop: 1
		        },
		        events: {
		            onReady: function(event) {
		                event.target.mute().setLoop(!0);
										nectarResizeVideoBackground($element);
		            }
		        }
		    }), nectarResizeVideoBackground($element), jQuery(window).bind("resize", function() {
		        nectarResizeVideoBackground($element);
		    });
				
				setTimeout(function(){
					 nectarResizeVideoBackground($element);
				},100);
		}

		function nectarResizeVideoBackground($element) {
		    var iframeW, iframeH, marginLeft, marginTop, containerW = $element.innerWidth(),
		        containerH = $element.innerHeight(),
		        ratio1 = 16,
		        ratio2 = 9;
		    ratio1 / ratio2 > containerW / containerH ? (iframeW = containerH * (ratio1 / ratio2), 
		    	iframeH = containerH, marginLeft = -Math.round((iframeW - containerW) / 2) + "px", marginTop = -Math.round((iframeH - containerH) / 2) + "px", iframeW += "px", iframeH += "px") : (iframeW = containerW, iframeH = containerW * (ratio2 / ratio1), marginTop = -Math.round((iframeH - containerH) / 2) + "px", 
		    	marginLeft = -Math.round((iframeW - containerW) / 2) + "px", iframeW += "px", iframeH += "px"), 
		    	$element.find(".vc_video-bg iframe").css({
			        maxWidth: "1000%",
			        marginLeft: marginLeft,
			        marginTop: marginTop,
			        width: iframeW,
			        height: iframeH
			    })
		}

		function nectarExtractYoutubeId(url) {
		    if ("undefined" == typeof url) return !1;
		    var id = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
		    return null !== id ? id[1] : !1
		}


	}

	if($('.nectar-video-wrap').length > 0 || $('.nectar-youtube-bg').length > 0) {
		videoBGInit();
	}


/*-------------------------------------------------------------------------*/
/*	4.	Header + Search
/*-------------------------------------------------------------------------*/	 

////mobile megamenus without titles / nested groupings
$mobileNavSelector = ($('.off-canvas-menu-container.mobile-only').length > 0) ? '.off-canvas-menu-container.mobile-only ': '#mobile-menu .container ';
$($mobileNavSelector+'.megamenu > ul > li > a').each(function(){
	if( $(this).text() == '–' ) {
		$navLIs = $(this).parent().find('> ul > li').clone();
		$(this).parent().find('ul').remove();
		$(this).parent().parent().append($navLIs);
		$(this).parent().remove();
	}
});

/***************** Slide Out Widget Area **********/

var $bodyBorderHeaderColorMatch = ($('.body-border-top').css('background-color') == '#ffffff' && $('body').attr('data-header-color') == 'light' || $('.body-border-top').css('background-color') == $('#header-outer').attr('data-user-set-bg')) ? true : false;
var $bodyBorderWidth = ($('.body-border-right').length > 0) ? $('.body-border-right').width() : 0;
var $resetHeader;


function mobileBreakPointCheck() {
	var $mobileBreakpoint = ( $('body[data-header-breakpoint]').length > 0 && $('body').attr('data-header-breakpoint') != '1000' ) ? parseInt($('body').attr('data-header-breakpoint')) : 1000;
	var $withinCustomBreakpoint = false;

	if($mobileBreakpoint != 1000) {
		if( $('body[data-user-set-ocm="1"][data-slide-out-widget-area-style="slide-out-from-right-hover"]').length == 0 && window.innerWidth > 1000 && window.innerWidth <= $mobileBreakpoint ) {
			$withinCustomBreakpoint = true;
		}
	}

	return $withinCustomBreakpoint;
}

//icon effect html creation
if($('#slide-out-widget-area.slide-out-from-right-hover').length > 0) {

	if($('#ajax-content-wrap > .slide-out-widget-area-toggle').length == 0) {
		$('<div class="slide-out-widget-area-toggle slide-out-hover-icon-effect" data-icon-animation="simple-transform"><div> <a href="#sidewidgetarea" class="closed"> <span> <i class="lines-button x2"> <i class="lines"></i> </i> </span> </a> </div> </div>').insertAfter('#slide-out-widget-area');
		if($('#header-outer[data-has-menu="true"]').length > 0 || $('body[data-header-search="true"]').length > 0) $('#ajax-content-wrap > .slide-out-widget-area-toggle').addClass('small');
	}


	function calculateHoverNavMinHeight() {
		$widgetHeights = 0;
		$('#slide-out-widget-area > .widget').each(function(){
			$widgetHeights += $(this).height();
		});
		$menuHeight = ( ($('#slide-out-widget-area').height() - 25 - $('.bottom-meta-wrap').outerHeight(true) -$widgetHeights) > $('#slide-out-widget-area .off-canvas-menu-container:last-child').height() ) ? $('#slide-out-widget-area').height() - 25 - $('.bottom-meta-wrap').outerHeight(true) -$widgetHeights : $('#slide-out-widget-area .off-canvas-menu-container:last-child').height();

		$('#slide-out-widget-area .inner').css({'height':'auto', 'min-height': $menuHeight  });

		$('#slide-out-widget-area.slide-out-from-right-hover > .inner .off-canvas-menu-container').transition({ y : '-' + ($('#slide-out-widget-area.slide-out-from-right-hover > .inner .off-canvas-menu-container:last-child').height()/2) + 'px' },0);
	
	}

	function openRightHoverNav() {

		

			calculateHoverNavMinHeight();

			if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/) && $('#slide-out-widget-area.open').length > 0) {
				mobileCloseNavCheck();
				return;
			}
			

			$('#slide-out-widget-area').css({ 'transform': 'translate3d(0,0,0)' }).addClass('open');

			var $bodyBorderHeaderColorMatch = ($('.body-border-top').length > 0 && $('.body-border-top').css('background-color') == '#ffffff' && $('body').attr('data-header-color') == 'light' || $('.body-border-top').length > 0 && $('.body-border-top').css('background-color') == $('#header-outer').attr('data-user-set-bg')) ? true : false;

			//icon effect
			////set pos 
			if($('header#top .container .span_9 > .slide-out-widget-area-toggle').length > 0) {
				
				var secondaryBarHeight = ($('#header-secondary-outer').length > 0) ? $('#header-secondary-outer').height() : 0; 
				

				if($('body.mobile').length > 0) {

					$('.slide-out-hover-icon-effect').css({'top': $('header#top .span_9 > .slide-out-widget-area-toggle a').offset().top - $(window).scrollTop(), 'right': parseInt($('#header-outer header > .container').css('padding-right')) + 1 });
				} else {

					if($bodyBorderHeaderColorMatch) {

						var $extraCushion = ($('#header-outer[data-has-menu="false"]').length > 0) ? 2 : 1;
						$('.slide-out-hover-icon-effect').css({
							'top': nectarDOMInfo.adminBarHeight + secondaryBarHeight + parseInt($('header#top nav >ul .slide-out-widget-area-toggle a').css('padding-top')), 
							'right': 29 + $extraCushion });

					} else {

						var $withinCustomBreakpoint = mobileBreakPointCheck();

						if($('body.ascend').length > 0 && $withinCustomBreakpoint != true) {
							var $extraCushion = ($('#header-outer[data-has-menu="false"]').length > 0) ? 2 : 1;
							$('.slide-out-hover-icon-effect').css({'top': nectarDOMInfo.adminBarHeight + secondaryBarHeight + parseInt($('header#top nav >ul .slide-out-widget-area-toggle a').css('padding-top')), 'right': parseInt($('#header-outer header >.container').css('padding-right')) + $extraCushion });
						} else {

							if($('body.material').length > 0) {
								if($('#header-outer[data-format="centered-menu-bottom-bar"]').length > 0) {
									var $nectarHamMenuPos = ($('header#top .span_9 > .slide-out-widget-area-toggle').css('display') == 'block') ? $('#header-outer header#top > .container .span_9 > .slide-out-widget-area-toggle.mobile-icon').position() : $('header#top .span_3 .right-side .slide-out-widget-area-toggle > div').offset();
									if($('#header-secondary-outer.hide-up').length > 0) { secondaryBarHeight = 0; }
									$('.slide-out-hover-icon-effect').css({'top': secondaryBarHeight + parseInt($nectarHamMenuPos.top) - nectarDOMInfo.scrollTop , 'right': parseInt($('#header-outer header >.container').css('padding-right')) + 2  });
								} else {
									var $nectarHamMenuPos = ($('header#top .span_9 > .slide-out-widget-area-toggle').css('display') == 'block') ? $('#header-outer header#top > .container .span_9 > .slide-out-widget-area-toggle.mobile-icon').position() : $('header#top nav .buttons .slide-out-widget-area-toggle > div').position();
									if($('#header-secondary-outer.hide-up').length > 0) { secondaryBarHeight = 0; }
									$('.slide-out-hover-icon-effect').css({'top': nectarDOMInfo.adminBarHeight + secondaryBarHeight + parseInt($nectarHamMenuPos.top) , 'right': parseInt($('#header-outer header >.container').css('padding-right')) + 2  });
								}
								
								
							} else {
								$menuToggleSelector = ($('header#top nav > ul .slide-out-widget-area-toggle').length > 0 && $('header#top nav > ul .slide-out-widget-area-toggle').css('display') != 'none') ? $('header#top nav > ul .slide-out-widget-area-toggle') : $('body header#top .span_9 >.slide-out-widget-area-toggle');
								$('.slide-out-hover-icon-effect').css({'top': nectarDOMInfo.adminBarHeight + secondaryBarHeight + parseInt($menuToggleSelector.css('padding-top')) + parseInt($('#header-outer').css('padding-top')), 'right': parseInt($('#header-outer header >.container').css('padding-right')) + 1 + parseInt($menuToggleSelector.css('margin-right')) });
							}
						}
					}
					
				}
			}

			////open

			$('.slide-out-hover-icon-effect .lines-button').removeClass('no-delay').addClass('unhidden-line');
			

			if($('#header-outer[data-permanent-transparent="1"]').length == 0 && $('#nectar_fullscreen_rows').length == 0) {

				if(!($(window).scrollTop() == 0 && $('#header-outer.transparent').length > 0)) {

					if($('body.mobile').length == 0 && $bodyBorderHeaderColorMatch) {

						$('#header-outer').attr('data-transparent','true').addClass('no-bg-color').addClass('slide-out-hover');
						$('#header-outer header, #header-outer > .cart-outer').addClass('all-hidden');
					}

				}

				if($('#header-outer[data-remove-fixed="1"]').length == 0 && $('body.mobile').length == 0 && $bodyBorderHeaderColorMatch) {
					var headerResize = $('#header-outer').attr('data-header-resize');
					if(headerResize == 1) {

						$(window).off('scroll',bigNav);
						$(window).off('scroll',smallNav);


					} else {
						
						$(window).off('scroll',opaqueCheck);
						$(window).off('scroll',transparentCheck);
					}
				}

			}

			if(!navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)) {
				$(window).on('mousemove.rightOffsetCheck',closeNavCheck);
			}

	}

	function closeNavCheck(e) {
		var $windowWidth = $(window).width();
		if(e.clientX < $windowWidth - 340 - $bodyBorderWidth) {

				$(window).off('mousemove.rightOffsetCheck',closeNavCheck);

				$('#slide-out-widget-area').css({ 'transform': 'translate3d(341px,0,0)' }).removeClass('open');

				$('#header-outer').removeClass('style-slide-out-from-right');

				$('.slide-out-hover-icon-effect .lines-button').removeClass('unhidden-line').addClass('no-delay');

				var $bodyBorderHeaderColorMatch = ($('.body-border-top').length > 0 && $('.body-border-top').css('background-color') == '#ffffff' && $('body').attr('data-header-color') == 'light' || $('.body-border-top').length > 0 && $('.body-border-top').css('background-color') == $('#header-outer').attr('data-user-set-bg')) ? true : false;

				if($('#header-outer[data-permanent-transparent="1"]').length == 0) {

					if($('#header-outer[data-remove-fixed="1"]').length == 0 && $('body.mobile').length == 0 && $bodyBorderHeaderColorMatch) {

						if($('body.mobile').length == 0) {
							$('#header-outer').removeClass('no-bg-color');
							$('#header-outer header, #header-outer > .cart-outer').removeClass('all-hidden');
						}
					}

					if($('#header-outer[data-remove-fixed="1"]').length == 0 && $('body.mobile').length == 0 && $bodyBorderHeaderColorMatch) {
						var headerResize = $('#header-outer').attr('data-header-resize');
						if(headerResize == 1) {
						
							$(window).off('scroll.headerResizeEffect');
							if($(window).scrollTop() == 0) {
								$(window).on('scroll.headerResizeEffect',smallNav); 

								if($('#header-outer[data-full-width="true"][data-transparent-header="true"]').length > 0 && $('.body-border-top').length > 0 && $bodyBorderHeaderColorMatch == true && $('#header-outer.pseudo-data-transparent').length > 0) {
									$('#header-outer[data-full-width="true"] header > .container').stop(true,true).animate({
										'padding' : '0'			
									},{queue:false, duration:250, easing: 'easeOutCubic'});	
								}
							}
							else {
								$(window).on('scroll.headerResizeEffect',bigNav);
							}
							
				
						} else {
							
							$(window).off('scroll.headerResizeEffectOpaque');
							$(window).on('scroll.headerResizeEffectOpaque',opaqueCheck);
						}
					}
				}

			}

	}

	function mobileCloseNavCheck(e) {
		

				$('#slide-out-widget-area').css({ 'transform': 'translate3d(341px,0,0)' }).removeClass('open');

				$('#header-outer').removeClass('style-slide-out-from-right');

				$('.slide-out-hover-icon-effect .lines-button').removeClass('unhidden-line').addClass('no-delay');

				if($('#header-outer[data-permanent-transparent="1"]').length == 0) {

					$('#header-outer').removeClass('no-bg-color');
					$('#header-outer header').removeClass('all-hidden');

				}

				var $bodyBorderHeaderColorMatch = ($('.body-border-top').length > 0 && $('.body-border-top').css('background-color') == '#ffffff' && $('body').attr('data-header-color') == 'light' || $('.body-border-top').length > 0 && $('.body-border-top').css('background-color') == $('#header-outer').attr('data-user-set-bg')) ? true : false;

				if($('#header-outer[data-remove-fixed="1"]').length == 0 && $('body.mobile').length == 0 && $bodyBorderHeaderColorMatch) {
					
					var headerResize = $('#header-outer').attr('data-header-resize');
					if(headerResize == 1) {
					
						$(window).off('scroll.headerResizeEffect');
						if($(window).scrollTop() == 0) {
							$(window).on('scroll.headerResizeEffect',smallNav); 

							if($('#header-outer[data-full-width="true"][data-transparent-header="true"]').length > 0 && $('.body-border-top').length > 0 && $bodyBorderHeaderColorMatch == true && $('#header-outer.pseudo-data-transparent').length > 0) {
								$('#header-outer[data-full-width="true"] header > .container').stop(true,true).animate({
									'padding' : '0'			
								},{queue:false, duration:250, easing: 'easeOutCubic'});	
							}
						}
						else {
							$(window).on('scroll.headerResizeEffect',bigNav);
						}
						
			
					} else {
						
						$(window).off('scroll.headerResizeEffectOpaque');
						$(window).on('scroll.headerResizeEffectOpaque',opaqueCheck);
					}
			}

			

	}

	//hover triggered
	if(!navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)) {
		$('body').on('mouseenter','#header-outer .slide-out-widget-area-toggle:not(.std-menu) a',openRightHoverNav);
	}
	else {
		$('body').on('click','.slide-out-widget-area-toggle:not(.std-menu) a',openRightHoverNav);
	}

	$(window).on('smartresize',calculateHoverNavMinHeight);

}



function setMaterialWidth() {
	
	
	$('#slide-out-widget-area.slide-out-from-right').css({
		'padding-top' : $(window).height()*0.1,
		'padding-bottom' : $(window).height()*0.1
	});

	slideOutWidgetOverflowState();
}

if($('body.material[data-slide-out-widget-area-style="slide-out-from-right"]').length > 0) {
	setMaterialWidth();
}

//icon hover effect
if($('body.material').length > 0 && $('body[data-slide-out-widget-area-style="slide-out-from-right-hover"]').length == 0) {

	if($('body[data-slide-out-widget-area-style*="fullscreen"]').length == 0) {
		var $menuIconClone = $('header#top nav ul .slide-out-widget-area-toggle a > span > i').clone();
		$menuIconClone.addClass('hover-effect');
		$('header#top nav ul .slide-out-widget-area-toggle a > span').append($menuIconClone);

		var $menuIconClone2 = $('header#top .slide-out-widget-area-toggle.mobile-icon a > span > i').clone();
		$menuIconClone2.addClass('hover-effect');
		$('header#top .slide-out-widget-area-toggle.mobile-icon a > span').append($menuIconClone2);
	}

	$('body:not([data-slide-out-widget-area-style="slide-out-from-right"]) header#top .slide-out-widget-area-toggle a > span').append($('<span class="close-wrap"> <span class="close-line close-line1"></span> <span class="close-line close-line2"></span> </span>'));
}

if($('body.material #boxed').length > 0 && $('body[data-slide-out-widget-area-style="slide-out-from-right-hover"]').length > 0) {
	$('#ajax-content-wrap > .slide-out-widget-area-toggle.slide-out-hover-icon-effect.small').insertAfter('.ocm-effect-wrap');
}

//move material skin default ocm
if($('body.material').length > 0 && $('body[data-slide-out-widget-area-style*="fullscreen"]').length == 0) {
	$('body.material #slide-out-widget-area.slide-out-from-right .slide_out_area_close').insertAfter('.ocm-effect-wrap');
	$('#slide-out-widget-area-bg').insertAfter('.ocm-effect-wrap');
	$('#slide-out-widget-area').insertAfter('.ocm-effect-wrap');
	
}

//remove trans on material search/ocm when resizing 
if($('body.material[data-header-search="true"]').length > 0 || $('body.material .ocm-effect-wrap').length > 0 ) {
		
	var materialTransTO;

	$(window).resize(function(){

		clearTimeout(materialTransTO);

		$('body[data-slide-out-widget-area-style="slide-out-from-right"] > a.slide_out_area_close, .material #header-outer, .ocm-effect-wrap, .ocm-effect-wrap-shadow').addClass('no-material-transition');

		materialTransTO = setTimeout(function(){
			$('body[data-slide-out-widget-area-style="slide-out-from-right"] > a.slide_out_area_close, .material #header-outer, .ocm-effect-wrap, .ocm-effect-wrap-shadow').removeClass('no-material-transition');
		},250);

		setMaterialWidth();

	});
}


function materialOCM_Size() {
	if($('.ocm-effect-wrap.material-ocm-open').length > 0 ) {

		//$('#ajax-content-wrap').css({'position' : 'relative', 'top' : '-' + $(window).scrollTop() + 'px' });
		$('.ocm-effect-wrap').css({'height': $(window).height() });
		$('.ocm-effect-wrap-inner').css({'padding-top': nectarDOMInfo.adminBarHeight });
		
		
	}
}
$(window).resize(materialOCM_Size);


function OCM_dropdown_function() {
	var $nectar_ocm_dropdown_func = ($('#slide-out-widget-area[data-dropdown-func]').length > 0) ? $('#slide-out-widget-area').attr('data-dropdown-func') : 'default';
	if($nectar_ocm_dropdown_func == 'separate-dropdown-parent-link') {
		$('#slide-out-widget-area .off-canvas-menu-container li.menu-item-has-children').append('<span class="ocm-dropdown-arrow"><i class="fa-angle-down"></i></span>');
	}
}

OCM_dropdown_function();

//click triggered
$('body').on('click','.slide-out-widget-area-toggle:not(.std-menu) a.closed:not(.animating)',function(){
	if(animating == 'true' || $('.slide-out-from-right-hover').length > 0) return false;
	var $that = $(this);

	$('#header-outer').removeClass('no-transition');

	//slide out from right
	if($('#slide-out-widget-area').hasClass('slide-out-from-right')) {

		$slideOutAmount = ($('.body-border-top').length > 0 && $('body.mobile').length == 0) ? $('.body-border-top').height() : 0;


		if($('body.material').length == 0) {

				//calc height if used bottom meta
				$('#slide-out-widget-area .inner').css({'height':'auto', 'min-height': $('#slide-out-widget-area').height() - 25 - $('.bottom-meta-wrap').height() });

				if($('#boxed').length == 0) {
					$('.container-wrap, .home-wrap, #header-secondary-outer, #footer-outer:not(#nectar_fullscreen_rows #footer-outer), .nectar-box-roll,   #page-header-wrap .page-header-bg-image,  #page-header-wrap .nectar-video-wrap, #page-header-wrap .mobile-video-image, #page-header-wrap #page-header-bg > .container, .page-header-no-bg, div:not(.container) > .project-title').stop(true).transition({ x: '-300px' },700,'easeInOutCubic');

					var $withinCustomBreakpoint = mobileBreakPointCheck();

					if($('#header-outer[data-format="centered-logo-between-menu"]').length == 0 || $withinCustomBreakpoint) {
						if($('#header-outer[data-transparency-option="1"]').length == 0 || ($('#header-outer[data-transparency-option="1"]').length > 0 && $('#header-outer[data-full-width="true"]').length == 0) || $('body.mobile').length > 0) {
							$('#header-outer').stop(true).css('transform','translateY(0)').transition({ x: '-' + (300+$slideOutAmount) +'px'},700,'easeInOutCubic');
						} else {
							$('#header-outer').stop(true).css('transform','translateY(0)').transition({ x: '-' + (300+$slideOutAmount) +'px', 'background-color':'transparent', 'border-bottom': '1px solid rgba(255,255,255,0.22)' },700,'easeInOutCubic');
						}
					} else {
						$('#header-outer header#top nav > ul.buttons, body:not(.material) #header-outer .cart-outer .cart-menu-wrap').transition({ x: '-300px'},700,'easeInOutCubic');
					}

					$('#ascrail2000').transition({ 'x': '-' + (300+$slideOutAmount) +'px' },700,'easeInOutCubic');
					$('body:not(.ascend):not(.material) #header-outer .cart-menu').stop(true).transition({ 'x': '300px' },700,'easeInOutCubic');
				}

				$('#slide-out-widget-area').stop(true).transition({ x: '-' + $slideOutAmount +'px' },700,'easeInOutCubic').addClass('open');


				if($('#boxed').length == 0) {
					//full width menu adjustments
					if($('#header-outer[data-full-width="true"]').length > 0 && !$('body').hasClass('mobile')) { 
						$('#header-outer').addClass('highzI'); 
						$('#ascrail2000').addClass('z-index-adj');

						if($('#header-outer[data-format="centered-logo-between-menu"]').length == 0) {
							if($bodyBorderWidth == 0)
								$('header#top #logo').stop(true).transition({ x: (300+$slideOutAmount) +'px' },700,'easeInOutCubic'); 
							
							//$('header#top nav > ul > li.megamenu > ul.sub-menu').stop(true).transition({'width': $(window).width() - 360, 'left': '300px' },700,'easeInOutCubic');

						}

						$('header#top .slide-out-widget-area-toggle .lines-button').addClass('close');

						if($('#header-outer[data-remove-border="true"]').length > 0) {
							$('body:not(.ascend) #header-outer[data-full-width="true"] header#top nav > ul.product_added').stop(true).transition({ x: '64px' },700,'easeInOutCubic');
						} else {
							$('body:not(.ascend) #header-outer[data-full-width="true"] header#top nav > ul.product_added').stop(true).transition({ x: '89px' },700,'easeInOutCubic'); 
						}

						$('body #header-outer nav > ul > li > a').css({'margin-bottom':'0'});
						
					}
				}

				$('#header-outer').addClass('style-slide-out-from-right');

				//fade In BG Overlay
				$('#slide-out-widget-area-bg').css({'height':'100%','width':'100%'}).stop(true).transition({
					'opacity' : 1
				},700,'easeInOutCubic',function(){
					$('.slide-out-widget-area-toggle:not(.std-menu) > div > a').removeClass('animating');
				});
				
				//hide menu if no space
				if($('#header-outer[data-format="centered-logo-between-menu"]').length == 0) {
					$logoWidth = ($('#logo img:visible').length > 0) ? $('#logo img:visible').width() : $('#logo').width();
					if($('header#top nav > .sf-menu').offset().left - $logoWidth - 300 < 20) $('#header-outer').addClass('hidden-menu');
				} else {
					$('#header-outer').addClass('hidden-menu-items');
				}

				var headerResize = $('#header-outer').attr('data-header-resize');

				if($('#header-outer[data-remove-fixed="1"]').length == 0) {
					if($bodyBorderHeaderColorMatch == true && headerResize == 1) {
						
						$('#header-outer').stop(true).transition({ y: '0' },0).addClass('transparent').css('transition','transform');
						if($('#header-outer').attr('data-transparent-header') != 'true') {
							$('#header-outer').attr('data-transparent-header','true').addClass('pseudo-data-transparent');
						}

						$(window).off('scroll',bigNav);
						//if($('.small-nav').length > 0 || $('#header-outer').hasClass('pseudo-data-transparent')) bigNav();
						$(window).off('scroll',smallNav);

					} else if ($bodyBorderHeaderColorMatch == true) {
						$('#header-outer').addClass('transparent');
						$(window).off('scroll',opaqueCheck);
						$(window).off('scroll',transparentCheck);

						if($('#header-outer').attr('data-transparent-header') != 'true') {
							$('#header-outer').attr('data-transparent-header','true').addClass('pseudo-data-transparent');
						}
					}
				}

		} else if ( $('body.material').length > 0 ) {

			//material
			
			//move ajax loading outside
			if($('#ajax-loading-screen').length > 0 && $('.ocm-effect-wrap #ajax-loading-screen').length > 0) {
				$('#ajax-loading-screen').insertBefore('.ocm-effect-wrap');
			}
			
			////hide secondary header when not at top with hhun
			if($(window).scrollTop() > 40) {
				
				$('body[data-hhun="1"] #header-secondary-outer').addClass('hidden');
			}


			setTimeout(function(){ $('.slide-out-widget-area-toggle:not(.std-menu) > div > a').removeClass('animating'); },300);
			$('#slide-out-widget-area, #slide-out-widget-area-bg, #header-outer .slide-out-widget-area-toggle').addClass('material-open');


			//handle bottom bar nav
			if($('body:not(.mobile) #header-outer[data-format="centered-menu-bottom-bar"][data-condense="true"]').length > 0  && $('#header-outer[data-format="centered-menu-bottom-bar"] .span_9').css('display') != 'none') {
				$('#header-outer:not(.fixed-menu)').css('top', nectarDOMInfo.adminBarHeight - $(window).scrollTop() + 'px' );
				
				if($('#header-secondary-outer').length > 0  && $('#header-outer.fixed-menu').length > 0) {
					$('#header-secondary-outer').css('visibility','hidden');
				}
				
			}
			
			$('#ajax-content-wrap').css({'position' : 'relative', 'top' : '-' + $(window).scrollTop() + 'px' });
			$('.ocm-effect-wrap-inner').css({'padding-top': nectarDOMInfo.adminBarHeight });
			$('#fp-nav').addClass('material-ocm-open');
			$('body').addClass('material-ocm-open');
			$('.ocm-effect-wrap').css({'height': window.innerHeight });

			setTimeout(function(){
				$('.ocm-effect-wrap').addClass('material-ocm-open');
			},40);
			
			
			$('body > .slide_out_area_close').addClass('follow-body');

			//icon effect
			$('#header-outer:not([data-format="left-header"]) header#top .slide-out-widget-area-toggle a').addClass('effect-shown');

			//handle hhun when at top
			$('body[data-hhun="1"]:not(.no-scroll):not(.mobile) #header-outer[data-permanent-transparent="false"]:not(.detached):not(.parallax-contained):not(.at-top-before-box)').css({'transition':'none', 'transform':'translateY('+ nectarDOMInfo.adminBarHeight +'px)'});

			setTimeout(function(){
				$('body > .slide_out_area_close').addClass('material-ocm-open');
			},350);


		}


	}
     else if($('#slide-out-widget-area').hasClass('fullscreen')) {

     	if ( $('body.material').length > 0 ) {
     		$('header#top .slide-out-widget-area-toggle a').addClass('menu-push-out');
     	}

		//scroll away from fixed reveal footer if shown (firefox bug with bluring over it)
		var $scrollDelay = 0;
		var $scrollDelay2 = 0;

		if($(window).scrollTop() + $(window).height() > $('.blurred-wrap').height() && $('#nectar_fullscreen_rows').length == 0) {
			$('body,html').stop().animate({
				scrollTop: $('.blurred-wrap').height() - $(window).height()
			},600,'easeInOutCubic');
			$scrollDelay = 550;
			$scrollDelay2 = 200;
		}

		$('header#top .slide-out-widget-area-toggle:not(.std-menu) .lines-button').addClass('close');
		setTimeout(function(){ $('.blurred-wrap').addClass('blurred'); },$scrollDelay);
		$('#slide-out-widget-area.fullscreen').show().addClass('open');

		if($('.nectar-social-sharing-fixed').length == 0) {
			hideToTop();
		}

		//remove box shadow incase at the top of the page with nectar box roll above
		$('.container-wrap').addClass('no-shadow');
		$('#header-outer').stop(true).css('transform','translateY(0)');

		setTimeout(function(){

			$('.off-canvas-menu-container .menu > li').each(function(i){
				$(this).delay(i*50).transition({y: 0, 'opacity': 1},800,'easeOutExpo');
			});

			$('#slide-out-widget-area.fullscreen .widget').each(function(i){
				$(this).delay(i*100).transition({y: 0, 'opacity': 1},800,'easeOutExpo');
			});
		},370+$scrollDelay2);

		setTimeout(function(){
			$('#slide-out-widget-area .off-canvas-social-links').addClass('line-shown');

			$('#slide-out-widget-area .off-canvas-social-links li').each(function(i){
				$(this).delay(i*50).transition({'scale':1},400,'easeOutCubic');
			});
			$('#slide-out-widget-area .bottom-text').transition({'opacity':0.7},400,'easeOutCubic');
		},750+$scrollDelay2);
		
		//fade In BG Overlay
		setTimeout(function(){
			$easing = ($('body.mobile').length > 0) ? 'easeOutCubic' : 'easeInOutQuint';
			$('#slide-out-widget-area-bg').css({'height':'100%','width':'100%'}).show().stop(true).transition({
				'y' : '0%'
			},920,$easing,function(){
				$('.slide-out-widget-area-toggle > div > a').removeClass('animating');
			});
		},50+$scrollDelay2);

		//overflow state 
		slideOutWidgetOverflowState();
		if($('.mobile #header-outer[data-permanent-transparent="false"]').length > 0 && $('.container-wrap').hasClass('no-scroll')) $('#ajax-content-wrap').addClass('at-content');
		if($('.mobile #header-outer[data-permanent-transparent="false"]').length > 0 || $('.mobile').length == 0 && $('#header-outer.transparent').length == 0) $('#slide-out-widget-area.fullscreen .inner-wrap').css('padding-top', $('#header-outer').height());
	} 

	else if($('#slide-out-widget-area').hasClass('fullscreen-alt')) {

		if ( $('body.material').length > 0 ) {
     		$('header#top .slide-out-widget-area-toggle a').addClass('menu-push-out');
     	}

		$('header#top .slide-out-widget-area-toggle:not(.std-menu) .lines-button').addClass('close');
		$('#slide-out-widget-area.fullscreen-alt').show().addClass('open');
		$('#slide-out-widget-area-bg').addClass('open');

		$('body > div[class*="body-border"]').css('z-index','9995');

		$('.off-canvas-menu-container .menu').transition({y: '0px', 'opacity': 1},0);	

		if($('.nectar-social-sharing-fixed').length == 0) {
			hideToTop();
		}

		if($('#header-outer.transparent').length == 0) {


		}
		else { 

			//$('#header-outer-bg-only').hide();
			if($('.body-border-top').length > 0) {
				$('.admin-bar #slide-out-widget-area-bg.fullscreen-alt').css({'padding-top': ($('.body-border-top').outerHeight(true)+32) + 'px'});
				$('body:not(.admin-bar) #slide-out-widget-area-bg.fullscreen-alt').css({'padding-top': ($('.body-border-top').outerHeight(true))+ 'px'});
			}
		}
		
		//set translateY to 0 in all cases
		$('#header-outer').stop(true).css('transform','translateY(0)');

		if($('#logo .starting-logo').length > 0 && $(window).width() > 1000 && $('#header-outer[data-format="centered-menu-bottom-bar"].fixed-menu').length == 0 && $('body.material #header-outer[data-condense="true"]').length == 0) {
			
			//$('#header-outer').addClass('no-transition').addClass('no-bg-color');
			
			$('#header-outer').stop(true).css('transform','translateY(0)').addClass('transparent');
			if($('#header-outer').attr('data-transparent-header') != 'true') {
				$('#header-outer').attr('data-transparent-header','true').addClass('pseudo-data-transparent');
			}
		}

		$('.off-canvas-menu-container .clip-wrap').css('transition-duration','0s');

		setTimeout(function(){

			$('.off-canvas-menu-container .menu > li').each(function(i){
				$(this).delay(i*50).transition({y: 0, 'opacity': 1},750,'easeOutCubic').addClass('no-pointer-events');
			});

			setTimeout(function(){
				$('.off-canvas-menu-container .menu > li').removeClass('no-pointer-events');
				$('.off-canvas-menu-container .clip-wrap').css('transition-duration','.45s');
			},500);
			
		

			$('#slide-out-widget-area.fullscreen-alt .widget').each(function(i){
				$(this).delay(i*100).transition({y: 0, 'opacity': 1},650,'easeOutCubic');
			});
		},200);

		setTimeout(function(){
			$('#slide-out-widget-area .off-canvas-social-links').addClass('line-shown');

			$('#slide-out-widget-area .off-canvas-social-links li').css('opacity','1').each(function(i){
				$(this).delay(i*50).transition({'scale':1},400,'easeOutCubic');
			});
			$('#slide-out-widget-area .bottom-text').transition({'opacity':1},600,'easeOutCubic');
		},200);
		
		//fade In BG Overlay
		if($('#slide-out-widget-area-bg').hasClass('solid')) $opacity = 1;
		if($('#slide-out-widget-area-bg').hasClass('dark')) $opacity = 0.97;
		if($('#slide-out-widget-area-bg').hasClass('medium')) $opacity = 0.6;
		if($('#slide-out-widget-area-bg').hasClass('light')) $opacity = 0.4;
		$('#slide-out-widget-area-bg').removeClass('no-transition');
		

		$('#slide-out-widget-area-bg').addClass('padding-removed').css({'height':'100%','width':'100%', 'left':'0','opacity': $opacity});
		

		setTimeout(function(){
			$('.slide-out-widget-area-toggle > div > a').removeClass('animating');
		},600);
			
		

		//overflow state 
		slideOutWidgetOverflowState();
		if($('.mobile #header-outer[data-permanent-transparent="false"]').length > 0 && $('.container-wrap').hasClass('no-scroll')) $('#ajax-content-wrap').addClass('at-content');
		if($('.mobile #header-outer[data-permanent-transparent="false"]').length > 0 || $('.mobile').length == 0 && $('#header-outer.transparent').length == 0) $('#slide-out-widget-area.fullscreen-alt .inner-wrap').css('padding-top', $('#header-outer').height());
	}



	//add open class
	$('#header-outer').removeClass('side-widget-closed').addClass('side-widget-open');

	//give header transparent state
	if($('#header-outer[data-transparency-option="1"]').length > 0 && $('#boxed').length == 0 && $('#header-outer[data-full-width="true"]').length > 0) {
			if($('body.material[data-slide-out-widget-area-style="slide-out-from-right"]').length == 0 && $('body.material #header-outer[data-condense="true"]').length == 0) {
				$('#header-outer').addClass('transparent');
			}
	}

	//dark slide transparent nav
	if($('#header-outer.dark-slide.transparent').length > 0  && $('#boxed').length == 0 && $('body.material-ocm-open').length == 0) $('#header-outer').removeClass('dark-slide').addClass('temp-removed-dark-slide');
	
	$('.slide-out-widget-area-toggle > div > a').removeClass('closed').addClass('open');
	$('.slide-out-widget-area-toggle > div > a').addClass('animating');

	return false;
});

$('body').on('click','.slide-out-widget-area-toggle:not(.std-menu) a.open:not(.animating), #slide-out-widget-area .slide_out_area_close,  > .slide_out_area_close, #slide-out-widget-area-bg.slide-out-from-right, .material-ocm-open #ajax-content-wrap',function(e){

	if (e.originalEvent == undefined && $('.slide_out_area_close.non-human-allowed').length == 0 ) { return; }

	if($('.slide-out-widget-area-toggle:not(.std-menu) a.animating').length > 0) return;

	$('#header-outer').removeClass('no-transition');

	

	var $that = $(this);

	$('.slide-out-widget-area-toggle:not(.std-menu) a').removeClass('open').addClass('closed');
	$('.slide-out-widget-area-toggle:not(.std-menu) a').addClass('animating');

	//slide out from right
	if($('#slide-out-widget-area').hasClass('slide-out-from-right')) {

			if($('body.material').length == 0) {

				$('.container-wrap, .home-wrap, #header-secondary-outer, #footer-outer:not(#nectar_fullscreen_rows #footer-outer), .nectar-box-roll, #page-header-wrap .page-header-bg-image,  #page-header-wrap .nectar-video-wrap, #page-header-wrap .mobile-video-image, #page-header-wrap #page-header-bg > .container, .page-header-no-bg, div:not(.container) > .project-title').stop(true).transition({ x: '0px' },700,'easeInOutCubic');

				if($('#header-outer[data-transparency-option="1"]').length > 0  && $('#boxed').length == 0) {
					$currentRowBG = ($('#header-outer[data-current-row-bg-color]').length > 0) ? $('#header-outer').attr('data-current-row-bg-color') : $('#header-outer').attr('data-user-set-bg');
					$('#header-outer').stop(true).transition({ x: '0px', 'background-color': $currentRowBG },700,'easeInOutCubic');
				} else {
					$('#header-outer').stop(true).transition({ x: '0px' },700,'easeInOutCubic');
				}

				$('#ascrail2000').stop(true).transition({ 'x': '0px' },700,'easeInOutCubic');
				$('body:not(.ascend):not(.material) #header-outer .cart-menu').stop(true).transition({ 'x': '0px' },700,'easeInOutCubic');

				$('#slide-out-widget-area').stop(true).transition({ x: '301px' },700,'easeInOutCubic').removeClass('open');


				if($('#boxed').length == 0) {
					if($('#header-outer[data-full-width="true"]').length > 0) {  
						$('#header-outer').removeClass('highzI'); 
						$('header#top #logo').stop(true).transition({ x: '0px' },700,'easeInOutCubic'); 
						//$('header#top nav > ul > li.megamenu > ul.sub-menu').stop(true).transition({'width': '100%', 'left': '0' },700,'easeInOutCubic');
						$('.lines-button').removeClass('close');

						$('body:not(.ascend) #header-outer[data-full-width="true"] header#top nav > ul.product_added').stop(true).transition({ x: '0px' },700,'easeInOutCubic');

					}
				}

				if($('#header-outer[data-format="centered-logo-between-menu"]').length > 0) {
					$('#header-outer header#top nav > ul.buttons, #header-outer .cart-outer .cart-menu-wrap').stop(true).transition({ x: '0px' },700,'easeInOutCubic'); 
				}

				//fade out overlay
				$('#slide-out-widget-area-bg').stop(true).transition({
					'opacity' : 0
				},700,'easeInOutCubic',function(){
					$('.slide-out-widget-area-toggle a').removeClass('animating');
					$(this).css({'height':'1px','width':'1px'});

					if($('#header-outer[data-remove-fixed="1"]').length == 0) {
						//hide menu if transparent, user has scrolled down and hhun is on
						if($('#header-outer').hasClass('parallax-contained') && $(window).scrollTop() > 0 && $('#header-outer[data-permanent-transparent="1"]').length == 0) {
							$('#header-outer').removeClass('parallax-contained').addClass('detached').removeClass('transparent');
						}
						else if($(window).scrollTop() == 0 && $('body[data-hhun="1"]').length > 0 && $('#page-header-bg[data-parallax="1"]').length > 0 ||
							$(window).scrollTop() == 0 && $('body[data-hhun="1"]').length > 0 && $('.parallax_slider_outer').length > 0) {

							if($('#header-outer[data-transparency-option="1"]').length > 0) $('#header-outer').addClass('transparent');
							$('#header-outer').addClass('parallax-contained').removeClass('detached');
						}
					}

					//fix for fixed subpage menu
					$('.container-wrap').css('transform','none');
				});


				$('#header-outer').removeClass('style-slide-out-from-right');


				var headerResize = $('#header-outer').attr('data-header-resize');
				if($('#header-outer[data-remove-fixed="1"]').length == 0) {

					if($bodyBorderHeaderColorMatch == true && headerResize == 1) {
					
						$(window).off('scroll.headerResizeEffect');
						if($(window).scrollTop() == 0) {
							$(window).on('scroll.headerResizeEffect',smallNav); 

							if($('#header-outer[data-full-width="true"][data-transparent-header="true"]').length > 0 && $('.body-border-top').length > 0 && $bodyBorderHeaderColorMatch == true && $('#header-outer.pseudo-data-transparent').length > 0) {
								$('#header-outer[data-full-width="true"] header > .container').stop(true,true).animate({
									'padding' : '0'			
								},{queue:false, duration:250, easing: 'easeOutCubic'});	
							}
						}
						else
							$(window).on('scroll.headerResizeEffect',bigNav); 
							//smallNav();

						if($('#header-outer').hasClass('pseudo-data-transparent')) {
							$('#header-outer').attr('data-transparent-header','false').removeClass('pseudo-data-transparent').removeClass('transparent');
						}

						$('#header-outer').css('transition','transform');

					} else if ($bodyBorderHeaderColorMatch == true) {
						
						$(window).off('scroll.headerResizeEffectOpaque');
						$(window).on('scroll.headerResizeEffectOpaque',opaqueCheck);

						$('#header-outer').css('transition','transform');

						if($('#header-outer').hasClass('pseudo-data-transparent')) {
							$('#header-outer').attr('data-transparent-header','false').removeClass('pseudo-data-transparent').removeClass('transparent');
						}
					}
				}
		}

		else if ( $('body.material').length > 0 ) {

			//material
			$('#slide-out-widget-area').removeClass('open');

			$('#slide-out-widget-area, #slide-out-widget-area-bg, #header-outer .slide-out-widget-area-toggle').removeClass('material-open');
			$('.ocm-effect-wrap, .ocm-effect-wrap-shadow, body > .slide_out_area_close, #fp-nav').removeClass('material-ocm-open');

			$('body > .slide_out_area_close').removeClass('follow-body');

			setTimeout(function(){
				$('.slide-out-widget-area-toggle a').removeClass('animating');
				$('body').removeClass('material-ocm-open');
				$('.ocm-effect-wrap').css({'height': '100%'});
				$('.ocm-effect-wrap-inner').css({'padding-top': '0' });
				$(window).scrollTop(Math.abs( parseInt($('#ajax-content-wrap').css('top')) ) );
				$('#ajax-content-wrap').css({'position' : '', 'top' : '' });
				

				//handle bottom bar nav
				if($('#header-outer[data-format="centered-menu-bottom-bar"]').length > 0 && $('#header-outer[data-format="centered-menu-bottom-bar"] .span_9').css('display') != 'none' && $('body.mobile').length == 0) {
					$('#header-outer:not(.fixed-menu)').css('top', '');
					$('#header-secondary-outer').css('visibility','');
				}
				
				//handle hhun when at top
				$('body[data-hhun="1"]:not(.no-scroll) #header-outer[data-permanent-transparent="false"]:not(.detached):not(.parallax-contained):not(.at-top-before-box)').css({'transform':''});
				setTimeout(function(){
					$('body[data-hhun="1"]:not(.no-scroll) #header-outer[data-permanent-transparent="false"]:not(.detached):not(.parallax-contained):not(.at-top-before-box)').css({'transition':''});
				},30);
				

				$('body[data-hhun="1"] #header-secondary-outer.hidden').removeClass('hidden');

			},900);

			setTimeout(function(){
				//icon effect
				$('#header-outer:not([data-format="left-header"]) header#top .slide-out-widget-area-toggle a').addClass('no-trans').removeClass('effect-shown');
			},200);

			setTimeout(function(){
				//icon
				$('#header-outer:not([data-format="left-header"]) header#top .slide-out-widget-area-toggle a').removeClass('no-trans')
			},500);
			
		}


	} 

	else if($('#slide-out-widget-area').hasClass('fullscreen')) {


		if ( $('body.material').length > 0 ) {
			setTimeout(function(){
				$('header#top .slide-out-widget-area-toggle a').removeClass('menu-push-out');
			},350);
     	}

		$('.slide-out-widget-area-toggle:not(.std-menu) .lines-button').removeClass('close');
		//$('.slide-out-widget-area-toggle a').removeClass('animating');
		$('.blurred-wrap').removeClass('blurred');
		$('#slide-out-widget-area.fullscreen').transition({'opacity': 0 },700,'easeOutQuad',function(){ $('#slide-out-widget-area.fullscreen').hide().css('opacity','1'); }).removeClass('open');
		$('#slide-out-widget-area.fullscreen .widget').transition({'opacity': 0},700,'easeOutQuad',function(){
			$(this).transition({y: '110px'},0);
		});

		setTimeout(function(){
			$('.off-canvas-menu-container .menu > li').transition({y: '80px', 'opacity': 0},0);		
			$('#slide-out-widget-area .off-canvas-social-links li').transition({'scale':0},0);
			$('#slide-out-widget-area .off-canvas-social-links').removeClass('line-shown');
			$('#slide-out-widget-area .bottom-text').transition({'opacity':0},0);	

			//close submenu items
			$('#slide-out-widget-area .menuwrapper .menu').removeClass( 'subview' );
			$('#slide-out-widget-area .menuwrapper .menu li').removeClass( 'subview subviewopen' );
			$('#slide-out-widget-area.fullscreen .inner .off-canvas-menu-container').css('height','auto');
		},800);

		setTimeout(function(){
			if($('.nectar-social-sharing-fixed').length == 0) {
				showToTop();
			}
			$('.container-wrap').removeClass('no-shadow');
		},500);

		//fade out overlay
		$('#slide-out-widget-area-bg').stop(true).transition({'opacity': 0},900,'easeOutQuad',function(){
			if($('.mobile #header-outer[data-permanent-transparent="false"]').length > 0 && $('.container-wrap').hasClass('no-scroll')) $('#ajax-content-wrap').removeClass('at-content');
			if($('.mobile #header-outer[data-permanent-transparent="false"]').length == 0) $('#slide-out-widget-area.fullscreen .inner-wrap').css('padding-top', '0');
			$('.slide-out-widget-area-toggle a').removeClass('animating');
			if($('#slide-out-widget-area-bg').hasClass('solid')) $opacity = 1;
			if($('#slide-out-widget-area-bg').hasClass('dark')) $opacity = 0.93;
			if($('#slide-out-widget-area-bg').hasClass('medium')) $opacity = 0.6;
			if($('#slide-out-widget-area-bg').hasClass('light')) $opacity = 0.4;
			$(this).css({'height':'1px','width':'1px', 'opacity': $opacity}).transition({ y : '-100%'},0);
		});

		
	}

	else if($('#slide-out-widget-area').hasClass('fullscreen-alt')) {


		if ( $('body.material').length > 0 ) {
			setTimeout(function(){
				$('header#top .slide-out-widget-area-toggle a').removeClass('menu-push-out');
			},350);
     	}

		$('.slide-out-widget-area-toggle:not(.std-menu) .lines-button').removeClass('close');
		//$('.slide-out-widget-area-toggle a').removeClass('animating');
		$('.blurred-wrap').removeClass('blurred');
		$('#slide-out-widget-area-bg').removeClass('open');
		
		$('#slide-out-widget-area.fullscreen-alt .widget').transition({'opacity': 0},500,'easeOutQuad',function(){
			$(this).transition({y: '40px'},0);
		});
		$('#slide-out-widget-area .bottom-text, #slide-out-widget-area .off-canvas-social-links li').transition({'opacity': 0},250,'easeOutQuad');
		$('#slide-out-widget-area .off-canvas-social-links').removeClass('line-shown');

		$('.off-canvas-menu-container .menu').transition({y: '-13px', 'opacity': 0},400);	


		setTimeout(function(){
			$('.off-canvas-menu-container .menu > li').stop(true,true).transition({y: '40px', 'opacity': 0},0);		
			$('#slide-out-widget-area .off-canvas-social-links li').transition({'scale':0},0);
			$('#slide-out-widget-area .off-canvas-social-links').removeClass('line-shown');	

			//close submenu items
			$('#slide-out-widget-area .menuwrapper .menu').removeClass( 'subview' );
			$('#slide-out-widget-area .menuwrapper .menu li').removeClass( 'subview subviewopen' );
			$('#slide-out-widget-area.fullscreen-alt .inner .off-canvas-menu-container').css('height','auto');

			if($('.mobile #header-outer[data-permanent-transparent="false"]').length > 0 && $('.container-wrap').hasClass('no-scroll')) $('#ajax-content-wrap').removeClass('at-content');
			if($('.mobile #header-outer[data-permanent-transparent="false"]').length == 0) $('#slide-out-widget-area.fullscreen-alt .inner-wrap').css('padding-top', '0');
			$('.slide-out-widget-area-toggle a').removeClass('animating');
			$('#slide-out-widget-area-bg').css({'height':'1px','width':'1px','left':'-100%'});
			$('#slide-out-widget-area.fullscreen-alt').hide().removeClass('open');
		},550);

		setTimeout(function(){
			if($('.nectar-social-sharing-fixed').length == 0) {
				showToTop();
			}
		},600);

		//fade out overlay
		setTimeout(function(){
			$('#slide-out-widget-area-bg').removeClass('padding-removed');
		},50);	

		
		var borderDelay = ($bodyBorderHeaderColorMatch == true) ? 150: 50;

		setTimeout(function(){
			$('#slide-out-widget-area-bg').stop(true).css({'opacity': 0});
			if($('[data-transparent-header="true"]').length > 0) $('body > div[class*="body-border"]').css('z-index','10000');
		},borderDelay);

		setTimeout(function(){
			$('#header-outer.transparent.small-nav, #header-outer.transparent.detached, #header-outer:not([data-permanent-transparent="1"]).transparent.scrolled-down').removeClass('transparent');
			
			if($('#header-outer').hasClass('pseudo-data-transparent')) {
				$('#header-outer').attr('data-transparent-header','false').removeClass('pseudo-data-transparent').removeClass('transparent');
			}

			

		},100);
		
	}


	

	//dark slide transparent nav
	if($('#header-outer.temp-removed-dark-slide.transparent').length > 0  && $('#boxed').length == 0) $('#header-outer').removeClass('temp-removed-dark-slide').addClass('dark-slide');

	//remove header transparent state

	if($('#header-outer[data-permanent-transparent="1"]').length == 0 && $('#slide-out-widget-area.fullscreen-alt').length == 0) {

		if($('.nectar-box-roll').length == 0) {
			if($('#header-outer.small-nav').length > 0 || $('#header-outer.scrolled-down').length > 0 || $('#header-outer.detached').length > 0) $('#header-outer').removeClass('transparent');
		} else {
			if($('#header-outer.small-nav').length > 0 || $('#header-outer.scrolled-down').length > 0 || $('.container-wrap.auto-height').length > 0) $('#header-outer').removeClass('transparent');
		}
	} 



	//remove hidden menu
	$('#header-outer').removeClass('hidden-menu');

	$('#header-outer').removeClass('side-widget-open').addClass('side-widget-closed');

	return false;
});

function slideOutWidgetOverflowState() {


	//switch position of social media/extra info based on screen size
	if(window.innerWidth < 1000 || $('body > #boxed').length > 0 || $('.ocm-effect-wrap-inner > #boxed').length > 0) {

		$('#slide-out-widget-area.fullscreen .off-canvas-social-links, #slide-out-widget-area.fullscreen-alt .off-canvas-social-links').appendTo('#slide-out-widget-area .inner');
		$('#slide-out-widget-area.fullscreen .bottom-text, #slide-out-widget-area.fullscreen-alt .bottom-text').appendTo('#slide-out-widget-area .inner');
	} else {
		$('#slide-out-widget-area.fullscreen .off-canvas-social-links,#slide-out-widget-area.fullscreen-alt .off-canvas-social-links').appendTo('#slide-out-widget-area .inner-wrap');
		$('#slide-out-widget-area.fullscreen .bottom-text, #slide-out-widget-area.fullscreen-alt .bottom-text').appendTo('#slide-out-widget-area .inner-wrap');
	}

	//add overflow
	if( $('#slide-out-widget-area[class*="fullscreen"] .inner').height() >= $(window).height()-100) { $('#slide-out-widget-area[class*="fullscreen"] .inner, #slide-out-widget-area[class*="fullscreen"]').addClass('overflow-state'); }
	else { $('#slide-out-widget-area[class*="fullscreen"] .inner, #slide-out-widget-area[class*="fullscreen"]').removeClass('overflow-state'); }

	$('#slide-out-widget-area[class*="fullscreen"] .inner').transition({ y : '-' + ($('#slide-out-widget-area[class*="fullscreen"] .inner').height()/2) + 'px' },0);

	//close mobile only slide out widget area if switching back to desktop
	if($('.slide-out-from-right.open .off-canvas-menu-container.mobile-only').length > 0 && $('body.mobile').length == 0) { $('#slide-out-widget-area .slide_out_area_close').trigger('click'); }

	//sizing for dropdown
	OCM_Dropdown_Icon_Pos();

	

}


function OCM_Dropdown_Icon_Pos() {
	$('#slide-out-widget-area[class*="slide-out-from-right"] .off-canvas-menu-container li.menu-item-has-children').each(function(){
		$(this).find('.ocm-dropdown-arrow').css({'top': $(this).find('a').height()/2 });
	});
}
OCM_Dropdown_Icon_Pos();


function fullWidthHeaderSlidingWidgetMenuCalc() {
	$('header#top nav > ul > li.megamenu > ul.sub-menu').stop(true).transition({'width': $(window).width() - 360, 'left': '300px' },700,'easeInOutCubic');
}

//slide out widget area scrolling 
function slideOutWidgetAreaScrolling(){ 
	$('#slide-out-widget-area').mousewheel(function(event, delta) {

	     this.scrollTop -= (delta * 30);
	    
	     event.preventDefault();

	});
}
setTimeout(slideOutWidgetAreaScrolling,500);


//handle mobile scrolling
if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)) {
	$('#slide-out-widget-area').addClass('mobile');
}


function closeOCM(item) {
    if($('#slide-out-widget-area.open').length > 0) {

    	var $windowCurrentLocation = window.location.href.split("#")[0];
		  var $windowClickedLocation = item.find('> a').attr('href').split("#")[0];

    	if($windowCurrentLocation == $windowClickedLocation || item.find('a[href^="#"]').length > 0) {
				if(item.parents('.slide-out-from-right-hover').length > 0) {
					$('.slide-out-widget-area-toggle.slide-out-hover-icon-effect a').trigger('click');
				} else {
					$('.slide-out-widget-area-toggle a').trigger('click');
				}
        
			}
    } 
		/*else if($('#slide-out-widget-area.slide-out-from-right.material-open').length > 0) {
			
			var $windowCurrentLocation = window.location.href.split("#")[0];
			var $windowClickedLocation = item.find('> a').attr('href').split("#")[0];
			
				if($windowCurrentLocation == $windowClickedLocation || item.find('a[href^="#"]').length > 0 && item.find('a').attr('href') != '#') {
					$('a.slide_out_area_close.material-ocm-open').trigger('click');
					
					setTimeout(function(){
						//scroll to window hash
						console.log('scroll to hash');
					},900);
					
				}
		} */
}


//left header
function leftHeaderSubmenus() {

	//remove megamenu class
	$('#header-outer[data-format="left-header"] nav li.megamenu').removeClass('megamenu');

	$ocm_link_selector = ($('#slide-out-widget-area[data-dropdown-func="separate-dropdown-parent-link"]').length > 0) ? '#slide-out-widget-area .off-canvas-menu-container li.menu-item-has-children > .ocm-dropdown-arrow' : 'body.material #slide-out-widget-area[class*="slide-out-from-right"] .off-canvas-menu-container li.menu-item-has-children > a';
	//click
	$('#header-outer[data-format="left-header"]  li.menu-item-has-children > a, '+$ocm_link_selector).click(function(){

		if($(this).parent().hasClass('open-submenu')) {
			$(this).parent().find('.sub-menu').css({
				'max-height' : '0'
			});
			$(this).parent().removeClass('open-submenu');
		}
		else {

			//get max height

			var $that = $(this);
			var $maxSubMenuHeight;

			$that.parent().find('> .sub-menu').addClass('no-trans');

			setTimeout(function(){

				$that.parent().find('> .sub-menu').css({
					'max-height' : 'none',
					'position' : 'absolute',
					'visibility' : 'hidden'
				});
				
				$maxSubMenuHeight =  $that.parent().find('> .sub-menu').height();

				$that.parent().find('> .sub-menu').removeClass('no-trans');
				$that.parent().find('> .sub-menu').css({
					'max-height' : '0',
					'position' : 'relative',
					'visibility' : 'visible'
				});

			},25);

			setTimeout(function(){
				
				//reset
				$that.closest('ul').find('li.menu-item-has-children').removeClass('open-submenu');
				$that.closest('ul').find('li.menu-item-has-children > .sub-menu').css({
					'max-height' : '0'
				});

				$that.parent().addClass('open-submenu');

				$that.parent().find('> .sub-menu').css('max-height', $maxSubMenuHeight);

				//add height to open parents
				if($that.parents('ul').length > 0) {

					$that.parents('ul:not(.sf-menu)').each(function(){
						$(this).css('max-height');
						$(this).css('max-height', parseInt($(this).height() + parseInt($(this).css('padding-top'))*2 + $maxSubMenuHeight)+'px');
					});
				} 
					

			},50);
		}
		
		return false;
	});
	
	//start open for current page items
	var $maxSubMenuHeightArr = [];

	$('#header-outer[data-format="left-header"] .current-menu-ancestor').find('.current-menu-item').parents('ul.sub-menu').each(function(i){

		

		$maxSubMenuHeightArr[i] =  $(this).parent().find('> .sub-menu').height();


		var $that = $(this);
		setTimeout(function(){

			var $totalSubMenuHeight = 0;

			for(var $i=0; $i < $maxSubMenuHeightArr.length; $i++) {
				$totalSubMenuHeight += parseInt($maxSubMenuHeightArr[i]);
			}
	
			$that.parent().addClass('open-submenu');
			$that.css('max-height', $totalSubMenuHeight);

		},40);
	});
}

if($('#header-outer[data-format="left-header"]').length > 0 || $('body.material[data-slide-out-widget-area-style*="slide-out-from-right"]').length > 0 || $('#slide-out-widget-area[data-dropdown-func="separate-dropdown-parent-link"]').length > 0) {
	leftHeaderSubmenus();
}




//fullscreen submenu
;( function( $, window, undefined ) {

	'use strict';

	// global
	var Modernizr = window.Modernizr, $body = $( 'body' );

	$.DLMenu = function( options, element ) {
		this.$el = $( element );
		this._init( options );
	};

	// the options
	$.DLMenu.defaults = {
		// classes for the animation effects
		animationClasses : { classin : 'dl-animate-in-1', classout : 'dl-animate-out-1' },
		onLevelClick : function( el, name ) { return false; },
		onLinkClick : function( el, ev ) { return false; }
	};

	$.DLMenu.prototype = {
		_init : function( options ) {

			// options
			this.options = $.extend( true, {}, $.DLMenu.defaults, options );
			// cache some elements and initialize some variables
			this._config();
			
			var animEndEventNames = {
					'WebkitAnimation' : 'webkitAnimationEnd',
					'OAnimation' : 'oAnimationEnd',
					'msAnimation' : 'MSAnimationEnd',
					'animation' : 'animationend'
				},
				transEndEventNames = {
					'WebkitTransition' : 'webkitTransitionEnd',
					'MozTransition' : 'transitionend',
					'OTransition' : 'oTransitionEnd',
					'msTransition' : 'MSTransitionEnd',
					'transition' : 'transitionend'
				};
			// animation end event name
			this.animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ] + '.menu';
			// transition end event name
			this.transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ] + '.menu',
			// support for css animations and css transitions
			this.supportAnimations = Modernizr.cssanimations,
			this.supportTransitions = Modernizr.csstransitions;

			this._initEvents();

		},
		_config : function() {
			this.open = false;
			this.$trigger = this.$el.children( '.trigger' );
			this.$menu = this.$el.children( 'ul.menu' );
			this.$menuitems = this.$menu.find( 'li:not(.back) > a' );
			this.$el.find( 'ul.sub-menu' ).prepend( '<li class="back"><a href="#"> '+$('#slide-out-widget-area').attr('data-back-txt')+' </a></li>' );
			this.$back = this.$menu.find( 'li.back' );
		},
		_initEvents : function() {

			var self = this;

			this.$trigger.on( 'click.menu', function() {
				
				if( self.open ) {
					self._closeMenu();
				} 
				else {
					self._openMenu();
				}
				return false;

			} );
			
			this.$menuitems.on( 'click.menu', function( event ) {
				
				//event.stopPropagation();

				var $item = $(this).parent('li'),
					$submenu = $item.children( 'ul.sub-menu' );

				$('.fullscreen-alt .off-canvas-menu-container .clip-wrap, .fullscreen-alt .off-canvas-menu-container .clip-wrap span').css('transition-duration','0s');	
	
				//exit if clicking on background LI (avoids effect wrongly triggering)
				//if( $item.find('> a').length > 0 && $item.find('> a').css('display') === 'none') return false;
			
				if( $submenu.length > 0 ) {

					var $flyin = $submenu.clone().css( 'opacity', 0 ).insertAfter( self.$menu ),
						onAnimationEndFn = function() {
							self.$menu.off( self.animEndEventName ).removeClass( self.options.animationClasses.classout ).addClass( 'subview' );
							$item.addClass( 'subviewopen' ).parents( '.subviewopen:first' ).removeClass( 'subviewopen' ).addClass( 'subview' );
							$flyin.remove();
							setTimeout(function(){
								$('.off-canvas-menu-container .menu > li').removeClass('no-pointer-events');
								$('.off-canvas-menu-container .clip-wrap, .off-canvas-menu-container .clip-wrap span').css('transition-duration','.45s');
							},300);
							
			
						};

					setTimeout( function() {
						$flyin.addClass( self.options.animationClasses.classin );
						self.$menu.addClass( self.options.animationClasses.classout );
						if( self.supportAnimations ) {
							self.$menu.on( self.animEndEventName, onAnimationEndFn );
						}
						else {
							onAnimationEndFn.call();
						}

						self.options.onLevelClick( $item, $item.children( 'a:first' ).text() );
					} );


					//adjust height for mobile / widgets below
					$item.parents('.off-canvas-menu-container').css('height',$item.parents('.off-canvas-menu-container').find('.menuwrapper .menu').height()).transition({ 'height': $flyin.height() },500,'easeInOutQuad' );


					return false;

				}
				else {
		
					self.options.onLinkClick( $item.find('> a'), event );
				}

				closeOCM($item);

			});

			


			this.$back.on( 'click.menu', function( event ) {
				
				var $this = $( this ),
					$submenu = $this.parents( 'ul.sub-menu:first' ),
					$item = $submenu.parent(),

					$flyin = $submenu.clone().insertAfter( self.$menu );

				var onAnimationEndFn = function() {
					self.$menu.off( self.animEndEventName ).removeClass( self.options.animationClasses.classin );
					$flyin.remove();
				};

				setTimeout( function() {
					$flyin.addClass( self.options.animationClasses.classout );
					self.$menu.addClass( self.options.animationClasses.classin );
					if( self.supportAnimations ) {
						self.$menu.on( self.animEndEventName, onAnimationEndFn );
					}
					else {
						onAnimationEndFn.call();
					}

					$item.removeClass( 'subviewopen' );
					
					var $subview = $this.parents( '.subview:first' );
					if( $subview.is( 'li' ) ) {
						$subview.addClass( 'subviewopen' );
					}
					$subview.removeClass( 'subview' );
				} );

		
				//adjust height for mobile / widgets below
				$item.parents('.off-canvas-menu-container').css('height', $item.parents('.off-canvas-menu-container').find('.menuwrapper .menu').height())
				setTimeout(function() { 
					$item.parents('.off-canvas-menu-container').transition({ 'height': $item.parent().height() },500,'easeInOutQuad');
				},50);


				return false;

			} );
			
		},
		closeMenu : function() {
			if( this.open ) {
				this._closeMenu();
			}
		},
		_closeMenu : function() {
			var self = this,
				onTransitionEndFn = function() {
					self.$menu.off( self.transEndEventName );
					self._resetMenu();
				};
			
			this.$menu.removeClass( 'menuopen' );
			this.$menu.addClass( 'menu-toggle' );
			this.$trigger.removeClass( 'active' );
			
			if( this.supportTransitions ) {
				this.$menu.on( this.transEndEventName, onTransitionEndFn );
			}
			else {
				onTransitionEndFn.call();
			}

			this.open = false;
		},
		openMenu : function() {
			if( !this.open ) {
				this._openMenu();
			}
		},
		_openMenu : function() {
			var self = this;
			// clicking somewhere else makes the menu close
			$body.off( 'click' ).on( 'click.menu', function() {
				self._closeMenu() ;
			} );
			this.$menu.addClass( 'menuopen menu-toggle' ).on( this.transEndEventName, function() {
				$( this ).removeClass( 'menu-toggle' );
			} );
			this.$trigger.addClass( 'active' );
			this.open = true;
		},
		// resets the menu to its original state (first level of options)
		_resetMenu : function() {
			this.$menu.removeClass( 'subview' );
			this.$menuitems.removeClass( 'subview subviewopen' );
		}
	};

	var logError = function( message ) {
		if ( window.console ) {
			window.console.error( message );
		}
	};

	$.fn.dlmenu = function( options ) {
		if ( typeof options === 'string' ) {
			var args = Array.prototype.slice.call( arguments, 1 );
			this.each(function() {
				var instance = $.data( this, 'menu' );
				if ( !instance ) {
					logError( "cannot call methods on menu prior to initialization; " +
					"attempted to call method '" + options + "'" );
					return;
				}
				if ( !$.isFunction( instance[options] ) || options.charAt(0) === "_" ) {
					logError( "no such method '" + options + "' for menu instance" );
					return;
				}
				instance[ options ].apply( instance, args );
			});
		} 
		else {
			this.each(function() {	
				var instance = $.data( this, 'menu' );
				if ( instance ) {
					instance._init();
				}
				else {
					instance = $.data( this, 'menu', new $.DLMenu( options, this ) );
				}
			});
		}
		return this;
	};

} )( jQuery, window );

function fullscreenMenuInit() {
	$('#slide-out-widget-area .off-canvas-menu-container .menu').wrap('<div class="menu-wrap menuwrapper" />');
	$('#slide-out-widget-area .off-canvas-menu-container .menu').addClass('menuopen');
	$ocmAnimationClassNum = ($('#slide-out-widget-area.fullscreen-alt').length > 0) ? '4' : '5';
	$('#slide-out-widget-area .off-canvas-menu-container .menu-wrap').dlmenu({ animationClasses : { classin : 'dl-animate-in-'+$ocmAnimationClassNum, classout : 'dl-animate-out-'+$ocmAnimationClassNum } });
	
}

if($('body.material[data-slide-out-widget-area-style*="slide-out-from-right"]').length == 0 && $('#slide-out-widget-area[data-dropdown-func="separate-dropdown-parent-link"]').length == 0 ) {
	fullscreenMenuInit();
} else if($('body.using-mobile-browser[data-slide-out-widget-area-style="slide-out-from-right-hover"]').length > 0){
	
	//close OCM on mobile when clicking anchor on same page 
	$('body #slide-out-widget-area .inner .off-canvas-menu-container li a[href]').on('click',function(){

		  if($(this).attr('href') != '#') {
				closeOCM($(this).parent());	
			}

	});
	
} 
var animating = 'false';

/*else if($('body.material[data-slide-out-widget-area-style="slide-out-from-right"]').length > 0) {
	
	$('body #slide-out-widget-area .inner .off-canvas-menu-container li a[href]').on('click',function(){
		
		  if($(this).attr('href') != '#' && $(this).attr('href').indexOf("#") != -1) {
				closeOCM($(this).parent());	
			}
	});
	
} */

//submenu link hover fix
$('body').on('mouseover','#slide-out-widget-area .off-canvas-menu-container .menuwrapper > .sub-menu li > a',function(){
	var $currentTxt = $(this).text();
	$('.off-canvas-menu-container .menuwrapper .menu li > a').removeClass('hovered');
	$('.off-canvas-menu-container .menuwrapper .menu li > a:contains('+$currentTxt+')').addClass('hovered');
});
$('body').on('mouseover','.off-canvas-menu-container .menuwrapper .menu li > a',function(){
	$('.off-canvas-menu-container .menuwrapper .menu li > a').removeClass('hovered');
});



//touch 
if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/) && $('.nectar-box-roll').length > 0) {
	$('body').swipe({
		tap: function(event,target) {
			if($(target).parents('.nectar-flip-box').length > 0)
				$(target).parents('.nectar-flip-box').trigger('click');
			if($(target).is('.nectar-flip-box'))
				$(target).trigger('click');
		},
		swipeStatus: function(event, phase, direction, distance, duration, fingers) {
			if($('#slide-out-widget-area.open').length > 0) return false;
			if(direction == 'up') {
				boxRoll(null,-1);
				if($('#ajax-content-wrap.no-scroll').length == 0) $('body').swipe("option", "allowPageScroll", 'vertical');
			} else if(direction == "down" && $(window).scrollTop() == 0) {
				boxRoll(null,1);
				$('body').swipe("option", "allowPageScroll", 'auto');
			}
		}
	});

}

function removeNiceScroll() {
		if($().niceScroll && $("html").getNiceScroll()){
			var nice = $("html").getNiceScroll();
			nice.stop();
			
			$('html').removeClass('no-overflow-y');
			$('.nicescroll-rails').hide();
			if($('#boxed').length == 0){
				$('body, body #header-outer, body #header-secondary-outer, body #search-outer, .midnightHeader .midnightInner').css('padding-right','0px');
			} else if($('body[data-ext-responsive="true"]').length == 0 ) {
				$('body').css('padding-right','0px');
			}

			$('body').attr('data-smooth-scrolling','0');
		}
	}
//called after box roll
$waypointsBound = false;
function waypoints() {
	colAndImgAnimations(); 
	dividers();
	svgAnimations(); 
	headerRowColorInheritInit();
	animatedColBorders();
	vcWaypoints();
	$waypointsBound = true;
}
	
/***************** Nav ******************/
	

	function centeredNavBottomBarReposition() {
		
		var $headerOuter = $('#header-outer');
		var $headerSpan9 = $('#header-outer[data-format="centered-menu-bottom-bar"] header#top .span_9');
		var $headerSpan3 = $('#header-outer[data-format="centered-menu-bottom-bar"] header#top .span_3');
		var $secondaryHeader = $('#header-secondary-outer');
		var $headerBtns = $headerSpan3.find('nav >ul.buttons');
		var $bodyBorderSize = ($('.body-border-top').length > 0 && $(window).width() > 1000) ? $('.body-border-top').height(): 0;
		var override_remove_check = false;
		
		function centeredNavBottomBarSecondary() {
			if($('body.mobile').length > 0) {
				$('#header-outer').css('margin-top','');
			} else {
				if($('#header-outer').css('top') == '0px') {
						
						$(window).off('scroll', centeredNavBottomFixed_Add);
						$(window).off('scroll', centeredNavBottomFixed_Remove);
						
						$navLogoMargin = parseInt($('body.material #header-outer #logo').css('margin-top'));
						if($headerSpan9.offset().top - $navLogoMargin - nectarDOMInfo.scrollTop <= $headerOuter.offset().top - parseInt($secondaryHeader.height()) ) {
								$(window).on('scroll', centeredNavBottomFixed_Add);
						} else {
								$(window).on('scroll', centeredNavBottomFixed_Remove);
						}
					
				}

				
				$('#header-outer').css('margin-top',nectarDOMInfo.secondaryHeaderHeight);
				
				// custom mobile breakpoint
				if($('#header-outer .span_9').css('display') == 'none') {
					 $('#header-outer').css('margin-top','');
				} else if($('#header-outer .span_9').css('display') != 'none' && parseInt($('#header-outer').css('top')) > 0) {
					 $('#header-outer').css('top','');
				}
				
				
			}
			
		}
		
		if($secondaryHeader.length > 0) {
			
			if($('#header-outer[data-remove-fixed="1"]').length == 0 && $('#header-outer[data-condense="true"]').length > 0) {
				setTimeout(function(){
					nectarDOMInfo.secondaryHeaderHeight = $('#header-secondary-outer').height();
					centeredNavBottomBarSecondary();
				},50);
				$(window).smartresize(centeredNavBottomBarSecondary);
			}
		
		} 
		

		var $navLogoMargin = parseInt($('body.material #header-outer #logo').css('margin-top'));
		

		if($('#header-outer[data-condense="true"]').length > 0) {
			
				$(window).on('scroll',centeredNavBottomFixed_Add);
				$(window).trigger('scroll');
				
				
				$(window).smartresize(function(){
					
					condenseCustomBreakPointHelper();
					
					//trigger condense if switching from mobile to desktop and user has scrolled down
					if(nectarDOMInfo.windowWidth > 1000 && $('#header-outer').css('position') == 'fixed' && $('#header-outer').css('top') == '0px' && override_remove_check == false) {
						setTimeout(function(){
							$(window).off('scroll', centeredNavBottomFixed_Add);
							$(window).off('scroll', centeredNavBottomFixed_Remove);
							override_remove_check = true;
							centeredNavBottomFixed_Remove();
						},100);
							
					}
				});
				
				function condenseCustomBreakPointHelper() {
					var $withinCustomBreakpoint = mobileBreakPointCheck();
					
					//adding class to prevent logo from hiding when opening OCM
					if($withinCustomBreakpoint) {
						$('#header-outer').addClass('within-custom-breakpoint');
					} else {
						$('#header-outer').removeClass('within-custom-breakpoint');
					}
				}
				
				condenseCustomBreakPointHelper();

		}

		
		
		
		function centeredNavBottomFixed_Add() {
				
				if(nectarDOMInfo.windowWidth < 1000 || $('body.material-ocm-open').length > 0 || $('#header-outer[data-has-menu="true"][data-format="centered-menu-bottom-bar"] .span_9').css('display') == 'none') { return; }
				
				$navLogoMargin = parseInt($('body.material #header-outer #logo').css('margin-top'));
				
				var $bodyBorderTop = ($('.body-border-top').length > 0 && nectarDOMInfo.secondaryHeaderHeight > 0) ? $('.body-border-top').height() : 0;
				
				if($headerSpan9.offset().top - $navLogoMargin - nectarDOMInfo.scrollTop <= $headerOuter.offset().top - parseInt(nectarDOMInfo.secondaryHeaderHeight) + $bodyBorderTop ) {
						
						var amountToMove = (parseInt($headerSpan9.position().top) - $navLogoMargin - parseInt(nectarDOMInfo.adminBarHeight)) + parseInt(nectarDOMInfo.secondaryHeaderHeight) - $bodyBorderTop;
						
						//megamenu RT
						if($('#header-outer[data-megamenu-rt="1"]').length > 0 && $('#header-outer .megamenu').length > 0 && $('#header-outer[data-transparent-header="true"]').length > 0) {
							$('#header-outer').removeClass('no-transition');	
						}
						
						$headerOuter.addClass('fixed-menu').removeClass('transparent').css({
							'top' : '-' + amountToMove + 'px',
							'position': 'fixed'
						});
						
						var $headerNavBarOffset = $('header#top .span_9 nav >ul').offset().top;
						var $headerButtonsOffset = $headerBtns.offset().top;
						
						if($('#boxed > #header-outer').length > 0) {
							$headerButtonsOffset = $headerButtonsOffset + 20;
						}

						var $headerButtonsHeight = ($headerSpan9.find('.sf-menu > li > a').length > 0) ? (20 - parseInt($headerSpan9.find('.sf-menu > li > a').height())) / 2 : 2;
						$headerBtns.css('transform','translateY('+ (parseInt($headerNavBarOffset) - parseInt($headerButtonsOffset) - $headerButtonsHeight - 1) +'px)');
						
						$headerBtns.find('.nectar-woo-cart').css('height',$headerOuter.height() + parseInt($headerOuter.css('top')) - parseInt(nectarDOMInfo.adminBarHeight) + parseInt(nectarDOMInfo.secondaryHeaderHeight) );

						//search
						//$('#search-outer .container').css({'transform':'translateY('+ amountToMove/2 +'px)', 'transition': 'transform 0.3s ease'});

						
						$(window).off('scroll', centeredNavBottomFixed_Add);
						$(window).on('scroll', centeredNavBottomFixed_Remove);
				}
		}
		
		function centeredNavBottomFixed_Remove() {
			
			if(nectarDOMInfo.windowWidth < 1000 || $('body.material-ocm-open').length > 0 || $('#header-outer[data-has-menu="true"][data-format="centered-menu-bottom-bar"] .span_9').css('display') == 'none') { return; }
			
			$navLogoMargin = parseInt($('body.material #header-outer #logo').css('margin-top'));
			
			if($headerSpan9.offset().top - $navLogoMargin - nectarDOMInfo.scrollTop > $headerOuter.offset().top - parseInt(nectarDOMInfo.secondaryHeaderHeight) || override_remove_check ) {
						$headerOuter.removeClass('fixed-menu').css({
							'top' : '0',
							'position': 'absolute'
						});
						
						//reset override_remove_check
						override_remove_check = false;
						
						//search
						//$('#search-outer .container').css('transform','translateY(0px)');
						
						$headerBtns.css('transform','translateY(0px)');
						
						$headerBtns.find('.nectar-woo-cart').css('height','');
						
						if($('#header-outer.transparent').length == 0) {
							
							if($('#header-outer[data-megamenu-rt="1"]').length > 0 && $('#header-outer .megamenu').length > 0) {
								$('#header-outer').removeClass('no-transition');	
							}
							
							if($('#header-outer[data-megamenu-rt="1"]').length > 0 && $('#header-outer[data-transparent-header="true"]').length > 0 && $('#header-outer .megamenu').length > 0) {
								
								if($('#header-outer').attr('data-transparent-header') == 'true' && $('.nectar-box-roll').length == 0 && $('.megamenu.sfHover').length == 0) {
									$('#header-outer').addClass('transparent');
									$('#header-outer').removeClass('no-transition');	
								}
								
								else if($('#header-outer').attr('data-transparent-header') == 'true' && $('.nectar-box-roll').length == 0 && $('.megamenu.sfHover').length > 0) {
									$('#header-outer').addClass('no-transition');	
								}
								
							} else {
								if($('#header-outer').attr('data-transparent-header') == 'true'  && $('.nectar-box-roll').length == 0) $('#header-outer').addClass('transparent');
							}
						}
						
						$(window).off('scroll', centeredNavBottomFixed_Remove);
						$(window).on('scroll', centeredNavBottomFixed_Add);
							
			}
			
		}
		
		
		
}
	
	

function condenseHeaderMobileOverride() {
	var $transCondenseHeader = $('#header-outer[data-has-menu="true"][data-format="centered-menu-bottom-bar"][data-condense="true"][data-transparent-header="true"] .span_9');
	
	if($transCondenseHeader.length > 0 && nectarDOMInfo.windowWidth > 1000 && $transCondenseHeader.css('display') == 'none') {
		$('#header-outer').removeClass('transparent').addClass('no-transition');
	} else if($transCondenseHeader.length > 0 && nectarDOMInfo.scrollTop < 20 && nectarDOMInfo.windowWidth > 1000 && $transCondenseHeader.css('display') != 'none') {
		$('#header-outer:not(.transparent)').addClass('transparent');
	}

	
}


	
	if($('#header-outer[data-format="centered-menu-bottom-bar"]').length > 0) {
		centeredNavBottomBarReposition();
		
		condenseHeaderMobileOverride();
		$(window).smartresize(condenseHeaderMobileOverride);
	}
	
	
	
	
	
	function centeredLogoMargins() {

		if($('#header-outer[data-format="centered-logo-between-menu"]').length > 0 && $(window).width() > 1000) {
			$midnightSelector = ($('#header-outer .midnightHeader').length > 0) ? '> .midnightHeader:first-child' : '';
			var $navItemLength = $('#header-outer[data-format="centered-logo-between-menu"] '+$midnightSelector+' nav > .sf-menu > li').length;
			if($('#header-outer #social-in-menu').length > 0) { $navItemLength--; }

			$centerLogoWidth = ($('#header-outer .row .col.span_3 #logo img:visible').length == 0) ? parseInt($('#header-outer .row .col.span_3').width()) : parseInt($('#header-outer .row .col.span_3 img:visible').width());

			$extraMenuSpace = ($('#header-outer[data-lhe="animated_underline"]').length > 0) ? parseInt($('#header-outer header#top nav > ul > li:first-child > a').css('margin-right')) : parseInt($('#header-outer header#top nav > ul > li:first-child > a').css('padding-right'));
			
			if($extraMenuSpace > 30) {
				$extraMenuSpace += 45;
			} else if($extraMenuSpace > 20) {
				$extraMenuSpace += 40;
			} else {
				$extraMenuSpace += 30;
			}

			$('#header-outer[data-format="centered-logo-between-menu"] nav > .sf-menu > li:nth-child('+Math.floor($navItemLength/2)+')').css({'margin-right': ($centerLogoWidth+$extraMenuSpace) + 'px'}).addClass('menu-item-with-margin');
			$leftMenuWidth = 0;
			$rightMenuWidth = 0;
			$('#header-outer[data-format="centered-logo-between-menu"] '+$midnightSelector+' nav > .sf-menu > li:not(#social-in-menu)').each(function(i){
				if(i+1 <= Math.floor($navItemLength/2)) {
					$leftMenuWidth += $(this).width();
				} else {
					$rightMenuWidth += $(this).width();
				}

			});

			var $menuDiff = Math.abs($rightMenuWidth - $leftMenuWidth);

			if($leftMenuWidth > $rightMenuWidth)  {
				$('#header-outer .row > .col.span_9').css('padding-right',$menuDiff);
			}
			else {
				$('#header-outer .row > .col.span_9').css('padding-left',$menuDiff);
			}

			$('#header-outer[data-format="centered-logo-between-menu"] nav').css('visibility','visible');
		}
	}
	
	var logoHeight = parseInt($('#header-outer').attr('data-logo-height'));
	var headerPadding = parseInt($('#header-outer').attr('data-padding'));
	var usingLogoImage = $('#header-outer').attr('data-using-logo');
	
	if( isNaN(headerPadding) || headerPadding.length == 0 ) { headerPadding = 28; }
	if( isNaN(logoHeight) || usingLogoImage.length == 0 ) { usingLogoImage = false; logoHeight = 30;}
	
	if($('header#top nav > ul li#search-btn a').length > 0) {
		$searchBtnHeight = $('header#top nav > ul li#search-btn a').height();
	} else {
		$searchBtnHeight = 24;
	}

	//inital calculations
	function headerInit() {
		
		if($('#header-outer[data-format="left-header"]').length > 0) return;


		if($('body.material').length == 0) {

			$('#header-outer #logo img').css({
				'height' : logoHeight				
			});	

			$('body:not(.material) #header-outer, .ascend #header-outer[data-full-width="true"][data-using-pr-menu="true"] header#top nav ul.buttons li.menu-item, .ascend #header-outer[data-full-width="true"][data-format="centered-menu"] header#top nav ul.buttons li#social-in-menu').css({
				'padding-top' : headerPadding
			});	
			
			if($('body.mobile').length == 0) {
				$('header#top nav > ul > li:not(#social-in-menu) > a').css({
					'padding-bottom' : Math.floor( ((logoHeight/2) - ($('header#top nav > ul > li > a').height()/2)) + headerPadding),
					'padding-top' : Math.floor( (logoHeight/2) - ($('header#top nav > ul > li > a').height()/2))
				});	
				
				var $socialInMenuHeight = ($('header#top nav > .sf-menu > li:not(#social-in-menu) a').length > 0 && $('header#top nav > .sf-menu > li:not(#social-in-menu) a').height() > 22) ? $('header#top nav > .sf-menu > li:not(#social-in-menu) a').height() : $('header#top nav > ul > li#social-in-menu > a i').height();
				
				$('header#top nav > ul > li#social-in-menu > a').css({
					'margin-top' : Math.ceil(logoHeight/2) - Math.ceil($socialInMenuHeight/2)
				});	
			}
			
			if($('#header-outer[data-format="centered-menu-under-logo"]').length == 0) {
				$('#header-outer .cart-menu').css({  
					'padding-bottom' : Math.ceil(((logoHeight/2) - ($searchBtnHeight/2)) + headerPadding),
					'padding-top' : Math.ceil(((logoHeight/2) - ($searchBtnHeight/2)) + headerPadding)
				});	
			} 
			
			
			$('header#top nav > ul li#search-btn, header#top nav > ul li#nectar-user-account, header#top nav > ul li.slide-out-widget-area-toggle').css({
				'padding-bottom' : (logoHeight/2) - ($searchBtnHeight/2),
				'padding-top' : (logoHeight/2) - ($searchBtnHeight/2)
			});	
			

			
			if($('body.ascend ').length > 0 && $('#header-outer[data-full-width="true"]').length > 0) {
				$('header#top nav > ul li#search-btn, header#top nav > ul li#nectar-user-account, header#top nav > ul li.slide-out-widget-area-toggle').css({
					'padding-top': 0,
					'padding-bottom': 0
				});

				$('header#top nav > ul.buttons').css({
					'margin-top' : - headerPadding,
					'height' : Math.floor(logoHeight + headerPadding*2) -1
				});

				$('header#top nav > ul li#search-btn a, header#top nav > ul li#nectar-user-account a, header#top nav > ul li.slide-out-widget-area-toggle a').css({
					'visibility' : 'visible',
					'padding-top': Math.floor((logoHeight/2) - ($searchBtnHeight/2) + headerPadding),
					'padding-bottom': Math.floor((logoHeight/2) - ($searchBtnHeight/2) + headerPadding)
				});
			}
			
			$('header#top .sf-menu > li > ul, header#top .sf-menu > li.sfHover > ul').css({
				'top' : $('header#top nav > ul.sf-menu > li > a').outerHeight() 
			});	
			
			
			setTimeout(function(){ 
				$('body:not(.ascend):not(.material) #search-outer #search-box .ui-autocomplete').css({
					'top': parseInt($('#header-outer').outerHeight())+'px'
				}); 
			},1000);
			
			$('body:not(.ascend):not(.material) #search-outer #search-box .ui-autocomplete').css({
				'top': parseInt($('#header-outer').outerHeight())+'px'
			});

		}//not material skin

		//header space
		if($('.nectar-parallax-scene.first-section').length == 0) {

			if($('#header-outer').attr('data-using-secondary') == '1'){
				if($('#header-outer[data-mobile-fixed="false"]').length > 0  || $('body.mobile').length == 0 ) {
					$('#header-space').css('height', parseInt($('#header-outer').outerHeight()) + $('#header-secondary-outer').height());
					
				} else {
					$('#header-space').css('height', parseInt($('#header-outer').outerHeight()));
					
				}
				
			} else {

				$('#header-space').css('height', $('#header-outer').outerHeight() );
			} 
		}
		
		$('#header-outer .container, #header-outer .cart-menu').css('visibility','visible');
		
		
		
		/* moved to priority
		//centeredLogoMargins();
		
		if(usingLogoImage && $('#header-outer[data-format="centered-logo-between-menu"]').length > 0 && $('header#top #logo img:first-child[src]').length > 0) {
			
			//fadein img on load
			var tempLogoImg = new Image();
			tempLogoImg.src = $('header#top #logo img:first-child').attr('src');

		    tempLogoImg.onload = function() {
		       centeredLogoMargins();
		    };
			
		} */

		if($('#header-outer[data-format="centered-menu-under-logo"]').length == 0) {
			$('body:not(.ascend):not(.material) #search-outer, #search .container').css({
				'height' : logoHeight + headerPadding*2
			});	
			
			$('body:not(.ascend):not(.material) #search-outer > #search input[type="text"]').css({
				'font-size'  : 43,
				'height' : '59px',
				'top' : ((logoHeight + headerPadding*2)/2) - $('#search-outer > #search input[type="text"]').height()/2
			});
			
			$('body:not(.ascend):not(.material) #search-outer > #search #close a').css({
				'top' : ((logoHeight + headerPadding*2)/2) - 8
			});	
		} else {
			$('body:not(.ascend):not(.material) #search-outer, #search .container').css({
				'height' : logoHeight + headerPadding*2 + logoHeight + 17
			});	
			
			$('body:not(.ascend):not(.material) #search-outer > #search input[type="text"]').css({
				'font-size'  : 43,
				'height' : '59px',
				'top' : ((logoHeight + headerPadding*2)/2) - ($('#search-outer > #search input[type="text"]').height()/2) + logoHeight/2 + 17
			});
			
			$('body:not(.ascend):not(.material) #search-outer > #search #close a').css({
				'top' : ((logoHeight + headerPadding*2)/2) - 8 + logoHeight/2 + 17
			});	
		}
		
		//if no image is being used
		//if(usingLogoImage == false) $('header#top #logo').addClass('no-image');
		
	}

	
	//is header resize on scroll enabled?
	var headerResize = $('#header-outer').attr('data-header-resize');
	var headerHideUntilNeeded = $('body').attr('data-hhun');

	//transparent fix

	if($('#header-outer[data-remove-fixed="1"]').length == 0) {
		if($(window).scrollTop() != 0 && $('#header-outer.transparent[data-permanent-transparent="false"]').length == 1) $('#header-outer').removeClass('transparent');
	}

	if( headerResize == 1 && headerHideUntilNeeded != '1'){
		
		headerInit();

		$(window).off('scroll.headerResizeEffect');

		if($('#nectar_fullscreen_rows').length == 0) {
			$(window).on('scroll.headerResizeEffect',smallNav);
		}
		else if($('#nectar_fullscreen_rows[data-mobile-disable="on"]').length > 0 && navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/) ) {
			$(window).on('scroll.headerResizeEffect',smallNav);
		}

	} else if(headerHideUntilNeeded != '1') {
		headerInit();
		$(window).off('scroll.headerResizeEffectOpaque');
		$(window).on('scroll.headerResizeEffectOpaque',opaqueCheck);
		
	} else if(headerHideUntilNeeded == '1') {

		headerInit();

		if($('.nectar-box-roll').length > 0) $('#header-outer').addClass('at-top-before-box');

		var previousScroll = 0, // previous scroll position
        menuOffset = $('#header-space').height()*2, // height of menu (once scroll passed it, menu is hidden)
        detachPoint = ($('body.mobile').length > 0) ? 150 : 600, // point of detach (after scroll passed it, menu is fixed)
        hideShowOffset = 6; // scrolling value after which triggers hide/show menu

	    // on scroll hide/show menu
	    function hhunCalcs(e) {

	     //stop scrolling while animated anchors
	     if($('body.animated-scrolling').length > 0 && $('#header-outer.detached').length > 0) return false;

	     //stop when material ocm or material search is open
	     if($('body.material-ocm-open').length > 0 || $('#search-outer.material-open').length > 0) { return false; }

	     //stop on mobile if not using sticky option
	      if($('#header-outer[data-mobile-fixed="false"]').length > 0 && $('body.mobile').length > 0) {  $('#header-outer').removeClass('detached'); return false; }

	      var currentScroll = $(this).scrollTop(), // gets current scroll position
	            scrollDifference = Math.abs(currentScroll - previousScroll); // calculates how fast user is scrolling

	      if (!$('#header-outer').hasClass('side-widget-open') && !$('#header-outer .slide-out-widget-area-toggle a').hasClass('animating')) {
	       
	        // if scrolled past menu
	        if (currentScroll > menuOffset) {
	          // if scrolled past detach point add class to fix menu
	          if (currentScroll > detachPoint) {
	            if (!$('#header-outer').hasClass('detached'))
	              $('#header-outer').addClass('detached').removeClass('parallax-contained');
	          	   $('#header-outer').removeClass('no-transition');	


	          	  if($('#header-outer[data-permanent-transparent="1"]').length == 0) $('#header-outer').removeClass('transparent');
	          }

	          // if scrolling faster than hideShowOffset hide/show menu
	          if (scrollDifference >= hideShowOffset) {
	            if (currentScroll > previousScroll) {
	              // scrolling down; hide menu
	              if (!$('#header-outer').hasClass('invisible')) {
								
									if($('#header-outer.at-top').length > 0) {
									
										//prevent header from transitioning when first hiding
										$('#header-outer').addClass('no-trans-hidden');
										setTimeout(function(){
											$('#header-outer').addClass('invisible').removeClass('at-top');
											$('#header-outer').removeClass('no-trans-hidden');
										},30);
										
									} else {
										
										$('#header-outer').addClass('invisible').removeClass('at-top');
									}
								
	                
									
	                 //close submenus
	                 if( $(".sf-menu").length > 0 && $().superfish ) {
		          		 $(".sf-menu").superfish('hide');
		          		 $('header#top nav > ul.sf-menu > li.menu-item-over').removeClass('menu-item-over');
		          	 }

	              }
	                $('.page-submenu.stuck').css('transform','translateY(0px)').addClass('header-not-visible');

	            	
	            } else {
	              // scrolling up; show menu
	              if ($('#header-outer').hasClass('invisible'))
	                $('#header-outer').removeClass('invisible');

	            	if($('.body-border-top').length > 0 && $('body.mobile').length == 0) {

	            		var $bodyBorderHeaderColorMatch = ($('.body-border-top').css('background-color') == '#ffffff' && $('body').attr('data-header-color') == 'light' || $('.body-border-top').css('background-color') == $('#header-outer').attr('data-user-set-bg')) ? true : false;
	            		var $bodyBorderSizeToRemove = ($bodyBorderHeaderColorMatch) ? $('.body-border-top').height() : 0;
	          	    	$('.page-submenu.stuck').css('transform','translateY('+ ($('#header-outer').outerHeight()-$bodyBorderSizeToRemove) +'px)').removeClass('header-not-visible');

	            	} else {
	            		$('.page-submenu.stuck').css('transform','translateY('+$('#header-outer').outerHeight()+'px)').removeClass('header-not-visible');
	            	}
	            }
	          }
	        } else {
	          // only remove "detached" class if user is at the top of document (menu jump fix)
	          $topDetachNum = ($('#header-outer[data-using-secondary="1"]').length > 0) ? 32 : 0;
	          if($('.body-border-top').length > 0) {
	          		$topDetachNum = ($('#header-outer[data-using-secondary="1"]').length > 0) ? 32 + $('.body-border-top').height() : $('.body-border-top').height();
	          }

	          if (currentScroll <= $topDetachNum){
	            $('#header-outer').removeClass('detached').removeClass('invisible').addClass('at-top');
	            
	            if($('#header-outer[data-megamenu-rt="1"]').length > 0 && $('#header-outer[data-transparent-header="true"]').length > 0 && $('#header-outer .megamenu').length > 0) {
		            if($('#header-outer[data-transparent-header="true"]').length > 0 && $('.nectar-box-roll').length == 0 && $('.megamenu.sfHover').length == 0) { 
		            	$('#header-outer').addClass('transparent').css('transform','translateY(0)');
		            	$('#header-outer').removeClass('no-transition');	
		            }
		            else if($('.nectar-box-roll').length > 0) $('#header-outer').css('transform','translateY(0)').addClass('at-top-before-box');
		          
		        } else {
		        	 if($('#header-outer[data-transparent-header="true"]').length > 0 && $('.nectar-box-roll').length == 0) $('#header-outer').addClass('transparent').css('transform','translateY(0)');
		            else if($('.nectar-box-roll').length > 0) $('#header-outer').css('transform','translateY(0)').addClass('at-top-before-box');
		        }

	            if( $('#page-header-bg[data-parallax="1"]').length > 0) $('#header-outer').addClass('parallax-contained').css('transform','translateY(0)');
	          }
	        }

	        // if user is at the bottom of document show menu
	        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
	          $('#header-outer').removeClass('invisible');

	            if($('.body-border-top').length > 0 && $('body.mobile').length == 0) {

            		var $bodyBorderHeaderColorMatch = ($('.body-border-top').css('background-color') == '#ffffff' && $('body').attr('data-header-color') == 'light' || $('.body-border-top').css('background-color') == $('#header-outer').attr('data-user-set-bg')) ? true : false;
            		var $bodyBorderSizeToRemove = ($bodyBorderHeaderColorMatch) ? $('.body-border-top').height() : 0;
          	    	$('.page-submenu.stuck').css('transform','translateY('+ ($('#header-outer').outerHeight()-$bodyBorderSizeToRemove) +'px)').removeClass('header-not-visible');
	            }
	            else {
	            	$('.page-submenu.stuck').css('transform','translateY('+$('#header-outer').outerHeight()+'px)').removeClass('header-not-visible');
	            }
	          
	        }

	      }

	      // replace previous scroll position with new one
	      previousScroll = currentScroll;

	    }

	    //don't trigger for header remove stickiness
	    if($('#header-outer[data-remove-fixed="1"]').length == 0) {
		    hhunCalcs();
		    $(window).scroll(hhunCalcs);
		}


	}//end if hhun
	
	
	if($('#nectar_fullscreen_rows').length > 0 ) { ($('#header-outer').attr('data-permanent-transparent','false')) }

	var shrinkNum = 6;
	var extraHeight = ($('#wpadminbar').length > 0) ? $('#wpadminbar').height() : 0; //admin bar
	var $bodyBorderHeaderColorMatch = ($('.body-border-top').css('background-color') == '#ffffff' && $('body').attr('data-header-color') == 'light' || $('.body-border-top').css('background-color') == $('#header-outer').attr('data-user-set-bg')) ? true : false;
	var $scrollTriggerOffset = ( $('body.material').length > 0 ) ? 150 : 0;

	if($('#header-outer[data-shrink-num]').length > 0 ) shrinkNum = $('#header-outer').attr('data-shrink-num');

	function smallNav(){
		var $offset = $(window).scrollTop();
		var $windowWidth = $(window).width();
		

		if($offset > $scrollTriggerOffset && $windowWidth > 1000 && $('body.material-ocm-open').length == 0) {
		

			//if going to small when material search is closing, it must be closed immediately
			if($('body.material').length > 0) {
				if($('#search-outer.material-open').length == 0) {
					$('#header-outer[data-transparent-header="true"] .bg-color-stripe').css('transition','none');
				}

				//secondary header
				if($('#header-secondary-outer').length > 0) {
					$('#header-secondary-outer').addClass('hide-up').css('transform','translateY(-100%)');
					$('#header-outer').css('transform', 'translateY(-' +$('#header-secondary-outer').height()+ 'px)');
				}

			}

			if($('#header-outer[data-megamenu-rt="1"]').length > 0 && $('#header-outer[data-transparent-header="true"]').length > 0 && $('#header-outer .megamenu').length > 0) {
				if($('#header-outer').attr('data-transparent-header') == 'true' && $('#header-outer.side-widget-open').length == 0 && $('#header-outer[data-permanent-transparent="1"]').length == 0 && $('.megamenu.sfHover').length == 0) { 
					$('#header-outer').removeClass('transparent');
					$('#header-outer').removeClass('no-transition');	
				}
			} else {
				if($('#header-outer').attr('data-transparent-header') == 'true' && $('#header-outer.side-widget-open').length == 0 && $('#header-outer[data-permanent-transparent="1"]').length == 0) $('#header-outer').removeClass('transparent');
			}

			
			$('#header-outer, #search-outer').addClass('small-nav');
			
			//body border full width side padding
			if($('#header-outer[data-full-width="true"][data-transparent-header="true"]').length > 0 && $('.body-border-top').length > 0 && $bodyBorderHeaderColorMatch == true) {
				$('#header-outer[data-full-width="true"] header > .container').stop(true,true).animate({
					'padding' : '0'			
				},{queue:false, duration:250, easing: 'easeOutCubic'});	
			}

			if($('body.material').length > 0) {
				$('header#top nav > ul > li.menu-item-with-margin').stop(true,true).animate({
					'margin-right': (parseInt($('header#top nav > ul > li.menu-item-with-margin').css('margin-right')) - parseInt(shrinkNum)*3) +'px'
				},{queue:false, duration:310, easing: 'easeOutQuad'});	
			}
			if($('body.material').length == 0) {

				$('#header-outer #logo img').stop(true,true).animate({
					'height' : logoHeight - shrinkNum
				},{queue:false, duration:250, easing: 'easeOutCubic'});	
					
				$('body:not(.material) #header-outer, .ascend #header-outer[data-full-width="true"][data-using-pr-menu="true"] header#top nav ul.buttons li.menu-item, .ascend #header-outer[data-full-width="true"][data-format="centered-menu"] header#top nav ul.buttons li#social-in-menu').stop(true,true).animate({
					'padding-top' : Math.ceil(headerPadding / 1.8)
				},{queue:false, duration:250, easing: 'easeOutCubic'});	
				
				if($('#header-outer[data-format="centered-menu-under-logo"]').length > 0) {
					$('#header-outer .row > .span_3').stop(true,true).animate({
						'padding-bottom' : Math.ceil(headerPadding / 1.8)
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
				}

				$('header#top nav > ul > li:not(#social-in-menu) > a').stop(true,true).animate({
					'padding-bottom' :  Math.floor((((logoHeight-shrinkNum)/2) - ($('header#top nav > ul > li > a').height()/2)) + headerPadding / 1.8) ,
					'padding-top' :  Math.floor(((logoHeight-shrinkNum)/2) - ($('header#top nav > ul > li > a').height()/2)) 
				},{queue:false, duration:250, easing: 'easeOutCubic'});	 

				

				$('header#top nav > ul > li#social-in-menu > a').stop(true,true).animate({
					'margin-bottom' :  Math.floor((((logoHeight-shrinkNum)/2) - ($('header#top nav > ul > li#social-in-menu > a').height()/2)) + headerPadding / 1.8) ,
					'margin-top' :  Math.floor(((logoHeight-shrinkNum)/2) - ($('header#top nav > ul > li#social-in-menu > a').height()/2)) 
				},{queue:false, duration:250, easing: 'easeOutCubic'});	 
				
				$('header#top nav > ul > li.menu-item-with-margin').stop(true,true).animate({
					'margin-right': (parseInt($('header#top nav > ul > li.menu-item-with-margin').css('margin-right')) - parseInt(shrinkNum)*3) +'px'
				},{queue:false, duration:250, easing: 'easeOutCubic'});	 

				if($bodyBorderHeaderColorMatch == true) {
					$('.body-border-top').stop(true,true).animate({
						'margin-top': '-'+$('.body-border-top').height()+'px'
					},{queue:false, duration:400, easing: 'easeOutCubic', complete: function() { $(this).css('margin-top',0)} });	 
				}

				if($('#header-outer[data-format="centered-menu-under-logo"]').length == 0) {
					$('#header-outer .cart-menu').stop(true,true).animate({
						'padding-top' : Math.ceil(((logoHeight-shrinkNum)/2) - ($searchBtnHeight/2) + headerPadding/ 1.7),
						'padding-bottom' : Math.ceil(((logoHeight-shrinkNum)/2) - ($searchBtnHeight/2) + headerPadding/ 1.7) +1
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
				} 
				
				if($('body.ascend ').length > 0 && $('#header-outer[data-full-width="true"]').length > 0) {
					$('header#top nav > ul.buttons').stop(true,true).animate({
						'margin-top' : - Math.ceil(headerPadding/ 1.8),
						'height' : Math.floor((headerPadding*2)/ 1.8 + logoHeight-shrinkNum)
					},{queue:false, duration:250, easing: 'easeOutCubic'});	

					$('header#top nav > ul li#search-btn a, header#top nav > ul li#nectar-user-account a, header#top nav > ul li.slide-out-widget-area-toggle a').stop(true,true).animate({
						'padding-top' : Math.ceil(((logoHeight-shrinkNum)/2) - ($searchBtnHeight/2) + headerPadding/ 1.7),
						'padding-bottom' : Math.ceil(((logoHeight-shrinkNum)/2) - ($searchBtnHeight/2) + headerPadding/ 1.7) +1
					},{queue:false, duration:250, easing: 'easeOutCubic'});	

				} else {
					$('header#top nav > ul li#search-btn, header#top nav > ul li#nectar-user-account, header#top nav > ul li.slide-out-widget-area-toggle').stop(true,true).animate({
						'padding-bottom' : Math.ceil(((logoHeight-shrinkNum)/2) - ($searchBtnHeight/2)),
						'padding-top' : Math.ceil(((logoHeight-shrinkNum)/2) - ($searchBtnHeight/2))
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
				}

				if($('#header-outer[data-format="centered-menu-under-logo"]').length > 0) {
					$('header#top .sf-menu > li > ul, header#top .sf-menu > li.sfHover > ul').stop(true,true).animate({
						'top' : Math.floor($('header#top nav > ul > li > a').outerHeight())
					},{queue:false, duration:250, easing: 'easeOutCubic'});		
				} else {
					$('header#top .sf-menu > li > ul, header#top .sf-menu > li.sfHover > ul').stop(true,true).animate({
						'top' : Math.floor($('header#top nav > ul > li > a').height() + (((logoHeight-shrinkNum)/2) - ($('header#top nav > ul > li > a').height()/2))*2 + headerPadding / 1.8),
					},{queue:false, duration:250, easing: 'easeOutCubic'});		
				}
				
				$('body:not(.ascend) #search-outer #search-box .ui-autocomplete').stop(true,true).animate({
					'top': Math.floor((logoHeight-shrinkNum) + (headerPadding*2)/ 1.8) +'px'
				},{queue:false, duration:250, easing: 'easeOutCubic'});	
			

				if($('#header-outer[data-format="centered-menu-under-logo"]').length == 0) {
					$('body:not(.ascend) #search-outer, #search .container').stop(true,true).animate({
						'height' : Math.floor((logoHeight-shrinkNum) + (headerPadding*2)/ 1.8)
					},{queue:false, duration:250, easing: 'easeOutCubic'});	

					$('body:not(.ascend) #search-outer > #search input[type="text"]').stop(true,true).animate({
						'font-size'  : 30,
						'line-height' : '30px',
						'height' : '44px',
						'top' : ((logoHeight-shrinkNum+headerPadding+5)/2) - ($('#search-outer > #search input[type="text"]').height()-15)/2
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
				
					$('body:not(.ascend) #search-outer > #search #close a').stop(true,true).animate({
						'top' : ((logoHeight-shrinkNum + headerPadding+5)/2) - 10
					},{queue:false, duration:250, easing: 'easeOutCubic'});	

				} else {
					$('body:not(.ascend) #search-outer, #search .container').stop(true,true).animate({
						'height' : Math.floor((logoHeight-shrinkNum) + (headerPadding*2)/ 1.8) + logoHeight - shrinkNum + 17
					},{queue:false, duration:250, easing: 'easeOutCubic'});	

					$('body:not(.ascend) #search-outer > #search input[type="text"]').stop(true,true).animate({
						'font-size'  : 30,
						'line-height' : '30px',
						'height' : '44px',
						'top' : ((logoHeight-shrinkNum+headerPadding+5)/2) - ($('#search-outer > #search input[type="text"]').height()-15)/2 + (logoHeight- shrinkNum)/2 + 8
					},{queue:false, duration:250, easing: 'easeOutCubic'});	

					$('body:not(.ascend) #search-outer > #search #close a').stop(true,true).animate({
						'top' : ((logoHeight-shrinkNum + headerPadding+5)/2) - 10 + (logoHeight- shrinkNum)/2 + 8
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
				}

			}//not material skin
		
			

			//box roll
			if($('.nectar-box-roll').length > 0 && $('#header-outer[data-permanent-transparent="1"]').length == 0) $('#ajax-content-wrap').animate({'margin-top':  (Math.floor((logoHeight-shrinkNum) +(headerPadding*2)/ 1.8 + extraHeight + secondaryHeader))  },{queue:false, duration:250, easing: 'easeOutCubic'})
			
			
			if($('body').hasClass('ascend')){ 
				$('#search-outer').stop(true,true).animate({
					'top' : Math.floor((logoHeight-shrinkNum) +(headerPadding*2)/ 1.8 + extraHeight + secondaryHeader)
				},{queue:false, duration:250, easing: 'easeOutCubic'});	
			}
			
			//if no image is being used
			if(usingLogoImage == false) $('body:not(.material) header#top #logo').stop(true,true).animate({
				'margin-top' : 0
			},{queue:false, duration:450, easing: 'easeOutExpo'});	
			
			$(window).off('scroll',smallNav);
			$(window).on('scroll',bigNav);

			//dark slider coloring border fix
			$('#header-outer[data-transparent-header="true"]').css('transition','transform 0.3s ease, background-color 0.30s ease, opacity 0.3s ease, box-shadow 0.30s ease, margin 0.25s ease-out');
			$('#header-outer[data-transparent-header="true"] .cart-menu').css('transition','none');
			setTimeout(function(){ 
				$('#header-outer[data-transparent-header="true"]').css('transition','transform 0.3s ease, background-color 0.30s ease, opacity 0.3s ease, box-shadow 0.30s ease, border-color 0.30s ease, margin 0.25s ease-out'); 
				$('#header-outer[data-transparent-header="true"] .cart-menu').css('transition','border-color 0.30s ease');
			},300);

		}

	}
	
	function bigNav(){
		var $offset = $(window).scrollTop();
		var $windowWidth = $(window).width();

		if($('body.material-ocm-open').length > 0) { return false; }

		if($offset <= $scrollTriggerOffset && $windowWidth > 1000 || $('.small-nav').length > 0 && $('#ajax-content-wrap.no-scroll').length > 0 ) {
			

			$('#header-outer, #search-outer').removeClass('small-nav');
			
			if($('#header-outer[data-megamenu-rt="1"]').length > 0 && $('#header-outer[data-transparent-header="true"]').length > 0 && $('#header-outer .megamenu').length > 0) {
				if($('#header-outer').attr('data-transparent-header') == 'true'  && $('.nectar-box-roll').length == 0 && $('.megamenu.sfHover').length == 0) {
					$('#header-outer').addClass('transparent');
					$('#header-outer').removeClass('no-transition');	
				}
			} else {
				if($('#header-outer').attr('data-transparent-header') == 'true'  && $('.nectar-box-roll').length == 0) $('#header-outer').addClass('transparent');
			}

			


			//body border full width side padding
			if($('#header-outer[data-full-width="true"][data-transparent-header="true"]').length > 0 && $('.body-border-top').length > 0 && $bodyBorderHeaderColorMatch == true) {
				$('#header-outer[data-full-width="true"] header > .container').stop(true,true).animate({
					'padding' : '0 28px'			
				},{queue:false, duration:250, easing: 'easeOutCubic'});	
			}


			if($('body.material').length > 0) {

				$('header#top nav > ul > li.menu-item-with-margin').stop(true,true).animate({
					'margin-right': (parseInt($('header#top nav > ul > li.menu-item-with-margin').css('margin-right')) + parseInt(shrinkNum)*3) +'px'
				},{queue:false, duration:140, easing: 'easeOutQuad'});

				//secondary header
				if($('#header-secondary-outer').length > 0) {
					$('#header-secondary-outer, #header-outer').removeClass('hide-up').css('transform','translateY(0%)');
				}
		
			}

			if($('body.material').length == 0) {

				$('#header-outer #logo img').stop(true,true).animate({
					'height' : logoHeight,				
				},{queue:false, duration:250, easing: 'easeOutCubic'});	

				$('body:not(.material) #header-outer, .ascend #header-outer[data-full-width="true"][data-using-pr-menu="true"] header#top nav ul.buttons li.menu-item, .ascend #header-outer[data-full-width="true"][data-format="centered-menu"] header#top nav ul.buttons li#social-in-menu').stop(true,true).animate({
					'padding-top' : headerPadding 
				},{queue:false, duration:250, easing: 'easeOutCubic'});	
				
				if($('#header-outer[data-format="centered-menu-under-logo"]').length > 0) {
					$('#header-outer .row > .span_3').stop(true,true).animate({
						'padding-bottom' : headerPadding 
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
				}

				$('header#top nav > ul > li:not(#social-in-menu) > a').stop(true,true).animate({
					'padding-bottom' : ((logoHeight/2) - ($('header#top nav > ul > li > a').height()/2)) + headerPadding,
					'padding-top' : (logoHeight/2) - ($('header#top nav > ul > li > a').height()/2) 
				},{queue:false, duration:250, easing: 'easeOutCubic'});	
				
				var $socialInMenuHeight = ($('header#top nav > .sf-menu > li:not(#social-in-menu) a').length > 0 && $('header#top nav > .sf-menu > li:not(#social-in-menu) a').height() > 22) ? $('header#top nav > .sf-menu > li:not(#social-in-menu) a').height() : $('header#top nav > ul > li#social-in-menu > a i').height();
				
				$('header#top nav > ul > li#social-in-menu > a').stop(true,true).animate({
					'margin-top' : Math.ceil(logoHeight/2) - Math.ceil($socialInMenuHeight/2)
				},{queue:false, duration:250, easing: 'easeOutCubic'});	
					
				$('header#top nav > ul > li.menu-item-with-margin').stop(true,true).animate({
					'margin-right': (parseInt($('header#top nav > ul > li.menu-item-with-margin').css('margin-right')) + parseInt(shrinkNum)*3) +'px'
				},{queue:false, duration:250, easing: 'easeOutCubic'});	 

				if($bodyBorderHeaderColorMatch == true) {
					$('.body-border-top').css({ 'margin-top': '-'+$('.body-border-top').height()+'px'}).stop(true,true).animate({
						'margin-top': '0'
					},{queue:false, duration:250, easing: 'easeOutCubic'});	 
				}

				if($('#header-outer[data-format="centered-menu-under-logo"]').length == 0) {
					$('#header-outer .cart-menu').stop(true,true).animate({
						'padding-bottom' : Math.ceil(((logoHeight/2) - ($searchBtnHeight/2)) + headerPadding),
						'padding-top' : Math.ceil(((logoHeight/2) - ($searchBtnHeight/2)) + headerPadding)
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
				}

				if($('body.ascend ').length > 0 && $('#header-outer[data-full-width="true"]').length > 0) {
					$('header#top nav > ul.buttons').stop(true,true).animate({
						'margin-top' : - Math.ceil(headerPadding),
						'height' : Math.floor(headerPadding*2 + logoHeight) -1
					},{queue:false, duration:250, easing: 'easeOutCubic'});	

					$('header#top nav > ul li#search-btn a, header#top nav > ul li#nectar-user-account a, header#top nav > ul li.slide-out-widget-area-toggle a').stop(true,true).animate({
						'padding-top': Math.floor((logoHeight/2) - ($searchBtnHeight/2) + headerPadding),
						'padding-bottom': Math.floor((logoHeight/2) - ($searchBtnHeight/2) + headerPadding)
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
				} else {
					$('header#top nav > ul li#search-btn, header#top nav > ul li#nectar-user-account, header#top nav > ul li.slide-out-widget-area-toggle').stop(true,true).animate({
						'padding-bottom' : Math.floor((logoHeight/2) - ($searchBtnHeight/2)),
						'padding-top' : Math.ceil((logoHeight/2) - ($searchBtnHeight/2))
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
				}
				
				if($('#header-outer[data-format="centered-menu-under-logo"]').length > 0) {
					$('header#top .sf-menu > li > ul, header#top .sf-menu > li.sfHover > ul').stop(true,true).animate({
						'top' : Math.floor($('header#top nav > ul > li > a').outerHeight())
					},{queue:false, duration:250, easing: 'easeOutCubic'});		
				} else {
					$('header#top .sf-menu > li > ul, header#top .sf-menu > li.sfHover > ul').stop(true,true).animate({
						'top' : Math.ceil($('header#top nav > ul > li > a').height() + (((logoHeight)/2) - ($('header#top nav > ul > li > a').height()/2))*2 + headerPadding),
					},{queue:false, duration:250, easing: 'easeOutCubic'});		
				}
				
				$('body:not(.ascend) #search-outer #search-box .ui-autocomplete').stop(true,true).animate({
					'top': Math.ceil(logoHeight + headerPadding*2) +'px'
				},{queue:false, duration:250, easing: 'easeOutCubic'});	
				
				
				if($('#header-outer[data-format="centered-menu-under-logo"]').length == 0) {
					$('body:not(.ascend) #search-outer, #search .container').stop(true,true).animate({
						'height' : Math.ceil(logoHeight + headerPadding*2)
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
					
					$('body:not(.ascend) #search-outer > #search input[type="text"]').stop(true,true).animate({
						'font-size'  : 43,
						'line-height' : '43px',
						'height' : '59px',
						'top' : ((logoHeight + headerPadding*2)/2) - 30
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
					
					
					$('body:not(.ascend) #search-outer > #search #close a').stop(true,true).animate({
						'top' : ((logoHeight + headerPadding*2)/2) - 8
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
					
				} else {
					$('body:not(.ascend) #search-outer, #search .container').stop(true,true).animate({
						'height' : Math.ceil(logoHeight + headerPadding*2) + logoHeight + 17
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
					
					$('body:not(.ascend) #search-outer > #search input[type="text"]').stop(true,true).animate({
						'font-size'  : 43,
						'line-height' : '43px',
						'height' : '59px',
						'top' : ((logoHeight + headerPadding*2)/2) - 30 + (logoHeight)/2 + 8
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
					
					
					$('body:not(.ascend) #search-outer > #search #close a').stop(true,true).animate({
						'top' : ((logoHeight + headerPadding*2)/2) - 8 + (logoHeight)/2 + 8
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
				}


				if($('body').hasClass('ascend')){ 
					$('#search-outer').stop(true,true).animate({
						'top' : (logoHeight) +(headerPadding*2) + extraHeight + secondaryHeader
					},{queue:false, duration:250, easing: 'easeOutCubic'});	
				}

				//if no image is being used
				if(usingLogoImage == false) $('header#top #logo').stop(true,true).animate({
					'margin-top' : 4
				},{queue:false, duration:450, easing: 'easeOutExpo'});	
				


			}//not material skin


				//box roll
			if($('.nectar-box-roll').length > 0 && $('#header-outer[data-permanent-transparent="1"]').length == 0) $('#ajax-content-wrap').animate({'margin-top':  (Math.floor((logoHeight) +(headerPadding*2) + extraHeight + secondaryHeader))  },{queue:false, duration:250, easing: 'easeOutCubic'})
			

			$(window).off('scroll',bigNav);
			$(window).on('scroll',smallNav);


			//dark slider coloring border fix
			$('#header-outer[data-transparent-header="true"]').css('transition','transform 0.3s ease, background-color 0.30s ease, opacity 0.3s ease, box-shadow 0.30s ease, margin 0.25s ease-out');
			$('#header-outer[data-transparent-header="true"] .cart-menu').css('transition','none');
			setTimeout(function(){ 
				$('#header-outer[data-transparent-header="true"]').css('transition','transform 0.3s ease, background-color 0.30s ease, opacity 0.3s ease, box-shadow 0.30s ease, border-color 0.30s ease, margin 0.25s ease-out'); 
				$('#header-outer[data-transparent-header="true"] .cart-menu').css('transition','border-color 0.30s ease');
			},300);
		}

	}
	
	
	function headerSpace() {
		if($('.mobile').length > 0) {
			if(window.innerHeight < window.innerWidth && window.innerWidth > 1000) {
				if($('#header-outer.small-nav').length == 0)
					$('#header-space').css('height', $('#header-outer').outerHeight() + $('#header-secondary-outer').height());
			} else {
				$('#header-space').css('height', $('#header-outer').outerHeight());
			}
			
		} else {
			if($('.nectar-parallax-scene.first-section').length == 0) {

				var shrinkNum = 6;	
				var headerPadding = parseInt($('#header-outer').attr('data-padding'));
				if($('#header-outer[data-shrink-num]').length > 0 ) shrinkNum = $('#header-outer').attr('data-shrink-num');
				var headerPadding2 = headerPadding - headerPadding/1.8;
				var $headerHeight = ($('#header-outer[data-header-resize="1"]').length > 0 && $('.small-nav').length > 0 ) ? $('#header-outer').outerHeight() + (parseInt(shrinkNum) + headerPadding2*2) : $('#header-outer').outerHeight();

				if($('#header-outer').attr('data-using-secondary') == '1'){
					$('#header-space').css('height', $headerHeight + $('#header-secondary-outer').height());
				} else {
					$('#header-space').css('height', $headerHeight);
				} 
			}
		}
		
	}

	//call immediately on mobile to make sure there's no gap with fixed setups
	if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|BlackBerry|Opera Mini)/) && $('#header-outer[data-mobile-fixed="1"]').length > 0 && $('#header-outer[data-permanent-transparent="false"]').length > 0) {
		$('#header-space').css('height', $('#header-outer').outerHeight());
	}


	var lastPosition = -1;
	var $headerScrollTop = nectarDOMInfo.scrollTop;

	function headerOffsetAdjust(){
		
		$headerScrollTop = nectarDOMInfo.scrollTop;

		 if (lastPosition == $headerScrollTop) {
            requestAnimationFrame(headerOffsetAdjust);
            return false;
        } else lastPosition = $headerScrollTop;
		
		headerOffsetAdjustCalc();

		requestAnimationFrame(headerOffsetAdjust);

	}
	var condenseHeaderLayout = $('#header-outer[data-condense="true"]').length > 0 ? true : false;
	var headerOuterCached = $('#header-outer');
	
	function headerOffsetAdjustCalc() {
		if($('body.mobile').length > 0 || condenseHeaderLayout == true && $('#header-outer .span_9').css('display') == 'none') {
			var $eleHeight = 0;

			var $endOffset = ($('#wpadminbar').css('position') == 'fixed') ? $('#wpadminbar').height() : 0;
			$eleHeight += ($('#header-secondary-outer').length > 0 && $('#header-secondary-outer').css('display') == 'block') ? nectarDOMInfo.secondaryHeaderHeight : 0;
			$eleHeight += nectarDOMInfo.adminBarHeight;
			
			if( $eleHeight - $headerScrollTop > $endOffset) { 
				headerOuterCached.css('top', $eleHeight - $headerScrollTop + 'px');
			}
			else { headerOuterCached.css('top', $endOffset); }

		} else {
				if(condenseHeaderLayout == false) {
					var $eleHeight = 0;
					
					$eleHeight += nectarDOMInfo.secondaryHeaderHeight;
					$eleHeight += nectarDOMInfo.adminBarHeight;
					headerOuterCached.css('top',$eleHeight+'px');
				}
		}
	}

	if($('#header-outer[data-mobile-fixed="1"]').length > 0 && $('#wpadminbar').length > 0 || $('#header-outer[data-mobile-fixed="1"]').length > 0 && $('#header-secondary-outer').length > 0) {
		if($('#nectar_fullscreen_rows').length == 0) { requestAnimationFrame(headerOffsetAdjust); }
		else if($('#nectar_fullscreen_rows').length > 0 && $onMobileBrowser) {
			requestAnimationFrame(headerOffsetAdjust);
		}
		$(window).smartresize(headerOffsetAdjustCalc);
	}
		

	function footerRevealCalcs() {
		var $headerNavSpace = ($('body[data-header-format="left-header"]').length > 0 && $(window).width() > 1000) ? 0 : $('#header-outer').outerHeight();

		if($(window).height() - $('#wpadminbar').height() - $headerNavSpace - $('#footer-outer').height() - 1 -$('#page-header-bg').height()  - $('.page-header-no-bg').height() > 0) {
			$resizeExtra = ($('body:not(.material) #header-outer[data-header-resize="1"]').length > 0) ? 55: 0;
			$('body[data-footer-reveal="1"] .container-wrap').css({'margin-bottom': $('#footer-outer').height()-1 });
			//let even non reveal footer have min height set when using material ocm
			$('.container-wrap').css({'min-height': $(window).height() - $('#wpadminbar').height() - $headerNavSpace - $('#footer-outer').height() -1  - $('.page-header-no-bg').height() -$('#page-header-bg').height() + $resizeExtra });
		} else {
			$('body[data-footer-reveal="1"] .container-wrap').css({'margin-bottom': $('#footer-outer').height()-1 });
		}
		
		if( $(window).width() < 1000) $('#footer-outer').attr('data-midnight','light');
		else $('#footer-outer').removeAttr('data-midnight');
	}
	if($('body[data-footer-reveal="1"]').length > 0 || $('body.material[data-slide-out-widget-area-style="slide-out-from-right"]').length > 0) { 
		setTimeout(function(){
			footerRevealCalcs();
		},60);
		footerRevealCalcs();
		//set shadow to match BG color if applicable
		if($('bodybody[data-footer-reveal="1"][data-footer-reveal-shadow="large_2"]').length > 0) $('.container-wrap').css({ boxShadow: '0 70px 110px -30px '+$('#footer-outer').css('backgroundColor') });
	}
	
	
	function opaqueCheck(){
		if($('#header-outer[data-format="centered-menu-bottom-bar"][data-condense="true"]').length > 0) { return; }
		
		var $offset = $(window).scrollTop();
		var $windowWidth = $(window).width();

		if($offset > 0 && $windowWidth > 1000) {
			
			if($('body.material').length > 0) {
				$('#header-outer').addClass('scrolled-down');

				//secondary header
				if($('#header-secondary-outer').length > 0) {
					$('#header-secondary-outer').addClass('hide-up').css('transform','translateY(-100%)');
					$('#header-outer').css('transform', 'translateY(-' +$('#header-secondary-outer').height()+ 'px)');
				}
			}

			if($('#header-outer').attr('data-transparent-header') == 'true' && $('#header-outer[data-permanent-transparent="1"]').length == 0) $('#header-outer').removeClass('transparent').addClass('scrolled-down');
			
			$(window).off('scroll',opaqueCheck);
			$(window).on('scroll',transparentCheck);
		}
	}
	
	
	function transparentCheck(){
		
		if($('#header-outer[data-format="centered-menu-bottom-bar"][data-condense="true"]').length > 0) { return; }
		
		var $offset = $(window).scrollTop();
		var $windowWidth = $(window).width();

		if($offset == 0 && $windowWidth > 1000 && $('body.material-ocm-open').length == 0) {
			
			if($('#header-outer[data-megamenu-rt="1"]').length > 0 && $('#header-outer[data-transparent-header="true"]').length > 0 && $('#header-outer .megamenu').length > 0) {
				
				if($('#header-outer').attr('data-transparent-header') == 'true' && $('.megamenu.sfHover').length == 0) { 
					$('#header-outer').addClass('transparent').removeClass('scrolled-down');
					$('#header-outer').removeClass('no-transition');	
				}
				else if($('#header-outer').attr('data-transparent-header') == 'true') { $('#header-outer').removeClass('scrolled-down'); }
				
			} else {
				if($('#header-outer').attr('data-transparent-header') == 'true') { $('#header-outer').addClass('transparent').removeClass('scrolled-down'); }
			}

			if($('body.material').length > 0) {
				$('#header-outer').removeClass('scrolled-down');

				//secondary header
				if($('#header-secondary-outer').length > 0) {
					$('#header-secondary-outer, #header-outer').removeClass('hide-up').css('transform','translateY(0%)');
				}
			}

			
			$(window).off('scroll',transparentCheck);
			$(window).on('scroll',opaqueCheck);
		}
	}
	

	
	
	//header inherit row color effect
	function headerRowColorInheritInit(){
		if($('body[data-header-inherit-rc="true"]').length > 0 && $('.mobile').length == 0){
			
			var headerOffset = ($('#header-outer[data-permanent-transparent="1"]').length == 0) ? (logoHeight - shrinkNum) + Math.ceil((headerPadding*2) / 1.8) + nectarDOMInfo.adminBarHeight : logoHeight/2 + headerPadding + nectarDOMInfo.adminBarHeight;
			
			$('.main-content > .row > .wpb_row').each(function(){


				var $that = $(this);
				var waypoint = new Waypoint({
	 				element: $that,
		 			handler: function(direction) {
	

						if(direction == 'down') {
							
							if($that.find('.row-bg.using-bg-color').length > 0) {
								
								var $textColor = ($that.find('> .col.span_12.light').length > 0) ? 'light-text' : 'dark-text';
								$('#header-outer').css('background-color',$that.find('.row-bg').css('background-color')).removeClass('light-text').removeClass('dark-text').addClass($textColor);
								$('#header-outer').attr('data-current-row-bg-color',$that.find('.row-bg').css('background-color'));

								$('body.material #header-outer .bg-color-stripe').css('background-color',$that.find('.row-bg').css('background-color'));
							} else {
								$('#header-outer').css('background-color',$('#header-outer').attr('data-user-set-bg')).removeClass('light-text').removeClass('dark-text');
								$('#header-outer').attr('data-current-row-bg-color',$('#header-outer').attr('data-user-set-bg'));

								$('body.material #header-outer .bg-color-stripe').css('background-color', '');
							}
						
						} else {

							if($that.prev('div.wpb_row').find('.row-bg.using-bg-color').length > 0) {
								var $textColor = ($that.prev('div.wpb_row').find('> .col.span_12.light').length > 0) ? 'light-text' : 'dark-text';
								$('#header-outer').css('background-color',$that.prev('div.wpb_row').find('.row-bg').css('background-color')).removeClass('light-text').removeClass('dark-text').addClass($textColor);
								$('#header-outer').attr('data-current-row-bg-color', $that.prev('div.wpb_row').find('.row-bg').css('background-color'));

								$('body.material #header-outer .bg-color-stripe').css('background-color', $that.prev('div.wpb_row').find('.row-bg').css('background-color'));
							} else {
								$('#header-outer').css('background-color',$('#header-outer').attr('data-user-set-bg')).removeClass('light-text').removeClass('dark-text');
								$('#header-outer').attr('data-current-row-bg-color',$('#header-outer').attr('data-user-set-bg'));

								$('body.material #header-outer .bg-color-stripe').css('background-color', '');
							}

						} 

				
					},
					offset: headerOffset

				}); 


			});
		}
	}

	//if($('.nectar-box-roll').length == 0) headerRowColorInheritInit();



/****************sticky page submenu******************/
	if($('.page-submenu[data-sticky="true"]').length > 0 && $('#nectar_fullscreen_rows').length == 0) {

		(function() {
		  'use strict'

		  var $ = window.jQuery
		  var Waypoint = window.Waypoint
		  var $offsetHeight = 0; 
		  var shrinkNum = 6;	
		  var headerPadding = parseInt($('#header-outer').attr('data-padding'));

		  if($('#header-outer[data-shrink-num]').length > 0 ) shrinkNum = $('#header-outer').attr('data-shrink-num');
		  var headerPadding2 = headerPadding - headerPadding/1.8;
		  var $headerNavSpace = ($('body[data-header-format="left-header"]').length > 0 && $(window).width() > 1000) ? 0 : $('#header-outer').outerHeight();
		  var $headerHeight = ($('#header-outer[data-header-resize="1"]').length > 0 && $('body.mobile').length == 0) ? $headerNavSpace - (parseInt(shrinkNum) + headerPadding2*2) : $headerNavSpace;
			
			if($('body.mobile').length == 0 && $('#header-outer[data-condense="true"]').length > 0) {
				
					var $headerSpan9 = $('#header-outer[data-format="centered-menu-bottom-bar"] header#top .span_9');
					var $secondaryHeader = $('#header-secondary-outer');
					
					$headerHeight = $('#header-outer').height() - (parseInt($headerSpan9.position().top) - parseInt($('#header-outer #logo').css('margin-top')) ) -  parseInt(nectarDOMInfo.secondaryHeaderHeight);
			}
			
		  if( $('.page-template-template-no-header-footer').length > 0 || $('.page-template-template-no-header').length > 0 ) { $headerNavSpace = 0; $headerHeight = 0; }

		  if($('#header-secondary-outer').length > 0 && $('body.mobile').length == 0 && $('body.material').length == 0) $headerHeight += $('#header-secondary-outer').height();
		  

		  $(window).on('smartresize',function(){

		    $headerNavSpace = ($('body[data-header-format="left-header"]').length > 0 && $(window).width() > 1000) ? 0 : $('#header-outer').outerHeight();
		  	$headerHeight = ($('#header-outer[data-header-resize="1"]').length > 0 && $('.small-nav').length == 0 && $('body.mobile').length == 0) ? $headerNavSpace - (parseInt(shrinkNum) + headerPadding2*2) : $headerNavSpace;
		  	
				if($('body.mobile').length == 0 && $('#header-outer[data-condense="true"]').length > 0) {
					
						var $headerSpan9 = $('#header-outer[data-format="centered-menu-bottom-bar"] header#top .span_9');
						var $secondaryHeader = $('#header-secondary-outer');
						
						$headerHeight = $('#header-outer').height() - (parseInt($headerSpan9.position().top) - parseInt($('#header-outer #logo').css('margin-top')) ) - parseInt(nectarDOMInfo.secondaryHeaderHeight);
				}
				
				if($('#header-secondary-outer').length > 0  && $('body.mobile').length == 0 && $('body.material').length == 0) $headerHeight += $('#header-secondary-outer').height();

		  	$offsetHeight = 0; 
		
		  	  if($('#wpadminbar').length > 0 && $('#wpadminbar').css('position') == 'fixed') $offsetHeight += $('#wpadminbar').height();
 			  if($('body[data-hhun="0"] #header-outer').length > 0 && !($('body.mobile').length > 0 && $('#header-outer[data-mobile-fixed="false"]').length > 0) ) {
 			  	  $offsetHeight += $headerHeight;
 			  }

 			  if($('.body-border-top').length > 0 && $(window).width() > 1000 && $('body[data-hhun="1"]').length > 0) $offsetHeight += $('.body-border-top').height();
 	
 			 // else if($('body[data-hhun="1"] #header-outer.detached:not(.invisible)').length > 0) $offsetHeight += $headerHeight;
 			
 			 //recalc for resizing (same as stuck/unstuck logic below)
 			 if($('.page-submenu.stuck').length > 0) {

		        	$('.page-submenu.stuck').addClass('no-trans').css('top',$offsetHeight).css('transform','translateY(0)').addClass('stuck');
		        	var $that = this;
		        	setTimeout(function(){ $('.page-submenu.stuck').removeClass('no-trans'); },50);
		        	$('.page-submenu.stuck').parents('.wpb_row').css('z-index',10000);

		        	//boxed
		        	if($('#boxed').length > 0) { 
		        		var $negMargin = ($(window).width() > 1000) ? $('.container-wrap').width()*0.04 :39;
		        		$('.page-submenu.stuck').css({'margin-left':'-'+$negMargin+'px', 'width' : $('.container-wrap').width()});
		        	}

		        }
		        else { 
		        	$('.page-submenu.stuck').css('top','0').removeClass('stuck');
		       	   $('.page-submenu.stuck').parents('.wpb_row').css('z-index','auto');

		       	    if($('#boxed').length > 0) $('.page-submenu.stuck').css({'margin-left':'0px', 'width' : '100%'});
		       	}

		  });

		  /* http://imakewebthings.com/waypoints/shortcuts/sticky-elements */
		  function Sticky(options) {
		    this.options = $.extend({}, Waypoint.defaults, Sticky.defaults, options)
		    this.element = this.options.element
		    this.$element = $(this.element)
		    this.createWrapper()
		    this.createWaypoint()
		  }

		  /* Private */
		  Sticky.prototype.createWaypoint = function() {
		    var originalHandler = this.options.handler

		    $offsetHeight = 0; 
		    if($('#wpadminbar').length > 0 && $('#wpadminbar').css('position') == 'fixed') $offsetHeight += $('#wpadminbar').height();
 			if($('body[data-hhun="0"] #header-outer').length > 0 && !($('body.mobile').length > 0 && $('#header-outer[data-mobile-fixed="false"]').length > 0) ) { 
 				$offsetHeight += $headerHeight;
 			}
 			 if($('.body-border-top').length > 0 && $(window).width() > 1000 && $('body[data-hhun="1"]').length > 0) $offsetHeight += $('.body-border-top').height();
 		
		    this.waypoint = new Waypoint($.extend({}, this.options, {
		      element: this.wrapper,
		      handler: $.proxy(function(direction) {
		        var shouldBeStuck = this.options.direction.indexOf(direction) > -1
		        var wrapperHeight = shouldBeStuck ? this.$element.outerHeight(true) : ''

		        this.$wrapper.height(wrapperHeight)
		        if(shouldBeStuck) {
		        	this.$element.addClass('no-trans').css('top',$offsetHeight).css('transform','translateY(0)').addClass('stuck');
		        	var $that = this;
		        	setTimeout(function(){ $that.$element.removeClass('no-trans'); },50);
		        	this.$element.parents('.wpb_row').css('z-index',10000);

		        	//boxed
		        	if($('#boxed').length > 0) { 
		        		var $negMargin = ($(window).width() > 1000) ? $('.container-wrap').width()*0.04 :39;
		        		this.$element.css({'margin-left':'-'+$negMargin+'px', 'width' : $('.container-wrap').width()});
		        	}

		        }
		        else { 
		        	this.$element.css('top','0').removeClass('stuck');
		       	    //this.$element.parents('.wpb_row').css('z-index','10000');

		       	    if($('#boxed').length > 0) this.$element.css({'margin-left':'0px', 'width' : '100%'});
		       	}

		        if (originalHandler) {
		          originalHandler.call(this, direction)
		        }
		      }, this),
		      offset: $offsetHeight
		    }))

		    var $that = this;

		   // if($('body[data-hhun="1"]').length > 0 ) {
			    setInterval(function(){ 

			    	if($('body[data-hhun="1"] #header-outer.detached:not(.invisible)').length > 0)
		        		$that.waypoint.options.offset = $offsetHeight + $headerHeight;
		        	else 
		        		$that.waypoint.options.offset = $offsetHeight;
			    	Waypoint.refreshAll();
			
			    },100); 
			//}

		  }

		  /* Private */
		  Sticky.prototype.createWrapper = function() {
		    if (this.options.wrapper) {
		      this.$element.wrap(this.options.wrapper)
		    }
		    this.$wrapper = this.$element.parent()
		    this.wrapper = this.$wrapper[0]
		  }

		  /* Public */
		  Sticky.prototype.destroy = function() {
		    if (this.$element.parent()[0] === this.wrapper) {
		      this.waypoint.destroy()
		      this.$element.removeClass(this.options.stuckClass)
		      if (this.options.wrapper) {
		        this.$element.unwrap()
		      }
		    }
		  }

		  Sticky.defaults = {
		    wrapper: '<div class="sticky-wrapper" />',
		    stuckClass: 'stuck',
		    direction: 'down right'
		  }

		  Waypoint.Sticky = Sticky
		}())
		;

		//remove outside of column setups 
		if($('.page-submenu').parents('.span_12').find('> .wpb_column').length > 1){
			var pageMenu = $('.page-submenu').clone();
			var pageMenuParentRow = $('.page-submenu').parents('.wpb_row');
			$('.page-submenu').remove();
			pageMenuParentRow.before(pageMenu);
		}

		var sticky = new Waypoint.Sticky({
		  element: $('.page-submenu[data-sticky="true"]')[0]
		});


	}

	if($('#nectar_fullscreen_rows').length == 0)
		$('.page-submenu').parents('.wpb_row').css('z-index',10000);

	$('.page-submenu .mobile-menu-link').on('click',function(){
		$(this).parents('.page-submenu').find('ul').stop(true).slideToggle(350);
		return false;
	});

	$('.page-submenu ul li a').on('click',function(){
		if($('body.mobile').length > 0) $(this).parents('.page-submenu').find('ul').stop(true).slideToggle(350);
	});


	//responsive nav
	$('body').on('click','#toggle-nav',function(){
		if(window.innerWidth > 1000) {
			window.scrollTo(0,0);	
		}

		$(this).find('.lines-button').toggleClass('close');
		
		
		if( $('body').hasClass('classic_mobile_menu_open') ) {
			
			$('#mobile-menu').hide();
			$('body').removeClass('classic_mobile_menu_open');
			
		} else {
			
			if($('#header-outer[data-transparent-header="true"][data-permanent-transparent="1"][data-mobile-fixed="false"].transparent').length > 0) {
				$('#mobile-menu').css({
					'top': parseInt($('#header-outer').outerHeight() + nectarDOMInfo.adminBarHeight) + 'px' ,
					'position': 'absolute',
					'width': '100%',
					'left': '0' 
				});
			}
			
			$('#mobile-menu').show();
			$('body').addClass('classic_mobile_menu_open');
			
		}
		
		
		
		
		return false;
	});
	
	
	//add wpml to mobile menu
	if($('header#top nav > ul > li.menu-item-language').length > 0 && $('#header-secondary-outer ul > li.menu-item-language').length == 0){
		var $langSelector = $('header#top nav > ul > li.menu-item-language').clone();
		$langSelector.insertBefore('#mobile-menu ul #mobile-search');
	}
	
	////append dropdown indicators / give classes
	$('#mobile-menu .container ul li').each(function(){
		if($(this).find('> ul').length > 0) {
			 $(this).addClass('has-ul');
			 $(this).find('> a').append('<span class="sf-sub-indicator"><i class="icon-angle-down"></i></span>');
		}
	});
	
	////events
	$('#mobile-menu .container ul li:has(">ul") > a .sf-sub-indicator').click(function(){
		$(this).parent().parent().toggleClass('open');
		$(this).parent().parent().find('> ul').stop(true,true).slideToggle();
		return false;
	});
	


/*-------------------------------------------------------------------------*/
/*	5.	Page Specific
/*-------------------------------------------------------------------------*/	
	
	function vcFullHeightRow() {
        var $element = $(".vc_row-o-full-height:first");
        if ($element.length) {
            var $window, windowHeight, offsetTop, fullHeight;
            $window = $(window), 
            windowHeight = $window.height();

            $(".vc_row-o-full-height").each(function(){
            	offsetTop = $(this).offset().top;

            	var $realRowIndex = ($(this).parent().hasClass('vc_ie-flexbox-fixer')) ? $(this).parent().index() : $(this).index();

            	if(offsetTop < windowHeight && $(this).hasClass('top-level')) {
            		fullHeight = 100 - offsetTop / (windowHeight / 100);
          	    	$(this).css("min-height", fullHeight + "vh");
          	    	$(this).find('> .col.span_12').css("min-height", fullHeight + "vh");
            	} else {
            		$(this).css("min-height", windowHeight);
            		$(this).find('> .col.span_12').css("min-height", windowHeight);
            	}
          	   
            });

            
        }

    }
    function fixIeFlexbox() {
        var ua = window.navigator.userAgent,
            msie = ua.indexOf("MSIE ");
        (msie > 0 || navigator.userAgent.match(/Trident.*rv\:11\./)) && $(".vc_row-o-full-height").each(function() {
            "flex" === $(this).find('> .span_12').css("display") && $(this).wrap('<div class="vc_ie-flexbox-fixer"></div>')
        })
    }
    fixIeFlexbox();

    vcFullHeightRow();

	//recent work
	function piVertCenter() {
		$('.portfolio-items  > .col').each(function(){
				
			//style 4
			$(this).find('.style-4 .work-info .bottom-meta:not(.shown)').stop().animate({
				'bottom' : '-'+$(this).find('.work-info .bottom-meta').outerHeight()-2+'px'
			},420,'easeOutCubic');

			
		});	 
	}
	
	
	//ie8 width fix
	function ie8Width(){
		if( $(window).width() >= 1300 ) {
			$('.container').css('max-width','1100px');
		} else {
			$('.container').css('max-width','880px');
		}
	}
	
	if( $(window).width() >= 1300 && $('html').hasClass('no-video')) { $('.container').css('max-width','1100px'); $(window).resize(ie8Width); };
	


	function smartResizeInit() {
		 
		 //full width content columns sizing
		 fullWidthContentColumns();
		 
		 //parallax BG Calculations
		 parallaxRowsBGCals();
		 
		
		 vcFullHeightRow();
		 headerSpace();

		 centeredLogoMargins();

		 slideOutWidgetOverflowState();

		 //minimal alt effect
		if($('.tabbed[data-style="minimal_alt"]').length > 0) {
			 magicLineCalc($('.tabbed[data-style="minimal_alt"] > ul > li > a.active-tab'));
		}
		

		 //full width menu megamenu adjust
		//if($('.slide-out-widget-area-toggle a.open').length > 0 && $('#header-outer[data-full-width="true"]').length > 0) fullWidthHeaderSlidingWidgetMenuCalc();
		  
	}

  $(window).off('smartresize.srInit'); 
	$(window).on('smartresize.srInit', smartResizeInit); 
	
	
	$usingNectarCarouselFlk = ($('.nectar-carousel.nectar-flickity:not(.masonry)').length > 0) ? true : false;
	
	function resizeInit() {
		 portfolioDeviceCheck();

		 //fullwidth page section calcs
		 fullWidthSections(); 
		 fullwidthImgOnlySizing();
		 fullWidthContentColumns();
		 if(nectarDOMInfo.usingMobileBrowser) {
		  fullWidthRowPaddingAdjustCalc();
		}
		 
		 //iframe video emebeds
		 responsiveVideoIframes();
		 videoshortcodeSize();
		 
		 //parallax BG Calculations
		 if(!nectarDOMInfo.usingMobileBrowser){
		   parallaxRowsBGCals();
		 }
		 
		 if($usingNectarCarouselFlk) {
			 setNectarCarouselFlkEH();
		 }

		 if($('body').hasClass('ascend') ){ 
			searchFieldCenter();
		 }

		 if($('body').hasClass('single-post')) { centerPostNextButtonImg(); }

		 cascadingImageBGSizing();

		 //vc mobile columns
		 if($('[class*="vc_col-xs-"], [class*="vc_col-md-"], [class*="vc_col-lg-"]').length > 0) { vcMobileColumns(); }

		 if($('body[data-footer-reveal="1"]').length > 0 || $('body.material[data-slide-out-widget-area-style="slide-out-from-right"]').length > 0) { footerRevealCalcs(); }

		 if($('#page-header-bg').length > 0) { pageHeader(); }

		 if($('.nectar-video-bg').length > 0) {
		 	resizeVideoToCover();
		 }
	}

	$(window).off('resize.srInit'); 
	$(window).on('resize.srInit', resizeInit); 
	
	
	$(window).load(function(){
		
		if($(window).scrollTop() == 0 ) { headerSpace(); }
		
		$('video').css('visibility','visible');
		videoshortcodeSize();
		
		if($('body[data-animated-anchors="true"]').length > 0) { 
				if($('.nectar-box-roll').length == 0 && $('#nectar_fullscreen_rows').length == 0) { pageLoadHash(); }
				
				if($('#nectar_fullscreen_rows[data-mobile-disable="on"]').length > 0 && $('.nectar-box-roll').length == 0 && nectarDOMInfo.usingMobileBrowser) {
					pageLoadHash();
				}
		}
		
		parallaxRowsBGCals();
		fullWidthContentColumns();
		resizeVideoToCover();

	});


	function style6Img(){
  
		//change sizer pos
		$('.style-5').each(function(){
			$(this).find('.sizer').insertBefore($(this).find('.parallaxImg'));
		});

		//set parent zindex
		$('.style-5').parents('.wpb_row').css('z-index','100');

		var d = document,
			de = d.documentElement,
			bd = d.getElementsByTagName('body')[0],
			htm = d.getElementsByTagName('html')[0],
			win = window,
			imgs = d.querySelectorAll('.parallaxImg'),
			totalImgs = imgs.length,
			supportsTouch = 'ontouchstart' in win || navigator.msMaxTouchPoints;

		if(totalImgs <= 0){
			return;
		}

		// build HTML
		for(var l=0;l<totalImgs;l++){

			var thisImg = imgs[l],
				layerElems = thisImg.querySelectorAll('.parallaxImg-layer'),
				totalLayerElems = layerElems.length;

			if(totalLayerElems <= 0){
				continue;
			}

			while(thisImg.firstChild) {
				thisImg.removeChild(thisImg.firstChild);
			}
			
			var lastMove = 0;

			//throttle performance for all browser other than chrome
			var eventThrottle = $('html').hasClass('cssreflections') ? 1 : 80;
			if(eventThrottle == 80) $('body').addClass('cssreflections');

			var containerHTML = d.createElement('div'),
				shineHTML = d.createElement('div'),
				shadowHTML = d.createElement('div'),
				layersHTML = d.createElement('div'),
				layers = [];

			thisImg.id = 'parallaxImg__'+l;
			containerHTML.className = 'parallaxImg-container';
			//shineHTML.className = 'parallaxImg-shine';
			shadowHTML.className = 'parallaxImg-shadow';
			layersHTML.className = 'parallaxImg-layers';

			for(var i=0;i<totalLayerElems;i++){
				var layer = d.createElement('div'),
					layerInner = d.createElement('div'),
					imgSrc = layerElems[i].getAttribute('data-img');

				$(layer).html($(layerElems[i]).html());
				layer.className = 'parallaxImg-rendered-layer';
				layer.setAttribute('data-layer',i);

				if(i==0 && $(thisImg).parents('.wpb_gallery').length == 0) { 
					layerInner.className = 'bg-img';
					layerInner.style.backgroundImage = 'url('+imgSrc+')';
					layer.appendChild(layerInner);
				}
				layersHTML.appendChild(layer);

				layers.push(layer);
			}

			containerHTML.appendChild(layersHTML);
			//containerHTML.appendChild(shineHTML);
			thisImg.appendChild(containerHTML);
			$(thisImg).wrap('<div class="parallaxImg-wrap" />');
			if(!(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)) { $(thisImg).parent().append(shadowHTML); }

			var w = thisImg.clientWidth || thisImg.offsetWidth || thisImg.scrollWidth;
			//thisImg.style.transform = 'perspective('+ w*3 +'px)';

			if(supportsTouch && $('body.using-mobile-browser').length > 0 ){
				
		    } else {
		    	(function(_thisImg,_layers,_totalLayers,_shine) {
					$(thisImg).parents('.style-5').on('mousemove', function(e){
						
						var parentEl = $(this);
					 	var now = Date.now();
				        if (now > lastMove + eventThrottle) {
				            lastMove = now;
							window.requestAnimationFrame(function(){
								processMovement(e,false,_thisImg,_layers,_totalLayers,_shine,parentEl);		
							});
						}

						//window.requestAnimationFrame(function(){
						//	processShineMovement(e,false,_thisImg,_layers,_totalLayers,_shine);		
						//});
					});
		            $(thisImg).parents('.style-5').on('mouseenter', function(e){
						processEnter(e,_thisImg,_layers,_totalLayers,_shine);		
					});
					$(thisImg).parents('.style-5').on('mouseleave', function(e){
						processExit(e,_thisImg,_layers,_totalLayers,_shine);		
					});
		        })(thisImg,layers,totalLayerElems,shineHTML);
		    }

		    //set the depths
		    (function(_thisImg,_layers,_totalLayers,_shine) {
			    depths(false,_thisImg,_layers,_totalLayers,_shine);
			     window.addEventListener('resize', function(e){
			    	  depths(false,_thisImg,_layers,_totalLayers,_shine);
			    });
			 })(thisImg,layers,totalLayerElems,shineHTML);
		}

		function processMovement(e, touchEnabled, elem, layers, totalLayers, shine, parentEl){
			
			//stop raf if exit already called
			if(!$(elem.firstChild).hasClass('over')) { processExit(e,elem,layers,totalLayers,shine); return false }

			//set up multipliers

			if($(elem).parents('.col.wide').length > 0 ) {
				var yMult = 0.03;
				var xMult = 0.063;
			} else if( $(elem).parents('.col.regular').length > 0  || $(elem).parents('.wpb_gallery').length > 0) {
				var yMult = 0.045;
				var xMult = 0.045;
			} else if($(elem).parents('.col.tall').length > 0 ) {
				var yMult = 0.05;
				var xMult = 0.015;
			} else if($(elem).parents('.col.wide_tall').length > 0) {
				var yMult = 0.04;
				var xMult = 0.04;
			} else if(parentEl.hasClass('nectar-fancy-box')) {
				var yMult = 0.045;
				var xMult = 0.022;
			} else {
				var yMult = 0.045;
				var xMult = 0.075;
			}
			
			var bdst = $(window).scrollTop(),
				bdsl = bd.scrollLeft,
				pageX = (touchEnabled)? e.touches[0].pageX : e.pageX,
				pageY = (touchEnabled)? e.touches[0].pageY : e.pageY,
				offsets = elem.getBoundingClientRect(),
				w = elem.clientWidth || elem.offsetWidth || elem.scrollWidth, // width
				h = elem.clientHeight || elem.offsetHeight || elem.scrollHeight, // height
				wMultiple = 320/w,
				offsetX = 0.52 - (pageX - offsets.left - bdsl)/w, //cursor position X
				offsetY = 0.52 - (pageY - offsets.top - bdst)/h, //cursor position Y
				dy = (pageY - offsets.top - bdst) - h / 2, //@h/2 = center of container
				dx = (pageX - offsets.left - bdsl) - w / 2, //@w/2 = center of container
				yRotate = (offsetX - dx)*(yMult * wMultiple), //rotation for container Y
				xRotate = (dy - offsetY)*(xMult * wMultiple); //rotation for container X //old

				//imgCSS = ' rotateX(' + xRotate + 'deg) rotateY(' + yRotate + 'deg) translateZ(0)'; //img transform

				if($(elem).parents('.wpb_gallery').length > 0) {
					var imgCSS = ' perspective('+ w*3 +'px) rotateX(' + -xRotate*1.9 + 'deg) rotateY(' + -yRotate*1.3 + 'deg)'; //img transform	
				} else {
					if($(elem).parents('.wide_tall').length == 0 && $(elem).parents('.wide').length == 0 && $(elem).parents('.tall').length == 0) {
						var $scaleAmount = (parentEl.hasClass('nectar-fancy-box')) ? '1.06' : '1.03';
						var $offsetAmount = (parentEl.hasClass('nectar-fancy-box')) ? '-2' : '-10';
						
						var imgCSS = ' perspective('+ w*3 +'px) rotateX(' + xRotate + 'deg) rotateY(' + yRotate + 'deg)  translateY('+offsetY*$offsetAmount+'px) translateX('+offsetX*$offsetAmount+'px) scale('+$scaleAmount+')'; //img transform
					} else {
						var imgCSS = ' perspective('+ w*3 +'px) rotateX(' + xRotate + 'deg) rotateY(' + yRotate + 'deg)  translateY('+offsetY*-10+'px) translateX('+offsetX*-10+'px) scale(1.013)'; //img transform	
					}
				}
				//imgCSS2 = 'perspective('+ w*3 +'px) rotateX(' + xRotate + 'deg) rotateY(' + yRotate + 'deg) translateZ(0) translateY('+offsetY*-10+'px) translateX('+offsetX*-10+'px)'; //img transform //old

				
			//container transform
			if(elem.firstChild.className.indexOf(' over') != -1){
				if($(elem).parents('.portfolio-items.masonry-items').length > 0){
					//imgCSS2 = ' scale3d(1.025,1.025,1.025)';
				} else {
					//imgCSS2 = ' scale3d(1.06,1.06,1.06)';
				}
			
				

			}
		
			//elem.firstChild.style.transform = imgCSS;
			$(elem).find('.parallaxImg-container').css('transform',imgCSS);

			if(!(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)) {
				$(elem).parents('.parallaxImg-wrap').find('.parallaxImg-shadow').css('transform',imgCSS);
			}
			//elem.style.transform = imgCSS2;

			
		}

		function processShineMovement(e, touchEnabled, elem, layers, totalLayers, shine){

		}

		function processEnter(e, elem, layers, totalLayers, shine){

			elem.firstChild.className += ' over';
			elem.className += ' over';

			//if(Idx == -1) {
				//$('body').addClass('cssreflections');
				//$(elem).addClass('transition');
			//	//var $timeout = setTimeout(function(){ $(elem).removeClass('transition'); },150);
			//} else {
				$(elem).addClass('transition');

				if($(elem).parents('.wpb_gallery').length > 0) {
					var $timeout = setTimeout(function(){ $(elem).removeClass('transition'); },450);
				} else {
					var $timeout = setTimeout(function(){ $(elem).removeClass('transition'); },200);
				}
			//}

			//depths(false, elem, layers, totalLayers, shine);
		}

		function processExit(e, elem, layers, totalLayers, shine){

			var w = elem.clientWidth || elem.offsetWidth || elem.scrollWidth;
			var container = elem.firstChild;

			container.className = container.className.replace(' over','');
			elem.className = elem.className.replace(' over','');
			$(container).css('transform', 'perspective('+ w*3 +'px) rotateX(0deg) rotateY(0deg) translateZ(0)');
			$(elem).parents('.parallaxImg-wrap').find('.parallaxImg-shadow').css('transform','perspective('+ w*3 +'px) rotateX(0deg) rotateY(0deg) translateZ(0)');

			$(elem).addClass('transition');
				var $timeout = setTimeout(function(){ $(elem).removeClass('transition'); },200);

			//removeDepths(false, elem, layers, totalLayers, shine);

		}

		function depths(touchEnabled, elem, layers, totalLayers, shine) {
			
			var w = elem.clientWidth || elem.offsetWidth || elem.scrollWidth;
			var revNum = totalLayers;
			var container = elem.firstChild;
			
			//set z
			for(var ly=0;ly<totalLayers;ly++){
				//layers[ly].style.transform = 'translateX(' + (offsetX * revNum) * ((ly * 2.5) / wMultiple) + 'px) translateY(' + (offsetY * totalLayers) * ((ly * 2.5) / wMultiple) + 'px) translateZ(0)';
				if(ly == 0) $(layers[ly]).css('transform', 'translateZ(0px)');
				else $(layers[ly]).css('transform','translateZ(' +(w*3)/27*(ly*1.1) + 'px) ');

				revNum--;
			}
			
			totalLayers = totalLayers + 3;

			//set perspective from beginning
			$(container).css('transform','perspective('+ w*3 +'px)');

			//$(elem).parents('.col').find('.parallaxImg-shine').css('transform','translateZ('+(w*3)/70*(totalLayers*1.1) +'px)');
		}

		function removeDepths(touchEnabled, elem, layers, totalLayers, shine) {
			
			var w = elem.clientWidth || elem.offsetWidth || elem.scrollWidth;
			var revNum = totalLayers;
			
			//set z
			for(var ly=0;ly<totalLayers;ly++){
				
				if(ly == 0) $(layers[ly]).css('transform', 'translateZ(' +(w*3)/45*(ly*1.1) + 'px) scale(1)');
				else $(layers[ly]).css('transform', 'translateZ(' +(w*3)/45*(ly*1.1) + 'px) scale(1)');

				revNum--;
			}
			
			totalLayers = totalLayers + 3;
			
		}
	}

	style6Img();

	function portfolioDeviceCheck(){
		if($('body').hasClass('mobile') || navigator.userAgent.match(/(iPad|IEMobile)/) ){
			
			//if using more details
			if($('.portfolio-items .col .work-item').find('a:not(".pp")').length > 0){
				$('.portfolio-items .col .work-item').find('a.pp').css('display','none');
			} 
			
			//if only using pp
			else {
				$('.portfolio-items .col .work-item').find('a:not(".pp")').css('display','none');
			}
		
		} else {
			$('.portfolio-items .col .work-item').find('a').css('display','inline');
		}
	}
	
	
	//portfolio fullscreen zoom slider

	//remove outside of column setups 
	$('.nectar_fullscreen_zoom_recent_projects').each(function(){
		if($(this).parents('.span_12').find('> .wpb_column').length > 1){
			var $zoomProjects = $(this).clone();
			var $zoomProjectsRow = $(this).parents('.span_12');
			$(this).remove();
			$zoomProjectsRow.prepend($zoomProjects);
		}
	});

	 $.fn.lines = function (opts) {
        var s = $.extend({
            'lineClass' : 'line'
        },opts);
        return this.each(function () {
            var self = this,
                $self = $(self),
                $line,
                $prev;
            $self.find('.' + s.lineClass).contents().unwrap();
            $self.html(function (i, h) {
                return h.replace(/(\b[\w']+\b)/g, '<span class="' + s.lineClass + '">$1</span>');
            });

            $self.find('.line + .line').each(function(i, el){
                $line = $(this),
                $prev = $line.prev('.line');
                if ($line.offset().top === $prev.offset().top) {
                    $prev.append(el.previousSibling, $line.contents());
                    $line.remove();
                }
            });
        });
    };

	function splitLineText() {
		$('.nectar_fullscreen_zoom_recent_projects, .nectar-recent-posts-single_featured.multiple_featured').each(function(){
			
			var $slideClass = ($(this).find('.project-slides').length > 0) ? '.project-slide' : '.nectar-recent-post-slide';
			var $slideInfoClass = ($(this).find('.project-slides').length > 0) ? '.project-info h1' : '.inner-wrap h2 a';
			
			$(this).find($slideClass).each(function(i){
				
				$(this).find($slideInfoClass).each(function(){
					
					var textArr = $(this).text();
		      textArr = textArr.trim();
		      textArr = textArr.split(' ');
		
		      $(this)[0].innerHTML = '';
		      
		      for(var i=0;i<textArr.length;i++) {
		          $(this)[0].innerHTML += '<span>'+ textArr[i] + '</span> ';
		      }
					
				});

				$(this).find($slideInfoClass + ' > span').wrapInner('<span class="inner" />');

			});
			
		});
		
	}


	//remove the portfolio filters that are not found in the current page
	function isotopeCatSelection() {


		$('.portfolio-items:not(".carousel")').each(function(){

			var isotopeCatArr = [];
			var $portfolioCatCount = 0;
			$(this).parent().parent().find('div[class^=portfolio-filters] ul li').each(function(i){
				if($(this).find('a').length > 0) {
					isotopeCatArr[$portfolioCatCount] = $(this).find('a').attr('data-filter').substring(1);	
					$portfolioCatCount++;
				}
			});
			
			////ice the first (all)
			isotopeCatArr.shift();
			
			
			var itemCats = '';
			
			$(this).find('> div').each(function(i){
				itemCats += $(this).attr('data-project-cat');
			});
			itemCats = itemCats.split(' ');
			
			////remove the extra item on the end of blank space
			itemCats.pop();
			
			////make sure the array has no duplicates
			itemCats = $.unique(itemCats);
			
			////if user has chosen a set of filters to display - only show those
			if($(this).attr('data-categories-to-show').length != 0 && $(this).attr('data-categories-to-show') != 'all') {
				$userSelectedCats = $(this).attr('data-categories-to-show').replace(/,/g , ' ');
				$userSelectedCats = $userSelectedCats.split(' ');
				
				if(!$(this).hasClass('infinite_scroll')) $(this).removeAttr('data-categories-to-show');
			} else {
				$userSelectedCats = itemCats;
			}
			
			
			////Find which categories are actually on the current page
			var notFoundCats = [];
			$.grep(isotopeCatArr, function(el) {

		    	if ($.inArray(el, itemCats) == -1) notFoundCats.push(el);
		    	if ($.inArray(el, $userSelectedCats) == -1) notFoundCats.push(el);

			});
			
			//manipulate the list
			if(notFoundCats.length != 0){
				$(this).parent().parent().find('div[class^=portfolio-filters] ul li').each(function(){
					if($(this).find('a').length > 0) {
						if( $.inArray($(this).find('a').attr('data-filter').substring(1), notFoundCats) != -1 ){ 
							
							if($(this).find('> ul.children').length > 0) {
								$(this).find('> a').hide(); 
							} else {
								$(this).hide(); 
							}
							
						} else {
							$(this).show();
						}
					}
				})
			}
		});
	}
	
	isotopeCatSelection();
	

	if(!navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)) {

		var $socialTimeout;
		$('body').on('mouseenter','#single-meta .meta-share-count, #project-meta .meta-share-count', function(){
			clearTimeout($socialTimeout);

			if($(this).parents('[id*="single-meta"]').length > 0 && $('[data-tab-pos="fullwidth"]').length == 0) 
				$(this).find('.nectar-social').show().stop(true).animate({'opacity': 1, 'right':'0px'},0);
			else 
				$(this).find('.nectar-social').show().stop(true).animate({'opacity': 1, 'left':'0px'},0);

			$(this).parents('[id*="-meta"]').addClass('social-hovered');

			$(this).parents('[id*="-meta"]').find('.n-shortcode a, .meta-comment-count a, .meta-share-count > a ').stop(true).animate({'opacity':0},250);
			$(this).find('.nectar-social a').each(function(i){
				$(this).stop(true).delay(i*40).animate({'opacity': 1,  'left':'0px'}, 150);
			});

		});

	
		$('body').on('mouseleave','#single-meta .meta-share-count, #project-meta .meta-share-count', function(){
			$(this).parents('[id*="-meta"]').removeClass('social-hovered');

			if($(this).parents('[id*="single-meta"]').length > 0 && $('[data-tab-pos="fullwidth"]').length == 0) 
				$(this).find('.nectar-social').stop(true).animate({'opacity': 0,  'right':'-20px'}, 200);
			else 
				$(this).find('.nectar-social').stop(true).animate({'opacity': 0,  'left':'-20px'}, 200);

			$(this).parents('[id*="-meta"]').find('.n-shortcode a, .meta-comment-count a, .meta-share-count > a ').stop(true).animate({'opacity':1},250);

			var $that = $(this);
			
			$socialTimeout = setTimeout(function(){ 
				$that.find('.nectar-social').hide(); 
				if($that.parents('[id*="single-meta"]').length > 0 && $('[data-tab-pos="fullwidth"]').length == 0) 
					$that.find('.nectar-social a').stop(true).animate({'opacity': 0,  'left':'20px'},0);   
				else 
					$that.find('.nectar-social a').stop(true).animate({'opacity': 0,  'left':'-20px'},0);   
			}, 200);
		});
	} else {
		var $socialTimeout;
		$('body').on('click','#single-meta .meta-share-count, #project-meta .meta-share-count', function(){
			clearTimeout($socialTimeout);

			if($(this).parents('[id*="single-meta"]').length > 0 && $('[data-tab-pos="fullwidth"]').length == 0) 
				$(this).find('.nectar-social').show().stop(true).animate({'opacity': 1, 'right':'0px'},0);
			else 
				$(this).find('.nectar-social').show().stop(true).animate({'opacity': 1, 'left':'0px'},0);

			$(this).parents('[id*="-meta"]').addClass('social-hovered');

			$(this).parents('[id*="-meta"]').find('.n-shortcode a, .meta-comment-count a, .meta-share-count > a ').stop(true).animate({'opacity':0},250);
			$(this).find('.nectar-social a').each(function(i){
				$(this).stop(true).delay(i*40).animate({'opacity': 1,  'left':'0px'}, 150);
			});

			return false;
		});

	}

	$('body').on('mouseenter','.fullscreen-header  .meta-share-count', function(){
		$(this).find('> a, > i').stop(true).animate({'opacity': 0},400);
		$(this).find('.nectar-social > *').each(function(i){
			$(this).stop(true).delay(i*50).animate({'opacity':'1', 'top': '0px'},250,'easeOutCubic');
		});
		//allow clickable on mobile
		setTimeout(function(){ $('.meta-share-count .nectar-sharing, .meta-share-count .nectar-sharing-alt').removeClass('inactive'); },300);
	});

	if(!navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)) {
		$('body').on('mouseleave','.fullscreen-header  .meta-share-count', function(){
			$(this).find('> a, > i').stop(true).animate({'opacity': 1},300,'easeInCubic');
			$(this).find('.nectar-social > *').each(function(i){
				$(this).stop(true).animate({'opacity':'0', 'top': '10px'},200,'easeInCubic');
			});
		});
	}
	
	
/*-------------------------------------------------------------------------*/
/*	6.	Scroll to top
/*-------------------------------------------------------------------------*/	

var $scrollTop = $(window).scrollTop();

//starting bind
function toTopBind() {
	if( $('#to-top').length > 0 && $(window).width() > 1020 || $('#to-top').length > 0 &&  $('#to-top.mobile-enabled').length > 0 ) {
		
		if($scrollTop > 350){
			$(window).on('scroll',hideToTop);
		}
		else {
			$(window).on('scroll',showToTop);
		}
	}
}

if($('.nectar-social-sharing-fixed').length == 0) {
	toTopBind();
} else {
	if($(window).width() < 1000 && $('body.single').length > 0) {
		if($scrollTop > 150){
			$(window).on('scroll',hideFixedSharing);
		}
		else {
			$(window).on('scroll',showFixedSharing);
		}
	}

	$(window).smartresize(function(){
		if($(window).width() > 1000) { 
			$('.nectar-social-sharing-fixed').addClass('visible');
		}	
		else if($scrollTop < 150) { 
			$(window).off('scroll',hideFixedSharing);
			$(window).on('scroll',showFixedSharing);
			$('.nectar-social-sharing-fixed').removeClass('visible');	
		} else {
			$(window).off('scroll',showFixedSharing);
			$(window).on('scroll',hideFixedSharing);
		}
	});
}

function showFixedSharing(){
	
  $scrollTop = $(window).scrollTop();
	if( $scrollTop > 150){

		$('.nectar-social-sharing-fixed').addClass('visible');	
		
		$(window).off('scroll',showFixedSharing);
		$(window).on('scroll',hideFixedSharing);
	}

}

function hideFixedSharing(){
  
	$scrollTop = $(window).scrollTop();
	if( $scrollTop < 150){

		$('.nectar-social-sharing-fixed').removeClass('visible');	
		
		$(window).off('scroll',hideFixedSharing);
		$(window).on('scroll',showFixedSharing);
	}

}



function showToTop(){

	if( $scrollTop > 350 && $('#slide-out-widget-area.fullscreen.open').length == 0){

		$('#to-top').stop().transition({
			'bottom' : '17px'
		},350,'easeInOutCubic');	
		
		$(window).off('scroll',showToTop);
		$(window).on('scroll',hideToTop);
	}

}

function hideToTop(){
	
	if( $scrollTop < 350 || $('#slide-out-widget-area.fullscreen.open').length > 0){

		$animationTiming = ($('#slide-out-widget-area.fullscreen.open').length > 0) ? 1150 : 350;

		$('#to-top').stop().transition({
			'bottom' : '-30px'
		},$animationTiming,'easeInOutQuint');	
		
		$(window).off('scroll',hideToTop);
		$(window).on('scroll',showToTop);	
		
	}
}


//to top color
if( $('#to-top').length > 0 ) {
	
	var $windowHeight, $pageHeight, $footerHeight, $ctaHeight;
	
	function calcToTopColor(){
		$scrollTop = $(window).scrollTop();
		$windowHeight = $(window).height();
		$pageHeight = $('body').height();
		$footerHeight = $('#footer-outer').height();
		$ctaHeight = ($('#call-to-action').length > 0) ? $('#call-to-action').height() : 0;
		
		if( ($scrollTop-35 + $windowHeight) >= ($pageHeight - $footerHeight) && $('#boxed').length == 0){
			$('#to-top').addClass('dark');
		}
		
		else {
			$('#to-top').removeClass('dark');
		}
	}
	
	if(!nectarDOMInfo.usingMobileBrowser) {
		//calc on scroll
		$(window).scroll(calcToTopColor);
		
		//calc on resize
		$(window).resize(calcToTopColor);
	}

}

//alt style
if($('body[data-button-style*="rounded"]').length > 0){
	var $clone = $('#to-top .fa-angle-up').clone();
	$clone.addClass('top-icon');
	$('#to-top').prepend($clone)
}

//scroll up event
$('body').on('click','#to-top, a[href="#top"]',function(){
	$('body,html').stop().animate({
		scrollTop:0
	},800,'easeOutQuad',function(){
		if($('.nectar-box-roll').length > 0) {
			$('body').trigger('mousewheel', [1, 0, 0]);
		}
	})
	return false;
});


/* one page scrolling */
function scrollSpyInit(){ 

	var $headerNavSpace = ($('body[data-header-format="left-header"]').length > 0 && $(window).width() > 1000) ? 0 : $('#header-outer').outerHeight();

	if( $('.page-template-template-no-header-footer').length > 0 || $('.page-template-template-no-header').length > 0 ) { $headerNavSpace = 0; }

	//prevent jump to rop on empty items
	$('header#top .sf-menu li a[href="#"]').on('click',function(e){
		e.preventDefault();
	});

	//remove full page URLs from hash if located on same page to fix current menu class
	//if(location.pathname.length > 1) {
	
	  //ocm
		if($('#slide-out-widget-area .off-canvas-menu-container').length > 0) {
			$('#slide-out-widget-area .off-canvas-menu-container').find("a[href*='" + location.pathname + "']").each(function(){
					
					var $href = $(this).attr('href');

					//regular animated anchors
					if($href != '#' && $href.indexOf("#") != -1 && $('div'+$href.substr($href.indexOf("#"))).length > 0 ) {
						$(this).attr('href',$href.substr($href.indexOf("#")));
						$(this).parent().removeClass('current_page_item').removeClass('current-menu-item');
					}
					
					//fullpage is a little different
					if($('div[data-fullscreen-anchor-id="'+$href.substr($href.indexOf("#")+1)+'"]').length > 0) {
						$(this).parent().removeClass('current_page_item').removeClass('current-menu-item');
					}
					
			});
		}
		
	  //header
		$("#header-outer").find("a[href*='" + location.pathname + "']").each(function(){
			var $href = $(this).attr('href');

			//regular animated anchors
			if($href.indexOf("#") != -1 && $('div'+$href.substr($href.indexOf("#"))).length > 0 ) {

				$(this).attr('href',$href.substr($href.indexOf("#")));
				$(this).parent().removeClass('current_page_item').removeClass('current-menu-item');
			}

			//fullpage is a little different
			if($('div[data-fullscreen-anchor-id="'+$href.substr($href.indexOf("#")+1)+'"]').length > 0) {
				$(this).parent().removeClass('current_page_item').removeClass('current-menu-item');
			}

		});
	//}

	$target = ($('.page-submenu[data-sticky="true"]').length == 0) ? '#header-outer nav': '.page-submenu';
	$('body').scrollspy({
		target: $target,
		offset: $headerNavSpace + nectarDOMInfo.adminBarHeight + 40
	});

}


/*helper function to scroll the page in an animated manner*/
function nectar_scrollToY(scrollTargetY, speed, easing) {

		var scrollY = window.scrollY || document.documentElement.scrollTop,
				scrollTargetY = scrollTargetY || 0,
				speed = speed || 2000,
				easing = easing || 'easeOutSine',
				currentTime = 0;

		var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));


		var easingEquations = {
						easeInOutQuint: function (pos) {
								if ((pos /= 0.5) < 1) {
										return 0.5 * Math.pow(pos, 5);
								}
								return 0.5 * (Math.pow((pos - 2), 5) + 2);
						}
				};


		function tick() {
				currentTime += 1 / 60;

				var p = currentTime / time;
				var t = easingEquations[easing](p);

				if (p < 1) {
						requestAnimationFrame(tick);

						window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
				} else {
						window.scrollTo(0, scrollTargetY);
				}
		}

		tick();
}


function pageLoadHash() {

	var $hash = window.location.hash;
	
	var $hashSubstrng = ($hash && $hash.length > 0) ? $hash.substring(1,$hash.length) : 0;

	//if hash has slashes 
	var $hasSlashLength = 0;
	if($hashSubstrng) {
		$hasSlashLength = $hashSubstrng.split("/");
		$hasSlashLength = $hasSlashLength.length;
	}
	
	if($hashSubstrng && $hasSlashLength > 1 ) { 
		$hashSubstrng = $hashSubstrng.replace(/\//g, ""); 
		$hash = $hash.replace(/\//g, ""); 
	}

	if($hash && $('.main-content').find($hash).length > 0 || $hash && $('.main-content').find('[data-fullscreen-anchor-id="'+$hashSubstrng+'"]').length > 0) {

		var $hashObj = ($('.main-content').find($hash).length > 0) ? $('.main-content').find($hash) : $('.main-content').find('[data-fullscreen-anchor-id="'+$hashSubstrng+'"]');

		var $headerNavSpace = ($('body[data-header-format="left-header"]').length > 0 && $(window).width() > 1000) ? 0 : $('#header-space').outerHeight();
		if( $('.page-template-template-no-header-footer').length > 0 || $('.page-template-template-no-header').length > 0 ) { $headerNavSpace = 0; }

		$timeoutVar = 0;
		if($('.nectar-box-roll').length > 0 && $('.container-wrap.bottomBoxOut').length > 0) {
			boxRoll(null,-1);
			$timeoutVar = 2050;
		} 
		setTimeout(function(){
		
			if( $('body[data-permanent-transparent="1"]').length == 0 ) {
				
				if(!$('body').hasClass('mobile')){
					$resize = ($('#header-outer[data-header-resize="0"]').length > 0) ? 0 : parseInt(shrinkNum) + headerPadding2*2;
					if($('#header-outer[data-remove-fixed="1"]').length > 0) { 
						$headerNavSpace = 0;
					}
					var $scrollTopDistance =  $hashObj.offset().top - parseInt($headerNavSpace) +$resize + 3 - nectarDOMInfo.adminBarHeight;
					
					
					
					//condesne
					if($('body.mobile').length == 0 && $('#header-outer[data-condense="true"]').length > 0) {
						
							var $headerSpan9 = $('#header-outer[data-format="centered-menu-bottom-bar"] header#top .span_9');
							var $secondaryHeader = $('#header-secondary-outer');
							var $headerHeightStored = $('#header-outer').height();
							
							$headerHeightCondensed = $headerHeightStored - ( parseInt($headerSpan9.height()) + parseInt($('#header-outer #logo').css('margin-top')) );
						
							$scrollTopDistance =  $hashObj.offset().top - parseInt($headerNavSpace) + $headerHeightCondensed - nectarDOMInfo.adminBarHeight;
					}
						
					
					
				} else {
					var $scrollTopDistance = ($('#header-outer[data-mobile-fixed="1"]').length > 0) ? $hashObj.offset().top + 2 - $headerNavSpace + nectarDOMInfo.adminBarHeight : $hashObj.offset().top - nectarDOMInfo.adminBarHeight + 1;	
				}

			} else {
				var $scrollTopDistance = $hashObj.offset().top - nectarDOMInfo.adminBarHeight + 1;
			}

			if($('body[data-hhun="1"]').length > 0 && $('#header-outer[data-remove-fixed="1"]').length == 0) {
				//alter offset 
				if($('#header-outer.detached').length == 0) 
					$scrollTopDistance = $scrollTopDistance + $headerNavSpace;
			}

			var $pageSubMenu = ($('.page-submenu[data-sticky="true"]').length > 0) ? $('.page-submenu').height() : 0;
			
			if($('body.material').length > 0 && 
				 $('#header-secondary-outer').length > 0 &&
				 $('body[data-hhun="1"]').length == 0 && 
				 $('#header-outer[data-remove-fixed="1"]').length == 0 && 
				 !$('body').hasClass('mobile')) { 
					 
					 var $headerSecondary = $('#header-secondary-outer').height();
					 
			 } else {
					 var $headerSecondary = 0;
			 }
			 
			nectar_scrollToY($scrollTopDistance - $pageSubMenu + $headerSecondary, 700, 'easeInOutQuint');

		},$timeoutVar);
	}
}

if($('body[data-animated-anchors="true"]').length > 0 || $('.single-product [data-gallery-style="left_thumb_sticky"]').length > 0) { 


+ function(t) {
    "use strict";

    function s(e, i) {
        var r = t.proxy(this.process, this);
        this.$body = t("body"), this.$scrollElement = t(t(e).is("body") ? window : e), this.options = t.extend({}, s.DEFAULTS, i), this.selector = (this.options.target || "") + " ul li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", r), this.refresh(), this.process()
    }

    function e(e) {
        return this.each(function() {
            var i = t(this),
                r = i.data("bs.scrollspy"),
                o = "object" == typeof e && e;
            r || i.data("bs.scrollspy", r = new s(this, o)), "string" == typeof e && r[e]()
        })
    }
    s.VERSION = "3.2.0", s.DEFAULTS = {
        offset: 10
    }, s.prototype.getScrollHeight = function() {
        return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
    }, s.prototype.refresh = function() {
        var s = "offset",
            e = 0;
        t.isWindow(this.$scrollElement[0]) || (s = "position", e = this.$scrollElement.scrollTop()), this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight();
        var i = this;
        this.$body.find(this.selector).map(function() {
            var i = t(this),
                r = i.data("target") || i.attr("href"),
                o = /^#./.test(r) && t(r);
            return o && o.length && o.is(":visible") && [
                [o[s]().top + e, r]
            ] || null
        }).sort(function(t, s) {
            return t[0] - s[0]
        }).each(function() {
            i.offsets.push(this[0]), i.targets.push(this[1])
        })
    }, s.prototype.process = function() {
    	var $pageSubMenu = ($('.page-submenu[data-sticky="true"]').length > 0 && $('body[data-hhun="1"]').length == 0 || $('.page-submenu[data-sticky="true"]').length > 0 && $('#header-outer[data-remove-fixed="1"]').length > 0 ) ? $('.page-submenu').height() : 0;

        var t, s = this.$scrollElement.scrollTop() + this.options.offset + $pageSubMenu,
            e = this.getScrollHeight(),
            i = this.options.offset + e - this.$scrollElement.height() -$pageSubMenu,
            r = this.offsets,
            o = this.targets,
            l = this.activeTarget;
        if (this.scrollHeight != e && this.refresh(), s >= i) return l != (t = o[o.length - 1]) && this.activate(t);
        if (l && s <= r[0]) return l != (t = o[0]) && this.activate(t);
        for (t = r.length; t--;) l != o[t] && s >= r[t] && (!r[t + 1] || s <= r[t + 1]) && this.activate(o[t])
    }, s.prototype.activate = function(s) {
        this.activeTarget = s, t(this.selector).parentsUntil(this.options.target, ".current-menu-item").removeClass("current-menu-item").removeClass('sfHover');
        var e = this.selector + '[data-target="' + s + '"],' + this.selector + '[href="' + s + '"]',
            i = t(e).parents("li").addClass("current-menu-item");
        i.parent(".dropdown-menu").length && (i = i.closest("li.dropdown").addClass("current-menu-item")), i.trigger("activate.bs.scrollspy")
    };
    var i = t.fn.scrollspy;
    t.fn.scrollspy = e, t.fn.scrollspy.Constructor = s, t.fn.scrollspy.noConflict = function() {
        return t.fn.scrollspy = i, this
    }
}(jQuery);


var shrinkNum = 6;	
if($('#header-outer[data-shrink-num]').length > 0 ) shrinkNum = $('#header-outer').attr('data-shrink-num');
headerPadding2 = headerPadding - headerPadding/1.8;

setTimeout(scrollSpyInit,200);

var $animatedScrollingTimeout;

$('body').on('click','#header-outer nav .sf-menu a, #footer-outer .nectar-button, .container-wrap a:not(.wpb_tabs_nav a):not(.magnific):not([data-fancybox]):not(.woocommerce-tabs a):not(.testimonial-next-prev a), .swiper-slide .button a, #slide-out-widget-area a, #mobile-menu .container ul li a, #slide-out-widget-area .inner div a',function(e){
	


	var $hash = $(this).prop("hash");	

	$('body').addClass('animated-scrolling');
	clearTimeout($animatedScrollingTimeout);
	$animatedScrollingTimeout = setTimeout(function(){ $('body').removeClass('animated-scrolling'); },850);
	var $headerNavSpace = ($('body[data-header-format="left-header"]').length > 0 && $(window).width() > 1000) ? 0 : $('#header-space').outerHeight();
	if( $('.page-template-template-no-header-footer').length > 0 || $('.page-template-template-no-header').length > 0 ) { $headerNavSpace = 0; }

	if($hash && $('body').find($hash).length > 0 && $hash != '#top' && $hash != '' && $(this).attr('href').indexOf(window.location.href.split("#")[0]) !== -1 || $(this).is('[href^="#"]') && $hash != '' && $('body').find($hash).length > 0 && $hash != '#top') {

		//stop scrolling for certain elements
		//if($(this).parents('.tabbed').length > 0) return false;
		
		//update hash
		if(!$(this).hasClass('skip-hash')) {
			if(history.pushState) {
			    history.pushState(null, null, $hash);
			}
			else {
			    location.hash = $hash;
			}
		}

		if($(this).parents('ul').length > 0) { 
			$(this).parents('ul').find('li').removeClass('current-menu-item');
			//$(this).parents('li').addClass('current-menu-item');
		}

		//side widget area click
		if($(this).parents('#slide-out-widget-area').length > 0){
			
			if($('body.material[data-slide-out-widget-area-style="slide-out-from-right"].material-ocm-open').length > 0) {
				$('body > .slide_out_area_close').addClass('non-human-allowed').trigger('click');
				//scroll
				var $clickedLinkStore = $(this);
				setTimeout(function(){
					$clickedLinkStore.trigger('click');
				},1000);
				
			} else {
				$('#slide-out-widget-area .slide_out_area_close').addClass('non-human-allowed').trigger('click');
			}
			
			setTimeout(function(){
				if($('body.material[data-slide-out-widget-area-style="slide-out-from-right"]').length > 0) {
					$('body > .slide_out_area_close').removeClass('non-human-allowed');

				} else {
					$('#slide-out-widget-area .slide_out_area_close').removeClass('non-human-allowed');
				}
			},100);
		}

		//mobile menu click
		if($(this).parents('#mobile-menu').length > 0) $('#toggle-nav').trigger('click');
		var $mobileMenuHeight = ($(this).parents('#mobile-menu').length > 0) ? $(this).parents('#mobile-menu').height() : null;
		
		$timeoutVar = 1;
		if($('.nectar-box-roll').length > 0 && $('.container-wrap.bottomBoxOut').length > 0) {
			boxRoll(null,-1);
			$timeoutVar = 2050;
		} 

		var $that = $(this);

		setTimeout(function(){

			//scrolling
			var $headerSpace = ($('body[data-permanent-transparent="1"]').length > 0) ? 0 : parseInt($headerNavSpace);
			
			if( $('body[data-permanent-transparent="1"]').length == 0 ) {
				
				if(!$('body').hasClass('mobile')){
					$resize = ($('#header-outer[data-header-resize="0"]').length > 0) ? 0 : parseInt(shrinkNum) + headerPadding2*2;
					if($('#header-outer[data-remove-fixed="1"]').length > 0) { 
						$headerNavSpace = 0;
					}
					
					var $scrollTopDistance =  $($hash).offset().top - $mobileMenuHeight - parseInt($headerNavSpace) +$resize + 3 - nectarDOMInfo.adminBarHeight;
					
					
					//condesne
					if($('body.mobile').length == 0 && $('#header-outer[data-condense="true"]').length > 0) {
							var $headerSpan9 = $('#header-outer[data-format="centered-menu-bottom-bar"] header#top .span_9');
							var $secondaryHeader = $('#header-secondary-outer');
							var $headerHeightStored = $('#header-outer').height();
							
							$headerHeightCondensed = $headerHeightStored - ( parseInt($headerSpan9.height()) + parseInt($('#header-outer #logo').css('margin-top')) );
						
							$scrollTopDistance =  $($hash).offset().top - parseInt($headerNavSpace) + $headerHeightCondensed - nectarDOMInfo.adminBarHeight;
					}
					
					
				} else {
					var $scrollTopDistance = ($('#header-outer[data-mobile-fixed="1"]').length > 0) ? $($hash).offset().top + 2 - $headerNavSpace + nectarDOMInfo.adminBarHeight : $($hash).offset().top - $mobileMenuHeight - nectarDOMInfo.adminBarHeight + 1;	
				}

			} else {
				var $scrollTopDistance = $($hash).offset().top - nectarDOMInfo.adminBarHeight + 1;
			}

			if($('body[data-hhun="1"]').length > 0 && $('#header-outer[data-remove-fixed="1"]').length == 0) {
				//alter offset 
				if($('#header-outer.detached').length == 0 || $that.parents('.page-submenu[data-sticky="true"]').length > 0) 
					$scrollTopDistance = $scrollTopDistance + $headerNavSpace;

				//hide top header
				if($that.parents('.page-submenu[data-sticky="true"]').length > 0) { 
					$('#header-outer.detached').addClass('invisible');
					$('.page-submenu').addClass('header-not-visible').css('transform','translateY(0px)');
				}
			} 

			var $pageSubMenu = ($that.parents('.page-submenu[data-sticky="true"]').length > 0) ? $that.parents('.page-submenu').height() : 0;
			
			 if($('body.material').length > 0 && 
			 		$('#header-secondary-outer').length > 0 &&
			    $('body[data-hhun="1"]').length == 0 && 
			    $('#header-outer[data-remove-fixed="1"]').length == 0 && 
					!$('body').hasClass('mobile')) { 
						
						var $headerSecondary = $('#header-secondary-outer').height();
						
				} else {
						var $headerSecondary = 0;
				}

			nectar_scrollToY($scrollTopDistance - $pageSubMenu + $headerSecondary, 700, 'easeInOutQuint');
			

		},$timeoutVar);
		

		e.preventDefault();

	}

	if($hash == '#top') {
		//side widget area click
		if($(this).parents('#slide-out-widget-area').length > 0){
			$('#slide-out-widget-area .slide_out_area_close').trigger('click');
		}
	}


});



}




function searchResultMasonry() {
	var $searchContainer = $('#search-results');
	var $dividerNum = ($searchContainer.is('[data-layout="masonry-no-sidebar"]')) ? 4 : 3;
	
	$searchContainer.imagesLoaded(function(){
		
		$searchContainer.isotope({
			 itemSelector: '.result',
			 layoutMode: 'packery',
			 packery: { columnWidth: $('#search-results').width() / $dividerNum }
		});
		
		$searchContainer.find('article').css('opacity','1');
		
	});
				

	$(window).resize(function(){
		 $searchContainer.isotope({
				layoutMode: 'packery',
				packery: { columnWidth: $('#search-results').width() / $dividerNum }
		 });
	});
	
}

if($('body.search-results').length > 0 && $('#search-results article').length > 0 && $('#search-results[data-layout="list-no-sidebar"]').length == 0) { searchResultMasonry(); }



	//portfolio colors
	if($('.portfolio-items .col .style-3-alt').length > 0 || $('.portfolio-items .col .style-3').length > 0 || $('.portfolio-items .col .style-2').length > 0 || $('.portfolio-items .col .style-5').length > 0 ) {
		var portfolioColorCss = '';
		$('.portfolio-items .col').each(function(){
			$titleColor = $(this).attr('data-title-color');
			$subTitleColor = $(this).attr('data-subtitle-color');

			 if($titleColor.length > 0 ) { 
			 	portfolioColorCss += '.col[data-title-color="'+$titleColor+'"] .vert-center h3, .portfolio-items[data-ps="6"] .col[data-title-color="'+$titleColor+'"] .work-meta h4 { color: '+$titleColor+'!important; } ';
			 	portfolioColorCss += ' .portfolio-items[data-ps="8"] .col[data-title-color="'+$titleColor+'"] .line { background-color: '+$titleColor+'; }';
			 	portfolioColorCss += '.portfolio-items[data-ps="8"] .col[data-title-color="'+$titleColor+'"] .next-arrow line { stroke: '+$titleColor+'; } ';
			 }
			 if($subTitleColor.length > 0 ) portfolioColorCss += '.col[data-subtitle-color="'+$subTitleColor+'"] .vert-center p, .portfolio-items[data-ps="6"] .col[data-title-color="'+$titleColor+'"] .work-meta p { color: '+$subTitleColor+'; } ';
	
		});


		var head = document.head || document.getElementsByTagName('head')[0];
		var style = document.createElement('style');

			style.type = 'text/css';
		if (style.styleSheet){
		  style.styleSheet.cssText = portfolioColorCss;
		} else {
		  style.appendChild(document.createTextNode(portfolioColorCss));
		}

		head.appendChild(style);
	}

	//bottom controls 2
	$('body').on('mouseleave','.container-wrap[data-nav-pos="after_project_2"] #portfolio-nav ul li, .blog_next_prev_buttons[data-style="fullwidth_next_prev"] ul li',function(){
		$(this).addClass('mouse-leaving');
	});	

	// masonryPortfolio

	var $portfolio_containers = [];

	$('.portfolio-items:not(.carousel)').each(function(i){
		$portfolio_containers[i] = $(this);
	});

	function masonryPortfolioInit() {

		$portfolio_containers = [];
		$('.portfolio-items:not(.carousel)').each(function(i){
			$portfolio_containers[i] = $(this);
		});

		//// cache window
		var $window = jQuery(window);	
		
			
			$.each($portfolio_containers,function(i){

				
				//// start up isotope with default settings
				$portfolio_containers[i].imagesLoaded(function(){
					
					//verify smooth scorlling
					if( $smoothCache == true && $(window).width() > 690 && $('body').outerHeight(true) > $(window).height() && Modernizr.csstransforms3d && !navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini)/)){ niceScrollInit(); $(window).trigger('resize') } 
					
					//transformns enabled logic
					var $isoUseTransforms = true;
					
					//Panr 
					if(!$('body').hasClass('mobile') && !navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)) {

						$isoUseTransforms = true;
						
					}

					piVertCenter();

					//initial call to setup isotope
					var $layoutMode = ( $portfolio_containers[i].hasClass('masonry-items')) ? 'packery' : 'fitRows';
					var $startingFilter = ($portfolio_containers[i].attr('data-starting-filter') != '' && $portfolio_containers[i].attr('data-starting-filter') != 'default') ? '.' + $portfolio_containers[i].attr('data-starting-filter') : '*';

					reLayout();
					
					$portfolio_containers[i].addClass('isotope-activated');
					
					$portfolio_containers[i].isotope({
					  itemSelector : '.element',
					  filter: $startingFilter,
					  layoutMode: $layoutMode,
					  transitionDuration: '0.6s',
					  packery: {
						 gutter: 0
					  }
					}).isotope( 'layout' );
					
					
					if($startingFilter != '*'){
						$('.portfolio-filters ul a[data-filter="'+$startingFilter+'"], .portfolio-filters-inline ul a[data-filter="'+$startingFilter+'"]').click();
					}

					//call the reLayout to get things rollin'
					masonryZindex();
					setTimeout(function(){masonryZindex(); },800);
					
				
					
					//inside fwc fix
					if($portfolio_containers[i].parents('.full-width-content').length > 0) { setTimeout(function(){ fullWidthContentColumns(); },200);  }

					//fadeout the loading animation
					$('.portfolio-loading').stop(true,true).fadeOut(200);
					
					//fadeIn items one by one
					if($portfolio_containers[i].find('.inner-wrap').attr('data-animation') == 'none') {
						$portfolio_containers[i].find('.inner-wrap').removeClass('animated');
					} else {

						//portfolioLoadIn(); moved to waypoints
					}

			
				});
				


			});//each
			
			if($portfolio_containers.length > 0) {
				$window.resize( reLayout );

				$window.smartresize( function(){
					setTimeout(masonryZindex,700);
				});
			}
			
		

	}

	masonryPortfolioInit();

	function portfolioLoadIn() {

		$($fullscreenSelector+'.portfolio-items').each(function(){

			$portfolioOffsetPos = ($('#nectar_fullscreen_rows').length > 0) ? '200%' : '90%';

			if($(this).find('.inner-wrap').attr('data-animation') == 'none') return;
		
			$(this).find('.col').each(function(i){

				var $that = $(this);

				//loaded visible
				if($(this).visible(true) || $(this).parents('#nectar_fullscreen_rows').length > 0) {

					var $portfolioAnimationDelay = ($that.is('[data-masonry-type="photography"].masonry-items')) ? 90 : 115;
					$(this).delay($portfolioAnimationDelay *i).queue(function(next){
					    $(this).addClass("animated-in");
					    next();
					});

				} else {

					//not already visible
					var waypoint = new Waypoint({
		 			element: $that,
		 			 handler: function(direction) {
						
						if($that.parents('.wpb_tab').length > 0 && $that.parents('.wpb_tab').css('visibility') == 'hidden' || $that.hasClass('animated-in')) { 
						     waypoint.destroy();
						     return;
						}

						var $portfolioAnimationDelay = ($that.is('[data-masonry-type="photography"].masonry-items')) ? 85 : 100;
					
						setTimeout(function(){
							$that.addClass("animated-in");
						},$portfolioAnimationDelay * $that.attr('data-delay-amount'));
					
						
						waypoint.destroy();
					},
					offset: $portfolioOffsetPos

					}); //waypoint
				}

			}); //each
		}); //each
					
	}


	//perspective load in
	if($('.portfolio-items .inner-wrap[data-animation="perspective"]').length > 0 || $('.posts-container[data-load-animation="perspective"]').length > 0) {

		var lastScrollTop = $(window).scrollTop();

		$('.portfolio-items, .posts-container[data-load-animation="perspective"]').css('perspective-origin','50% '+ (lastScrollTop + $(window).height()) + 'px');

		requestAnimationFrame(updatePerspectiveOrigin);
		
		function updatePerspectiveOrigin() {

			var scrollTop = $(window).scrollTop();

			if (lastScrollTop === scrollTop) {
				requestAnimationFrame(updatePerspectiveOrigin);
				return;
			} else {
				lastScrollTop = scrollTop;
				$('.portfolio-items,.posts-container[data-load-animation="perspective"]').css('perspective-origin','50% '+ (lastScrollTop + $(window).height()) + 'px');
				requestAnimationFrame(updatePerspectiveOrigin);
			}
		}

	}

	var mediaQuerySize;
	function reLayout() {

		clearTimeout(clearIsoAnimation);
	    $('.portfolio-items .col').addClass('no-transition');
	    clearIsoAnimation = setTimeout(function(){  $('.portfolio-items .col').removeClass('no-transition'); },700); 

		var windowSize = $window.width();
		var masonryObj;
		var masonryObjHolder = [];

		//remove double quotes for FF
		//if (navigator.userAgent.match('MSIE 8') == null) {
		//	mediaQuerySize = mediaQuerySize.replace(/"/g, '');
		//}
		
		//user defined cols
		var userDefinedColWidth;

		$.each($portfolio_containers,function(i,v){

			if( $portfolio_containers[i].attr('data-user-defined-cols') == 'span4') {
				userDefinedColWidth = 3
			} 
			
			else if( $portfolio_containers[i].attr('data-user-defined-cols') == 'span3') {
				userDefinedColWidth = 4
			} 
			
			var isFullWidth = $portfolio_containers[i].attr('data-col-num') == 'elastic';
			
			
			//chrome 33 approved method for getting column sizing
			if(window.innerWidth > 1600){
				
				if($portfolio_containers[i].hasClass('fullwidth-constrained')) {
					if($portfolio_containers[i].is('[data-masonry-type="photography"]')) {
						mediaQuerySize = 'three';
					} else {
						mediaQuerySize = 'four';
					}
	
				} else {
					if($portfolio_containers[i].hasClass('constrain-max-cols')) {
						mediaQuerySize = 'four';
					} else {
						mediaQuerySize = 'five';
					}
				}
				
			} else if(window.innerWidth <= 1600 && window.innerWidth > 1300){

				if($portfolio_containers[i].hasClass('fullwidth-constrained')) {
					if($portfolio_containers[i].is('[data-masonry-type="photography"]')) {
						mediaQuerySize = 'three';
					} else {
						mediaQuerySize = 'four';
					}
				} else {
					mediaQuerySize = 'four';
				}
			} else if(window.innerWidth <= 1300 && window.innerWidth > 990){
				
				if($portfolio_containers[i].hasClass('constrain-max-cols')) {
					mediaQuerySize = 'four';
				} else {
					mediaQuerySize = 'three';
				}
				
			} else if(window.innerWidth <= 990 && window.innerWidth > 470){
				mediaQuerySize = 'two';
			} else if(window.innerWidth <= 470){
				mediaQuerySize = 'one';
			}
			
			//boxed
			if($('#boxed').length > 0) {
				if(window.innerWidth > 1300){
					mediaQuerySize = 'four';
				} else if(window.innerWidth < 1300 && window.innerWidth > 990){
					
					if($portfolio_containers[i].hasClass('constrain-max-cols')) {
						mediaQuerySize = 'four';
					} else {
						mediaQuerySize = 'three';
					}

				} else if(window.innerWidth < 990){
					mediaQuerySize = 'one';
				}
				
			}
			
			//change masonry columns depending on screen size
			switch (mediaQuerySize) {
				case 'five':
					(isFullWidth) ? colWidth = 5 : colWidth = userDefinedColWidth;
					//change cols for photography
					if(isFullWidth && $portfolio_containers[i].is('[data-masonry-type="photography"]')) colWidth = 6;

					masonryObj = { columnWidth: Math.floor($portfolio_containers[i].width() / parseInt(colWidth)) };
				break;
				
				case 'four':
					(isFullWidth) ? colWidth = 4 : colWidth = userDefinedColWidth;
					//change cols for photography
					if(isFullWidth && $portfolio_containers[i].is('[data-masonry-type="photography"]')) colWidth = 5;

					masonryObj = { columnWidth: Math.floor($portfolio_containers[i].width() / parseInt(colWidth)) };
				break;
				
				case 'three':
					(isFullWidth) ? colWidth = 3 : colWidth = userDefinedColWidth;
					//change cols for photography
					if(isFullWidth && $portfolio_containers[i].is('[data-masonry-type="photography"]')) colWidth = 4;
					
					masonryObj = { columnWidth: Math.floor($portfolio_containers[i].width() / parseInt(colWidth)) };
				break;
				
				case 'two':
					masonryObj = { columnWidth: Math.floor($portfolio_containers[i].width() / 2) };
				break;
				
				case 'one':
					masonryObj = { columnWidth: Math.floor($portfolio_containers[i].width() / 1) };
				break;
			}


			 //set widths
			 portfolioItemWidths(i,v);
			

			//sizing for large items
			if( $portfolio_containers[i].find('.col.elastic-portfolio-item[class*="regular"]:visible').length > 0 || $portfolio_containers[i].find('.col.elastic-portfolio-item[class*="wide"]:visible').length > 0 || $portfolio_containers[i].find('.col.elastic-portfolio-item[class*="tall"]:visible').length > 0 || $portfolio_containers[i].find('.col.elastic-portfolio-item[class*="wide_tall"]:visible').length > 0) {

				var $gutterSize = ($portfolio_containers[i].is('[data-gutter*="px"]') && $portfolio_containers[i].attr('data-gutter').length > 0 && $portfolio_containers[i].attr('data-gutter') != 'none') ? parseInt($portfolio_containers[i].attr('data-gutter')) : 0;
				var multipler = (window.innerWidth > 470) ? 2 : 1;

				//reset height for calcs
				$itemClassForSizing = 'regular';

				if($portfolio_containers[i].find('.col.elastic-portfolio-item[class*="regular"]:visible').length == 0 && $portfolio_containers[i].find('.col.elastic-portfolio-item.wide:visible').length > 0) {
					$itemClassForSizing = 'wide';
				} else if($portfolio_containers[i].find('.col.elastic-portfolio-item[class*="regular"]:visible').length == 0 && $portfolio_containers[i].find('.col.elastic-portfolio-item.wide_tall:visible').length > 0) {
					$itemClassForSizing = 'wide_tall';
					multipler = 1;
				} else if($portfolio_containers[i].find('.col.elastic-portfolio-item[class*="regular"]:visible').length == 0 && $portfolio_containers[i].find('.col.elastic-portfolio-item.tall:visible').length > 0) {
					$itemClassForSizing = 'tall';
					multipler = 1;
				}

			    $portfolio_containers[i].find('.col.elastic-portfolio-item.'+$itemClassForSizing+' img').css('height','auto');

				var tallColHeight = $portfolio_containers[i].find('.col.elastic-portfolio-item.'+$itemClassForSizing+':visible img').height();
				
				 $portfolio_containers[i].find('.col.elastic-portfolio-item[class*="tall"] img, .col.elastic-portfolio-item.wide img, .col.elastic-portfolio-item.regular img').removeClass('auto-height');
				 $portfolio_containers[i].find('.col.elastic-portfolio-item[class*="tall"] img:not(.custom-thumbnail)').css('height',(tallColHeight*multipler) + ($gutterSize*2));

				 if($itemClassForSizing == 'regular' || $itemClassForSizing == 'wide') {
				 	$portfolio_containers[i].find('.col.elastic-portfolio-item.wide img:not(.custom-thumbnail), .col.elastic-portfolio-item.regular img:not(.custom-thumbnail)').css('height',tallColHeight);
				 } else {
				 	$portfolio_containers[i].find('.col.elastic-portfolio-item.wide img:not(.custom-thumbnail), .col.elastic-portfolio-item.regular img:not(.custom-thumbnail)').css('height',(tallColHeight/2) - ($gutterSize*2));
				 }

				 $portfolio_containers[i].find('.col.elastic-portfolio-item[class*="tall"] .parallaxImg').css('height',(tallColHeight*multipler) + parseInt($portfolio_containers[i].find('.col.elastic-portfolio-item').css('padding-bottom'))*2 );
				 
				 if($itemClassForSizing == 'regular' || $itemClassForSizing == 'wide') {
					 $portfolio_containers[i].find('.col.elastic-portfolio-item.regular .parallaxImg, .col.elastic-portfolio-item.wide .parallaxImg').css('height',tallColHeight);
				} else {
				 	 $portfolio_containers[i].find('.col.elastic-portfolio-item.regular .parallaxImg, .col.elastic-portfolio-item.wide .parallaxImg').css('height',(tallColHeight/2) - ($gutterSize*2));
				 }

			} else {
				$portfolio_containers[i].find('.col.elastic-portfolio-item[class*="tall"] img, .col.elastic-portfolio-item.wide img, .col.elastic-portfolio-item.regular img').addClass('auto-height');
			}

			//non masonry
			if($portfolio_containers[i].hasClass('no-masonry') && $portfolio_containers[i].find('.col:first:visible').length > 0 && $portfolio_containers[i].parents('.wpb_gallery').length == 0){
			  
				//skip style 9
				if( !$portfolio_containers[i].is('[data-ps="9"]') ) {

				   	//reset height for calcs
			   	   $portfolio_containers[i].find('.col img').css('height','auto');
			   	   var tallColHeight = $portfolio_containers[i].find('.col:first:visible img').height();
			   	   $portfolio_containers[i].find('.col img:not(.custom-thumbnail)').css('height',tallColHeight);
			   	   $portfolio_containers[i].find('.col .parallaxImg').css('height',tallColHeight);
				 }
				 
			}
	


			masonryObjHolder[i] = masonryObj;
			
			if($portfolio_containers[i].isotope()) $portfolio_containers[i].isotope( 'layout' ); 
				
			

		}); //each
	
	}

	function portfolioItemWidths(i,v) {
		// passing each index value where function is called first to avoid nested loops
		// $.each($portfolio_containers,function(i,v){
		 		var isFullWidth = $portfolio_containers[i].attr('data-col-num') == 'elastic';

		 		if(isFullWidth) { 

				 	var $colSize = 4;
				 	var $mult = (mediaQuerySize == 'one') ? 1 : 2;
				 	if(mediaQuerySize == 'five') $colSize = 5;
				 	if(mediaQuerySize == 'four') $colSize = 4;
				 	if(mediaQuerySize == 'three') $colSize = 3;
				 	if(mediaQuerySize == 'two') $colSize = 2;
				 	if(mediaQuerySize == 'one') $colSize = 1;
				 	if($(v).is('[data-ps="6"]') && $colSize == 5) $colSize = 4;

				 	//photography
				 	if(isFullWidth && $portfolio_containers[i].is('[data-masonry-type="photography"]') && !$portfolio_containers[i].hasClass('no-masonry')) {
				 		if(mediaQuerySize == 'five') $colSize = 6;
				 		if(mediaQuerySize == 'four') $colSize = 5;
				 		if(mediaQuerySize == 'three') $colSize = 4;
				 	}
				 	
				 	if($(v).width() % $colSize == 0) {
					 	$(v).find('.elastic-portfolio-item:not(.wide):not(.wide_tall)').css('width',Math.floor($(v).width()/$colSize) +'px');
					 	$(v).find('.elastic-portfolio-item.wide, .elastic-portfolio-item.wide_tall').css('width',Math.floor($(v).width()/$colSize*$mult) +'px');
					 } else {
					 	var $loopEndNum = ($(window).width() > 1000) ? 6 : 3;
					 	if($portfolio_containers[i].hasClass('fullwidth-constrained') && $(window).width() > 1000) $loopEndNum = 4;
					 	//find closest number to give 0
					 	for(var i = 1; i<$loopEndNum; i++) {

					 		if(($(v).width() - i) % $colSize == 0) {
					 			$(v).find('.elastic-portfolio-item:not(.wide):not(.wide_tall)').css('width',($(v).width()- i)/$colSize +'px');
					 			$(v).find('.elastic-portfolio-item.wide, .elastic-portfolio-item.wide_tall').css('width',($(v).width()-i)/$colSize*$mult +'px');
					 		}

					 	}
					 }

				} // isFullWidth

		 	
		// });
	}

	//z-index for masonry
	function masonryZindex(){

		//escape if no browser support
		if($('body .portfolio-items:not(".carousel") > .col').length > 0 && $('body .portfolio-items:not(".carousel") > .col').offset().left) {

			$('body .portfolio-items:not(".carousel")').each(function(){

				var $coords = {};
				var $zindexRelation = {};
				var $that = $(this);

				$(this).find('> .col').each(function(){
					var $itemOffset = $(this).offset();
					$itemOffset = $itemOffset.left;

					$coords[$(this).index()] = $itemOffset;
					$(this).css('z-index',Math.abs(Math.floor($(this).offset().left/20)));
				});

				var $corrdsArr = $.map($coords, function (value) { return value; });

				$corrdsArr = removeDuplicates($corrdsArr);
				$corrdsArr.sort(function(a,b){return a-b});

				for(var i = 0; i < $corrdsArr.length; i++){
					$zindexRelation[$corrdsArr[i]] = i; 
				}
		
				$.each($coords,function(k,v){
					
					var $zindex;
					var $coordCache = v;
					$.each($zindexRelation,function(k,v){
						if($coordCache == k) {
							$zindex = v;
						}
					});
				
					$that.find('> .col:eq('+k+')').attr('data-delay-amount',$zindex);
				});

				
			});
			
			
			
		}


	}

	function blogMasonryZindex(){
	
		//escape if no browser support
		if($('body .post-area .masonry-blog-item').length > 0 && $('body .post-area .masonry-blog-item').offset().left) {
		
			$('body .post-area.masonry').each(function(){
				
				var $coords = {};
				var $zindexRelation = {};
				var $that = $(this);

				$(this).find('.masonry-blog-item').each(function(){
					var $itemOffset = $(this).offset();
					$itemOffset = $itemOffset.left;

					$coords[$(this).index()] = $itemOffset;
					$(this).css('z-index',Math.abs(Math.floor($(this).offset().left/20)));
				});

				var $corrdsArr = $.map($coords, function (value) { return value; });

				$corrdsArr = removeDuplicates($corrdsArr);
				$corrdsArr.sort(function(a,b){return a-b});
		
				for(var i = 0; i < $corrdsArr.length; i++){
					$zindexRelation[$corrdsArr[i]] = i*1; 
				}
		
				$.each($coords,function(k,v){
					
					var $zindex;
					var $coordCache = v;
					$.each($zindexRelation,function(k,v){
						if($coordCache == k) {
							$zindex = v;
						}
					});
		
					$that.find('.masonry-blog-item:eq('+k+')').css('z-index',$zindex).attr('data-delay-amount',$zindex);
					
				});
			
			});
			
		}
		
	}
	
	function matrixToArray(matrix) {
	    return matrix.substr(7, matrix.length - 8).split(', ');
	}
	
	function removeDuplicates(inputArray) {
        var i;
        var len = inputArray.length;
        var outputArray = [];
        var temp = {};

        for (i = 0; i < len; i++) {
            temp[inputArray[i]] = 0;
        }
        for (i in temp) {
            outputArray.push(i);
        }
        return outputArray;
    }

    //// filter items when filter link is clicked
	var clearIsoAnimation = null;
	var $checkForScrollBar = null;


	//number portfolios so multiple sortable ones can work easily on same page
	$('.portfolio-items:not(".carousel")').each(function(i){
		$(this).attr('instance',i);
		$(this).parent().parent().find('div[class^=portfolio-filters]').attr('instance',i);
	});

    function isoClickFilter(){
		 var $timeout;		 
		 if(window.innerWidth > 690 && !navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini)/)){
		 	
			  
			 clearTimeout($timeout);
			 $timeout = setTimeout(function(){masonryZindex();  },600);

			  
		 }
		  
		  var selector = $(this).attr('data-filter');
		  var $instance = $(this).parents('div[class^=portfolio-filters]').attr('instance');

		  $.each($portfolio_containers,function(i){
		  	if($portfolio_containers[i].attr('instance') == $instance) { 
		  		$portfolio_containers[i].isotope({ filter: selector }).attr('data-current-cat',selector);

		  		//fade in all incase user hasn't scrolled down yet
		  		if($portfolio_containers[i].find('.inner-wrap[data-animation="none"]').length == 0) {
			   		$portfolio_containers[i].find('.col').addClass('animated-in');
				}
		  	}
		  });




		  //active classes
		  $(this).parent().parent().find('li a').removeClass('active');
		  $(this).addClass('active');
		  
		  //update pp
		  if($('.portfolio-items a[rel^="prettyPhoto"]').length > 0) {
		  	setTimeout(updatePrettyPhotoGallery,170);
		  }

		  else {
		  	setTimeout(updateMagPrettyPhotoGallery,170);
		  }

		  return false;
	}

	////filter event
	$('body').on('click','.portfolio-filters ul li a, .portfolio-filters-inline ul li a', isoClickFilter);


	function updatePrettyPhotoGallery(){
		$('.portfolio-items').each(function(){

			if($(this).find('a[rel^="prettyPhoto"]').length > 0) {

			var $unique_id = Math.floor(Math.random()*10000);
			var $currentCat = $(this).attr('data-current-cat');
			$(this).find('.col'+$currentCat).find('a[rel^="prettyPhoto"]').attr('rel','prettyPhoto['+$unique_id+'_sorted]');
			
			} 
			
		});
	}

	function updateMagPrettyPhotoGallery(){
		$('.portfolio-items').each(function(){

			var $currentCat = $(this).attr('data-current-cat');
			var $unique_id = Math.floor(Math.random()*10000);
			
			if($(this).is('[data-lightbox-only="true"]')){
				
					$(this).find('.col').each(function(){
		
						$(this).find('a.gallery').removeClass('gallery').removeClass('magnific');
						
						if($(this).is($currentCat)) {
							
							//parallax styles
							if($(this).find('.parallaxImg-wrap').length > 0) {
								
									if($('body[data-ls="fancybox"]').length > 0) {
										$(this).find('.work-item > a').attr('data-fancybox','group_'+$unique_id);
									} else {
										$(this).find('.work-item > a').addClass('gallery').addClass('magnific');
									}
									
							} else {
								//others
								
									if($('body[data-ls="fancybox"]').length > 0) {
										$(this).find('.work-item a').attr('data-fancybox','group_'+$unique_id);
									} else {
										$(this).find('.work-info a').addClass('gallery').addClass('magnific');
									}
									
							}
							
						}
		
					});
				
			}
			
			else if ($(this).find('.work-item.style-1').length > 0){
				
					$(this).find('.col').each(function(){
		
						$(this).find('a.gallery').removeClass('gallery').removeClass('magnific');
						
						if($(this).is($currentCat)) {
							
								if($('body[data-ls="fancybox"]').length > 0) {
									$(this).find('.work-info .vert-center a:first-of-type').attr('data-fancybox','group_'+$unique_id);
								} 
								else {
									$(this).find('.work-info .vert-center a:first-of-type').addClass('gallery').addClass('magnific');
								}
								
						}
		
					});
			}
			
		
			
		});
	}



	$blog_containers = [];

	$('.posts-container').each(function(i){
		$blog_containers[i] = $(this);
	});

  function masonryBlogInit() {
	  
	  $blog_containers = [];

	$('.posts-container').each(function(i){
		$blog_containers[i] = $(this);
	});

		//// cache window
		var $window = jQuery(window);	
		
			
		$.each($blog_containers,function(i){

			
				if($blog_containers[i].parent().hasClass('masonry') && !$blog_containers[i].parent().hasClass('auto_meta_overlaid_spaced')) { 
					
					$blog_containers[i].find('article').addClass('masonry-blog-item');
					
					if($blog_containers[i].parents('.masonry.classic').length > 0) {
						$blog_containers[i].find('article').prepend('<span class="bottom-line"></span>');
					}
					
					//move the meta to the bottom
					$blog_containers[i].find('article').each(function(){
						
						var $metaClone = $(this).find('.post-meta').clone();

						$(this).find('.post-meta').remove();

						if($blog_containers[i].parents('.post-area.meta_overlaid').length > 0){
							$(this).find('.post-header h2').after($metaClone);
						} else {
							$(this).find('.content-inner').after($metaClone);
						}
						
						$blog_containers[i].addClass('meta-moved');
						
					});
				
					
					if($blog_containers[i].parent().hasClass('masonry') && $blog_containers[i].parents('.blog-fullwidth-wrap').length > 0){
						//$blog_containers[i].parent().wrap('<div class="full-width-content blog-fullwidth-wrap"> </div>').removeClass('full-width-content').css({'margin-left':'0','width':'auto'});
						
						//page header animation fix
						if( $blog_containers[i].parents('.wpb_row').length > 0 ) $blog_containers[i].parents('.wpb_row').css('z-index',100);

						if(!$blog_containers[i].parent().hasClass('meta_overlaid') && !$blog_containers[i].parent().hasClass('auto_meta_overlaid_spaced')) {

							if($blog_containers[i].parent().hasClass('classic_enhanced')) {
								$blog_containers[i].parent().parents('.full-width-content').css({
									'padding' : '0px 0.2% 0px 2.4%'
								});
							} else {
								$blog_containers[i].parent().parents('.full-width-content').css({
									'padding' : '0px 0.2% 0px 3.2%'
								});
							}
							
						} else {
							$blog_containers[i].parent().parents('.full-width-content').addClass('meta-overlaid');
							$('.container-wrap').addClass('meta_overlaid_blog');
						}

						fullWidthSections(); 
					}
					
					var $cols = 3;
					var $element = $blog_containers[i];
					
					if($blog_containers[i].find('img').length == 0) $element = $('<img />');
					
					imagesLoaded($element,function(instance){
						
						if($('body').hasClass('mobile') || $blog_containers[i].parents('.post-area').hasClass('span_9')) {
							$cols = 2;
						}

						//set img as BG if masonry classic enhanced
						if($blog_containers[i].parent().hasClass('classic_enhanced')){
							$blog_containers[i].find('.large_featured.has-post-thumbnail .post-featured-img, .wide_tall.has-post-thumbnail .post-featured-img').each(function(){
								var $src = $(this).find('img').attr('src');
								$(this).css('background-image','url('+$src+')');
							});

							$blog_containers[i].find('.large_featured .nectar-flickity, .wide_tall .nectar-flickity').each(function(){

								$(this).find('.cell').each(function(){
									var $src = $(this).find('img').attr('src');
									$(this).css('background-image','url('+$src+')');
								});
								
							});
						}

						$cols = blogColumnNumbCalcs($blog_containers[i]);
						blogHeightCalcs($blog_containers[i], $cols);

						if($blog_containers[i].parents('.post-area.meta_overlaid').length > 0) {
							$blog_containers[i].isotope({
							   itemSelector: 'article',
							   transitionDuration: '0s',
							   layoutMode: 'packery',
							   packery: { 
							   	 gutter: 0
							   	}
							}).isotope( 'layout' );

						   
						} else if ($blog_containers[i].parents('.auto_meta_overlaid_spaced').length > 0) {
							
							$multiplier = .025;
							if($blog_containers[i].parents('.blog-fullwidth-wrap').length > 0) {
									$multiplier = .02;
							}
							
							$blog_containers[i].isotope({
								layoutMode: 'packery',
								transitionDuration: '0s',
								packery: {
									 /* gutter: $blog_containers[i].parents('.post-area').width() * $multiplier */
									 gutter: 0
								}
						 });
						 
						}  else {
						   if($blog_containers[i].parent().hasClass('classic_enhanced')) {
							   	if($blog_containers[i].parents('.span_9.masonry').length == 0) {
							   		$multiplier = (window.innerWidth >= 1600) ? .015 : .02;
							   	} else {
							   		$multiplier = .04;
							    } 
						    }
							else {
 								 		$multiplier = ($blog_containers[i].parents('.span_9.masonry').length == 0) ? .03: .055;
 							 }
						   
							$blog_containers[i].isotope({
							   itemSelector: 'article',
							   transitionDuration: '0s',
							   layoutMode: 'packery',
							   packery: { 
							   	 gutter: $blog_containers[i].parents('.post-area').width()*$multiplier
							   	}
							}).isotope( 'layout' );
						}

						blogLoadIn($blog_containers[i]);

						
						$(window).trigger('resize');

							
					});
					
					$(window).resize(function(){
						

					   //size all items in grid 
					   //sizing for large items
						 if(typeof $blog_containers[i] !== 'undefined') { 
							 
							  $cols = blogColumnNumbCalcs($blog_containers[i]);
								blogHeightCalcs($blog_containers[i], $cols);
								
								if($blog_containers[i].parents('.post-area.meta_overlaid').length > 0) {
								
								    $blog_containers[i].isotope({
								      layoutMode: 'packery',
								      packery: {
								      	 gutter: 0
								      }
								   });
								} else if ($blog_containers[i].parents('.auto_meta_overlaid_spaced').length > 0) {
									
									$multiplier = .025;
									if($blog_containers[i].parents('.blog-fullwidth-wrap').length > 0) {
											$multiplier = .02;
									}
									
									$blog_containers[i].isotope({
										layoutMode: 'packery',
										transitionDuration: '0s',
										packery: {
											/*
											 gutter: $blog_containers[i].parents('.post-area').width() * $multiplier
											 */
											 gutter: 0
										}
								 });
								} else {
								   
								   if($blog_containers[i].parent().hasClass('classic_enhanced')) {
								   		if($blog_containers[i].parents('.span_9.masonry').length == 0) {
									   		$multiplier = (window.innerWidth >= 1600) ? .015 : .02;
									   	} else {
									   		$multiplier = .04;
									    } 
								   } else {
										 		$multiplier = ($blog_containers[i].parents('.span_9.masonry').length == 0) ? .03: .055;
												if($blog_containers[i].parents('.blog-fullwidth-wrap').length > 0) {
														$multiplier = .02;
												}
									 } 
								  
								   $blog_containers[i].isotope({
								   	layoutMode: 'packery',
								      packery: { 
								      	gutter: $blog_containers[i].parents('.post-area').width()*$multiplier
								      }
								   });
								}
								
							} // if $blog_containers[i]

					});
					
					
			    } else {
			    	blogLoadIn($blog_containers[i]);
			    }

		});
		
		//set z-index / animation order only once
		setTimeout(blogMasonryZindex,700);
		$window.smartresize( function(){
			setTimeout(blogMasonryZindex,700);
		});
		
	}

//PPTA////////////////////////////////////////////////////////////////////////////////////////////////////////

// Create Base64 Object
let Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

function checkbookmarks(){
	
	let locals = allStorage();
	if (locals.length > 0){
		$('#nectar-user-account').removeClass('tohide');
     	$('.favourites').removeClass('tohide');
		$('.mobile-user-account').attr('style','');
		
	} else {
		$('#nectar-user-account').addClass('tohide');
     	$('.favouritest').addClass('tohide');
		$('.mobile-user-account').attr('style','display:none !important;');
	}
  
	$('article').each( function( index, element ){
		if (localStorage.getItem("fav-"+$(this).attr('id')) != null) {
			$(this).find('.star').removeClass('tohide');
		}
	});		
			
}

function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

function gec(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
	  var c = ca[i];
	  while (c.charAt(0) == ' ') {
		c = c.substring(1);
	  }
	  if (c.indexOf(name) == 0 && c.substring(name.length, c.length).length > 2) {
		return c.substring(name.length, c.length);
	  }
	}
	return 0;
  }

function InitApp(){

    if ($('.favouritespage').length == 0) { 

        var prefix = "";

        if(listings.items.length > 5){
        
            //Init Pagination
            document.getElementById("controlbar").style.bottom = "-60px";
        
            $(window).scroll(function() {
            
            if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
                document.getElementById("controlbar").style.bottom = "0px";
            } else {
                document.getElementById("controlbar").style.bottom = "-60px";
            }
            
            });
        }
    } else {
        var prefix = 'book-';
    }

	 //Get screen height percentage
  var percent = 0.8;
  var adjustedHeight =  ($(window).height()*percent)-40;
  
	checkbookmarks();
if (!isMobileDevice()) { 
	$('.modalurl').each( function( index, element ){
		
		var id = $(this).attr('data-id');
        var url = $('#'+prefix+id).attr('data-url');
		var title = $('#'+prefix+id).attr('data-title');
		var agent = $('#'+prefix+id).attr('data-agent');

		//PERRY FIX
		if (agent == "6"){ url = "http://gallarija.mt/widgets/redirect.html?"+encodeURIComponent(url);	}
      
		if ($(window).width() < 500) { var full = true; } else  { var full = false; }
		
		$('#'+prefix+id).iziModal({
		headerColor: '#2E2D2C',
		background: '#2E2D2C',
		title: title,
		icon: 'icon-settings_system_daydream',
		width: '80%',
		iframeHeight: adjustedHeight,
		iframeURL: url,
		arrowKeys: true,
		navigateCaption: true,
		navigateArrows: 'closeScreenEdge',
		zindex: 99999999,
		closeOnEscape: true,
		overlayClose: true,
		overlayColor: 'rgba(0, 0, 0, 0.9)',
		iframe : true,
		history: true,
		fullscreen: true,
		transitionIn: 'comingIn',
		transitionOut: 'comingOut',
		transitionInOverlay: 'fadeIn',
		transitionOutOverlay: 'fadeOut',
		openFullscreen: full,
		borderBottom: false,
		onOpening: function(){
			$('html').addClass('modalopen'); 
			
			if (agent == "6"){ 
				$('.iziModal-iframe')[0].removeAttribute('sandbox'); 
			 } else{
				$('.iziModal-iframe')[0].setAttribute('sandbox', 'allow-scripts allow-forms');
			}

			$.ajax({url: "https://search.gallarija.mt/events/"+Base64.encode('1@'+$("#l-"+id).attr('data-lid')+'@'+gec('_ga')),type: "GET"});
			gtag('event', 'view', {'event_category': 'listing','event_label': $("#l-"+id).attr('data-lid') });
		},
		onClosing: function(){$('html').removeClass('modalopen'); window.history.pushState(null, "", window.location.href.split('#')[0]); }
		});
		
	});
}
	
	//Refine search form
	$('#refinesearch').iziModal({
		headerColor: '#2E2D2C',
		background: '#2E2D2C',
		title: "Refine Search",
		icon: 'icon-settings_system_daydream',
		width: 400,
		iframeHeight: 550,
		iframeURL: '/widgets/search.html?'+search,
		zindex: 99999999,
		closeOnEscape: true,
		overlayClose: true,
		overlayColor: 'rgba(0, 0, 0, 0.9)',
		iframe : true,
		fullscreen: false,
		history: false,
		transitionIn: 'comingIn',
		transitionOut: 'comingOut',
		transitionInOverlay: 'fadeIn',
		transitionOutOverlay: 'fadeOut',
		openFullscreen: false,
		borderBottom: false,
		onOpening: function(){$('html').addClass('modalopen');},
		onClosing: function(){$('html').removeClass('modalopen');}
		});


	$('.modalurl').on('click', function(e) {
			
			e.preventDefault();
			var id = $(this).attr('data-id');

			if (isMobileDevice()) { 
				$.ajax({url: "https://search.gallarija.mt/events/"+Base64.encode('1@'+$("#l-"+id).attr('data-lid')+'@'+gec('_ga')),type: "GET"});
				gtag('event', 'view', {'event_category': 'listing','event_label': $("#l-"+id).attr('data-lid') });
				var win = window.open($('#'+id).attr('data-url'), '_self'); 
				win.focus();
			 } else  {  
				$('#'+prefix+id).iziModal('open');
			}

	});

	$('.bookmark').on('click', function(e) {
			
		e.preventDefault();
		var id = "l-"+$(this).attr('data-id');
		
		
		if (localStorage.getItem("fav-"+id) === null) {
		$.ajax({url: "https://search.gallarija.mt/events/"+Base64.encode('2@'+$("#"+id).attr('data-lid')+'@'+gec('_ga')),type: "GET"});
		gtag('event', 'addbookmark', {'event_category': 'listing','event_label': $("#"+id).attr('data-lid') });
			 localStorage.setItem("fav-"+id, Base64.encode($('#'+id)[0].outerHTML));
			 iziToast.success({
				title: '',
				theme: 'light',
				overlay: false,
				iconColor: '#ffffff',
				backgroundColor: '#00af66',
				icon: 'fa-thumbs-up',
				transitionIn: 'fadeInUp',
				transitionOut: 'fadeOutDown',
				balloon: true,
			   buttons: [
					['<button>View Favourites</button>', function (instance, toast) {
						 window.location.replace("/favourites/");
					}, true]
				],
				position: 'bottomRight',
				zindex: 99999999,
				overlayClose: true,
				overlayColor: 'rgba(0, 0, 0, 0.8)',
				message: 'This Listing has been added to your favourites.',
			  });
			$('#'+id+' .star').removeClass('tohide');
			
		} else {
			$.ajax({url: "https://search.gallarija.mt/events/"+Base64.encode('3@'+$("#"+id).attr('data-lid')+'@'+gec('_ga')),type: "GET"});
			gtag('event', 'removebookmark', {'event_category': 'listing','event_label': $("#"+id).attr('data-lid') });
			$('#'+id+' .star').addClass('tohide');
			localStorage.removeItem("fav-"+id);
			
			if (prefix == "book-"){
			  var localitems = allStorage();
			  	$('#'+id).remove();
                $(window).trigger('resize');
				if(localitems.length > 0){
					$('#totalfav').html(localitems.length+' Propert'+ (localitems.length > 1 ? 'ies' : 'y') +' Found');
				}else{
					$('#totalfav').addClass('tohide');
					$('.goback').removeClass('tohide');
					$('#pagetitle').html('You have no<br/>favourite listings yet');
				}
				
			}
            
		  
		   iziToast.error({
				title: '',
				theme: 'light',
				overlay: false,
				iconColor: '#ffffff',
				backgroundColor: '#bd5b5b',
				transitionIn: 'fadeInUp',
				transitionOut: 'fadeOutDown',
				   icon: ' fa-exclamation',
				balloon: true,
				position: 'bottomRight',
				zindex: 99999999,
				overlayClose: true,
				overlayColor: 'rgba(0, 0, 0, 0.8)',
				message: 'This listing has been removed from your favourites.',
			  });
			
		}
		
		checkbookmarks();
});
	
	$('.getlink').on('click', function(e) {
		
		e.preventDefault();
      
		var id = $(this).attr('data-id');
 		var url = $('#'+prefix+id).attr('data-url');
		copyStringToClipboard(url);
      
        iziToast.success({
        title: '',
        theme: 'light',
        overlay: false,
        iconColor: '#ffffff',
        backgroundColor: '#00af66',
        transitionIn: 'fadeInUp',
        transitionOut: 'fadeOutDown',
        balloon: true,
        icon: 'fa-copy',
        position: 'bottomRight',
        zindex: 99999999,
        overlayClose: true,
        overlayColor: 'rgba(0, 0, 0, 0.8)',
        message: 'Listing URL copied to your clipboard.',
		});
		
		$.ajax({url: "https://search.gallarija.mt/events/"+Base64.encode('4@'+$("#l-"+id).attr('data-lid')+'@'+gec('_ga')),type: "GET"});
		gtag('event', 'copylink', {'event_category': 'listing','event_label': $("#l-"+id).attr('data-lid') });
	
	});
	
	$('.refine').on('click', function(e) {
		e.preventDefault();
		$('#refinesearch').iziModal('open');
	});

	$('.contact').on('click', function(e) {

		e.preventDefault();
		var id = $(this).attr('data-id');
		var title = $('#'+prefix+id).attr('data-title');
		$('#contactform').iziModal('destroy');
		$('#contactform').iziModal({
			headerColor: '#2E2D2C',
			background: '#2E2D2C',
			title: title,
			icon: 'icon-settings_system_daydream',
			width: 500,
			iframeHeight: 450,
			iframeURL: '/widgets/contact.html?'+$("#l-"+id).attr('data-lid'),
			zindex: 99999999,
			closeOnEscape: true,
			overlayClose: true,
			overlayColor: 'rgba(0, 0, 0, 0.9)',
			iframe : true,
			fullscreen: false,
			history: false,
			transitionIn: 'comingIn',
			transitionOut: 'comingOut',
			transitionInOverlay: 'fadeIn',
			transitionOutOverlay: 'fadeOut',
			openFullscreen: false,
			borderBottom: false,
			onOpening: function(){$('html').addClass('modalopen');},
			onClosing: function(){$('html').removeClass('modalopen'); }
		});
		$('#contactform').iziModal('open');
	
	});

}

function copyStringToClipboard (str) {
   var el = document.createElement('textarea');
   el.value = str;
   el.setAttribute('readonly', '');
   el.style = {position: 'absolute', left: '-9999px'};
   document.body.appendChild(el);
   el.select();
   document.execCommand('copy');
   document.body.removeChild(el);
}

  //Fix Mobile Hover 
 function checkTouchDevice() {
	return 'ontouchstart' in document.documentElement;
}
function fixios(){
if (checkTouchDevice()) {
	$('.logo-overlay').addClass("touch");
	$('.logo-overlay').on('click', function() {
		
		if ($(this).hasClass("mobilehover")){
			$(this).removeClass("mobilehover");
		}else{
			$(".mobilehover").removeClass("mobilehover");
			$(this).addClass("mobilehover");
		}
	});
	$('.cta-container').on('click', function() {
		$(".mobilehover").removeClass("mobilehover");
	});
}
}

function setListings(){

     if (listings.status == 200) {
      	
      		if(listings.items.length > 0){
      		
			  //console.log(listings);
		  
      			//Show Results
				listitems();
      		    $('#searchtotal').html(listings.total.toLocaleString());
      			$('#currentpage').html(listings.page);
      			if (listings.nextpage === true) { 
      				$('#nextpage').html(Math.ceil(listings.total / 28).toLocaleString()); 
      				$('.nextarrow a').attr('href', '/search/?'+search+'@'+(listings.page+1)+'!'); 
      			} else { 
      				$('#nextpage').html(listings.page);
      				$('.nextarrow').addClass('hidden'); 
      			}
      			if (listings.page == 1) {
      				$('.prevarrow').addClass('hidden'); 
      			} else {
      				$('.prevarrow a').attr('href', '/search/?'+search+'@'+(listings.page-1)+'!'); 
				  }
				  
				gtag('event', url.split('@')[0], {'event_category': 'search','event_label': 'Page: '+$('#currentpage').text()+'/'+$('#nextpage').text() });

				//Refine listings suggestion popup
				if (listings.total > 50 && listings.page == 1){

					var popup = 0;
					
					$(window).scroll(function(e){
						var scrollTop = $(window).scrollTop();
						var docHeight = $(document).height();
						var winHeight = $(window).height();
						var scrollPercent = (scrollTop) / (docHeight - winHeight);
						var scrollPercentRounded = Math.round(scrollPercent*100);
						if (scrollPercentRounded > 30 && popup < 1){
							popup = 1;
							iziToast.success({
								title: '',
								theme: 'light',
								overlay: false,
								iconColor: '#ffffff',
								backgroundColor: '#00af66',
								icon: 'fa-search',
								transitionIn: 'fadeInUp',
								transitionOut: 'fadeOutDown',
								balloon: true,
							   buttons: [
									['<button>Refine Search</button>', function (instance, toast) {
										$('#refinesearch').iziModal('open');
										instance.hide({ transitionOut: 'fadeOut' }, toast);
									}, true]
								],
								position: 'bottomRight',
								zindex: 99999999,
								overlayClose: true,
								overlayColor: 'rgba(0, 0, 0, 0.8)',
								message: 'Too many results?',
							});
						}
					});
					
				}
      			
      		} else {
      		
      			//No results
      			$('#searchtotal').html('No');
      			$('#controlbar').addClass('tohide');
      			
			}
			  
			$('#searchtitle').html(ptitle);	
      	
      } else {
      	window.location.href = "/400";
      }

}

function setFavListings(){

    var localitems = allStorage();
	if(localitems.length > 0){
		$('#totalfav').html(localitems.length+' Propert'+ (localitems.length > 1 ? 'ies' : 'y') +' Found');
		$.each(localitems, function( index, value ) {
			$('.posts-container').append(Base64.decode(value));
		});
	}else{
		//$('.extras').addClass('tohide');
		$('#totalfav').addClass('tohide');
		$('.goback').removeClass('tohide');
		$('#pagetitle').html('You have no<br/>favourite listings yet');
	}

}

function setHistory(){

    var localitems = allHistory();
	if(localitems.length > 0){
		$('#totalhis').html(localitems.length+' Search'+ (localitems.length > 1 ? 'es' : '') +' Found');
		
		let sorted = {};
		let keys = [];
		
		$.each(localitems, function( index, value ) {
			let decoded = Base64.decode(value[1]);
			let id =  value[0];
			let url =  "/search?"+id.replace("his-",""); 
			let date =  decoded.split(',')[0];
			let timestamp = moment(date, 'DD/MM/YYYY HH:mm:ss').format('x');
			let search = getTitle(id.replace("his-",""));
			let total = parseInt(decoded.split(',')[1]).toLocaleString() +' Propert'+ (parseInt(decoded.split(',')[1]) > 1 ? 'ies' : 'y') +' Found';
			let item = `<article id="${id}" class="regular masonry-blog-item post type-post status-publish format-standard has-post-thumbnail category-uncategorized"> <div class="inner-wrap animated"> <div class="post-content"> <div class="content-inner"> <div data-logo="gallarija" class="logo-overlay"> <div class="cta-container"> <a class="nectar-button small regular accent-color wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown regular-button animated" href="${url}" data-color-override="false" data-hover-color-override="false" data-hover-text-color-override="#fff"><span>Search Again</span></a> <a class="deleteitem nectar-button small regular accent-color has-icon wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown regular-button" data-id="${id}" data-color-override="false" href="#" data-hover-color-override="false" data-hover-text-color-override="#fff"><span>Delete</span><i class="fa fa-times-circle"></i></a></div> </div <a class="entire-meta-link" href=""></a> <div class="article-content-wrap"> <h3 class="h_date">${date.split(' ')[0]}</h3> <p class="h_search">${search}</p> <h3 class="h_results">${total}</h3> </div> </div> </div> </div> </article>`;

			keys.push(timestamp);
			sorted[timestamp] = item;
			
		});
		
		keys.sort(function(a, b){return b - a});
		$.each(keys, function( index, value ) {
			$('.posts-container').append(sorted[value]);
		});
		
		$('.deleteitem').on('click', function(e) {
			e.preventDefault();
			var id = $(this).attr('data-id');
			localStorage.removeItem(id);
			let localitems = allHistory();
			$(this).closest('article').remove();
            $(window).trigger('resize');
			if(localitems.length > 0){
				$('#totalhis').html(localitems.length+' Search'+ (localitems.length > 1 ? 'es' : '') +' Found');
			}else{
				$('#totalhis').addClass('tohide');
				$('.goback').removeClass('tohide');
				$('#pagetitle').html("Your search history<br/>is empty");
			}

		    iziToast.error({
				title: '',
				theme: 'light',
				overlay: false,
				iconColor: '#ffffff',
				backgroundColor: '#bd5b5b',
				transitionIn: 'fadeInUp',
				transitionOut: 'fadeOutDown',
				   icon: ' fa-exclamation',
				balloon: true,
				position: 'bottomRight',
				zindex: 99999999,
				overlayClose: true,
				overlayColor: 'rgba(0, 0, 0, 0.8)',
				message: 'This search has been removed from your history.',
			});
			
		});
		
	}else{
		$('#totalhis').addClass('tohide');
		$('.goback').removeClass('tohide');
		$('#pagetitle').html("Your search history<br/>is empty");
	}

}


function allStorage() {
    var values = [],
        keys = Object.keys(localStorage),
        i = keys.length;

    while ( i-- ) {
        if(keys[i].substring(0,4) == "fav-") {values.push( localStorage.getItem(keys[i]) );}
    }
    return values;
}

function allHistory() {
    var values = [],
        keys = Object.keys(localStorage),
        i = keys.length;

    while ( i-- ) {
        if(keys[i].substring(0,4) == "his-") {values.push( [keys[i], localStorage.getItem(keys[i])] );}
    }
    return values;
}

if($('.searchpage').length == 0 && $('.favouritespage').length == 0 && $('.historypage').length == 0){
	
	checkbookmarks();
	splitLineHeadings();
	animated_titles();
	highlighted_text();
	masonryBlogInit();
	
}

if($('.searchpage').length == 1){

	function checkajax() {
		if(listings.length === 0) {
		   window.setTimeout(checkajax, 50);
		} else {
			setListings();
			fixios();
			totalimages = $('.attachment-large').length;
            checkload();
		}
	}
    checkajax();
    
    function checkload() {
        if(totalimages == imgcounter) {
			checkbookmarks();
			if ($(window).width() < 680) { $('.has-icon').removeClass('has-icon'); }
            $('#ajax-loading-screen').transition({'opacity':0},500,function(){ $(this).css({'display':'none'}); }); 
            $('#ajax-loading-screen .loading-icon').transition({'opacity':0},500);
            splitLineHeadings();
            masonryBlogInit();
            InitApp();
        } else {
            window.setTimeout(checkload, 50);
        }
    }
	
}

if($('.favouritespage').length == 1){

	setFavListings();
	fixios();
    checkbookmarks();
    $('#ajax-loading-screen').transition({'opacity':0},500,function(){ $(this).css({'display':'none'}); }); 
    $('#ajax-loading-screen .loading-icon').transition({'opacity':0},500);
    splitLineHeadings();
    animated_titles();
	masonryBlogInit();
	$(".mobilehover").removeClass("mobilehover");
   InitApp();
	
}

if($('.historypage').length == 1){

    checkbookmarks();
    setHistory();
    $('#ajax-loading-screen').transition({'opacity':0},500,function(){ $(this).css({'display':'none'}); }); 
    $('#ajax-loading-screen .loading-icon').transition({'opacity':0},500);
    splitLineHeadings();
    animated_titles();
    masonryBlogInit();
	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function listitems(){

	listings.items.forEach(function(item) {
		let id = item._id.replace(/=/g,'');
		let agent = item._source.agent;
		//Reduce Size if more than 220 chars
		let imgid = item._id;
		if (imgid.length > 220) { imgid = imgid.substr(imgid.length - 220); }
		let img = item._source.hasimg === true ? '/images/'+imgid+'.jpg' : '/assets/images/placeholder.jpg';
		let place = p_places[parseInt(item._source.place)];
		let type = p_types[parseInt(item._source.type)];
		let price = '€'+item._source.price.toLocaleString();
		let title = type+", "+place+", "+price;
		let url = item._source.url;
		let features = "";
		let oldprice = "";
		let reduction = "";
		if (item._source.oldprice > 0) { reduction = "reduction"; oldprice ='<span class="oldprice">€'+item._source.oldprice.toLocaleString()+"</span>"; }
		if (item._source.category == "0"){ price += '<span class="small"> / Month</span>'; }
		let right = "";
		let left = "";
		
		let firstFound = moment(item._source.firstFound, "DD/MM/YYYY HH:mm:ss");
		let lastUpdate = moment(item._source.lastUpdate, "DD/MM/YYYY HH:mm:ss");
		let today = moment();
		let age = today.diff(firstFound, 'days');
		let upd = today.diff(lastUpdate, 'days');
		
		if (age == 0){right = "Found Today";}		
		if (age > 0 && age < 31){right = age > 1 ? age+" days ago" : age+" day ago";}	
		if (age > 30){age = Math.round(age/30); right = age > 1 ? age+" months ago" : age+" month ago";}	

		//Badges Override
		if (age <= 5) {right = '<span class="new">NEW</span>';}
		if (age > 5 && upd <= 5) {right = '<span class="updated">UPDATED</span>';}
		
		if (item._source.bedrooms != ""){ left += '<i class="fa fa-bed"></i>&nbsp;'+item._source.bedrooms+'&nbsp;&nbsp;'; }
		if (item._source.bathrooms != ""){ left +='<i class="fa fa-bath"></i>&nbsp;'+item._source.bathrooms+'&nbsp;&nbsp;'; }
		if (item._source.area != ""){ left += '<i class="fa fa-arrows-v"></i>'+item._source.area; }

	let listing = `<article id="l-${id}" data-lid="${item._id}" class="regular masonry-blog-item post type-post status-publish format-standard has-post-thumbnail category-uncategorized"> <div class="inner-wrap animated"> <div class="post-content"> <div class="content-inner"> <a class="entire-meta-link" href=""></a> <span class="post-featured-img"> <div data-logo="${agent}" class="logo-overlay"> <div class="cta-container"> <a data-id="${id}" class="modalurl nectar-button medium regular extra-color-3 wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown regular-button animated" href="#" data-color-override="false" data-hover-color-override="false" data-hover-text-color-override="#fff"><span>View Listing</span></a> <br><div class="nectar_icon_wrap" data-style="soft-bg" data-draw="" data-border-thickness="2px" data-padding="10px" data-color="extra-color-2"> <div class="nectar_icon"><a href="#" data-id="${id}" class="getlink"></a><i class="fa cta fa-link"></i></div></div><div class="nectar_icon_wrap" data-style="soft-bg" data-draw="" data-border-thickness="2px" data-padding="10px" data-color="extra-color-2"> <div class="nectar_icon"><a href="#" class="bookmark" data-id="${id}"></a><i class="fa cta fa-star"></i></div></div><div class="nectar_icon_wrap" data-style="soft-bg" data-draw="" data-border-thickness="2px" data-padding="10px" data-color="extra-color-2"> <div class="nectar_icon"><a href="#" data-id="${id}" class="contact"></a><i class="fa cta fa-envelope"></i></div></div></div></div><img width="680" height="471" src="${img}" onerror="imgError(this, '${item._source.img}');" onload="checkheight(this);" class="attachment-large size-large skip-lazy wp-post-image" alt="" title=""/> </span> <div class="article-content-wrap"> <div class="post-header ${reduction}"> <div class="grav-wrap"> <a href="#" class="star tohide"><img src="/assets/images/star.png" class="avatar avatar-70 photo auto-height" height="70" width="70"></a> <div class="text"><a href="#" rel="author">${place}</a><span>${type}</span></div></div><h3 class="title">${oldprice}<div>${price}</div></h3> </div></div><div class="features-container ${features}"> <div class="feature">${left}</div><div class="feature">${right}</div></div></div></div></div><div id="${id}" data-url="${url}" data-title="${title}" data-agent="${agent}" data-izimodal-group="links" class="propertyframe"></div><div id="book-${id}" data-url="${url}" data-title="${title}" data-agent="${agent}" data-izimodal-group="bookmarks" class="propertyframe"></div></article>`;
		$('.posts-container').append(listing);
	});
	
}
//PPTA////////////////////////////////////////////////////////////////////////////////////////////////////////	

	function blogLoadIn(post_container){


		if(post_container.attr('data-load-animation') == 'none') {
		
			post_container.find('.inner-wrap').removeClass('animated');

		} else {

			post_container.find('article').each(function(i){

				//loaded visible
				if($(this).visible(true)) {

					$(this).delay(110*i).queue(function(next){
					    $(this).addClass("animated-in");
					    next();
					});


				} else {
					//not already visible
					var $that = $(this);
					var waypoint = new Waypoint({

			 			element: $that,
			 			 handler: function(direction) {
							
							setTimeout(function(){
							    $that.addClass("animated-in");
							},80*$that.attr('data-delay-amount'));

							waypoint.destroy();
						},
						offset: '90%'

					}); 
				}


			});
	
			
		}

	}

	function blogHeightCalcs($posts_container, cols) {
		if( $posts_container.parent().hasClass('meta_overlaid') && $posts_container.find('article[class*="regular"]').length > 0) {

			//widths
			$.each($posts_container,function(i,v){
			 	var $colSize = 4;
			 	var $mult = (cols == 1) ? 1 : 2;

			 	//check if higher than IE9 -- bugs out with width calc
			 	if($('html.no-csstransitions').length == 0) {
			 		$(v).find('article[class*="regular"]').css('width',Math.floor($(v).width()/cols) +'px');
			 		$(v).find('article[class*="tall"]').css('width',Math.floor($(v).width()/cols*$mult) +'px');
			 	} else {
			 		$('.post-area.masonry').css('width','100%');
			 	}
			 	
			 	
			 });

			   //reset height for calcs
			   $posts_container.find('article[class*="regular"] img').css('height','auto');

			   var tallColHeight = Math.ceil($posts_container.find('article[class*="regular"]:not(".format-link"):not(".format-quote") img').first().height());
			   var multipler = (window.innerWidth > 690) ? 2 : 1 ;
			   $posts_container.find('article[class*="tall"] img, .article.wide img, article.regular img').removeClass('auto-height');
			   $posts_container.find('article[class*="tall"] img').css('height',(tallColHeight*multipler));
			   $posts_container.find('article[class*="regular"] img').css('height',(tallColHeight));
			   //quote/links
			   $posts_container.find('article.regular.format-link,article.regular.format-quote').each(function(){

			   		if(window.innerWidth > 690) {
			   			$(this).css({
			  	 			'height': tallColHeight
			   			});
			   		} else {
			   			$(this).css({
			  	 			'height': 'auto'
			   			});			 		
			   		}
			  	 	
			   	});


		} else {
			$posts_container.find('article[class*="tall"] img, article.regular img').addClass('auto-height');
		}


		if( $posts_container.parent().hasClass('classic_enhanced') && $posts_container.find('article[class*="regular"]').length > 0) {
			
			if($(window).width() > 690 ) {
				classicEnhancedSizing($posts_container.find('article:not(.large_featured):not(.wide_tall)'));
			}
			else { 
				classicEnhancedSizing($posts_container.find('article:not(.wide_tall)'));
			}

			var tallColHeight = ($posts_container.find('article[class*="regular"]:not(".format-link"):not(".format-quote").has-post-thumbnail').first().length > 0) ? Math.ceil($posts_container.find('article[class*="regular"]:not(".format-link"):not(".format-quote").has-post-thumbnail').first().css('height','auto').height()) : 600;

			if($(window).width() > 690 ) {
				$posts_container.find('article.large_featured, article.regular, article[class*="wide_tall"]').css('height',(tallColHeight));
			}
			else {
				$posts_container.find('article.regular, article[class*="wide_tall"]').css('height',(tallColHeight));
			}

		//for when no regular articles exist	
		} else if( $posts_container.parent().hasClass('classic_enhanced') && $posts_container.find('article[class*="regular"]').length == 0) {
			var tallColHeight = ($posts_container.find('article[class*="regular"]:not(".format-link"):not(".format-quote").has-post-thumbnail').first().length > 0) ? Math.ceil($posts_container.find('article[class*="regular"]:not(".format-link"):not(".format-quote").has-post-thumbnail').first().css('height','auto').height()) : 600;

			if($(window).width() > 690 ) {
				$posts_container.find('article.large_featured, article.regular, article[class*="wide_tall"]').css('height',(tallColHeight));
			}
			else {
				$posts_container.find('article.regular, article[class*="wide_tall"]').css('height',(tallColHeight));
			}
		}

		//IE9 fix
		if($('html.no-csstransitions').length > 0) 		
			$('.post-area.masonry').css('width','100%');
			 	
			 	
	}

	function classicEnhancedSizing(elements) {

		var tallestCol = 0;
		elements.find('.article-content-wrap').css('height','auto');
		elements.filter('.has-post-thumbnail').each(function(){
			($(this).find('.article-content-wrap').outerHeight(true) > tallestCol) ? tallestCol = $(this).find('.article-content-wrap').outerHeight(true) : tallestCol = tallestCol;
		});	
		
		elements.filter('.has-post-thumbnail').find('.article-content-wrap').css('height',(tallestCol));

	}


	function blogStickySS() {
		$('#sidebar[data-nectar-ss="true"], #sidebar[data-nectar-ss="1"]').each(function(){

			//padding from top of screen
			$ssExtraTopSpace = 50;

			if($('#header-outer[data-remove-fixed="0"]').length > 0 && $('body[data-hhun="1"]').length == 0) { 
			 	$ssExtraTopSpace += $('#header-outer').outerHeight();	
				
				//resize effect
				if($('#header-outer[data-shrink-num][data-header-resize="1"]').length > 0 ) {
					 var shrinkNum = 6;		
					 var headerPadding2 = parseInt($('#header-outer').attr('data-padding')) - parseInt($('#header-outer').attr('data-padding'))/1.8;
					 shrinkNum = $('#header-outer').attr('data-shrink-num');
					 $ssExtraTopSpace -= shrinkNum;
					 $ssExtraTopSpace -= headerPadding2;
				}
				
				//condesne
				if($('body.mobile').length == 0 && $('#header-outer[data-condense="true"]').length > 0) {
					
						var $headerSpan9 = $('#header-outer[data-format="centered-menu-bottom-bar"] header#top .span_9');
						var $secondaryHeader = $('#header-secondary-outer');
						
						$ssExtraTopSpace = 50;
						$ssExtraTopSpace += $('#header-outer').height() - (parseInt($headerSpan9.position().top) - parseInt($('#header-outer #logo').css('margin-top')) ) - parseInt(nectarDOMInfo.secondaryHeaderHeight);
				}
				

			}

			if($('#wpadminbar').length > 0) {
				$ssExtraTopSpace += $('#wpadminbar').outerHeight();
			}

 			if($('#header-outer').attr('data-using-secondary') == '1') {
				$ssExtraTopSpace += $('#header-secondary-outer').outerHeight();
			}
			
			if($(this).parents('.wpb_widgetised_column').length > 0) {
				$(this).parents('.wpb_column').theiaStickySidebar({
			      additionalMarginTop: $ssExtraTopSpace,
						updateSidebarHeight: false
			    });
			} else {
				$(this).theiaStickySidebar({
			      additionalMarginTop: $ssExtraTopSpace,
						updateSidebarHeight: false
			    });
			}
			
		});
	}

	if( $().theiaStickySidebar ) {
		blogStickySS();
	}

	var blogMediaQuerySize;
	function blogColumnNumbCalcs(post_container){
		   if($('body').hasClass('mobile') && window.innerWidth < 990 || post_container.parents('.post-area').hasClass('span_9') && post_container.parents('.post-area.meta_overlaid').length == 0) {
			   $cols = 2;
		   } else if( post_container.parents('.post-area').hasClass('full-width-content') || post_container.parents('.post-area').parent().hasClass('full-width-content') && $('#boxed').length == 0 || post_container.parents('.post-area.meta_overlaid').length > 0 ){
		   		
				var windowSize = $(window).width();

				
				if(window.innerWidth >= 1600){
					blogMediaQuerySize = (post_container.parents('.post-area.meta_overlaid').length > 0) ? 'four' :'five';
				} else if(window.innerWidth < 1600 && window.innerWidth >= 1300){
					blogMediaQuerySize = 'four';
				} else if(window.innerWidth < 1300 && window.innerWidth >= 990){
					blogMediaQuerySize = (post_container.parents('.post-area.meta_overlaid').length > 0) ? 'four' :'three';
				} else if(window.innerWidth < 990 && window.innerWidth >= 470){
					blogMediaQuerySize = 'two';
				} else if(window.innerWidth < 470){
					blogMediaQuerySize = (post_container.parents('.post-area.meta_overlaid').length > 0) ? 'two' :'one';
				}
			
				
				//boxed
				if($('#boxed').length > 0) {
					if(window.innerWidth > 1300){
						blogMediaQuerySize = 'four';
					} else if(window.innerWidth < 1300 && window.innerWidth > 990){
						blogMediaQuerySize = (post_container.parents('.post-area.meta_overlaid').length > 0) ? 'four' :'three';
					} else if(window.innerWidth < 990){
						blogMediaQuerySize = (post_container.parents('.post-area.meta_overlaid').length > 0) ? 'two' :'one';
					}
					
				}
				
				
				switch (blogMediaQuerySize) {
					case 'five':
						$cols = 5;
					break;
					
					case 'four':
						$cols = 4;
					break;
					
					case 'three':
						$cols = 3;
					break;
					
					case 'two':
						$cols = 2;
					break;
					
					case 'one':
						$cols = 1;
					break;
				}
		   		
			
		   } else {

		   	   $cols = 3;
		   }

		   return $cols;

	}






var shrinkNum = 6;
		
if($('#header-outer[data-shrink-num]').length > 0 ) shrinkNum = $('#header-outer').attr('data-shrink-num');

headerPadding2 = headerPadding - headerPadding/1.8;

$('body').on('click','.section-down-arrow',function(){
	
	if($(this).parents('.nectar-box-roll').length > 0) return false;

	var $currentSection = $(this).parents('#page-header-bg');
	var $topDistance = $currentSection.height();
	var $offset = ($currentSection.parents('.first-section').length == 0 || $('body[data-transparent-header="false"]').length > 0) ? $currentSection.offset().top : 0;
	var $bodyBorderSize = ($('.body-border-top').length > 0 && $(window).width() > 1000) ? $('.body-border-top').height(): 0;
	var $headerNavSpace = ($('body[data-header-format="left-header"]').length > 0) ? 0 : $('#header-space').height();
	
	var $materialSecondary = 0;
	if($('body.material').length > 0 && $('#header-secondary-outer').length > 0) { $materialSecondary = $('#header-secondary-outer').height(); }
	
	if($('body[data-permanent-transparent="1"]').length == 0) {
		//regular
		if(!$('body').hasClass('mobile')){
			
			if($('body[data-hhun="1"]').length > 0 && $('#header-outer[data-remove-fixed="1"]').length == 0) {
				$('body,html').stop().animate({
					scrollTop: parseInt($topDistance) + $offset + 2 - $bodyBorderSize*2
				},1000,'easeInOutCubic')
			} 
			else {
				
				$resize = ($('#header-outer[data-header-resize="0"]').length > 0) ? 0 : parseInt(shrinkNum) + headerPadding2*2;
				if($('#header-outer[data-remove-fixed="1"]').length > 0) { 
					$headerNavSpace = 0;
					$offset = 0;
				}
					
				//condesne
				if($('body.mobile').length == 0 && $('#header-outer[data-condense="true"]').length > 0) {
					
						var $headerSpan9 = $('#header-outer[data-format="centered-menu-bottom-bar"] header#top .span_9');
						var $secondaryHeader = $('#header-secondary-outer');
						
						$headerNavSpace = $('#header-outer').height() - (parseInt($headerSpan9.position().top) - parseInt($('#header-outer #logo').css('margin-top')) ) - parseInt(nectarDOMInfo.secondaryHeaderHeight);
				}
				
				
				$('body,html').stop().animate({
					scrollTop: parseInt($topDistance - $headerNavSpace) +$resize + 3 + $offset + $materialSecondary
				},1000,'easeInOutCubic')

			}
			
		} else {
			$scrollPos = ($('#header-outer[data-mobile-fixed="1"]').length > 0) ? parseInt($topDistance) - $headerNavSpace + parseInt($currentSection.offset().top) + 2 : parseInt($topDistance) + parseInt($currentSection.offset().top) + 2;
			$('body,html').stop().animate({
				scrollTop: $scrollPos - $bodyBorderSize*2
			},1000,'easeInOutCubic')
		}
	} else {
		//permanent transparent
		$('body,html').stop().animate({
			scrollTop: parseInt($topDistance) + parseInt($currentSection.offset().top) + 2 - $bodyBorderSize*2
		},1000,'easeInOutCubic')
	}
	return false;
});


$('body').on('mouseover','.post-area.featured_img_left .grav-wrap .text a, .masonry.material .masonry-blog-item .grav-wrap .text a, .blog-recent[data-style="material"] .grav-wrap .text a',function(){
	$(this).parents('.grav-wrap').find('img').addClass('hovered');
});
$('body').on('mouseleave','.post-area.featured_img_left .grav-wrap .text a, .masonry.material .masonry-blog-item .grav-wrap .text a, .blog-recent[data-style="material"] .grav-wrap .text a',function(){
	$(this).parents('.grav-wrap').find('img').removeClass('hovered');
});  




/*-------------------------------------------------------------------------*/
/*	7.	Cross Browser Fixes
/*-------------------------------------------------------------------------*/	
	 
	 function crossBrowserFixes() {
	
		 
		//Fix current class in menu 
		if ($("body").hasClass("single-portfolio") || $('body').hasClass("error404") || $('body').hasClass("search-results")) {   
			$("li").removeClass("current_page_parent").removeClass("current-menu-ancestor").removeClass('current_page_ancestor');   
		}
		
		//for users updating to 8 with a custom, old header template file
		if($('html.js').length == 0) { $('html').removeClass('no-js').addClass('js'); }

		//fix for IE8 nth-child
		$('.recent_projects_widget div a:nth-child(3n+3), #sidebar #flickr div:nth-child(3n+3) a, #footer-outer #flickr div:nth-child(3n+3) a').css('margin-right','0px');
		
		//remove br's from code tag
		$('code').find('br').remove();	
		
		//if a clear is the last div, remove the padding
		if($('.container.main-content > .row > div:last-child').hasClass('clear')) {
			$('.container.main-content > .row > div:last-child').css('padding-bottom','0');
		}
		
		//homepage recent blog for IE8
		$('.container-wrap .blog-recent > div:last-child').addClass('col_last');
		
		//blog ascend bottom padding
		if($('.single .blog_next_prev_buttons').length > 0) $('.container-wrap').css('padding-bottom',0);

		//contact form
		$('.wpcf7-form p:has(input[type=submit])').css('padding-bottom','0px');

		$('.full-width-content .wpcf7-submit').on('click',function(){ setTimeout(function(){ fullWidthContentColumns() },1000); setTimeout(function(){ fullWidthContentColumns() },2000); });
		
		//no caption home slider fix
		$('#featured article').each(function(){
			if($(this).find('h2').attr('data-has-caption') == '0') {
				$(this).parents('.slide').addClass('no-caption');
			}
		});
		
		//add class for IE
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("Edge/");
		if(msie > 0)
			$('body').addClass('msie');

		//gravity form inside fw content row
		$('.gform_body').click(function(){
		   setTimeout(function(){ fullWidthContentColumns(); },200);
		 });
		 
		//pum salient close 
		$('.pum.pum-theme-salient-page-builder-optimized button.pum-close').wrapInner('<span />'); 
		
		//pum inside fspr
		if($('#nectar_fullscreen_rows').length > 0 && $('.pum-container .pum-content > .wpb_row .full-page-inner > .container > .span_12').length > 0) {
			$('.pum-container .pum-content > .wpb_row .full-page-inner > .container > .span_12').unwrap();
		}

		//chat post format nth child color
		$('article.post.format-chat .content-inner dt:odd').css('color','#333');
		
		//remove margin on last cols inside of full width sections 
		$('.full-width-section').each(function(){
			$(this).find('> .span_12 > div.col_last').last().css('margin-bottom','0');
		});
		
		//remove p tags from extra content editor when warpping only an img 
		$('#portfolio-extra p').each(function(){
			if($(this).find('*').length == 1 && $(this).find('img').length == 1) {
				$(this).find('img').unwrap();
			}
		});
	

		//vc text_separator color
		$('.vc_text_separator').each(function(){
			if( $(this).parents('.full-width-section').length > 0 ) $(this).find('div').css('background-color',$(this).parents('.full-width-section').find('.row-bg').css('background-color'));
		});
		
		//carousel head button alignment  
		$('.carousel-heading').each(function(){
			if($(this).find('h2').length > 0) $(this).find('.carousel-prev, .carousel-next').css('top','7px');
		});
		
		//remove carousel heading if not being used
		$('.carousel-wrap').each(function(){
			if($(this).find('.carousel-heading .container:empty').length > 0) $(this).find('.carousel-heading').remove();
		});
		
		//woocommerce product thuimbnails
		$('.woocommerce div.product div.images div.thumbnails a:nth-child(4n+4)').css('margin-right','0px');
		
		//remove extra galleries when using the nectar gallery slider on projects and posts
		$('article.post .gallery-slider .gallery,  article.post .gallery-slider .jetpack-slideshow, .single-portfolio .gallery-slider .gallery, .single-portfolio .gallery-slider .jetpack-slideshow').remove();
		
		
		$('.woocommerce .span_9 .products.related .products li:nth-child(4), .woocommerce .span_9 .products.upsells .products li:nth-child(4)').remove();
		$('.woocommerce .span_9 .products.related .products li:nth-child(3), .woocommerce .span_9 .products.upsells .products li:nth-child(3)').css('margin-right','0');	
		
		$('.cart-menu a, .widget_shopping_cart a').addClass('no-ajaxy');

		//clients no hover if no link
		$('div.clients').each(function(){
			$(this).find('> div').each(function(){
				if($(this).find('a').length == 0) {
					$(this).addClass('no-link');
				}
			});
		});

		//blog next color bg only 
		if($('.blog_next_prev_buttons').find('.bg-color-only-indicator').length > 0) $('.blog_next_prev_buttons').addClass('bg-color-only').find('.bg-color-only-indicator').remove();
		
		if($('#single-below-header').hasClass('fullscreen-header') && $('.blog_next_prev_buttons').length == 0 ) $('#author-bio, .comment-wrap').addClass('lighter-grey');

		//shop header parallax margin 
		if($('body.woocommerce').find('#page-header-bg').length > 0){
			$('.container-wrap').css({'margin-top':'0px','padding-top':'30px'});
		}
		
		//if using wooCommerce sitewide notice
		if($('.demo_store').length > 0) $('#header-outer, #header-space').css('margin-top','32px');
		
		//footer last column class for IE8
		$('#footer-widgets .container .row > div:last-child').addClass('col_last');
		
		//nectar slider external links
		$('.swiper-slide.external-button-1 .buttons > div:nth-child(1) a').attr('target','_blank');
		$('.swiper-slide.external-button-2 .buttons > div:nth-child(2) a').attr('target','_blank');
		
		//portfolio external links
		$(".portfolio-items").find("a[href*='http://']:not([href*='"+window.location.hostname+"'])").attr("target","_blank"); 
		$(".recent_projects_widget").find("a[href*='http://']:not([href*='"+window.location.hostname+"'])").attr("target","_blank"); 
		
		$(".portfolio-items").find("a[href*='https://']:not([href*='"+window.location.hostname+"'])").attr("target","_blank"); 
		$(".recent_projects_widget").find("a[href*='https://']:not([href*='"+window.location.hostname+"'])").attr("target","_blank"); 
		
		//remove excess inner content when empty row
		$('.container-wrap .row > .wpb_row').each(function(){
			if($(this).find('> .span_12 > .wpb_column > .wpb_wrapper').length > 0 && $(this).find('> .span_12 > .wpb_column > .wpb_wrapper').find('*').length == 0) $(this).find('> .span_12 ').remove();
		});
		
		//remove boxed style from full width content
		$('.full-width-content .col.boxed').removeClass('boxed');
		
		//remove full width attribute on slider in full width content section
		$('.full-width-content .wpb_column .nectar-slider-wrap[data-full-width="true"]').attr('data-full-width','false');	

	    //neg marg z-index
	    $('.wpb_column.neg-marg').parents('.wpb_row').css('z-index','110');


	    //portfolio description remove on hover
	    var $tmpTitle = null;
	    $('.portfolio-items > .col a[title]').hover(function(){
	    	$tmpTitle = $(this).attr('title');
	    	$(this).attr('title','');
	    },function(){
	    	$(this).attr('title', $tmpTitle);
	    });
	    $('.portfolio-items > .col a[title]').click(function(){
			$(this).attr('title', $tmpTitle);
	    });

	};

	crossBrowserFixes();



	jQuery( document.body ).on( 'updated_cart_totals', function() { 
		if($('.plus').length == 0) 
			$( 'div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<input type="button" value="+" class="plus" />' ).prepend( '<input type="button" value="-" class="minus" />' );
	});

	// Quantity buttons
	if($('.plus').length == 0) {
		$( 'div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<input type="button" value="+" class="plus" />' ).prepend( '<input type="button" value="-" class="minus" />' );

		$( document ).on( 'click', '.plus, .minus', function() {

			// Get values
			var $qty		= $( this ).closest( '.quantity' ).find( '.qty' ),
				currentVal	= parseFloat( $qty.val() ),
				max			= parseFloat( $qty.attr( 'max' ) ),
				min			= parseFloat( $qty.attr( 'min' ) ),
				step		= $qty.attr( 'step' );

			// Format values
			if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
			if ( max === '' || max === 'NaN' ) max = '';
			if ( min === '' || min === 'NaN' ) min = 0;
			if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

			// Change the value
			if ( $( this ).is( '.plus' ) ) {

				if ( max && ( max == currentVal || currentVal > max ) ) {
					$qty.val( max );
				} else {
					$qty.val( currentVal + parseFloat( step ) );
				}

			} else {

				if ( min && ( min == currentVal || currentVal < min ) ) {
					$qty.val( min );
				} else if ( currentVal > 0 ) {
					$qty.val( currentVal - parseFloat( step ) );
				}

			}

			// Trigger change event
			$qty.trigger( 'change' );

		});
	}

	function wooPriceSlider(){


		// woocommerce_price_slider_params is required to continue, ensure the object exists
		if ( typeof woocommerce_price_slider_params === 'undefined' || !$('body').hasClass('woocommerce') ) {
			return false;
		}

		// Get markup ready for slider
		$( 'input#min_price, input#max_price' ).hide();
		$( '.price_slider, .price_label' ).show();

		// Price slider uses jquery ui
		var min_price = $( '.price_slider_amount #min_price' ).data( 'min' ),
			max_price = $( '.price_slider_amount #max_price' ).data( 'max' );

		current_min_price = parseInt( min_price, 10 );
		current_max_price = parseInt( max_price, 10 );

		if ( woocommerce_price_slider_params.min_price ) current_min_price = parseInt( woocommerce_price_slider_params.min_price, 10 );
		if ( woocommerce_price_slider_params.max_price ) current_max_price = parseInt( woocommerce_price_slider_params.max_price, 10 );

		$( 'body' ).bind( 'price_slider_create price_slider_slide', function( event, min, max ) {
			if ( woocommerce_price_slider_params.currency_pos === 'left' ) {

				$( '.price_slider_amount span.from' ).html( woocommerce_price_slider_params.currency_symbol + min );
				$( '.price_slider_amount span.to' ).html( woocommerce_price_slider_params.currency_symbol + max );

			} else if ( woocommerce_price_slider_params.currency_pos === 'left_space' ) {

				$( '.price_slider_amount span.from' ).html( woocommerce_price_slider_params.currency_symbol + " " + min );
				$( '.price_slider_amount span.to' ).html( woocommerce_price_slider_params.currency_symbol + " " + max );

			} else if ( woocommerce_price_slider_params.currency_pos === 'right' ) {

				$( '.price_slider_amount span.from' ).html( min + woocommerce_price_slider_params.currency_symbol );
				$( '.price_slider_amount span.to' ).html( max + woocommerce_price_slider_params.currency_symbol );

			} else if ( woocommerce_price_slider_params.currency_pos === 'right_space' ) {

				$( '.price_slider_amount span.from' ).html( min + " " + woocommerce_price_slider_params.currency_symbol );
				$( '.price_slider_amount span.to' ).html( max + " " + woocommerce_price_slider_params.currency_symbol );

			}

			$( 'body' ).trigger( 'price_slider_updated', min, max );
		});

		$( '.price_slider' ).slider({
			range: true,
			animate: true,
			min: min_price,
			max: max_price,
			values: [ current_min_price, current_max_price ],
			create : function( event, ui ) {

				$( '.price_slider_amount #min_price' ).val( current_min_price );
				$( '.price_slider_amount #max_price' ).val( current_max_price );

				$( 'body' ).trigger( 'price_slider_create', [ current_min_price, current_max_price ] );
			},
			slide: function( event, ui ) {

				$( 'input#min_price' ).val( ui.values[0] );
				$( 'input#max_price' ).val( ui.values[1] );

				$( 'body' ).trigger( 'price_slider_slide', [ ui.values[0], ui.values[1] ] );
			},
			change: function( event, ui ) {

				$( 'body' ).trigger( 'price_slider_change', [ ui.values[0], ui.values[1] ] );

			},
		});

	}
	

//vc col mobile fixes
function vcMobileColumns() {

	$('.wpb_row').each(function(){
		if(typeof $(this).find('.span_12').offset() != 'undefined' ) {
		
			$(this).find('[class*="vc_col-"]').each(function(){

				var $firstChildOffset = $(this).parents('.span_12').offset().left;
				
				$(this).removeClass('no-left-margin');
				if($(this).offset().left < $firstChildOffset + 27) { 
					$(this).addClass('no-left-margin');
				} else {
					$(this).removeClass('no-left-margin');
				}
			});
		}
	});
}

if( $('[class*="vc_col-xs-"], [class*="vc_col-md-"], [class*="vc_col-lg-"]').length > 0) { vcMobileColumns(); }


/*-------------------------------------------------------------------------*/
/*	10.	Page transitions
/*-------------------------------------------------------------------------*/	

	if($('body[data-ajax-transitions="true"]').length > 0 && $('#ajax-loading-screen[data-method="ajax"]').length > 0 && !navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/) && $(window).width() > 690 ) {

		$('#ajax-content-wrap').ajaxify({
			'selector':'#ajax-content-wrap a:not(.no-ajaxy):not([target="_blank"]):not([href^="#"]):not(.comment-edit-link):not(#cancel-comment-reply-link):not(.comment-reply-link):not(#toggle-nav):not(.cart_list a):not(.logged-in-as a):not(.no-widget-added a):not(.add_to_cart_button):not(.product-wrap a):not(.section-down-arrow):not([data-filter]):not([data-fancybox]):not(.product_list_widget a):not(.magnific):not(.pp):not([rel^="prettyPhoto"]):not(.pretty_photo), #header-outer li:not(.no-ajaxy) > a:not(.no-ajaxy), #header-outer #logo',
			'verbosity': 0, 
			requestDelay: 400,
			previewoff : true,
			memoryoff: true,
			turbo : false
		});

		$(window).on("pronto.render", initPage)
		.on("pronto.load", destroyPage)
		.on("pronto.request", transitionPage);

		if($('.nectar-box-roll').length == 0 && $('#ajax-loading-screen[data-effect*="horizontal_swipe"]').length > 0) setTimeout(function() { waypoints(); }, 750);
		else if($('.nectar-box-roll').length == 0) setTimeout(function() { waypoints(); },300);

		if($('#ajax-loading-screen[data-effect*="horizontal_swipe"]').length > 0) {
			setTimeout(function(){ 
				$('#ajax-loading-screen').addClass('loaded');
			},30);
		}

		initPage();

	} else if($('body[data-ajax-transitions="true"]').length > 0 && $('#ajax-loading-screen[data-method="standard"]').length > 0 ) {
		
		//chrome white BG flash fix
		$('html').addClass('page-trans-loaded');

		//fadeout loading animation
		if($('#ajax-loading-screen[data-effect="standard"]').length > 0) {

			if($('.nectar-particles').length == 0) {
				
				//Search Page, Ajax Page loader to be hidden manually
				if($('.ajaxsearchpage').length == 0){
					
						if($('.searchpage').length == 0){
					
							$('#ajax-loading-screen').transition({'opacity':0},500,function(){ 
								$(this).css({'display':'none'}); 
							}); 
							$('#ajax-loading-screen .loading-icon').transition({'opacity':0},500);
							
						}
					
				}
				
			}

			//bind waypoints after loading screen has left
			if($('.nectar-box-roll').length == 0) setTimeout(function() { waypoints(); },550);

			//safari back/prev fix
			if(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1 || navigator.userAgent.match(/(iPod|iPhone|iPad)/)){
				window.onunload = function(){ $('#ajax-loading-screen').stop().transition({'opacity':0},800,function(){ $(this).css({'display':'none'}); }); $('#ajax-loading-screen .loading-icon').transition({'opacity':0},600) };
				window.onpageshow = function(event) {
					
		    		if (event.persisted) {
							
							
		    			$('#ajax-loading-screen').stop().transition({'opacity':0},800,function(){ 
		    				$(this).css({'display':'none'}); 
		    			}); 
		    			$('#ajax-loading-screen .loading-icon').transition({'opacity':0},600);
							
		    		} //presisted
						
		    	}
					
			} else if(navigator.userAgent.indexOf('Firefox') != -1) {
				window.onunload = function(){};
			}

	    		
	    	
			
		} else {

			//for swipe trans add loaded immediately
			if($('#ajax-loading-screen[data-effect*="horizontal_swipe"]').length > 0) {
				setTimeout(function(){ 
					$('#ajax-loading-screen').addClass('loaded');
				},60);
			}

			if($('#page-header-wrap #page-header-bg[data-animate-in-effect="zoom-out"] .nectar-video-wrap').length == 0 && $('.first-nectar-slider').length == 0) {
				setTimeout(function(){ 
					$('#ajax-loading-screen:not(.loaded)').addClass('loaded');
					setTimeout(function(){ $('#ajax-loading-screen').addClass('hidden'); },1000);
				},150);
			}

			//safari back/prev fix
			if(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1 || navigator.userAgent.match(/(iPod|iPhone|iPad)/)){
				window.onunload = function(){ $('#ajax-loading-screen').stop().transition({'opacity':0},800,function(){ $(this).css({'display':'none'}); }); $('#ajax-loading-screen .loading-icon').transition({'opacity':0},600) };
				window.onpageshow = function(event) {
		    		if (event.persisted) { 
		    			$('#ajax-loading-screen').stop().transition({'opacity':0},800,function(){ 
		    				$(this).css({'display':'none'}); 
		    			}); 
		    			$('#ajax-loading-screen .loading-icon').transition({'opacity':0},600);
		    		}
		    	}
			} else if(navigator.userAgent.indexOf('Firefox') != -1) {
				window.onunload = function(){};
			}
			
			//bind waypoints after loading screen has left
			if($('.nectar-box-roll').length == 0 && $('#ajax-loading-screen[data-effect*="horizontal_swipe"]').length > 0) { setTimeout(function() { waypoints(); }, 750); }
			else if($('.nectar-box-roll').length == 0) setTimeout(function() { waypoints(); },350);

		}

		//remove excess loading images now
		$('.portfolio-loading, .nectar-slider-loading .loading-icon').remove();


		if($('#ajax-loading-screen[data-disable-fade-on-click="1"]').length == 0) {
				
				if( $('body.using-mobile-browser #ajax-loading-screen[data-method="standard"][data-disable-mobile="1"]').length == 0) {
					window.addEventListener('beforeunload', function () {
								$('#ajax-loading-screen').addClass('set-to-fade');
								transitionPageStandard();
					});
				}

		} // if disable fade out is not on
		
		
	} else {	
		//bind waypoints regularly
		if($('.nectar-box-roll').length == 0) setTimeout(function() { waypoints(); },100);
	}


	function transitionPage(e) {
		
		if($('#ajax-loading-screen[data-effect*="horizontal_swipe"]').length > 0) {

			if($(window).scrollTop() > 0) {

				//stop nicescroll
				if($().niceScroll && $("html").getNiceScroll()){
					var nice = $("html").getNiceScroll();
					nice.stop();
				}
				
				$('body,html').stop(true,true).animate({
					scrollTop:0
				},500,'easeOutQuad',function(){ 
					
					//close widget area
					setTimeout(function(){  if($('#header-outer').hasClass('side-widget-open')) $('.slide-out-widget-area-toggle a').trigger('click');  },400);
					$('#ajax-loading-screen').removeClass('loaded');
					$('#ajax-loading-screen').addClass('in-from-right');
					setTimeout(function(){
						$('#ajax-loading-screen').addClass('loaded');
					},30);

				});
			}
			else { 
				//close widget area
				setTimeout(function(){  if($('#header-outer').hasClass('side-widget-open')) $('.slide-out-widget-area-toggle a').trigger('click');  },400);
				$('#ajax-loading-screen').removeClass('loaded');
				$('#ajax-loading-screen').addClass('in-from-right');
				setTimeout(function(){
					$('#ajax-loading-screen').addClass('loaded');
				},30);
			}

		} else {
			if($(window).scrollTop() > 0) {

				//stop nicescroll
				if($().niceScroll && $("html").getNiceScroll()){
					var nice = $("html").getNiceScroll();
					nice.stop();
				}
				
				$('body,html').stop(true,true).animate({
					scrollTop:0
				},500,'easeOutQuad',function(){ 
					$('#ajax-loading-screen').css({'opacity':'1', 'display':'none'});
					$('#ajax-loading-screen').stop(true,true).fadeIn(600,function(){
						$('#ajax-loading-screen .loading-icon').animate({'opacity':1},400);
						//close widget area
						setTimeout(function(){  if($('#header-outer').hasClass('side-widget-open')) $('.slide-out-widget-area-toggle a').trigger('click');  },400);
					});
				});

			} else {
				$('#ajax-loading-screen').css('opacity','1').stop().fadeIn(600,function(){
					$('#ajax-loading-screen .loading-icon').animate({'opacity':1},400);
				});

				//close widget area
				setTimeout(function(){  if($('#header-outer').hasClass('side-widget-open')) $('.slide-out-widget-area-toggle a').trigger('click');  },400);
			}
		}

		
	}

	function transitionPageStandard(e) {

		if($('#ajax-loading-screen[data-effect*="horizontal_swipe"]').length > 0) {
			$('#ajax-loading-screen').removeClass('loaded');
			$('#ajax-loading-screen').addClass('in-from-right');
			setTimeout(function(){
				$('#ajax-loading-screen').addClass('loaded');
			},30);
		} else {
			if($('#ajax-loading-screen[data-effect="center_mask_reveal"]').length > 0) {
				$('#ajax-loading-screen').css('opacity','0').css('display','block').transition({'opacity':'1'},450);
			} else {
				$('#ajax-loading-screen').show().transition({'opacity':'1'},450);
			}
		}
		
		//setTimeout(function(){ $('#ajax-loading-screen .loading-icon').animate({'opacity':1},500); },400);

	}

	function destroyPage(e) {
		$(window).off('scroll.appear');
		if($('#nectar_fullscreen_rows').length > 0 && $().fullpage) 
			$.fn.fullpage.destroy('all');
	}

	function initPage(e) {

		if(!$('body').hasClass('ajax-loaded')) return false;


		//init js
		lightBoxInit();
		addOrRemoveSF();
		
		if($('body[data-header-format="left-header"]').length == 0) 
			$(".sf-menu").superfish('destroy');
		
		//$('#header-outer').removeClass('dark-slide');
		initSF();
		SFArrows();
		headerInit();
		var $effectTimeout = ($('#ajax-loading-screen').length > 0) ? 800 : 0;
		pageHeaderTextEffectInit();
		if($('#page-header-bg .nectar-video-wrap video').length == 0) { setTimeout(pageHeaderTextEffect,$effectTimeout); }
		coloredButtons();
		columnBGColors();
		fwCarouselLinkFix();
		if($('.owl-carousel').length > 0) owlCarouselInit();
		if($('.products-carousel').length > 0 || $('.nectar-woo-flickity').length > 0) productCarouselInit();
		if($('#nectar_fullscreen_rows').length > 0 && $().fullpage) {
			setFPNames();
			initFullPageFooter();
			fullscreenRowLogic();
			initNectarFP();
		}
		flexsliderInit();
		accordionInit();
		tabbedInit();
		tabbbedDeepLinking();
		accordionDeepLinking();
		ulChecks();
		oneFourthClasses();
		carouselfGrabbingClass();
		cascadingImageBGSizing();
		fullWidthSections();
		fwsClasses();
		fullwidthImgOnlySizingInit();
		fullwidthImgOnlySizing();
		if(nectarDOMInfo.usingMobileBrowser) {
			fullWidthRowPaddingAdjustCalc();
		}
		boxRollInit();
		setTimeout(function(){ 
		   waypoints();
		   flickityInit();
		},100); 
		if($('body[data-animated-anchors="true"]').length > 0) setTimeout(scrollSpyInit,200);
		socialSharingInit();
		hotSpotHoverBind();
		pricingTableHeight();
		createTestimonialControls();
		imageWithHotspotClickEvents();
		testimonialSliderHeight(); 
		largeIconHover();
		if($('body.material[data-slide-out-widget-area-style="slide-out-from-right"]').length == 0) {
			fullscreenMenuInit();
		}
		boxRollMouseWheelInit();
		responsiveVideoIframesInit();
		responsiveVideoIframes();
		fullWidthContentColumns();
		videoBGInit();
		$window.unbind('scroll.parallaxSections').unbind('resize.parallaxSections');
		parallaxScrollInit();
		
		$blog_containers = [];
		$('.posts-container').each(function(i){
			$blog_containers[i] = $(this);
		});
		
		nectarAccountPageTabs();
		masonryBlogInit();
		masonryPortfolioInit();
		portfolioAccentColor();
		portfolioHoverEffects();
		portfolioFiltersInit();
		style6Img();
		isotopeCatSelection();
		$(window).unbind('.infscr');
		infiniteScrollInit();
		toTopBind();
		postNextButtonEffect();
		pageLoadHash();
		slideOutWidgetAreaScrolling();
		
		//search results
		if($('body.search-results').length > 0 && $('#search-results article').length > 0) { searchResultMasonry(); }
		
		//cf7
		if($().wpcf7InitForm) $('div.wpcf7 > form').wpcf7InitForm();

		//woocommerce price slider
		wooPriceSlider();

		//twitter widget 
		if(typeof twttr != 'undefined') { twttr.widgets.load(); }

		//Calendarize.it
		if(typeof init_rhc === 'function') { init_rhc(); }

		//unwrap post and protfolio videos
		$('.video-wrap iframe').unwrap();
		$('#sidebar iframe[src]').unwrap();

		$('video:not(.slider-video)').attr('width','100%');
		$('video:not(.slider-video)').attr('height','100%'); 

		$('.wp-video-shortcode.mejs-container').each(function(){
			$(this).attr('data-aspectRatio', parseInt($(this).css('height')) / parseInt($(this).css('width')));
		});

		//mediaElement
		$('video.wp-media-shortcode-ajax, audio.wp-media-shortcode-ajax').each(function(){ 
			if(!$(this).parent().hasClass('mejs-mediaelement') && $().mediaelementplayer) {
				$(this).mediaelementplayer();  
			}
		});

		$('.mejs-container').css({'height': '100%', 'width': '100%'});
		
		$('audio').attr('width','100%');
		$('audio').attr('height','100%');

		$('audio').css('visibility','visible');

		if($('body').hasClass('mobile')){
			$('video').css('visibility','hidden');
		} else {
			$('video').css('visibility','visible');
		}

		$('.wpb_row:has(".nectar-video-wrap")').each(function(i){
			$(this).css('z-index',100 + i);
		});

		mouseParallaxInit();

		//chrome self hosted slider bg video correct
		 if(navigator.userAgent.indexOf('Chrome') > 0) { 
		 	$('.swiper-wrapper .video-wrap').each(function(i){
			  	var webmSource = jQuery(this).find('video source[type="video/webm"]').attr('src') + "?id="+Math.ceil(Math.random()*10000);
	          	var firstVideo = jQuery(this).find('video').get(0);
	          	firstVideo.src = webmSource;
	          	firstVideo.load();
            });
	    }


		if($('.nectar-video-bg').length > 0) {
			setTimeout(function(){
			    	resizeVideoToCover();
			    	$('.video-color-overlay').each(function(){
			    		$(this).css('background-color',$(this).attr('data-color'));
			    	});
			    	$('.nectar-video-wrap').transition({'opacity':'1'},0);
			    	$('.video-color-overlay').transition({'opacity':'0.7'},0);
			    	
			    },400);
		}

		
		nectarPageHeader();
	

		//cart dropdown
		$('#header-outer div.cart-outer').hoverIntent(function(){
			$('#header-outer .widget_shopping_cart').addClass('open').stop(true,true).fadeIn(400);
			$('#header-outer .cart_list').stop(true,true).fadeIn(400);
			clearTimeout(timeout);
			$('#header-outer .cart-notification').fadeOut(300);
		});


		//remove excess loading images now
		$('.portfolio-loading, .nectar-slider-loading .loading-icon').remove();

		crossBrowserFixes();


		$(window).trigger('resize');

		//fix admin bar
		$("#wpadminbar").show();	


		//close widget area
		if($('#header-outer').hasClass('side-widget-open')) $('.slide-out-widget-area-toggle a').trigger('click'); 

		if($('#ajax-loading-screen[data-effect*="horizontal_swipe"]').length > 0) {
			closeSearch();
			$('#ajax-loading-screen').removeClass('in-from-right').removeClass('loaded');
			setTimeout(function(){ $('#ajax-loading-screen').addClass('loaded'); },30);
		} else {
			//fade in page
			setTimeout(function(){ $('#ajax-loading-screen').stop(true,true).fadeOut(500, function(){ $('#ajax-loading-screen .loading-icon').css({'opacity':0}); }); closeSearch();  },200);
			setTimeout(function(){ $('#ajax-loading-screen').stop(true,true).fadeOut(500, function(){ $('#ajax-loading-screen .loading-icon').css({'opacity':0}); }); closeSearch(); },900);
		}
	} 



(function($){
	if(!$.fn.textareaCount) { 
		$.fn.textareaCount = function(options, fn) {
	        var defaults = {
				maxCharacterSize: -1,
				originalStyle: 'originalTextareaInfo',
				warningStyle: 'warningTextareaInfo',
				warningNumber: 20,
				displayFormat: '#input characters | #words words'
			};

			var options = $.extend(defaults, options);

			var container = $(this);

			$("<div class='charleft'>&nbsp;</div>").insertAfter(container);


			//create charleft css
			var charLeftCss = {
				'width' : container.width()
			};

			var charLeftInfo = getNextCharLeftInformation(container);
			charLeftInfo.addClass(options.originalStyle);
			//charLeftInfo.css(charLeftCss);


			var numInput = 0;
			var maxCharacters = options.maxCharacterSize;
			var numLeft = 0;
			var numWords = 0;

			container.bind('keyup', function(event){limitTextAreaByCharacterCount();})
					 .bind('mouseover', function(event){setTimeout(function(){limitTextAreaByCharacterCount();}, 10);})
					 .bind('paste', function(event){setTimeout(function(){limitTextAreaByCharacterCount();}, 10);});

	        limitTextAreaByCharacterCount();

			function limitTextAreaByCharacterCount(){
				charLeftInfo.html(countByCharacters());

				//function call back
				if(typeof fn != 'undefined'){
					fn.call(this, getInfo());
				}
				return true;
			}

			function countByCharacters(){
				var content = container.val();
				var contentLength = content.length;
				//Start Cut
				if(options.maxCharacterSize > 0){
					//If copied content is already more than maxCharacterSize, chop it to maxCharacterSize.
					if(contentLength >= options.maxCharacterSize) {
						content = content.substring(0, options.maxCharacterSize);
					}

					var newlineCount = getNewlineCount(content);

					// newlineCount new line character. For windows, it occupies 2 characters
					var systemmaxCharacterSize = options.maxCharacterSize - newlineCount;
					if (!isWin()){
						 systemmaxCharacterSize = options.maxCharacterSize
					}
					if(contentLength > systemmaxCharacterSize){
						//avoid scroll bar moving
						var originalScrollTopPosition = this.scrollTop;
						container.val(content.substring(0, systemmaxCharacterSize));
						this.scrollTop = originalScrollTopPosition;
					}
					charLeftInfo.removeClass(options.warningStyle);
					if(systemmaxCharacterSize - contentLength <= options.warningNumber){
						charLeftInfo.addClass(options.warningStyle);
					}

					numInput = container.val().length + newlineCount;
					if(!isWin()){
						numInput = container.val().length;
					}

					numWords = countWord(getCleanedWordString(container.val()));

					numLeft = maxCharacters - numInput;
				} else {
					//normal count, no cut
					var newlineCount = getNewlineCount(content);
					numInput = container.val().length + newlineCount;
					if(!isWin()){
						numInput = container.val().length;
					}
					numWords = countWord(getCleanedWordString(container.val()));
				}

				return formatDisplayInfo();
			}

			function formatDisplayInfo(){
				var format = options.displayFormat;
				format = format.replace('#input', numInput);
				format = format.replace('#words', numWords);
				//When maxCharacters <= 0, #max, #left cannot be substituted.
				if(maxCharacters > 0){
					format = format.replace('#max', maxCharacters);
					format = format.replace('#left', numLeft);
				}
				return format;
			}

			function getInfo(){
				var info = {
					input: numInput,
					max: maxCharacters,
					left: numLeft,
					words: numWords
				};
				return info;
			}

			function getNextCharLeftInformation(container){
					return container.next('.charleft');
			}

			function isWin(){
				var strOS = navigator.appVersion;
				if (strOS.toLowerCase().indexOf('win') != -1){
					return true;
				}
				return false;
			}

			function getNewlineCount(content){
				var newlineCount = 0;
				for(var i=0; i<content.length;i++){
					if(content.charAt(i) == '\n'){
						newlineCount++;
					}
				}
				return newlineCount;
			}

			function getCleanedWordString(content){
				var fullStr = content + " ";
				var initial_whitespace_rExp = /^[^A-Za-z0-9]+/gi;
				var left_trimmedStr = fullStr.replace(initial_whitespace_rExp, "");
				var non_alphanumerics_rExp = rExp = /[^A-Za-z0-9]+/gi;
				var cleanedStr = left_trimmedStr.replace(non_alphanumerics_rExp, " ");
				var splitString = cleanedStr.split(" ");
				return splitString;
			}

			function countWord(cleanedWordString){
				var word_count = cleanedWordString.length-1;
				return word_count;
			}
		};
	}
})(jQuery);	
	
	
	
	
});


 }(window.jQuery, window, document));


function resizeIframe() {
	var element = document.getElementById("pp_full_res").getElementsByTagName("iframe");
	var height = element[0].contentWindow.document.body.scrollHeight;
    
    //iframe height
    element[0].style.height = height + 'px';
	
	//pp height
	document.getElementsByClassName("pp_content_container")[0].style.height = height+40+ 'px';
	document.getElementsByClassName("pp_content")[0].style.height = height+40+ 'px';
	
}



/*!
 * hoverIntent v1.9.0 // 2017.09.01 // jQuery v1.7.0+
 * http://briancherne.github.io/jquery-hoverIntent/
 *
 * You may use hoverIntent under the terms of the MIT license. Basically that
 * means you are free to use hoverIntent as long as this header is left intact.
 * Copyright 2007-2017 Brian Cherne
 */
!function(factory){"use strict";"function"==typeof define&&define.amd?define(["jquery"],factory):jQuery&&!jQuery.fn.hoverIntent&&factory(jQuery)}(function($){"use strict";var cX,cY,_cfg={interval:100,sensitivity:6,timeout:0},INSTANCE_COUNT=0,track=function(ev){cX=ev.pageX,cY=ev.pageY},compare=function(ev,$el,s,cfg){if(Math.sqrt((s.pX-cX)*(s.pX-cX)+(s.pY-cY)*(s.pY-cY))<cfg.sensitivity)return $el.off(s.event,track),delete s.timeoutId,s.isActive=!0,ev.pageX=cX,ev.pageY=cY,delete s.pX,delete s.pY,cfg.over.apply($el[0],[ev]);s.pX=cX,s.pY=cY,s.timeoutId=setTimeout(function(){compare(ev,$el,s,cfg)},cfg.interval)},delay=function(ev,$el,s,out){return delete $el.data("hoverIntent")[s.id],out.apply($el[0],[ev])};$.fn.hoverIntent=function(handlerIn,handlerOut,selector){var instanceId=INSTANCE_COUNT++,cfg=$.extend({},_cfg);$.isPlainObject(handlerIn)?(cfg=$.extend(cfg,handlerIn),$.isFunction(cfg.out)||(cfg.out=cfg.over)):cfg=$.isFunction(handlerOut)?$.extend(cfg,{over:handlerIn,out:handlerOut,selector:selector}):$.extend(cfg,{over:handlerIn,out:handlerIn,selector:handlerOut});var handleHover=function(e){var ev=$.extend({},e),$el=$(this),hoverIntentData=$el.data("hoverIntent");hoverIntentData||$el.data("hoverIntent",hoverIntentData={});var state=hoverIntentData[instanceId];state||(hoverIntentData[instanceId]=state={id:instanceId}),state.timeoutId&&(state.timeoutId=clearTimeout(state.timeoutId));var mousemove=state.event="mousemove.hoverIntent.hoverIntent"+instanceId;if("mouseenter"===e.type){if(state.isActive)return;state.pX=ev.pageX,state.pY=ev.pageY,$el.off(mousemove,track).on(mousemove,track),state.timeoutId=setTimeout(function(){compare(ev,$el,state,cfg)},cfg.interval)}else{if(!state.isActive)return;$el.off(mousemove,track),state.timeoutId=setTimeout(function(){delay(ev,$el,state,cfg.out)},cfg.timeout)}};return this.on({"mouseenter.hoverIntent":handleHover,"mouseleave.hoverIntent":handleHover},cfg.selector)}});
